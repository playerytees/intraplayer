(function ($) {
    $("#formAcceso").on("submit", function (e) {
        e.preventDefault();
        logina = $("#username").val();
        clavea = $("#password").val();

        $.post("../ajax/usuario.php?opcion=verificar",
            { "logina": logina, "clavea": clavea },
            function (data, textStatus, jqXHR) {
                // console.log(data);
                if (data == "null") {
                    toastr.error('Usuario y/o Contraseña incorrectos.', 'ERROR', {'progressBar': true,})
                } else {
                    $(location).attr("href", "index.php");
                }
            }
        );
    });
})(jQuery);