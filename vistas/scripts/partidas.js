var tabla, tablapartidas;

function init()
{
    listarpartidas();
    listartemporary();
    $(".loader").hide();
}

function listarpartidas()
{
    tablapartidas = $("#tablapartidas").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "frtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/embarque.php?opcion=listarpartidas",
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listartemporary()
{
    tabla = $("#tablatemporary").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "frtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/embarque.php?opcion=listartemporary",
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function agregarcajas(id, barcode, partida, estilo, color, talla, piezas, cajas, partidalimpia, codigo)
{
    $.confirm({
        title: 'Agregar',
        content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Cajas que desea agregar para embarcar</label>' +
            '<input type="number" id="cajasagregar" class="form-control" value="'+cajas+'" min="1" max="'+cajas+'" />' +
            '</div>' +
            '</form>',
        buttons: {
            formSubmit: {
                text: 'Agregar',
                btnClass: 'btn-blue',
                action: function () {
                    var cajasagregar = this.$content.find('#cajasagregar').val();
                    if (!cajasagregar) {
                        $.alert('Se requiere un numero de cajas');
                        return false;
                    }
                    // if (cajasagregar > cajas) {
                    //     $.alert('No se puede exceder el numero de cajas');
                    //     return false;
                    // }
                    if (cajasagregar < 1) {
                        $.alert('El valor debe ser mayor o igual a 1');
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: "../ajax/embarque.php?opcion=agregarcajas",
                        data: { id: id, barcode: barcode, partida: partida, estilo: estilo, color: color, talla: talla, piezas: piezas, cajas: cajasagregar, maxcajas: cajas, partidalimpia: partidalimpia, codigo: codigo },
                        success: function (response) {
                            $.alert(response);
                            tabla.ajax.reload();
                            tablapartidas.ajax.reload();
                        }
                    });
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // si el usuario envía el formulario presionando Enter en el campo.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // hacer referencia al botón y hacer clic en él
            });
        }
    });
}

function quitarcajas(id, barcode, partida, estilo, color, talla, piezas, cajas, partidalimpia, codigo)
{
    $.confirm({
        title: 'Quitar',
        content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Cajas que desea quitar del embarque</label>' +
            '<input type="number" id="cajasquitar" class="form-control" value="'+cajas+'" min="1" max="'+cajas+'" />' +
            '</div>' +
            '</form>',
        buttons: {
            formSubmit: {
                text: 'Quitar',
                btnClass: 'btn-red',
                action: function () {
                    var cajasquitar = this.$content.find('#cajasquitar').val();
                    if (!cajasquitar) {
                        $.alert('Se requiere un numero de cajas');
                        return false;
                    }
                    /* if (cajasquitar > cajas) {
                        $.alert('No se puede exceder el numero de cajas');
                        return false;
                    } */
                    if (cajasquitar < 1) {
                        $.alert('El valor debe ser mayor o igual a 1');
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: "../ajax/embarque.php?opcion=quitarcajas",
                        data: {id: id, barcode: barcode, partida: partida, estilo: estilo, color: color, talla: talla, piezas: piezas, cajas: cajasquitar, maxcajas: cajas, partidalimpia: partidalimpia, codigo: codigo },
                        success: function (response) {
                            $.alert(response);
                            tabla.ajax.reload();
                            tablapartidas.ajax.reload();
                        }
                    });
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // si el usuario envía el formulario presionando Enter en el campo.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // hacer referencia al botón y hacer clic en él
            });
        }
    });
}

function guardarembarque(e) {
    e.preventDefault();
    if (tabla.data().length > 0) {
        $.post("../ajax/folioEmbarque.php?opcion=ultimoid",
            function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                var proximoid = parseInt(data.id) + 1;
                console.log(proximoid);
                $("#inputFolio").val(proximoid);
            }
        );
        var dt = new Date();
        
        var month = (dt.getMonth()+1 < 10)?"0"+(dt.getMonth()+1):dt.getMonth()+1;
        var day = (dt.getDate() < 10)?"0"+(dt.getDate()):dt.getDate();
        var year = dt.getFullYear();
        
        $.confirm({
            title: 'Asignar Embarque',
            content: '' +
                `<form action="" class="formName">
                    <div class="form-group">
                        <label for="inputFolio">Folio</label>
                        <input type="text" id="inputFolio" class="form-control" disabled/>
                    </div>
                    <div class="form-group">
                        <label for="inputFecha">Fecha Embarque</label>
                        <input type="date" id="inputFecha" class="form-control" value="${year}-${month}-${day}" />
                    </div>
                    <div class="form-group">
                        <label for="inputMaquila">Maquiladora</label>
                        <input type="text" id="inputMaquila" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label for="inputChofer">Chofer</label>
                        <input type="text" id="inputChofer" class="form-control" />
                    </div>
					<div class="form-group">						
						<label for="inputcomentarios">Comentarios</label>
                        <input type="text" id="inputcomentarios" class="form-control" />
                    </div>
                </form>`,
            buttons: {
                formSubmit: {
                    text: 'Aceptar',
                    btnClass: 'btn-blue',
                    action: function () {
                        var folio = this.$content.find("#inputFolio").val();
                        var fecha = this.$content.find("#inputFecha").val();
                        var maquila = this.$content.find("#inputMaquila").val();
                        var chofer = this.$content.find("#inputChofer").val();
						var comentarios = this.$content.find('#inputcomentarios').val();
                        if ( !fecha ) {
                            $.alert('Se requiere una fecha válida');
                            return false;
                        }
                        if ( !maquila ) {
                            $.alert('Introduzca nombre de Maquiladora');
                            return false;
                        }
                        if ( !chofer ) {
                            $.alert('Se requiere un Chofer');
                            return false;
                        }
                        $.ajax({
                            type: "POST",
                            url: "../ajax/folioEmbarque.php?opcion=guardar",
                            data: {fecha: fecha, maquila: maquila, chofer: chofer, comentarios: comentarios},
                            success: function (response) {
                                console.log(response);
                                if (response == 1) {
                                    $.alert({ type: 'green', title: 'Correcto', content: 'Embarque guardado Correctamente' });
                                    window.open('orden.php?folio='+folio,'_blank');
                                } else {
                                    $.alert({ type: 'red', title: 'Error', content: 'Embarque No se ha guardado' });
                                }
                                tabla.ajax.reload();
                                tablapartidas.ajax.reload();
                            }
                        });
                    }
                },
                cancelar: function () {
                    //close
                },
            },
            onContentReady: function () {
                // bind to events
                var jc = this;
                this.$content.find('form').on('submit', function (e) {
                    // si el usuario envía el formulario presionando Enter en el campo.
                    e.preventDefault();
                    jc.$$formSubmit.trigger('click'); // hacer referencia al botón y hacer clic en él
                });
            }
        });
    } else {
        $.alert("No hay partidas que embarcar!!");
    }
}

function desactivar(id) {
    $.confirm({
        title: 'Advertencia!',
        content: '¿Está Seguro de eliminar la Partida?',
        buttons: {
            confirm: {
                text: "Aceptar",
                btnClass: 'btn-blue',
                action: function () {
                    $.post("../ajax/embarque.php?opcion=desactivar", { id: id }, function (response) {
                        console.log(response);
                        $.alert(response);
                        tablapartidas.ajax.reload();
                    });
                }
            },
            cancel: {
                text: "Cancelar",
                btnClass: 'btn-red',
                action: function () {
                    $.alert('Se ha Cancelado la operación');
                }
            }
        }
    });
}


init();