var tabla;
// var selected = [];
function init() {
   $(".loader").hide();
   $("#fileExcel").change(function (e) { 
       e.preventDefault();
       $("#btnGuardar").prop("disabled", this.files.length == 0);
       $("#btnlimpiar").prop("disabled", this.files.length == 0);
    });
   listar();
}

function limpiar() {
    $("#fileExcel").val("");
    $("#contentExcel").text("");

    $("#id").val("");
    $("#sucursal").val("");
    $("#meta_anterior").val("");
    $("#meta_actual").val("");
    $("#mes").val("");
}

function guardartable() {
    var tablaToJSON = $("#tblExcel").tableToJSON();
    var tablaObj = JSON.stringify(tablaToJSON);
    $(".loader").show();
    $.post("../ajax/presupuesto.php?opcion=guardartable", {tblExcel: tablaObj},
        function (data, textStatus, jqXHR) {
            $(".loader").hide();
            console.log(data);
            if (data == "Existe") {
                $.alert({ type: 'orange', title: 'Advertencia!', content: 'Verificar que las metas No existean ya en la Base de Datos' });
            } else if (data == 1) {
                $.alert({ type: 'green', title: 'Correcto', content: 'Guardado correctamente' });
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'No guardardo <br> Intente nuevamente' });
            }
            tabla.ajax.reload();
            limpiar();
        }
    );
}

$("#fileExcel").change(function (e) { 
    e.preventDefault();
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 100000) {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no debe superar 1MB' });
        this.value = '';
        this.files[0].name = '';
    } else if (fileName.split('.').pop() != "xlsx") {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no tiene la extensión adecuada' });
        this.value = '';
        this.files[0].name = '';
    } else {
        var reader = new FileReader();

        reader.readAsArrayBuffer(e.target.files[0]);

        reader.onload = function (e) {
            var data = new Uint8Array(reader.result);
            var wb = XLSX.read(data, { type: 'array' });
            //console.log(wb);
            if (wb.SheetNames[0]) {
                var htmlstr = XLSX.write(wb, {type: 'binary', bookType: 'html' });
                $("#contentExcel")[0].innerHTML += htmlstr;

                $("#contentExcel > table").attr({ 'id': 'tblExcel' });
                $("#tblExcel").addClass('table table-bordered table-sm table-responsive-sm');
            }
        }
    }
});

function listar() {
    tabla=$("#tblPresupuesto").DataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Blfrtip',
        buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
                ],
        "ajax":
                {
                    url: '../ajax/presupuesto.php?opcion=listar',
                    type : "get",
                    dataType : "json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        /* "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('table-active');
            }
        }, */
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[ 0, "desc" ]]
    });
}

function guardaryeditar() {
    $.confirm({
        title: 'Presupuesto',
        content: '' +
            `<form action="" class="formName">
                <div class="form-group">
                    <label for="sucursal">Sucursal:</label>
                    <input type="text" name="sucursal" id="sucursal" class="form-control" />
                    <input type="hidden" name="id" id="id" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="meta_anterior">Meta anterior</label>
                    <input type="number" name="meta_anterior" id="meta_anterior" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="meta_actual">Meta actual</label>
                    <input type="number" name="meta_actual" id="meta_actual" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="mes">Mes y año</label>
                    <input type="month" name="mes" id="mes" class="form-control" />
                </div>
            </form>`,
        buttons: {
            formSubmit: {
                text: 'Guardar',
                btnClass: 'btn-blue',
                action: function () {
                    var id = this.$content.find("#id").val();
                    var sucursal = this.$content.find("#sucursal").val();
                    var meta_anterior = this.$content.find("#meta_anterior").val();
                    var meta_actual = this.$content.find("#meta_actual").val();
                    var mes = this.$content.find('#mes').val();
                    if ( sucursal == "" ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Se requiere el nombre de la Sucursal' });
                        return false;
                    }
                    if ( mes == "" ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Introduzca Mes y Año' });
                        return false;
                    }
                    /* if ( !fechaentrega ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Fecha invalida' });
                        return false;
                    } */
                    $.ajax({
                        type: "POST",
                        url: "../ajax/presupuesto.php?opcion=agregaryeditar",
                        data: {
                            id: id,
                            sucursal: sucursal,
                            meta_anterior: meta_anterior,
                            meta_actual: meta_actual,
                            mes: mes
                        },
                        success: function (response) {
                            console.log(response);
                            if (response=="Existe") {
                                $.alert({ type: 'orange', title: 'Advertencia!', content: 'La sucursal con mes y año ya contiene una Meta' });
                            } else if (response == 1) {
                                $.alert({ type: 'green', title: 'Correcto', content: 'Guardado correctamente' });
                            } else {
                                $.alert({ type: 'red', title: 'Error', content: 'No se ha podido guardar' });
                            }
                            limpiar();
                            tabla.ajax.reload();
                        }
                    });
                }
            },
            cancelar: function () {
                limpiar();
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // si el usuario envía el formulario presionando Enter en el campo.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // hacer referencia al botón y hacer clic en él
            });            
        }
    });
}

function mostrar(id) {
    guardaryeditar();
    setTimeout(function () {
        $.post("../ajax/presupuesto.php?opcion=mostrar", {id : id},
            function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                $("#id").val(data.id);
                $("#sucursal").val(data.sucursal);
                $("#meta_anterior").val(data.meta_anterior);
                $("#meta_actual").val(data.meta_actual);
                $("#mes").val(data.anio+"-"+data.mes);
            }
        );
    }, 300);
}

/* $('#tblPresupuesto tbody').on('click', 'tr', function () {
    var id = this.id;
    var index = $.inArray(id, selected);

    if ( index === -1 ) {
        selected.push( id );
    } else {
        selected.splice( index, 1 );
    }

    $(this).toggleClass('table-active');
} ); */

init();