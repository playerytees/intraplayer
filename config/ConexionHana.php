<?php

class ConexionHana
{
    private $username;
    private $password;
    private $dsn;

    public function connect() {
        $this->username = "SYSTEM";
        $this->password = "P4ssw0rd1";
        $this->dsn = "odbc:Test";

        if (! extension_loaded('pdo_odbc'))
        {
            die('ODBC extension not enabled / loaded');
        }

        try {
            $pdo = new PDO($this->dsn, $this->username, $this->password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $pdo;
        } catch (PDOException $e) {
            echo "Connection failed: ".$e->getMessage();
        }
    }
}