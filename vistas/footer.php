</div><!-- /#right-panel Comienza en el header.php-->

    <!-- Right Panel -->

    <script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="../public/assets/js/popper.min.js"></script>
    <script src="../public/assets/js/vendor/bootstrap.min.js"></script>
    <script src="../public/assets/js/plugins.js"></script>
    <script src="../public/assets/js/main.js"></script>    

    <!-- <script src="../public/assets/js/lib/chart-js/Chart.bundle.js"></script> -->
    <!-- <script src="../public/assets/js/dashboard.js"></script> -->
    <!-- <script src="../public/assets/js/widgets.js"></script> -->
    <!-- <script src="../public/assets/js/lib/vector-map/jquery.vmap.js"></script>
    <script src="../public/assets/js/lib/vector-map/jquery.vmap.min.js"></script>
    <script src="../public/assets/js/lib/vector-map/jquery.vmap.sampledata.js"></script>
    <script src="../public/assets/js/lib/vector-map/country/jquery.vmap.world.js"></script> -->

    <script src="../public/assets/js/lib/data-table/datatables.min.js"></script>
    <script src="../public/assets/js/lib/data-table/dataTables.bootstrap.min.js"></script>
    <script src="../public/assets/js/lib/data-table/dataTables.buttons.min.js"></script>
    <script src="../public/assets/js/lib/data-table/buttons.bootstrap.min.js"></script>
    <script src="../public/assets/js/lib/data-table/jszip.min.js"></script>
    <script src="../public/assets/js/lib/data-table/pdfmake.min.js"></script>
    <script src="../public/assets/js/lib/data-table/vfs_fonts.js"></script>
    <script src="../public/assets/js/lib/data-table/buttons.html5.min.js"></script>
    <script src="../public/assets/js/lib/data-table/buttons.print.min.js"></script>
    <script src="../public/assets/js/lib/data-table/buttons.colVis.min.js"></script>
    <script src="../public/assets/js/lib/data-table/datatables-init.js"></script>
    <script src="scripts/footer.js"></script>
</body>
</html>