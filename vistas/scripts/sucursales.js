var tabla;

function init()
{
    mostrarform(false);
    listar();

    $("#form_sucursal").on("submit", function(e)
    {
        guardaryeditar(e);
    });

    $("#divdescripcion").hide();
    $(".loader").hide();
}

function limpiar()
{
    $("#id").val("");
    $("#descr").val("");
    $("#direccion").val("");
    $("#encargado").val("");
    $("#telefono").val("");
    $("#estado").val("A");
    $("#descripcion").val("");
    $("#divdescripcion").hide();
    $("#title_suc").html("Nueva Sucursal");
}

function mostrarform(flag)
{
    limpiar();
    if (flag) {
        $("#listadoSucursales").hide();
        $("#form_reg_sucursal").show();
        $("#btn_guardar").prop("disabled",false);
        $("#btnagregar").hide();
    } else {
        $("#listadoSucursales").show();
        $("#form_reg_sucursal").hide();
        $("#btnagregar").show();
    }
}

function cancelarform()
{
    limpiar();
    mostrarform(false);
}

function listar()
{
    tabla = $("#tabla_sucursales").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "lfrtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/sucursales.php?opcion=listar",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 10,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

function guardaryeditar(e)
{
    e.preventDefault();
    $("#btn_guardar").prop("disabled",true);
    var formData = new FormData($("#form_sucursal")[0]);

    $.ajax({
        type: "POST",
        url: "../ajax/sucursales.php?opcion=guardaryeditar",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            console.log(response);
            if (response == 1) {
                $.alert({type: 'green',title: 'Correcto',content: 'Sucursal/Distribuidor guardado correctamente'});
            } else {
                $.alert({type: 'red',title: 'Error',content: 'Sucursal/Distribuidor no se pudo guardar'});
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(id)
{
    $.post("../ajax/sucursales.php?opcion=mostrar", {id:id},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            mostrarform(true);
            $("#title_suc").html("Editar Sucursal");
            $("#descr").val(data.descr);
            $("#direccion").val(data.direccion);
            $("#encargado").val(data.encargado);
            $("#telefono").val(data.telefono);
            $("#id").val(data.id);
        }
    );
}

function activarestado(estado) {
    if (estado.value == 'I') {
        $("#divdescripcion").show();
        $("#descripcion").attr("required", "true");
        $("#descripcion").val("").removeAttr("novalidate")
    } else {
        $("#divdescripcion").hide();
        $("#descripcion").val("").removeAttr("required");
    }
}

init();