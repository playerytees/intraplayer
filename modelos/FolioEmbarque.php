<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");
session_start();
class FolioEmbarque
{
	private $tablatemporary;
    public function __construct()
    {
        $this->tablatemporary = ($_SESSION['user'] == 'embarques2') ? "temporary2" : "temporary";
    }

    public function insertar($fecha, $maquila, $chofer, $comentarios)
    {
        $sqlfolio = "INSERT INTO embarque VALUES (null, '$fecha', '$maquila', '$chofer', 1, now(), now(), '".$_SESSION["user"]."', '$comentarios')";

        $idfolionew = ejecutarConsulta_retornarID($sqlfolio);

        $sql = "INSERT INTO detalle_embarque (id, barcode, partida, estilo, color, talla, piezas, cajas, id_embarque, modified, created, partidalimpia, codigo) SELECT null, barcode, partida, estilo, color, talla, piezas, cajas, '$idfolionew', now(), now(), partidalimpia, codigo FROM $this->tablatemporary WHERE cajas > 0";

        return ejecutarConsulta($sql);
    }

    public function editar($id, $fecha, $maquila, $chofer, $comentarios)
    {
        $sql = "UPDATE embarque SET fecha = '$fecha', maquila = '$maquila', chofer = '$chofer', comentarios = '$comentarios', modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function desactivar($id)
    {
        $sql = "UPDATE embarque SET estado = 0, modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function activar($id)
    {
        $sql = "UPDATE embarque SET estado = 1, modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($id)
    {
        $sql = "SELECT * FROM embarque WHERE id = '$id'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar(){
        $sql = "SELECT * FROM embarque";
        return ejecutarConsulta($sql);
    }

    public function ultimoid()
    {
        $sql = "SELECT LAST_INSERT_ID(id) as id FROM embarque ORDER BY id DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function deletetemporary()
    {
        $delete = "DELETE FROM $this->tablatemporary";

        ejecutarConsulta($delete);
    }
}

?>