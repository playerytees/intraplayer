<?php
require_once "../modelos/Distribuidores.php";
session_start();
$distribuidores = new Distribuidores();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$fecha = isset($_POST["fecha"])?htmlspecialchars(trim($_POST["fecha"])):"";

switch ($_GET["opcion"]) {
    case 'listar':
        require_once "../modelos/Ventas.php";
        $ventas = new Ventas();

        $data = array();
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0]."-".$fechaArray[1]."-"."01";

        $distribuidoresGP = $distribuidores->listar(GP);
        $distribuidoresDPT = $distribuidores->listar(DPT);

        $ventasDiarias = $distribuidores->ventas("'$fecha'", "'$fecha'", 'GP_BD');
        $ventasMensuales = $distribuidores->ventas("'$inicioMes'", "'$fecha'", 'GP_BD');

        $devolucionDiariaGP = $distribuidores->devoluciones("'$fecha'", "'$fecha'", 'GP_BD');
        $devolucionMensualGP = $distribuidores->devoluciones("'$inicioMes'", "'$fecha'", 'GP_BD');

        foreach ($distribuidoresDPT as $distribuidorDPT) {
            if (!in_array($distribuidorDPT->Nombre, array_column($distribuidoresGP, 'Nombre'))) {
                array_push($distribuidoresGP, (object)array('Nombre' => $distribuidorDPT->Nombre));
            }
        }

        for ($i=0; $i < count($distribuidoresGP); $i++) { 
            if (in_array($distribuidoresGP[$i]->Nombre,  array_column($ventasMensuales, 'Nombre'))) {
                # code...
            } else {
                array_push($ventasMensuales, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
            
            if (in_array($distribuidoresGP[$i]->Nombre, array_column($ventasDiarias, 'Nombre'))) {
                # code...
            } else {
                array_push($ventasDiarias, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
            
            if (in_array($distribuidoresGP[$i]->Nombre, array_column($devolucionDiariaGP, 'Nombre'))) {
                # code...
            } else {
                array_push($devolucionDiariaGP, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
            
            if (in_array($distribuidoresGP[$i]->Nombre, array_column($devolucionMensualGP, 'Nombre'))) {
                # code...
            } else {
                array_push($devolucionMensualGP, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
        }

        $ventasDiariasDPT = $distribuidores->ventas("'$fecha'", "'$fecha'", 'DPT_BD');
        $ventasMensualesDPT = $distribuidores->ventas("'$inicioMes'", "'$fecha'", 'DPT_BD');

        if (!empty($ventasDiariasDPT)) { //Se verifica si existe alguna venta
            foreach ($ventasDiariasDPT as $ventaDiariaDPT) { // se recorre las ventas DPT
                //Se comparar si existe la sucursal si no se agrega al array
                if (in_array($ventaDiariaDPT->Nombre, array_column($ventasDiarias, 'Nombre'))) {
                    foreach ($ventasDiarias as $ventaDiaria) { // Recorrer las ventas GP
                        //comparar, si existe sumar su venta
                        if($ventaDiaria->Nombre == $ventaDiariaDPT->Nombre) { 
                            $ventaDiaria->Monto = floatval($ventaDiaria->Monto) + floatval($ventaDiariaDPT->Monto);
                        }
                    }
                } else {
                    array_push($ventasDiarias, $ventaDiariaDPT);
                }
            }
        }

        if (!empty($ventasMensualesDPT)) {
            foreach ($ventasMensualesDPT as $ventaMensualDPT) {
                if (in_array($ventaMensualDPT->Nombre, array_column($ventasMensuales, 'Nombre'))) {
                    foreach ($ventasMensuales as $ventaMensual) {
                        if($ventaMensual->Nombre == $ventaMensualDPT->Nombre) {
                            $ventaMensual->Monto = floatval($ventaMensual->Monto) + floatval($ventaMensualDPT->Monto);
                        }
                    }
                } else {
                    array_push($ventasMensuales, $ventaMensualDPT);
                }
            }
        }

        $metaActual = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metaAnterior = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        $ultimoDiaMes = date("d",(mktime(0,0,0,$fechaArray[1]+1,1,$fechaArray[0])-1));

        $diasHabiles = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01", $fechaArray[0]."-".$fechaArray[1]."-".$ultimoDiaMes);

        $diasTranscurridos = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01",$fecha);

        $diasRestantes = count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]) - (!empty($diasTranscurridos)?count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]):0);

        for ($i=0; $i < count($ventasMensuales); $i++) { 
            // Indice Venta Diaria
            $indiceVD = array_search($ventasMensuales[$i]->Nombre, array_column($ventasDiarias, 'Nombre'));
            $indiceDVD = array_search($ventasMensuales[$i]->Nombre, array_column($devolucionDiariaGP, 'Nombre'));//Devolucion Venta Diaria
            $montoVD = ($indiceVD === false)?"000.00":floatval($ventasDiarias[$indiceVD]->Monto) - floatval($devolucionDiariaGP[$indiceDVD]->Monto);
            $indiceDVM = array_search($ventasMensuales[$i]->Nombre, array_column($devolucionMensualGP, 'Nombre'));//Devolucion Venta Mensual
            $montoVM = floatval($ventasMensuales[$i]->Monto) - floatval($devolucionMensualGP[$indiceDVM]->Monto);
            // Indice Meta Mes Año Actual MySQL
            $indiceMetaAct = array_search($ventasMensuales[$i]->Nombre, array_column($metaActual, 'sucursal'));
            $metaMesAct = ($indiceMetaAct === false)?"000.00":$metaActual[$indiceMetaAct]["meta"];
            // Indice Meta Mes Año Anterior MySQL
            $indiceMetaAnt = array_search($ventasMensuales[$i]->Nombre, array_column($metaAnterior, 'sucursal'));
            $metaMesAnt = ($indiceMetaAnt === false)?"000.00":$metaAnterior[$indiceMetaAnt]["meta"];

            $promedioDiario = $montoVM/ (!empty($diasTranscurridos)?count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]):1);
            $tendencia = $promedioDiario*count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]);
            $difVsAA = $tendencia-$metaMesAnt;
            $porcentajeVsAA = ($metaMesAnt=="000.00")?"0.00":(($difVsAA/$metaMesAnt)*100);
            $difVsCuota = $tendencia-$metaMesAct;
            $porcentajeVsCuota = ($metaMesAct=="000.00")?"0.00":(($difVsCuota/$metaMesAct)*100);
            $prom_necesario = ($diasRestantes==0)?"0":($metaMesAct-$montoVM)/$diasRestantes;

            $data[] = array(
                /* '0' => $ventasMensuales[$i]->Codigo, */
                '0' => $ventasMensuales[$i]->Nombre,
                '1' => number_format($montoVD, 2, ".", ","),
                '2' => number_format($montoVM, 2, ".", ","),
                '3' => number_format($promedioDiario, 2, ".", ","),
                '4' => number_format($tendencia, 2, ".", ","),
                '5' => number_format($metaMesAnt, 2, ".", ","),
                '6' => ($difVsAA<0)?'<span style="color: red">'.number_format($difVsAA, 2, ".", ",").'</span>':number_format($difVsAA, 2, ".", ","),
                '7' => ($porcentajeVsAA<0)?'<span style="color: red">'.number_format($porcentajeVsAA, 2, ".", ",").'</span>':number_format($porcentajeVsAA, 2, ".", ","),
                '8' => number_format($metaMesAct, 2, ".", ","),
                '9' => ($difVsCuota<0)?'<span style="color: red">'.number_format($difVsCuota, 2, ".", ",").'</span>':number_format($difVsCuota, 2, ".", ","),
                '10' => ($porcentajeVsCuota<0)?'<span style="color: red">'.number_format($porcentajeVsCuota, 2, ".", ",").'</span>':number_format($porcentajeVsCuota, 2, ".", ","),
                '11' => ($prom_necesario<0)?'<span style="color: red">'.number_format($prom_necesario, 2, ".", ",").'</span>':number_format($prom_necesario, 2, ".", ",")
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;

    default:
        # code...
        break;
}