var tabla;
var myChart;
function init() {
    $("#formulario").on("submit", function (e) {
        guardar(e);
    });
    listar($("#fecha").val());
    listarSucursales();
    $(".loader").hide();
    mostrarform(false);
    grafica();
    addData();
}

function limpiar() {
    $("#txtmonto").val("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#cardventas").hide();
        $("#formpresupuesto").show();
        $("#btnagregar").hide();
    } else {
        $("#cardventas").show();
        $("#formpresupuesto").hide();
        $("#btnagregar").show();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function guardar(e) {
    e.preventDefault();
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        type: "POST",
        url: "../ajax/ventasGral.php?opcion=guardar",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response) {
                $.alert({type: 'green', title: 'Correcto', content: "Presupuesto actualizado correctamente"});
            } else {
                $.alert({type: 'red', title: 'Error', content: 'No se ha podido actualizar'});
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function listar(fecha) {
    tabla = $("#tablaventas").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "initComplete": function() {
            sumaColumnas();
        },
        "ajax": {
            url: "../ajax/ventasGral.php?opcion=ventas",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 15,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarSucursales() {
    $.post("../ajax/ventasGral.php?opcion=listarSucursales",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                select += `<option value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#selectsucursal").append(select);
        }
    );
}

$("#btn-ver").click(function (e) { 
    e.preventDefault();
    tabla.clear().draw();
    listar($("#fecha").val());
    addData();
    //tabla.ajax.reload();
});

function sumaColumnas() {
    $("#totalDailySales").text("$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 1 ).data().sum().toFixed(2) ) );
    $("#totalMonthlySales").text("$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 2 ).data().sum().toFixed(2) ) );
    $("#AlcanceTotal").text( ((tabla.column( 2 ).data().sum() * 100)/tabla.column( 3 ).data().sum()).toFixed()+"%" );
}

function grafica() {
    var ctx = document.getElementById('grafica').getContext('2d');
    myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: "",
            datasets: [{
                label: 'Año Actual',
                data: "",
                // backgroundColor: 'rgba(75, 192, 192, 0.8)',
                // borderColor: 'rgba(75, 192, 192, 1)',
                backgroundColor: 'rgba(8, 151, 20, 0.8)',
                borderColor: 'rgba(8, 151, 20, 1)',
                borderWidth: 1
            },
            {
                label: 'Año Anterior',
                data: "",
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        // max: 100,
                        callback: function(value) {
                            return value + "%"
                        }
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Porcentaje"
                    }
                }]
            }
        }
    });
}

function addData() {
    var etiquetas = [];
    var lbl2019 = [];
    var lbl2018 = [];
    $(".loading").show();
    $("#btn-ver").prop("disabled", true);
   $.post("../ajax/ventasGral.php?opcion=grafica", {fecha: $("#fecha").val()},
        function (response) {
            $(".loading").hide();
            $("#btn-ver").prop("disabled", false);
            response = JSON.parse(response);
            etiquetas.length=0;
            lbl2019.length=0;
            lbl2018.length=0;
            for (let i = 0; i < response.length; i++) {
                etiquetas.push(response[i][0]);
                lbl2019.push(response[i][1]);
                lbl2018.push(response[i][2]);
            }
            myChart.data.labels = etiquetas;
            myChart.data.datasets[0].data = lbl2019;
            myChart.data.datasets[1].data = lbl2018;
            myChart.update();
        }
    );
}

init();