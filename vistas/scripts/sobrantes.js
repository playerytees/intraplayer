var tabla;

function init()
{
    barcode();
    $(".loader").hide();
}

function mostrarsobrante()
{
    var barcodesob = $("#barcodesob").val().trim();
    $.post("../ajax/sobrante.php?opcion=mostrar", { barcode: barcodesob },
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            if (data == null) {
                $("#message-sku").html("<i class='fa fa-times fa-times fa-2x'></i>");
                $("#barcodesob").focus();
                $("#barcodesob").val("");
            } else {
                $("#message-sku").html("<i class='fa fa-check fa-2x'></i>");

                $("#table_sob tr").find('td:eq(0)').each(function () {
                    sku = $(this).html();
                    if (sku == barcodesob) {
                        $(this).parents().eq(0).remove();
                    }
                });

                var tabla = `<tr>
                    <td class="d-none">${barcodesob}</td>
                    <td class="d-none">${data.id}</td>
                    <td>${data.partida}</td>
                    <td>${data.estilo}</td>
                    <td>${data.color}</td>
                    <td>${data.talla}</td>
                    <td>${data.piezas}</td>
                    <td><i style="cursor: pointer;" class="fa fa-trash-o ml-3" id="trash_row" title="Eliminar"></i></td>
                </tr>`;

                $("#table_sob > tbody").append(tabla);
                $("#barcodesob").focus();
                $("#barcodesob").val("");
            }
        }
    );
}

function pulsarenter(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13 && tecla != 32);
}

function barcode()
{
    $.fn.delayPasteKeyUp = function (fn, ms) {
        var timer = 0;
        $(this).on("propertychange input", function () {
            clearTimeout(timer);
            timer = setTimeout(fn, ms);
        });
    };

    $("#barcodesob").delayPasteKeyUp(function () {
        $("#btnverificar").trigger("click");
    }, 200);
}

/* Eliminar una fila de la tabla */
$('#table_sob').on('click', '#trash_row', function(){
    var result = confirm("¿Desea Eliminarlo?");
        if (result == true) {
            $(this).parents('tr').eq(0).remove();
        }
});

function guardarsobrantes()
{
    $("#barcodesob").focus();
    var tablaToJSON = $("#table_sob").tableToJSON();
    var tablaObt = JSON.stringify(tablaToJSON);
    console.log(tablaToJSON);
    if (tablaToJSON.length == 0) {
        $.alert("No hay Partidas que guardar");
    } else {
        $.ajax({
            type: "POST",
            url: "../ajax/sobrante.php?opcion=guardarsobrantes",
            data: { tablasobrantes: tablaObt },
            beforeSend: function () {
                $(".loader").show();
            },
            success: function (response) {
                $(".loader").hide();
                console.log(response);
                if (response == 1) {
                    $.alert({ type: 'green', title: 'Correcto', content: 'Partidas guardado correctamente' });
                    $("#table_sob > tbody").html("");
                } else {
                    $.alert({ type: 'red', title: 'Error', content: 'Partidas no se pudo guardar' });
                }
            }
        });
    }
}

init();