<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

    require 'header.php';

    if ($_SESSION['embarques']==1) {
?>
    <style>
        table#tablapartidas > tbody td:first-child,
        table#tablatemporary > tbody td:first-child
        {
            text-align: center;
        }
    </style>
<?php /*
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Embarques</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Embarques</a></li>
                        <li class="active">Escanear</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
*/ ?>
    <div class="content mt-1">
        <div class="row">
            <div class="col-lg-12">
                <div class="card" style="margin-bottom: .5em;">
                    <div class="card-header" style="padding-bottom: 0;">
                        <strong class="card-title float-left text-playerytees" style="margin-bottom: .5em"> Escanear </strong>
                        <a href="partidas.php" class="btn btn-playerytees btn-sm float-right">Mostrar Partidas</a>
                    </div>
                    <div class="card-body" style="padding-bottom: 0;">
                        <form class="form-horizontal">
                            <div class="row form-group">
                                <div class="col-md-3"></div>
                                <div class="col col-md-2">
                                    <label for="barcodebox" class="form-control-label text-playerytees" style="font-size: 14px;">Codigo de Caja:</label>
                                </div>
                                <div class="col-12 col-md-2">
                                    <input id="barcodebox" name="barcodebox" class="form-control form-control-sm" type="text" maxlength="8" autofocus onkeypress="return pulsarenter(event)">
                                </div>
                                <div class="col-md-2">
                                    <button type="button" id="btnverificar" class="btn btn-playerytees btn-sm mt-sm-1" onclick="mostraretiqueta()">Verificar</button>
                                </div>
                                <div class="col-md-3">
                                    <div id="message-sku"></div>
                                </div>
                            </div>
                        </form>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header" style="padding-bottom: 0;">
                        <strong class="card-title text-playerytees"> Partidas </strong>
                    </div>
                    <div class="card-body">
                        <table id="boxscanner" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th class="d-none">barcode</th>
                                <th class="d-none">partidalimpia</th>
                                <th class="d-none">Codigo</th>
                                <th>Partida</th>
                                <th>Estilo</th>
                                <th>Color</th>
                                <th>Talla</th>
                                <th>Piezas</th>
                                <th>Cajas</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-playerytees btn-sm" onclick="guardarpartidas()">Guardar</button>
                        <?php /*<button type="button" class="btn btn-default btn-sm float-right" onclick="mostrarlocalStorage()" id="btn-showvalues">+</button> */ ?>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content .mt-3 -->
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/embarque.js"></script>
<?php
}
ob_end_flush();
// include 'crearjson.php';
?>