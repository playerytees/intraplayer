<?php
    require "../config/Conexion.php";

    class Presupuesto 
    {
        public function __construct()
        {
            # code...
        }

        public function guardarTable($sucursal,$meta_anterior,$meta_actual,$mes,$anio)
        {
            $sql = "INSERT INTO `bdpw_intraplayer`.`presupuesto`
            (`sucursal`,
            `meta_anterior`,
            `meta_actual`,
            `mes`,
            `anio`)
            VALUES
            ('$sucursal',
            '$meta_anterior',
            '$meta_actual',
            '$mes',
            '$anio');";

            return ejecutarConsulta($sql);
        }

        public function agregar($sucursal,$meta_anterior,$meta_actual,$mes,$anio)
        {
            $sql = "INSERT INTO `bdpw_intraplayer`.`presupuesto`
            (`sucursal`,
            `meta_anterior`,
            `meta_actual`,
            `mes`,
            `anio`)
            VALUES
            ('$sucursal',
            '$meta_anterior',
            '$meta_actual',
            '$mes',
            '$anio');";

            return ejecutarConsulta($sql);
        }

        public function editar($id,$sucursal,$meta_anterior,$meta_actual,$mes,$anio)
        {
            $sql = "UPDATE `bdpw_intraplayer`.`presupuesto`
            SET
            `id` = '$id',
            `sucursal` = '$sucursal',
            `meta_anterior` = '$meta_anterior',
            `meta_actual` = '$meta_actual',
            `mes` = '$mes',
            `anio` = '$anio'
            WHERE `id` = '$id';";

            return ejecutarConsulta($sql);
        }

        public function mostrar($id)
        {
            $sql = "SELECT * FROM `bdpw_intraplayer`.`presupuesto` WHERE `id` = '$id'";

            return ejecutarConsultaSimpleFila($sql);
        }

        public function listar()
        {
            $sql = "SELECT * FROM `bdpw_intraplayer`.`presupuesto`";

            return ejecutarConsulta($sql);
        }

        public function consultarSiExiste($mes,$anio,$sucursal)
        {
            $sql = "SELECT * FROM presupuesto WHERE mes = '$mes' AND anio = '$anio' AND sucursal = '$sucursal'";

            return ejecutarConsultaSimpleFila($sql);
        }
    }
    