<?php
require_once "../modelos/DetalleEmbarque.php";

$detalleEmbarque = new DetalleEmbarque();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$barcode = isset($_POST['barcode'])?limpiarCadena($_POST['barcode']):"";
$partida = isset($_POST['partida'])?limpiarCadena($_POST['partida']):"";
$estilo = isset($_POST['estilo'])?limpiarCadena($_POST['estilo']):"";
$color = isset($_POST['color'])?limpiarCadena($_POST['color']):"";
$talla = isset($_POST['talla'])?limpiarCadena($_POST['talla']):"";
$pza_x_caja = isset($_POST['pza_x_caja'])?limpiarCadena($_POST['pza_x_caja']):"";
$cajas = isset($_POST['cajas'])?limpiarCadena($_POST['cajas']):"";
$id_embarque = isset($_POST['id_embarque'])?limpiarCadena($_POST['id_embarque']):"";
$partidalimpia = isset($_POST['partidalimpia'])?limpiarCadena($_POST['partidalimpia']):"";
$codigo = isset($_POST['codigo'])?limpiarCadena($_POST['codigo']):"";

switch ($_GET['opcion']) {
    /* case 'guardar':
        $response = $detalleEmbarque->insertar();
        break; */
    case 'listar':
        $response = $detalleEmbarque->listar($id_embarque);
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->partida,
                "1" => $reg->estilo,
                "2" => $reg->color,
                "3" => $reg->talla,
                "4" => $reg->piezas,
                "5" => $reg->cajas,
                "6" => "<a href='#' onclick=\"quitardefolio('$reg->id','$reg->barcode', '$reg->partida', '$reg->estilo', '$reg->color', '$reg->talla', '$reg->pza_x_caja', '$reg->cajas', '$reg->partidalimpia', '$reg->codigo')\"><i class='fa fa-trash-o'></i></a>"
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listarpartidas':
        require_once "../modelos/Embarque.php";
        $embarque = new Embarque();

        $response = $embarque->listartemp();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => "<i style='cursor: pointer; color: #28a745; text-align: center;' class='fa fa-plus-circle' onclick=\"javascript:asignarfolio('$reg->id','$reg->barcode','$reg->partida','$reg->estilo','$reg->color','$reg->talla','$reg->pza_x_caja','$reg->cajas', '$reg->partidalimpia', '$reg->codigo')\"></i>",
                "1" => $reg->partida,
                "2" => $reg->estilo,
                "3" => $reg->color,
                "4" => $reg->talla,
                "5" => $reg->piezas,
                "6" => $reg->cajas
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'asignarfolio':
        require_once "../modelos/Embarque.php";
        $embarque = new Embarque();

        $response = $detalleEmbarque->asignarfolio($id,$barcode,$partida,$estilo,$color,$talla,$pza_x_caja,$cajas,$id_embarque,$partidalimpia,$codigo);
        
        $embarque->actualizartemp($id, $cajas);
        
        echo $response ? "Agregado Correctamente" : "No se ha podido agregar";
        break;
    case 'quitardefolio':
        require_once "../modelos/Embarque.php";
        $embarque = new Embarque();
        $response = $embarque->insertartemp($barcode, $partida, $estilo, $color, $talla, $pza_x_caja, $cajas, $partidalimpia, $codigo);
        if ($response == 1) {
            $response = $detalleEmbarque->delete($id);
        } else {
            $response = 0;
        }
        
        echo $response ? "Se ha realizado con Éxito" : "No se pudo realizar la Operación";
    break;
    default:
        echo "No se encontro la opcion";
        break;
}
?>