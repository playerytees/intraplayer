<?php
    require "../config/Conexion.php";
    date_default_timezone_set("America/Mexico_City");

    class Catalogo
    {
        public function __construct()
        {

        }

        public function insertar($sku_manufactura, $sku_custom, $sugerido, $id_sucursal)
        {
            $sql = "INSERT INTO catalogo VALUES (NULL, '$sku_manufactura', '$sku_custom', '$sugerido', '$id_sucursal', now(), now())";

            return ejecutarConsulta($sql);
        }

        public function editar($id, $sku_manufactura, $sku_custom, $sugerido)
        {
            $sql = "UPDATE catalogo SET sku_manufactura = '$sku_manufactura', sku_custom = '$sku_custom', sugerido = '$sugerido' WHERE id = '$id'";

            return ejecutarConsulta($sql);
        }

        public function mostrar($id)
        {
            $sql = "SELECT * FROM catalogo WHERE id = '$id'";

            return ejecutarConsultaSimpleFila($sql);
        }

        public function listar($id_sucursal)
        {
            $sql = "SELECT c.sku_manufactura, c.sku_custom, c.sugerido, s.descr FROM catalogo AS c JOIN sucursales AS s ON c.id_sucursal = s.id WHERE c.id_sucursal = '$id_sucursal'";

            return ejecutarConsulta($sql);
        }

        public function deletecatalogo($id_sucursal)
        {
            $delete = "DELETE FROM catalogo WHERE id_sucursal = '$id_sucursal'";
            
            ejecutarConsulta($delete);
        }
    }
    
?>