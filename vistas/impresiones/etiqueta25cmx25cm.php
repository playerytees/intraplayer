<table style="width: 100%; height: 100%;">
    <tr>
        <td>
            <table style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                <tr>
                    <td style="font-weight: bold;">Cliente:</td>
                    <td><?= $_GET["cliente"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Item:</td>
                    <td><?= $_GET["item"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">OC:</td>
                    <td><?= $_GET["oc"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Cantidad:</td>
                    <td><?= $_GET["cantidad"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Fecha:</td>
                    <td><?= $_GET["fecha"]; ?></td>
                </tr>
            </table>
        </td>
        <td>&nbsp;</td>
        <td>
            <table style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; margin-left: 25px;">
                <tr>
                    <td style="font-weight: bold;">Cliente:</td>
                    <td><?= $_GET["cliente"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Item:</td>
                    <td><?= $_GET["item"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">OC:</td>
                    <td><?= $_GET["oc"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Cantidad:</td>
                    <td><?= $_GET["cantidad"]; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: bold;">Fecha:</td>
                    <td><?= $_GET["fecha"]; ?></td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script>
function init() {
    if (window.print) {
        setTimeout(function(){window.print();window.close();},300); // 500ms = 0.5s - 1000 = 1s
    }
}
init();
</script>