<?php
require_once "../modelos/Ventas.php";
session_start();
$ventas = new Ventas();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$meta = isset($_POST["txtmonto"])?limpiarCadena($_POST["txtmonto"]):"";
$mes = isset($_POST["selectmes"])?limpiarCadena($_POST["selectmes"]):"";
$anio = isset($_POST["selectanio"])?limpiarCadena($_POST["selectanio"]):"";
$sucursal = isset($_POST["selectsucursal"])?limpiarCadena($_POST["selectsucursal"]):"";
$fecha = isset($_POST["fecha"])?limpiarCadena($_POST["fecha"]):"";
switch ($_GET["opcion"]) {
    case 'guardar':
        $exist = $ventas->consultarSiExiste($mes, $anio, $sucursal);
        if ($exist) {
            echo false;
        } else {
            $response = $ventas->insertar($meta, $mes, $anio, $sucursal);

            echo $response;
        }
        break;
    case 'listar':
        require_once "../modelos/VentasSapHana.php";
        $ventasSap = new VentasSapHana();
        $data = array();
        $fechaForSap = str_replace("-", "", $fecha);
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0].$fechaArray[1]."01";

        $Permisos = array(
            ($_SESSION['5febrero']==1)?"'FEB'":"",
            ($_SESSION['cuernavaca']==1)?"'CUE'":"",
            ($_SESSION['hermosillo']==1)?"'HE7'":"",
            ($_SESSION['isabel']==1)?"'ISA'":"",
            ($_SESSION['cdmx']==1)?"'IS2'":"",
            ($_SESSION['lorey']==1)?"'LOR'":"",
            ($_SESSION['monterrey']==1)?"'MO5'":"",
            ($_SESSION['noria']==1)?"'NO4'":"",
            ($_SESSION['oaxaca'])?"'OA6'":"",
            ($_SESSION['oaxaca2']==1)?"'OA2'":"",
            ($_SESSION['pachuca']==1)?"'PAC'":"",
            ($_SESSION['sanluis']==1)?"'SL2'":"",
            ($_SESSION['CedisDPT'])?"'CE1'":""
        );
        if (empty(implode( ",",array_filter($Permisos) ) )) {
            $sucursales = array();
        } else {
            $sucursales = $ventasSap->listarSucursales( implode( ",",array_filter($Permisos) ) );
        }
        $ventasMensuales = $ventasSap->ventasMensuales("'$inicioMes'", "'$fechaForSap'");
        $ventasDiarias = $ventasSap->ventasDiarias("'$fechaForSap'");
        $devolucionDiaria = $ventasSap->devolucionDiaria("'$fechaForSap'");
        $devolucionMensual = $ventasSap->devolucionMensual("'$inicioMes'", "'$fechaForSap'");
        for ($i=0; $i < count($sucursales); $i++) { 
            if(in_array($sucursales[$i]->Codigo, array_column($ventasDiarias,'Sucursal'))){

            } else {
                array_push($ventasDiarias, (object)array('Monto' => '0', 'Sucursal' => $sucursales[$i]->Codigo));
            }

            if (in_array($sucursales[$i]->Codigo, array_column($ventasMensuales,'Sucursal'))){
                # code...
            } else {
                array_push($ventasMensuales, (object)array('Monto' => '0', 'Sucursal' => $sucursales[$i]->Codigo));
            }

            if(in_array($sucursales[$i]->Codigo, array_column($devolucionDiaria,'Sucursal'))){
                # code...
            } else {
                array_push($devolucionDiaria, (object)array('Monto' => '0', 'Sucursal' => $sucursales[$i]->Codigo));
            }

            if (in_array($sucursales[$i]->Codigo, array_column($devolucionMensual, 'Sucursal'))) {
                # code...
            } else {
                array_push($devolucionMensual, (object)array('Monto' => '0', 'Sucursal' => $sucursales[$i]->Codigo));
            }
        }

        if ($_SESSION['Distribuidores']==1) {
            array_push($sucursales, (object)array('Codigo' => 'DIS102', 'Nombre' => 'DISTRIBUIDORES'));

            $ventasDiariasDistribuidores = $ventasSap->ventasDiariasDistribuidores("'$fechaForSap'", 'GP_BD');

            $ventasDiariasDistribuidoresDPT = $ventasSap->ventasDiariasDistribuidores("'$fechaForSap'", 'DPT_BD');

            if (!empty($ventasDiariasDistribuidoresDPT)) {
                $ventasDiariasDistribuidores[0]->Monto = floatval($ventasDiariasDistribuidores[0]->Monto) + floatval($ventasDiariasDistribuidoresDPT[0]->Monto);
            }

            array_push($ventasDiarias, empty($ventasDiariasDistribuidores)?(object)array('Monto' => '0', 'Sucursal' => 'DIS102'):$ventasDiariasDistribuidores[0] );

            $ventasMensualesDistribuidores = $ventasSap->ventasMensualesDistribuidores("'$inicioMes'","'$fechaForSap'", 'GP_BD');

            $ventasMensualesDistribuidoresDPT =  $ventasSap->ventasMensualesDistribuidores("'$inicioMes'","'$fechaForSap'", 'DPT_BD');

            if (!empty($ventasMensualesDistribuidoresDPT)) {
                $ventasMensualesDistribuidores[0]->Monto = floatval($ventasMensualesDistribuidores[0]->Monto) + floatval($ventasMensualesDistribuidoresDPT[0]->Monto);
            }

            array_push($ventasMensuales, empty($ventasMensualesDistribuidores)?(object)array('Monto' => '0', 'Sucursal' => 'DIS102'):$ventasMensualesDistribuidores[0]);

            $devolucionDiariaDistribuidor = $ventasSap->devolucionDiariaDistGP("'$fechaForSap'");
            array_push($devolucionDiaria, empty($devolucionDiariaDistribuidor)?(object)array('Monto' => '0', 'Sucursal' => 'DIS102'):$devolucionDiariaDistribuidor[0]);

            $devolucionMensualDistribuidor = $ventasSap->devolucionMensualDistGP("'$inicioMes'","'$fechaForSap'");
            array_push($devolucionMensual, empty($devolucionMensualDistribuidor)?(object)array('Monto' => '0', 'Sucursal' => 'DIS102'):$devolucionMensualDistribuidor[0]);
        }

        if ($_SESSION['CEDIS']==1) {
            $sucursalesGP = $ventasSap->listarSucursalesGP();
            array_push($sucursales, empty($sucursalesGP) ? (object)array('Codigo' => 'CE1', 'Nombre' => 'CEDIS (GP)') : $sucursalesGP[0]);
            //$posVM = array_search( 'CE1', array_column($ventasMensuales, 'Sucursal') );

            $ventasDiariasGP = $ventasSap->ventasDiariasGP("'$fechaForSap'");
            array_push($ventasDiarias, empty($ventasDiariasGP) ? (object)array('Monto' => '0', 'Sucursal' => 'CE1') : $ventasDiariasGP[0]);

            //$posVD = array_search( 'CE1', array_column($ventasDiarias, 'Sucursal') );

            $ventasMensualesGP = $ventasSap->ventasMensualesGP("'$inicioMes'","'$fechaForSap'");
            array_push($ventasMensuales,empty($ventasMensualesGP) ? (object)array('Monto' => '0', 'Sucursal' => 'CE1') : $ventasMensualesGP[0]);

            $devolucionDiariaGP = isset($ventasSap->devolucionDiariaGP("'$fechaForSap'")[0])?$ventasSap->devolucionDiariaGP("'$fechaForSap'")[0]->Monto:'0';

            $devolucionMensualGP = isset($ventasSap->devolucionMensualGP("'$inicioMes'","'$fechaForSap'")[0])?$ventasSap->devolucionMensualGP("'$inicioMes'","'$fechaForSap'")[0]->Monto:'0';

           // $ventasDiarias[$posVD]->Monto = (floatval($ventasDiarias[$posVD]->Monto) + floatval($ventasDiariasGP)) - floatval($devolucionDiariaGP);

            //$ventasMensuales[$posVM]->Monto = (floatval($ventasMensuales[$posVM]->Monto) + floatval($ventasMensualesGP)) - floatval($devolucionMensualGP);
        }

        $metaActual = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metaAnterior = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        $ultimoDiaMes = date("d",(mktime(0,0,0,$fechaArray[1]+1,1,$fechaArray[0])-1));

        $diasHabiles = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01", $fechaArray[0]."-".$fechaArray[1]."-".$ultimoDiaMes);

        $diasTranscurridos = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01",$fecha);

        $diasRestantes = count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]) - count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]);

        for ($i=0; $i < count($sucursales); $i++) { 
            // Indice Venta Diaria Hana
            $indiceVD = array_search($sucursales[$i]->Codigo, array_column($ventasDiarias, 'Sucursal'));
            $indiceDVD = array_search($sucursales[$i]->Codigo, array_column($devolucionDiaria, 'Sucursal'));//Devolucion Venta Diaria
            $montoVD = floatval($ventasDiarias[$indiceVD]->Monto) - floatval($devolucionDiaria[$indiceDVD]->Monto);
            // Indice Venta Mensual Hana
            $indiceVM = array_search($sucursales[$i]->Codigo, array_column($ventasMensuales, 'Sucursal'));
            $indiceDVM = array_search($sucursales[$i]->Codigo, array_column($devolucionMensual, 'Sucursal'));//Devolucion Venta Mensual
            $montoVM = floatval($ventasMensuales[$indiceVM]->Monto) - floatval($devolucionMensual[$indiceDVM]->Monto);
            // Indice Meta Mes Año Actual MySQL
            $indiceMetaAct = array_search($sucursales[$i]->Codigo, array_column($metaActual, 'sucursal'));
            $metaMesAct = ($indiceMetaAct === false)?"000.00":$metaActual[$indiceMetaAct]["meta"];
            // Indice Meta Mes Año Anterior MySQL
            $indiceMetaAnt = array_search($sucursales[$i]->Codigo, array_column($metaAnterior, 'sucursal'));
            $metaMesAnt = ($indiceMetaAnt === false)?"000.00":$metaAnterior[$indiceMetaAnt]["meta"];

            $promedioDiario = $montoVM/count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]);
            $tendencia = $promedioDiario*count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]);
            $difVsAA = $tendencia-$metaMesAnt;
            $porcentajeVsAA = ($metaMesAnt=="000.00")?"0.00":(($difVsAA/$metaMesAnt)*100);
            $difVsCuota = $tendencia-$metaMesAct;
            $porcentajeVsCuota = ($metaMesAct=="000.00")?"0.00":(($difVsCuota/$metaMesAct)*100);
            $prom_necesario = ($diasRestantes==0)?"0":($metaMesAct-$montoVM) / $diasRestantes;
            $url = ($sucursales[$i]->Codigo == "DIS102")?"<a href='../vistas/distribuidores.php' class='text-playerytees'><u>".$sucursales[$i]->Nombre."</u></a>":$sucursales[$i]->Nombre;
            $data[] = array(
                '0' => $url,
                '1' => number_format($montoVD, 2, ".", ","),
                '2' => number_format($montoVM, 2, ".", ","),
                '3' => number_format($promedioDiario, 2, ".", ","),
                '4' => number_format($tendencia, 2, ".", ","),
                '5' => number_format($metaMesAnt, 2, ".", ","),
                '6' => ($difVsAA<0)?'<span style="color: red">'.number_format($difVsAA, 2, ".", ",").'</span>':number_format($difVsAA, 2, ".", ","),
                '7' => ($porcentajeVsAA<0)?'<span style="color: red">'.number_format($porcentajeVsAA, 2, ".", ",").'</span>':number_format($porcentajeVsAA, 2, ".", ","),
                '8' => number_format($metaMesAct, 2, ".", ","),
                '9' => ($difVsCuota<0)?'<span style="color: red">'.number_format($difVsCuota, 2, ".", ",").'</span>':number_format($difVsCuota, 2, ".", ","),
                '10' => ($porcentajeVsCuota<0)?'<span style="color: red">'.number_format($porcentajeVsCuota, 2, ".", ",").'</span>':number_format($porcentajeVsCuota, 2, ".", ","),
                '11' => number_format($prom_necesario, 0, ".", ",")
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listarSucursales':
        require_once "../modelos/VentasSapHana.php";
        $ventasSap = new VentasSapHana();

        $Permisos = array("'FEB'","'CUE'","'HE7'","'ISA'","'IS2'","'LOR'","'MO5'","'NO4'","'OA2'","'PAC'","'SL2'");

        $sucursales = $ventasSap->listarSucursales( implode( ",", array_filter($Permisos) ) );
        $data = array();

        foreach ($sucursales as $sucursal) {
            $data[] = array(
                '0' => $sucursal->Codigo,
                '1' => $sucursal->Nombre
            );
        }

        foreach ($ventasSap->listarSucursalesGP() as $sucursalGP) {
            array_push($data, array($sucursalGP->Codigo, $sucursalGP->Nombre));
        }

        echo json_encode($data);
        break;
    case 'ventasMensuales':
        require_once "../modelos/VentasSapHana.php";
        $ventasSap = new VentasSapHana();
        $response = $ventasSap->ventasMensuales();

        var_dump($response);
        break;
    case 'grafica':
        require_once "../modelos/VentaSap.php";
        $ventaSap = new VentaSap();
        $data = array();
        $fechaForSap = str_replace("-", "", $fecha);
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0].$fechaArray[1]."01";

        $Permisos = array("'FEB'","'CUE'","'HE07'","'ISA'","'ISA2'","'LOR'","'MO05'","'NO04'","'OA2'","'PAC'","'SL02'","'SLP'");

        $sucursales = $ventaSap->listarSucursal( implode( ",", array_filter($Permisos) ), DPT );

        /* if ($_SESSION['CEDIS']==1) {
            array_push($sucursales, $ventasSap->listarSucursalesGP()[0]);
        } */

        $ventasMensuales = $ventaSap->groupBy( $ventaSap->ventas("'$inicioMes'", "'$fechaForSap'", DPT), 'Sucursal', 'Monto');
        // array_push($ventasMensuales, $ventasSap->ventasMensualesGP("'$inicioMes'", "'$fechaForSap'")[0]);

        $ventasDiarias = $ventaSap->groupBy( $ventaSap->ventas("'$fechaForSap'", "'$fechaForSap'", DPT), 'Sucursal', 'Monto');
        //$ventasDiariasGP = (isset($ventasSap->ventasDiariasGP("'$fechaForSap'")[0]))?$ventasSap->ventasDiariasGP("'$fechaForSap'")[0]:(object)array('Monto'=>'0','Sucursal'=>'CE1');
        // array_push($ventasDiarias, $ventasDiariasGP);

        $metaActual = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metaAnterior = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        $ultimoDiaMes = date("d",(mktime(0,0,0,$fechaArray[1]+1,1,$fechaArray[0])-1));
        $diasHabiles = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01", $fechaArray[0]."-".$fechaArray[1]."-".$ultimoDiaMes);
        $diasTranscurridos = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01",$fecha);
        $diasRestantes = count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]) - (!empty($diasTranscurridos)?count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]):0);

        for ($i=0; $i < count($sucursales); $i++) {
            // Venta Diaria Hana
            $indiceVD = array_search($sucursales[$i]->Codigo, array_column($ventasDiarias, 'Sucursal'));
            $montoVD = ($indiceVD === false)?"000.00":$ventasDiarias[$indiceVD]['Monto'];
            // Venta Mensual Hana
            $indiceVM = array_search($sucursales[$i]->Codigo, array_column($ventasMensuales, 'Sucursal'));
            $montoVM = ($indiceVM === false)?"000.00":$ventasMensuales[$indiceVM]['Monto'];
            // Meta Mes Año Actual MySQL
            $indiceMetaAct = array_search($sucursales[$i]->Codigo, array_column($metaActual, 'sucursal'));
            $metaMesAct = ($indiceMetaAct === false)?"000.00":$metaActual[$indiceMetaAct]["meta"];
            // Meta Mes Año Anterior MySQL
            $indiceMetaAnt = array_search($sucursales[$i]->Codigo, array_column($metaAnterior, 'sucursal'));
            $metaMesAnt = ($indiceMetaAnt === false)?"000.00":$metaAnterior[$indiceMetaAnt]["meta"];

            $promedioDiario = $montoVM/(!empty($diasTranscurridos)?count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]):1);
            $tendencia = $promedioDiario*count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]);

            $data[] = array(
                '0' => $sucursales[$i]->Nombre,
                '1' => $tendencia,
                '2' => $metaMesAnt,
                '3' => $metaMesAct
            );
        }

        echo json_encode($data);
        break;
    case 'bussiness_days':
        $data = array();

        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0].$fechaArray[1]."01";

        $ultimoDiaMes = date("d",(mktime(0,0,0,$fechaArray[1]+1,1,$fechaArray[0])-1));

        $diasHabiles = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01", $fechaArray[0]."-".$fechaArray[1]."-".$ultimoDiaMes);
        $diasTranscurridos = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01",$fecha);
        $diasRestantes = count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]) - count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]);

        $data[] = array(
            '0' => count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]),
            '1' => count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]),
            '2' => $diasRestantes
        );

        echo json_encode($data);
        break;
    case 'ventas':
        require_once "../modelos/VentaSap.php";
        $ventaSap = new VentaSap();
        $fechaActual = $fecha;
        $fechaArray = explode("-", $fechaActual);
        $inicioMes = $fechaArray[0].'-'.$fechaArray[1].'-'.'01';

        $Permisos = array(
            ($_SESSION['CedisDPT'])?"'CE01'":"",
            ($_SESSION['cuernavaca'])?"'CUE'":"",
            ($_SESSION['5febrero'])?"'FEB'":"",
            ($_SESSION['hermosillo'])?"'HE07'":"",
            ($_SESSION['isabel'])?"'ISA'":"",
            ($_SESSION['cdmx'])?"'ISA2'":"",
            ($_SESSION['lorey'])?"'LOR'":"",
            ($_SESSION['monterrey'])?"'MO05'":"",
            ($_SESSION['noria'])?"'NO04'":"",
            ($_SESSION['oaxaca'])?"'OA06'":"",
            ($_SESSION['oaxaca2'])?"'OA2'":"",
            ($_SESSION['pachuca'])?"'PAC'":"",
            ($_SESSION['sanluis'])?"'SL02'":"",
            ($_SESSION['sanluis2'])?"'SLP'":"",
        );

        if (empty(implode( ",",array_filter($Permisos) ) )) {
            $sucursales = array();
        } else {
            $sucursales = $ventaSap->listarSucursal( implode( ",",array_filter($Permisos) ), DPT );
        }

        $ventasMensualesDPT = $ventaSap->groupBy( $ventaSap->ventas("'$inicioMes'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');
        $ventasDiariasDPT = $ventaSap->groupBy( $ventaSap->ventas("'$fechaActual'", "'$fechaActual'", DPT), "Sucursal", "Monto");

        $devolucionesMensualesDPT = $ventaSap->groupBy( $ventaSap->devoluciones("'$inicioMes'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');
        $devolucionesDiariasDPT = $ventaSap->groupBy( $ventaSap->devoluciones("'$fechaActual'", "'$fechaActual'", DPT), "Sucursal", "Monto");

        foreach ($sucursales as $sucursal) {
            if ($sucursal->Codigo == 'CE01') {
                $sucursal->Nombre = 'CEDIS (PT)';
            }
            if (!in_array($sucursal->Codigo, array_column($ventasMensualesDPT, 'Sucursal'))) {
                array_push($ventasMensualesDPT, array('Sucursal' => $sucursal->Codigo, 'Monto' => 0));
            }
            if (!in_array($sucursal->Codigo, array_column($ventasDiariasDPT, 'Sucursal'))) {
                array_push($ventasDiariasDPT, array('Sucursal' => $sucursal->Codigo, 'Monto' => 0));
            }
        }

        if (!empty($devolucionesMensualesDPT)) {
            for ($i=0; $i < count($ventasMensualesDPT); $i++) { 
                for ($j=0; $j < count($devolucionesMensualesDPT); $j++) {
                    if ($ventasMensualesDPT[$i]['Sucursal'] == $devolucionesMensualesDPT[$j]['Sucursal']) {
                        $ventasMensualesDPT[$i]['Monto'] = $ventasMensualesDPT[$i]['Monto'] - $devolucionesMensualesDPT[$j]['Monto'];
                    }
                }
            }
        }

        if (!empty($devolucionesDiariasDPT)) {
            for ($i=0; $i < count($ventasDiariasDPT); $i++) { 
                for ($j=0; $j < count($devolucionesDiariasDPT); $j++) { 
                    if ($ventasDiariasDPT[$i]['Sucursal'] == $devolucionesDiariasDPT[$j]['Sucursal']) {
                        $ventasDiariasDPT[$i]['Monto'] = $ventasDiariasDPT[$i]['Monto'] - $devolucionesDiariasDPT[$j]['Monto'];
                    }
                }
            }
        }

        if ($_SESSION['CEDIS']==1) {
            $sucursalesGP = $ventaSap->listarSucursal("'CE01'", GP);
            if ($sucursalesGP[0]->Codigo == 'CE01') {
                $sucursalesGP[0]->Codigo = 'CE01GP';
                $sucursalesGP[0]->Nombre = 'CEDIS (GP)';
            }
            array_push($sucursales, $sucursalesGP[0]);

            $ventasDiariasGP = $ventaSap->groupBy( $ventaSap->ventas("'$fechaActual'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            $devolucionesDiariasGP =  $ventaSap->groupBy( $ventaSap->devoluciones("'$fechaActual'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            if (empty($ventasDiariasGP)) {
                array_push($ventasDiariasDPT, array('Sucursal' => 'CE01GP', 'Monto' => 0));
            } else {
                if (!empty($devolucionesDiariasGP)) {
                    if ($ventasDiariasGP[0]['Sucursal'] == $devolucionesDiariasGP[0]['Sucursal']) {
                        $ventasDiariasGP[0]['Monto'] = $ventasDiariasGP[0]['Monto'] - $devolucionesDiariasGP[0]['Monto'];
                    }
                }
                if ($ventasDiariasGP[0]['Sucursal'] == 'CE01') {
                    $ventasDiariasGP[0]['Sucursal'] = 'CE01GP';
                }
                array_push($ventasDiariasDPT, $ventasDiariasGP[0]);
            }

            $ventasMensualesGP = $ventaSap->groupBy( $ventaSap->ventas("'$inicioMes'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            $devolucionesMensualesGP = $ventaSap->groupBy( $ventaSap->devoluciones("'$inicioMes'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            if (empty($ventasMensualesGP)) {
                array_push($ventasMensualesDPT, array('Sucursal' => 'CE01GP', 'Monto' => 0));
            } else {
                if (!empty($devolucionesMensualesGP)) {
                    if ($ventasMensualesGP[0]['Sucursal'] == $devolucionesMensualesGP[0]['Sucursal']) {
                        $ventasMensualesGP[0]['Monto'] = $ventasMensualesGP[0]['Monto'] - $devolucionesMensualesGP[0]['Monto'];
                    }
                }
                if ($ventasMensualesGP[0]['Sucursal'] == 'CE01') {
                    $ventasMensualesGP[0]['Sucursal'] = 'CE01GP';
                }
                array_push($ventasMensualesDPT, $ventasMensualesGP[0]);
            }
        }

        if ($_SESSION['Distribuidores']==1) {
            array_push($sucursales, (object)array('Codigo' => 'DIS102', 'Nombre' => 'DISTRIBUIDORES'));

            $VentasDiariasDistribuidoresGP = $ventaSap->ventasDistribuidores("'$fechaActual'", "'$fechaActual'", GP);

            $ventasDiariasDistribuidoresDPT = $ventaSap->ventasDistribuidores("'$fechaActual'", "'$fechaActual'", DPT);

            $sumaVentaDiaDistr = floatval($VentasDiariasDistribuidoresGP[0]->Monto) + floatval($ventasDiariasDistribuidoresDPT[0]->Monto);

            $devolucionesDiariaDistribuidoresGP = $ventaSap->devolucionesDistribuidores("'$fechaActual'", "'$fechaActual'", GP);

            $devolucionesDiariaDistribuidoresDPT = $ventaSap->devolucionesDistribuidores("'$fechaActual'", "'$fechaActual'", DPT);

            $sumaDevolucionDiaDistr = floatval($devolucionesDiariaDistribuidoresGP[0]->Monto) + floatval($devolucionesDiariaDistribuidoresDPT[0]->Monto);

            array_push($ventasDiariasDPT, array('Sucursal' => 'DIS102', 'Monto' => $sumaVentaDiaDistr - $sumaDevolucionDiaDistr));

            $ventasMensualesDistribuidoresGP = $ventaSap->ventasDistribuidores("'$inicioMes'", "'$fechaActual'", GP);

            $ventasMensualesDistribuidoresDPT =  $ventaSap->ventasDistribuidores("'$inicioMes'", "'$fechaActual'", DPT);

            $sumaVentaMesDistr = floatval($ventasMensualesDistribuidoresGP[0]->Monto) + floatval($ventasMensualesDistribuidoresDPT[0]->Monto);

            $devolucionesMensualesDistribuidoresGP = $ventaSap->devolucionesDistribuidores("'$inicioMes'", "'$fechaActual'", GP);

            $devolucionesMensualesDistribuidoresDPT = $ventaSap->devolucionesDistribuidores("'$inicioMes'", "'$fechaActual'", DPT);

            $sumaDevolucionMesDistr = floatval($devolucionesMensualesDistribuidoresGP[0]->Monto) + floatval($devolucionesMensualesDistribuidoresDPT[0]->Monto);

            array_push($ventasMensualesDPT, array('Sucursal' => 'DIS102', 'Monto' => $sumaVentaMesDistr - $sumaDevolucionMesDistr));
        }

        $metasActuales = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metasAnteriores = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        $ultimoDiaMes = date("d",(mktime(0,0,0,$fechaArray[1]+1,1,$fechaArray[0])-1));

        $diasHabiles = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01", $fechaArray[0]."-".$fechaArray[1]."-".$ultimoDiaMes);

        $diasTranscurridos = $ventas->bussiness_days($fechaArray[0]."-".$fechaArray[1]."-01",$fecha);

        $diasRestantes = count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]) - (!empty($diasTranscurridos)?count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]):0);

        $data = array();
        foreach ($sucursales as $sucursal) {
            foreach ($ventasDiariasDPT as $ventaDiariaDPT) {
                if ($sucursal->Codigo == $ventaDiariaDPT['Sucursal']) {
                    $sucursal->Diaria = $ventaDiariaDPT['Monto'];
                }
            }

            foreach ($ventasMensualesDPT as $ventaMensualDPT) {
                if ($sucursal->Codigo == $ventaMensualDPT['Sucursal']) {
                    $sucursal->Mensual = $ventaMensualDPT['Monto'];
                }
            }

            foreach ($metasActuales as $metaActual) {
                if ($sucursal->Codigo == $metaActual['sucursal']) {
                    $sucursal->metaActual = floatval($metaActual['meta']);
                }
            }

            foreach ($metasAnteriores as $metaAnterior) {
                if ($sucursal->Codigo == $metaAnterior['sucursal']) {
                    $sucursal->metaAnterior = floatval($metaAnterior['meta']);
                }
            }

            $promedioDiario = $sucursal->Mensual / (!empty($diasTranscurridos)?count($diasTranscurridos[$fechaArray[0]."-".$fechaArray[1]]):1);
            $tendencia = $promedioDiario*count($diasHabiles[$fechaArray[0]."-".$fechaArray[1]]);
            $difVsAA = $tendencia - (isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0);;
            $porcentajeVsAA = (!isset($sucursal->metaAnterior) OR $sucursal->metaAnterior==0) ? 0 : (( $difVsAA / $sucursal->metaAnterior ) * 100);
            $difVsCuota = $tendencia - (isset($sucursal->metaActual)?$sucursal->metaActual:0);
            $porcentajeVsCuota = ( !isset($sucursal->metaActual) OR $sucursal->metaActual==0 ) ? 0 : (( $difVsCuota / $sucursal->metaActual ) * 100);
            $prom_necesario = ($diasRestantes==0) ? 0 : ( (isset($sucursal->metaActual) ? ( $sucursal->metaActual - $sucursal->Mensual) : 0) / $diasRestantes );

            $url = ($sucursal->Codigo == "DIS102")?"<a href='javascript:void(0);' onclick='mostrarDistrDetalle(true)' class='text-playerytees'><u>".$sucursal->Nombre."</u></a>":$sucursal->Nombre;

            $data[] = array(
                '0' => $url,
                '1' => number_format($sucursal->Diaria, 2, ".", ","),
                '2' => number_format($sucursal->Mensual, 2, ".", ","),
                '3' => number_format($promedioDiario, 2, ".", ","),
                '4' => number_format($tendencia, 2, ".", ","),
                '5' => isset($sucursal->metaAnterior)?number_format($sucursal->metaAnterior, 2, '.', ','):0,
                '6' => ($difVsAA<0)?'<span style="color: red">'.number_format($difVsAA, 2, ".", ",").'</span>':number_format($difVsAA, 2, ".", ","),
                '7' => ($porcentajeVsAA<0)?'<span style="color: red">'.number_format($porcentajeVsAA, 2, ".", ",").'</span>':number_format($porcentajeVsAA, 2, ".", ","),
                '8' => isset($sucursal->metaActual)?number_format($sucursal->metaActual, 2, '.', ','):0,
                '9' =>  ($difVsCuota<0)?'<span style="color: red">'.number_format($difVsCuota, 2, ".", ",").'</span>':number_format($difVsCuota, 2, ".", ","),
                '10' => ($porcentajeVsCuota<0)?'<span style="color: red">'.number_format($porcentajeVsCuota, 2, ".", ",").'</span>':number_format($porcentajeVsCuota, 2, ".", ","),
                '11' => number_format($prom_necesario, 0, ".", ",")
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        echo "No se encontro la opcion";
        break;
}