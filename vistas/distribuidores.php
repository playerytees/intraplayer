<?php
ob_start();
session_start();
date_default_timezone_set("America/Mexico_City");
if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';

    if ($_SESSION['Distribuidores']==1) {
?>
<style>
#tabladistribuidores td:nth-child(1){
    text-align: left;
    white-space: nowrap;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <!-- <h1 style="float: left;" class="text-playerytees">Presupuesto </h1> -->
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right text-playerytees">
                    <li><a href="ventas.php">Ventas</a></li>
                    <li class="active">Distribuidores</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header pb-0">
                    <strong class="card-title text-playerytees float-left">Distribuidores</strong>
                </div> <!-- .card-header -->
                <div class="card-body card-block">
                    <div class="form-inline">
                        <div class="form-group">
                            <label class="form-control-label mr-1">Seleccionar Fecha:</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="date" class="form-control form-control-sm" id="fecha" name="fecha" required value="<?= date("Y-m-d")?>">
                                <button type="button" class="btn btn-playerytees" id="btn-ver">Ver</button>
                            </div>
                        </div>
                    </div>
                    <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tabladistribuidores" style="width: 100%;">
                        <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                            <tr>
                                <!-- <th>CODIGO</th> -->
                                <th>NOMBRE</th>
                                <th>VENTA DIA</th>
                                <th>VENTA ACUMULADA MES</th>
                                <th>PROM DIARIO</th>
                                <th>TENDENCIA</th>
                                <th>AÑO ANTERIOR</th>
                                <th>DIF VS A.A</th>
                                <th>% VS A.A</th>
                                <th>CUOTA</th>
                                <th>DIF VS CUOTA</th>
                                <th>% VS CUOTA</th>
                                <th>PROM NECESARIO</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 14px; text-align: right;"></tbody>
                        <tfoot style="font-size: 14px; white-space: nowrap; text-align: right;">
                            <tr>
                                <!-- <th></th> -->
                                <th class="text-left"></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div> <!-- card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->
</div> <!-- .content -->
<?php
    }
    else {
        require "noacceso.php";
    }
    require "footer.php";
?>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script src="scripts/distribuidores.js"></script>
<?php
}
ob_end_flush();
