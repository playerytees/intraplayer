var tabla;

function init(){
    listar();
    $("#formtag").on("submit",function(e) {
        mostrarinventario( e, $("#cve_art").val() );
    });
    $("#etiquetas_imprimir").hide();
    $("#sobrantes_imprimir").hide();
    mostrartabla(false);
    $("#btn_imprimir").hide();
    $("#btn_imprimir_chrome").hide();
    $(".loader").hide();
    $("#div_tabla_eliminados").hide();
    listareliminados();
}

function limpiar()
{
    $("#id").val("");
    $(".barcode").val("");
    $("#cve_art").val("");
    $("#partida").val("");
    $("#estilo").html("");
    $("#talla").html("");
    $("#color").html("");
    $("#pza_x_caja").html("");
    $("#pzas_caja").val("");
    $("#sobrante").val("");
    $("#total_rem").val("");
    $("#cajas").val("");

}

function mostrartabla(flag)
{
    if (flag) {
        $("#divetiqueta").hide();
        $("#listadoEtiquetas").show();
        $("#btnlistadotag").hide();
    } else {
        $("#divetiqueta").show();
        $("#listadoEtiquetas").hide();
        $("#btnlistadotag").show();
    }
}

function guardar()
{
    var barcode = $(".barcode").val();
    var cve_art = $(".cve_art_barcode").attr('alt');
    var partida = $("#partida").val();
    var estilo = $(".estilo").html();
    var talla = $(".talla").html();
    var color = $(".color").html();
    var pza_x_caja = $(".pza_x_caja").html();
    var total_rem = $("#total_rem").val();
    var sobrante = total_rem % pza_x_caja;
    var pzas_caja = total_rem - sobrante;
    var cajas = pzas_caja / pza_x_caja;
    console.log(`barcode: ${barcode} \n clave: ${cve_art} \n partida: ${partida} \n estilo: ${estilo} \n talla: ${talla} \n color: ${color} \n pza x caja: ${pza_x_caja} \n total_rem: ${total_rem}`);
    $.ajax({
        type: "POST",
        url: "../ajax/etiqueta.php?opcion=guardar",
        data: {
            "barcode" :   barcode,
            "cve_art" :   cve_art,
            "partida" :   partida,
            "estilo"  :   estilo,
            "talla"   :   talla,
            "color"   :   color,
            "pza_x_caja"  :   pza_x_caja,
            "pzas_caja"   :   pzas_caja,
            "sobrante"    :   sobrante,
            "total_rem"   : total_rem,
            "cajas"       :   cajas
        },
        success: function (response) {
            console.log(response);
            if (response) {
                btn_imprimir_chrome();
            } else {
                $.alert({
                    type: 'red',
                    title: 'Error',
                    content: 'Ocurrio un error al imprimir la Etiqueta <br> Intente Nuevamente...',
                    buttons: {
                        'confirm': {
                            text: 'OK',
                            action: function(){
                                $.confirm({
                                    title: 'Reimprimir',
                                    content: '¿Desea reimprimir la etiqueta actual?',
                                    buttons: {
                                        confirm: {
                                            text: 'SI',
                                            btnClass: 'btn-blue',
                                            action: function(){
                                                btn_imprimir_chrome();
                                            }
                                        },
                                        cancel: {
                                            text: 'Crear Nueva',
                                            action: function(){
                                                btn_imprimir_chrome();
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    }
                });
            }
            $("#cve_art").prop("disabled", true);
            $("#partida").prop("disabled", true);
            $("#total_rem").prop("disabled", true);
            tabla.ajax.reload();
        }
    });
}

function listar(){
    tabla = $("#tabla_etiqueta").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "Blfrtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/etiqueta.php?opcion=listar",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 10,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

function mostrarinventario(e, cve_art)
{
    e.preventDefault();
    barcode();
    var browser = getBrowserInfo().split(' ');
    $.post("../ajax/etiqueta.php?opcion=mostrarinventario", {cve_art: cve_art},
        function (data, textStatus, jqXHR) {
            console.log(data);
            if( data != "null" ) {
                data = JSON.parse(data);
                console.log(data);
                (browser[0] == "Chrome")?$("#btn_imprimir_chrome").show():$("#btn_imprimir").show();
                $(".partida").html( $("#partida").val().toUpperCase() );
                $(".estilo").html(data.estilo);
                $(".talla").html(data.talla);
                $(".color").html(data.color_ing);
                $(".pza_x_caja").html(data.pza_x_caja);
                JsBarcode(".cve_art_barcode", data.cve_ing, { width: 1, height: 30, fontSize: 18, font: "Arial", fontOptions: "bold" });
                $(".cve_art_barcode").attr("alt", data.cve_ing);
                var sobrante = $("#total_rem").val() % data.pza_x_caja;
                var pzas_caja = $("#total_rem").val() - sobrante;
                var cajas = pzas_caja / data.pza_x_caja;
                $("#etiquetas_imprimir").show().html("ETIQUETAS A IMPRIMIR: <b>" + cajas + "</b>");
                if (sobrante > 0) {
                    $("#sobrantes_imprimir").show().html("PIEZAS SOBRANTES: <b>" + sobrante + "</b>");
                } else {
                    $("#sobrantes_imprimir").hide();
                }
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'La "clave de producto" no es Correcto' });
            }
        }
    );
}

function barcode(){
    $.ajax({
        type: "POST",
        url: "../ajax/etiqueta.php?opcion=ultimoid",
        success: function (response) {
            response = JSON.parse(response);
            var proximoid = parseInt(response.id) + 1;
            console.log(proximoid);
            $(".barcode").val("lbl"+proximoid);
            // JsBarcode(".barcode", "lbl" + proximoid, { width: 1, height: 15, displayValue: false, margin: 0 });
        }
    });
}

function nuevaetiqueta()
{
    $(".partida").html("");
    $(".estilo").html("");
    $(".talla").html("");
    $(".color").html("");
    $(".pza_x_caja").html("");
    $("#total_rem").val("");
    $(".cve_art_barcode").attr({"alt": "", "src": ""});
    $(".barcode").val("");
    $("#etiquetas_imprimir").hide().html("");
    $("#sobrantes_imprimir").hide().html("");
    $("#btn_imprimir").hide();
    $("#btn_imprimir_chrome").hide();
    $("#cve_art").prop("disabled", false);
    $("#partida").prop("disabled", false);
    $("#total_rem").prop("disabled", false);
    barcode();
}

function desactivar(id)
{
    $.confirm({
        title: 'Advertencia!',
        content: '¿Está Seguro de eliminar la Etiqueta?',
        buttons: {
            confirm: {
                text: "Aceptar",
                btnClass: 'btn-blue',
                action: function () {
                    $.post("../ajax/etiqueta.php?opcion=desactivar", { id: id }, function(response) {
                        console.log(response);
                        $.alert(response);
                        tabla.ajax.reload();
                    });
                }
            },
            cancel: {
                text: "Cancelar",
                btnClass: 'btn-red',
                action: function () {
                    $.alert('Se ha Cancelado la operación');
                }
            }
        }
    });
}

$("#txtBuscraXProd").keyup(function(e){
    e.preventDefault();
    if ($("#txtBuscraXProd").val() != "") {
        $.ajax({
            data: {"cve_art": $("#txtBuscraXProd").val()},
            type: "POST",
            url: "../ajax/etiqueta.php?opcion=listarinventario",
            success: function(data){
                var tabla = `<table width='100%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>
                    <tr>
                        <th>Clave ESP</th>
                        <th>Clave ING</th>
                    </tr>
                <tbody>`;
                data = JSON.parse(data);
                for (let i = 0; i < data.length; i++) {  
                    tabla += `<tr><td>${data[i][0]}</td><td>${data[i][1]}</td></tr>`;
                }
                tabla += `</tbody></table>`;
                $("div.scroll").html(tabla);
            }
        });
    }
});

/* $("#btn_imprimir").click(function (e) { 
    e.preventDefault();
    var estilo = $(".estilo").html();
    var talla = $(".talla").html();
    var color = $(".color").html();
    var pza_x_caja = $(".pza_x_caja").html();
    var partida = $(".partida").html();
    var barcode = $(".barcode").val();
    var cve_art = $(".cve_art_barcode").attr('alt');
    window.open("etiquetafirefox.php?estilo="+estilo+"&talla="+talla+"&color="+color+"&pza="+pza_x_caja+"&partida="+partida+"&barcode="+barcode+"&cve_art="+cve_art+"","_reporte","{height=450,width=550,scrollTo,resizable=1,scrollbars=1,location=0}");
}); */

function btn_imprimir_chrome() {
    //e.preventDefault();
    var estilo = $(".estilo").html();
    var talla = $(".talla").html();
    var color = $(".color").html();
    var pza_x_caja = $(".pza_x_caja").html();
    var partida = $(".partida").html();
    var barcode = $(".barcode").val();
    var cve_art = $(".cve_art_barcode").attr('alt');
    var sobrante = $("#total_rem").val() % pza_x_caja;
    var pzas_caja = $("#total_rem").val() - sobrante;
    var cajas = pzas_caja / pza_x_caja;

    var browser = getBrowserInfo().split(' ');
    if (browser[0] == "Chrome") {
        window.open("etiquetachrome.php?estilo="+estilo+"&talla="+talla+"&color="+color+"&pza="+pza_x_caja+"&partida="+partida+"&barcode="+barcode+"&cve_art="+cve_art+"&cajas="+cajas+"","_reporte","{opciones}");
    } else {
        window.open("etiquetafirefox.php?estilo="+estilo+"&talla="+talla+"&color="+color+"&pza="+pza_x_caja+"&partida="+partida+"&barcode="+barcode+"&cve_art="+cve_art+"","_reporte","{height=450,width=550,scrollTo,resizable=1,scrollbars=1,location=0}");
    }
};

function imprimir(pag) {
    var caracteristicas = "height=500,width=700,scrollTo,resizable=1,scrollbars=1,location=0";
    window.open(pag, 'Popup', caracteristicas);
    return false;
}

$('#mostrareliminados').change(function() {
    if(this.checked != true) {
        $("#div_tabla_eliminados").hide();
        $("#div_tabla_etiqueta").show();
    }
  else{
        $("#div_tabla_eliminados").show();
        $("#div_tabla_etiqueta").hide();
  }
});

function listareliminados() {
    tabla = $("#tabla_eliminados").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "Blfrtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/etiqueta.php?opcion=listareliminados",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 10,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

var getBrowserInfo = function() {
    var ua= navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1] === 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if( ( tem= ua.match( /version\/(\d+)/i ) ) != null ) M.splice(1, 1, tem[1]);
    return M.join(' ');
}

init();