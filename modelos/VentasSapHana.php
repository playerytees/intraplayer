<?php
    require "../config/ConexionHana.php";

    class VentasSapHana extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listarSucursales($sucursal)
        {
            try {
                $sql = 'SELECT
                    CASE WHEN "Code" = \'CE1\' THEN \'CE1P\' ELSE "Code" END "Codigo",
                    "Name" "Nombre"
                FROM
                    "DPT_BD"."@SO1_01SUCURSAL"
                WHERE
                    "Code" IN('.$sucursal.')
                ORDER BY "Code";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarSucursalesGP()
        {
            try {
                $sql = 'SELECT
                    T0."U_SO1_SUCURSAL" "Codigo",
                    T1."Name" "Nombre"
                FROM
                    "GP_BD"."@SO1_01AUTEMPLEADSUC" T0
                INNER JOIN
                    "GP_BD"."@SO1_01SUCURSAL" T1
                ON
                    T1."Code" = T0."U_SO1_SUCURSAL"
                WHERE
                    T0."U_SO1_SUCURSAL" IN(\'CE1\')
                GROUP BY
                    T0."U_SO1_SUCURSAL", T1."Name"
                ORDER BY
                    T0."U_SO1_SUCURSAL"';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDiarias($fecha)
        {
            // SUM((A."U_SO1_TOTALNETO" - IFNULL(A."U_SO1_IMPUESTO",0))) "Monto",
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    CASE WHEN A."U_SO1_SUCURSAL" = \'CE1\' THEN \'CE1P\' ELSE A."U_SO1_SUCURSAL" END "Sucursal"
                FROM "DPT_BD"."@SO1_01VENTA" A
                LEFT JOIN "DPT_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_STATUS" NOT IN(\'S\', \'N\')
                AND C."ListNum" NOT IN (\'7\')
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDiariasGP($fecha)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    A."U_SO1_SUCURSAL" "Sucursal"
                FROM "GP_BD"."@SO1_01VENTA" A
                LEFT JOIN "GP_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN (\'S\', \'N\')
                AND C."ListNum" NOT IN (\'7\')
                AND C."GroupCode" != \'102\'
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL"';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDiariasDistribuidores($fecha, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    \'DIS102\' "Sucursal"
                FROM "'.$empresa.'"."@SO1_01VENTA" A
                LEFT JOIN "'.$empresa.'"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN(\'S\', \'N\')
                AND C."GroupCode" = \'102\'
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL";';
            
                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasMensuales($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    CASE WHEN A."U_SO1_SUCURSAL" = \'CE1\' THEN \'CE1P\' ELSE A."U_SO1_SUCURSAL" END "Sucursal"
                FROM "DPT_BD"."@SO1_01VENTA" A
                LEFT JOIN "DPT_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_STATUS" NOT IN (\'S\', \'N\')
                AND C."ListNum" NOT IN (\'7\')
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasMensualesGP($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    A."U_SO1_SUCURSAL" "Sucursal"
                FROM "GP_BD"."@SO1_01VENTA" A
                LEFT JOIN "GP_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN(\'S\', \'N\')
                AND C."ListNum" NOT IN (\'7\')
                AND C."GroupCode" != \'102\'
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasMensualesDistribuidores($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    \'DIS102\' "Sucursal"
                FROM "'.$empresa.'"."@SO1_01VENTA" A
                LEFT JOIN "'.$empresa.'"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN(\'S\', \'N\')
                AND C."GroupCode" = \'102\'
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionDiaria($date)
        {
            try {
                $sql = 'SELECT
                    A."U_SO1_SUCURSAL" "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                "DPT_BD"."@SO1_01DEVOLUCION" A
                LEFT JOIN "DPT_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$date.'
                AND C."ListNum" NOT IN (\'7\')
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionDiariaGP($date)
        {
            try {
                $sql = 'SELECT
                    A."U_SO1_SUCURSAL" "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                    "GP_BD"."@SO1_01DEVOLUCION" A
                LEFT JOIN "GP_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                INNER JOIN "GP_BD"."@SO1_01VENTA" D
                ON (A."U_SO1_FOLIOCAJA" = D."Name")
                WHERE
                    A."U_SO1_FECHA" = '.$date.'
                AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND C."ListNum" NOT IN (\'5\', \'7\')
                AND D."U_SO1_TIPO" IN (\'CA\',\'CR\',\'DO\')
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionDiariaDistGP($date)
        {
            try {
                $sql = 'SELECT
                    \'DIS102\' "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                    "GP_BD"."@SO1_01DEVOLUCION" A
                LEFT JOIN "GP_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$date.'
                AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND C."GroupCode" = \'102\'
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionMensual($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    A."U_SO1_SUCURSAL" "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                    "DPT_BD"."@SO1_01DEVOLUCION" A
                LEFT JOIN "DPT_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND C."ListNum" NOT IN (\'7\')
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionMensualGP($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    A."U_SO1_SUCURSAL" "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                    "GP_BD"."@SO1_01DEVOLUCION" A
                LEFT JOIN "GP_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                INNER JOIN "GP_BD"."@SO1_01VENTA" D
                ON (A."U_SO1_FOLIOCAJA" = D."Name")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND C."ListNum" NOT IN (\'5\', \'7\')
                AND D."U_SO1_TIPO" IN (\'CA\',\'CR\',\'DO\')
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionMensualDistGP($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    \'DIS102\' "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                    "GP_BD"."@SO1_01DEVOLUCION" A
                LEFT JOIN "GP_BD"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND C."GroupCode" = \'102\'
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
    