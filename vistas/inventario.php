<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['resurtido']==1) {
?>
    <?php $idsucursal = isset($_GET['sucursal'])?$_GET['sucursal']:""; ?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title text-playerytees">
                    <h1 style="float: left;">Existencias </h1> <span id ="spansucursal" style="float: right;padding: 15px 0; margin-left: 5px;"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Resurtido</a></li>
                        <li><a href="sucursales.php">Sucursales</a></li>
                        <li class="active">Existencias</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="idsucursal" id="idsucursal" value="<?php echo $idsucursal; ?>">
    <div class="content mt-3">
        <div class="row" id="div_inventario">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-playerytees">
                        <b>Agregar Existencias </b>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <form name="form_inventario" id="form_inventario" method="POST" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="file-inventario" class="form-control-label text-playerytees">Seleccionar Inventario</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="file-inventario" name="file-inventario" class="form-control-file">
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row" id="temp_inventario">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title text-playerytees">Mostrar Existencias</strong>
                    </div>
                    <div class="card-body">
                        <div id="contenedor_inve" style="overflow: auto; height: 300px;"></div>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-playerytees" id="btn_guardar_inve" onclick="guardar()" disabled="disabled">Guardar</button>
                        <a href="sucursales.php" class="btn btn-danger">Cancelar</a>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row" id="listado_resurtido">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Resurtido General</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body">
                        <table id="tabla_inventario" class="table table-bordered table-sm table-hover" style="width: 100%;">
                            <thead>
                                <th>Manufactura</th>
                                <th>Personalizado</th>
                                <th>Linea</th>
                                <th>Minimo/Ideal</th>
                                <th>Existencias</th>
                                <th>Faltante</th>
                                <th>Surtir</th>
                                <th>No. Piezas</th>
                                <th>Piezas surtir</th>
                                <th>Exis CEDIS</th>
                                <th>Dif CEDIS</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-primary" onclick="guardarresurtido()">Resurtir</button>
                        <button type="button" class="btn btn-danger" onclick="cancelarform()">Cancelar</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content -->
    <div id="divjson"></div>
<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="../public/assets/js/xlsx.full.min.js"></script>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/inventario.js"></script>
<?php
}
ob_end_flush();
?>