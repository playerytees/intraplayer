<?php
require_once "../modelos/VentasArticulos.php";
session_start();
$ventas = new VentasArticulos();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$start_date = isset($_POST["start_date"])?htmlspecialchars(trim($_POST["start_date"])):"";
$end_date = isset($_POST["end_date"])?htmlspecialchars(trim($_POST["end_date"])):"";
$empresa = isset($_POST["empresa"])?htmlspecialchars(trim($_POST["empresa"])):"";
$almacen = isset($_POST["almacen"])?($_POST["almacen"]):"";

switch ($_GET["opcion"]) {
    case 'listar':
        if (empty($almacen)) {
            $almacen = array("''");
        }
        $almacenes = implode( ",",array_filter($almacen) );

        $result = $ventas->listar( $empresa, "'$start_date'", "'$end_date'", "$almacenes" );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => utf8_encode($reg->FolioR1),
                '1' => date_format(date_create(utf8_encode($reg->FechaCreacion)), 'd/m/y'),
                '2' => utf8_encode($reg->Articulo),
                '3' => utf8_encode($reg->Linea),
                '4' => utf8_encode($reg->Talla),
                '5' => utf8_encode($reg->Color),
                '6' => number_format($reg->Cantidad, 0, ".", ","),
                '7' => '$'.number_format($reg->PrecioUnitario, 2, ".", ","), 
                '8' => '$'.number_format($reg->Total, 2, ".", ","),
                '9' => $reg->ListadePrecio,
                '10' => utf8_encode($reg->CodigoCliente)." - ".utf8_encode($reg->NombreCliente),
                '11' => utf8_encode($reg->Vendedor),
                '12' => utf8_encode($reg->Almacen),
                '13' => "<div title='".utf8_encode($reg->Comentarios)."' style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width:200px;'>".utf8_encode($reg->Comentarios)."</div>",
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);

        break;

    case 'listarSucursales':
        $result = $ventas->Sucursales( $empresa );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => utf8_encode($reg->Code),
                '1' => utf8_encode($reg->Name)
            );
        }

        echo json_encode($data);

        break;

    default:
        echo "No se encontro la opcion";
        break;
}