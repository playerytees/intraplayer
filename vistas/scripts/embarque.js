var tabla;
var guardarPartidas = [];
var limpiarPartidas = [];
function init()
{
    barcode();
    $(".loader").hide();
}

function limpiar()
{

}

function mostraretiqueta()
{
    var inputbarcode = $("#barcodebox").val().trim();
    $.post("../ajax/etiqueta.php?opcion=mostrar", { barcode: inputbarcode }, // opcion=mostrarfile
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            if (data == null) {
                $("#message-sku").html("<i class='fa fa-times fa-2x'></i>");
                $("#barcodebox").focus();
                $("#barcodebox").val("");
            } else {
                $("#message-sku").html("<i class='fa fa-check fa-2x'></i>");

                var piezas = parseInt(data.pza_x_caja);

                $("#boxscanner tr").find('td:eq(0)').each(function () {

                    sku = $(this).html();

                    if (sku == inputbarcode) {
                        $(this).parents().eq(0).remove();
                        var trDelResultado = $(this).parent();
                        var piezasTD = 0;

                        piezasTD = parseInt(trDelResultado.find("td:eq(7)").html());

                        piezas = piezasTD + parseInt(data.pza_x_caja);
                    }
                });

                var cajas = piezas / parseInt(data.pza_x_caja);

                var partida_array = data.partida.split('-');
                var partida_segunda_pos = (typeof partida_array[1]!=='undefined')?partida_array[1]:"";
                var partida_ultima_pos = (typeof partida_array[2]!=='undefined')?'-'+partida_array[2]:"";
                var partidalimpia = partida_segunda_pos + partida_ultima_pos;

                var tabla = `<tr>
                <td class="d-none">${inputbarcode}</td>
                <td class="d-none">${partidalimpia}</td>
                <td class="d-none">${data.cve_art}</td>
                <td>${data.partida}</td>
                <td>${data.estilo}</td>
                <td>${data.color}</td>
                <td>${data.talla}</td>
                <td>${piezas}</td>
                <td>${cajas}</td>
                <td><i style="cursor: pointer;" class="fa fa-trash-o ml-3" id="trash_row" title="Eliminar"></i></td>
            </tr>`;
                $("#boxscanner > tbody").append(tabla);
                $("#barcodebox").focus();
                $("#barcodebox").val("");

                guardarPartidas.push({barcode: data.barcode, partida: data.partida, estilo: data.estilo, color: data.color, talla: data.talla, piezas: piezas, cajas: cajas });

                limpiarPartidas = removeDuplicates(guardarPartidas, "barcode");

                localStorage.setItem("partidasGuardadas", JSON.stringify(limpiarPartidas));
            }
        }
    );
}

function guardarpartidas()
{
    $("#barcodebox").focus();
    var tablaToJSON = $("#boxscanner").tableToJSON();
    var tablaObt = JSON.stringify(tablaToJSON);
    console.log(tablaToJSON);
    if (tablaToJSON.length == 0) {
        $.alert("No hay Partidas que guardar");
    } else {
        $.ajax({
            type: "POST",
            url: "../ajax/embarque.php?opcion=guardarpartidas",
            data: { tablapartidas: tablaObt },
            beforeSend: function () {
                $(".loader").show();
            },
            success: function (response) {
                $(".loader").hide();
                console.log(response);
                if (response == 1) {
                    $.alert({ type: 'green', title: 'Correcto', content: 'Partidas guardado correctamente' });
                    $("#boxscanner > tbody").html("");
                } else {
                    $.alert({ type: 'red', title: 'Error', content: 'Partidas no se pudo guardar' });
                }
            }
        });
    }
    localStorage.setItem("jsonData", tablaObt);
}

function pulsarenter(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13 && tecla != 32);
}

function barcode()
{
    $.fn.delayPasteKeyUp = function (fn, ms) {
        var timer = 0;
        $(this).on("propertychange input", function () {
            clearTimeout(timer);
            timer = setTimeout(fn, ms);
        });
    };

    $("#barcodebox").delayPasteKeyUp(function () {
        $("#btnverificar").trigger("click");
    }, 200);
}

/* Eliminar una fila de la tabla */
$('#boxscanner').on('click', '#trash_row', function(){
    var result = confirm("Desea Eliminarlo?");
        if (result == true) {
            $(this).parents('tr').eq(0).remove();
        }
});

function mostrarlocalStorage()
{
    var jsonData = JSON.parse(localStorage.getItem("jsonData"));
    var tabla = "";
    for (let i = 0; i < jsonData.length; i++)
    {
        tabla += `<tr>
                <td class="d-none">${jsonData[i].barcode}</td>
                <td>${jsonData[i].Partida}</td>
                <td>${jsonData[i].Estilo}</td>
                <td>${jsonData[i].Color}</td>
                <td>${jsonData[i].Talla}</td>
                <td>${jsonData[i].Piezas}</td>
                <td>${jsonData[i].Cajas}</td>
                <td><i style="cursor: pointer;" class="fa fa-trash-o ml-3" id="trash_row" title="Eliminar"></i></td>
            </tr>`;
    }
    $("#boxscanner > tbody").append(tabla);

    $("#btn-showvalues").attr("disabled", "");

    //localStorage.removeItem("jsonData");
}

function mostrarPartidasGuardadas()
{
    var partidasGuardadas = JSON.parse(localStorage.getItem("partidasGuardadas"));
    var tabla = "";

    for (let i = 0; i < partidasGuardadas.length; i++)
    {
        tabla += `<tr>
                <td class="d-none">${partidasGuardadas[i].barcode}</td>
                <td>${partidasGuardadas[i].partida}</td>
                <td>${partidasGuardadas[i].estilo}</td>
                <td>${partidasGuardadas[i].color}</td>
                <td>${partidasGuardadas[i].talla}</td>
                <td>${partidasGuardadas[i].piezas}</td>
                <td>${partidasGuardadas[i].cajas}</td>
                <td><i style="cursor: pointer;" class="fa fa-trash-o ml-3" id="trash_row" title="Eliminar"></i></td>
            </tr>`;
    }
    $("#boxscanner > tbody").append(tabla);
}

function removeDuplicates(originalArray, prop) {
    var newArray = [];
    var lookupObject  = {};

    for(var i in originalArray) {
       lookupObject[originalArray[i][prop]] = originalArray[i];
    }

    for(i in lookupObject) {
        newArray.push(lookupObject[i]);
    }
    return newArray;
}

init();