<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';
if($_SESSION['proximasllegadas']==1) {
?>
<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title d-sm-inline text-playerytees">Proximas llegadas</strong>
                </div> <!-- .card-header -->
                <div class="card-body">

                    <div class="default-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Lista de partidas</a>
                                <a class="nav-item nav-link" id="nav-lista-file-tab" data-toggle="tab" href="#nav-lista-file" role="tab" aria-controls="nav-lista-file" aria-selected="false">Subir archivo</a>
                            </div>
                        </nav>
                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <button id="btnagregar" class="btn btn-playerytees btn-sm float-sm-right" type="button" onclick="guardaryeditar()">Nuevo</button>
                                <table class="table table-striped table-sm table-bordered table-hover table-responsive-sm" id="tblPartidas" style="width: 100%">
                                    <thead>
                                        <th>Num.</th>
                                        <th>Partida</th>
                                        <th>Codigo</th>
                                        <th>Cantidad</th>
                                        <th>Fecha entrega</th>
                                        <th>Opciones</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-lista-file" role="tabpanel" aria-labelledby="nav-lista-file-tab">
                                <form name="form" id="form" method="POST" class="form-horizontal">
                                    <div class="row form-group">
                                        <!-- <div class="col col-md-3">
                                            <label for="fileExcel" class=" form-control-label">Subir archivo...</label>
                                        </div> -->
                                        <div class="col-12">
                                            <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="fileExcel" name="fileExcel" class="form-control-file">
                                            <div id="contentExcel" style="overflow: auto;height: 300px;"></div>
                                        </div>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-playerytees btn-sm" id="btnGuardar" onclick="guardartable()" disabled="disabled">Guardar</button>
                                <button type="button" class="btn btn-info btn-sm" id="" onclick="limpiar()" disabled="disabled">Limpiar</button>
                            </div>
                        </div>
                    </div>

                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div>
    </div> <!-- .row -->
</div> <!-- .content -->
<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="../public/assets/js/xlsx.full.min.js"></script>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script type="text/javascript" src="scripts/proximasllegadas.js"></script>
<?php
}
ob_end_flush();
?>