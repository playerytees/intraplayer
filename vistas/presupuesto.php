<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';
if($_SESSION['RegistrarPresupuesto']==1) {
?>
<div class="content mt-3">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title d-sm-inline text-playerytees">Presupuesto</strong>
                </div> <!-- .card-header -->
                <div class="card-body">
                    <div class="sufee-alert alert with-close alert-primary alert-dismissible fade show">
                        <span class="badge badge-pill badge-primary">Información</span>
                            <a href="#" data-toggle="modal" data-target="#instrucciones">Instrucciones para subir el <b>Presupuesto</b>.</a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="default-tab">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active show" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Listar</a>
                                <a class="nav-item nav-link" id="nav-lista-file-tab" data-toggle="tab" href="#nav-lista-file" role="tab" aria-controls="nav-lista-file" aria-selected="false">Subir archivo</a>
                            </div>
                        </nav>
                        <div class="tab-content pl-3 pt-2" id="nav-tabContent">
                            <div class="tab-pane fade active show" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <button id="btnagregar" class="btn btn-playerytees float-sm-right" type="button" onclick="guardaryeditar()">Nuevo</button>
                                <table class="table table-striped table-sm table-bordered table-hover table-responsive-sm" id="tblPresupuesto" style="width: 100%">
                                    <thead>
                                        <th>#</th>
                                        <th>Sucursal</th>
                                        <th>Meta Anterior</th>
                                        <th>Meta Actual</th>
                                        <th>Mes</th>
                                        <th>Año</th>
                                        <th>Acciones</th>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="nav-lista-file" role="tabpanel" aria-labelledby="nav-lista-file-tab">
                                <form name="form" id="form" method="POST" class="form-horizontal">
                                    <div class="row form-group">
                                        <!-- <div class="col col-md-3">
                                            <label for="fileExcel" class=" form-control-label">Subir archivo...</label>
                                        </div> -->
                                        <div class="col-12">
                                            <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="fileExcel" name="fileExcel" class="form-control-file">
                                            <div id="contentExcel" style="overflow: auto;height: 300px;"></div>
                                        </div>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-playerytees btn-sm" id="btnGuardar" onclick="guardartable()" disabled="disabled">Guardar</button>
                                <button type="button" class="btn btn-info btn-sm" id="btnlimpiar" onclick="limpiar()" disabled="disabled">Limpiar</button>
                            </div>
                        </div>
                    </div>

                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div>
    </div> <!-- .row -->
</div> <!-- .content -->

<div class="modal fade" id="instrucciones" tabindex="-1" role="dialog" aria-labelledby="instruccionesLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="instruccionesLabel">Subir Presupuesto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" style="height:550px;overflow-y: auto;">
                <p>Usted podrá descargar el formato del presupuesto <a href="../formatos/descargar-formato-presupuesto.php" style="font-size: 18px;font-weight: bold;">Aquí</a>.</p>
                <p>Antes de que llene el formato debera de tener en cuenta las siguientes especificaciones:</p>
                <ul class="px-4">
                    <li><b>Sucursal:</b> Colocar el codigo de la sucursal en caso de que sea una sucursal como tal, en el caso de que sea distribuidores o Intercopañias colocar el nombre que tienen de "Alias"</li>
                    <li><b>Meta Anterior:</b> Colocar el monto con IVA del año anterior, ejemplo, si se colocara el año con <em>2020</em> el año anteior seria el monto del <em>2019</em></li>
                    <li><b>Meta Actual:</b> Colocar el monto con IVA del año actual que se esta colocando.</li>
                    <li><b>Mes:</b> Colocar el mes con numero aplicando un formato de dos digitos, ejemplo si el mes es <em>Agosto</em> colocar <b>08</b>.</li>
                    <li><b>Año:</b> Colocar el año actual utilizando un formato de 4 digitos ej: <em>2020</em></li>
                </ul>
                <p><b>Ejemplo:</b></p>
                <table class="table table-sm">
                    <thead>
                        <th>Sucursal</th>
                        <th>Meta Anterior</th>
                        <th>Meta Actual</th>
                        <th>Mes</th>
                        <th>Año</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>MO05</td>
                            <td>1617154</td>
                            <td>1780000</td>
                            <td>08</td>
                            <td>2020</td>
                        </tr>
                        <tr>
                            <td>NO04</td>
                            <td>447062</td>
                            <td>530000</td>
                            <td>08</td>
                            <td>2020</td>
                        </tr>
                    </tbody>
                </table>
                <p><b>Nota:</b> Para las metas solo utilizar <b>numeros</b>, si es necesario utilizar el punto para los decimales. <br> Si alguna sucursal no cuenta con alguna de las metas colocar <em>0</em>.</p>
                <p><b>NO cambiar el nombre del encabezado</b></p>
                <p><b>Codigo para las sucursales:</b></p>
                <ul class="px-4">
                    <li>FEB => 5 de Febrero</li>
                    <li>ISA2  => Isabel Gildan</li>
                    <li>ISA  => Isabel Bones</li>
                    <li>LOR  => Lorey</li>
                    <li>OA2  => Oaxaca</li>
                    <li>HE07  => Hermosillo</li>
                    <li>CUE  => Cuernavaca</li>
                    <li>PAC  => Pachuca</li>
                    <li>SL02  => San Luis Potosi</li>
                    <li>NO04  => La Noria</li>
                    <li>MO05  => Monterrey</li>
                    <li>CE01GP  => Cedis</li>
                    <li>DIS102  => Distribuidores</li>
                    <li>SLP => San Luis Potosi 2</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="../public/assets/js/xlsx.full.min.js"></script>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script type="text/javascript" src="scripts/presupuesto.js"></script>
<?php
}
ob_end_flush();
?>