var tabla;

function init() {
    mostrarform(false);
    listar();
    mostrar();
    $(".loader").hide();

    $("#file-catalogo").change(function () {
        $("#btn_guardar_catalog").prop("disabled", this.files.length == 0);
    });
}

function limpiar() {
    $("#file-catalogo").val("");
    $("#contenedor_catalogo").html("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#listadoCatalogo").hide();
        $("#agregar_catalogo").show();
        $("#temp_catalogo").show();
    } else {
        $("#listadoCatalogo").show();
        $("#agregar_catalogo").hide();
        $("#temp_catalogo").hide();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar() {
    tabla = $("#tabla_catalogo").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Blfrtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/catalogo.php?opcion=listar",
            type: "POST",
            data: { id_sucursal: $("#idsucursal").val() },
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function guardar()
{
    var id_sucursal = $("#idsucursal").val();
    var tablaToJSON = $("#tabla_catalogo_temp").tableToJSON();
    var tablaObt = JSON.stringify(tablaToJSON);

    $.ajax({
        type: "POST",
        url: "../ajax/catalogo.php?opcion=guardar",
        data: { id_sucursal: id_sucursal, tabla_catalog: tablaObt },
        beforeSend: function () {
            $(".loader").show();
        },
        success: function (response) {
            console.log(response);
            if (response == 1) {
                $.alert({ type: 'green', title: 'Correcto', content: 'Catalogo guardado correctamente' });
                tabla.ajax.reload();
                $(".loader").hide();
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'Catalogo no se pudo guardar <br> Intente nuevamente' });
                $(".loader").hide();
            }
            mostrarform(false);
        }
    });
    limpiar();
}

function mostrar() {
    $.post("../ajax/sucursales.php?opcion=mostrar", { id: $("#idsucursal").val() },
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            $("#spansucursal").html(" / " + data.descr)
        }
    );
}

$("#file-catalogo").change(function (e) {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 100000) {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no debe superar 1MB' });
        this.value = '';
        this.files[0].name = '';
    } else if (fileName.split('.').pop() != "xlsx") {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no tiene la extensión adecuada' });
        this.value = '';
        this.files[0].name = '';
    } else {
        var reader = new FileReader();

        reader.readAsArrayBuffer(e.target.files[0]);

        reader.onload = function (e) {
            var data = new Uint8Array(reader.result);
            var wb = XLSX.read(data, { type: 'array' });

            if (wb.SheetNames[0] == 'catalogo') {
                var htmlstr = XLSX.write(wb, { sheet: 'catalogo', type: 'binary', bookType: 'html' });
                $("#contenedor_catalogo")[0].innerHTML += htmlstr;

                $("#contenedor_catalogo > table").attr({ 'id': 'tabla_catalogo_temp' });
                $("#tabla_catalogo_temp").addClass('table table-bordered table-sm');
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'El archivo no contiene el nombre correcto' });
                $("#file-catalogo").val('');
            }
        }
    }
});

init();