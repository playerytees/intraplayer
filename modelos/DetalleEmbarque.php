<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");
session_start();
class DetalleEmbarque
{
    public function __construct()
    {

    }

    /* public function insertar($id_embarque)
    {
        $sql = "INSERT INTO detalle_embarque (id, barcode, partida, estilo, color, talla, piezas, cajas, id_embarque, modified, created) SELECT null, barcode, partida, estilo, color, talla, piezas, cajas, '$id_embarque', '".date("Y-m-d h:i:s")."', '".date("Y-m-d h:i:s")."' FROM temporary WHERE cajas > 0";

        return ejecutarConsulta($sql);
    } */

    public function editar($id)
    {
        $sql = "UPDATE detalle_embarque SET modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($id)
    {
        $sql = "SELECT * FROM detalle_embarque WHERE id = '$id'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar($id_embarque)
    {
        $sql = "SELECT id, barcode, partida, estilo, color, talla, (piezas*cajas) AS piezas, piezas AS pza_x_caja, cajas, partidalimpia, codigo FROM detalle_embarque WHERE id_embarque = '$id_embarque'";
        return ejecutarConsulta($sql);
    }

    public function delete($id)
    {
        $sql = "DELETE FROM detalle_embarque WHERE id = '$id'";
        return ejecutarConsulta($sql);
    }

    public function deletetemporary()
    {
		$tabla = ($_SESSION['user'] == 'embarques2') ? "temporary2" : "temporary";										
        $delete = "DELETE FROM $tabla";
            
        return ejecutarConsulta($delete);
    }

    public function asignarfolio($id,$barcode,$partida,$estilo,$color,$talla,$pza_x_caja,$cajas,$id_embarque,$partidalimpia,$codigo)
    {
        $sql = "SELECT * FROM detalle_embarque WHERE barcode='$barcode' AND partida='$partida' AND estilo='$estilo' AND color='$color' AND talla='$talla' AND id_embarque='$id_embarque'";
        
        if (ejecutarConsultaSimpleFila($sql) == NULL) {
            $insert = "INSERT INTO detalle_embarque VALUES (NULL, '$barcode', '$partida', '$estilo', '$color', '$talla', '$pza_x_caja', '$cajas', '$id_embarque', now(), now(), '$partidalimpia', '$codigo')";

            return ejecutarConsulta($insert);
        } else {
            $update = "UPDATE detalle_embarque SET cajas = cajas+'$cajas', modified = now() WHERE barcode='$barcode' AND partida='$partida' AND estilo='$estilo' AND color='$color' AND talla='$talla' AND id_embarque='$id_embarque'";

            return ejecutarConsulta($update);
        }
    }
}
?>