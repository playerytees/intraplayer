<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");
session_start();
Class Etiqueta
{
    public function __construct()
    {

    }

    public function insertar($barcode, $cve_art, $partida, $estilo, $talla, $color, $pza_x_caja, $pzas_caja, $sobrante, $total_rem, $cajas)
    {
        $sql = "INSERT INTO etiqueta VALUES (NULL, '$barcode', '$cve_art', '$partida', '$estilo', '$talla', '$color', '$pza_x_caja', '$pzas_caja', '$sobrante', '$total_rem', '$sobrante', '$cajas', now(), '1', 'NULL', now(), now(), '".$_SESSION["user"]."')";

        return ejecutarConsulta($sql);
    }

    public function editar($id)
    {
        $sql = "UPDATE etiqueta SET campo = 'campo' WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function desactivar($id)
    {
        $sql = "UPDATE etiqueta SET activo = 0 WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function activar($id)
    {
        $sql = "UPDATE etiqueta SET activo = 1 WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($barcode)
    {
        $sql = "SELECT * FROM etiqueta WHERE barcode = '$barcode' AND activo = 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function mostrarinventario($cve_art)
    {
        $sql = "SELECT * FROM inventario_sae WHERE (cve_esp = '$cve_art' OR cve_ing = '$cve_art') AND activo = 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar(){
        $sql = "SELECT * FROM etiqueta WHERE activo = 1";
        return ejecutarConsulta($sql);
    }

    public function ultimoid()
    {
        $sql = "SELECT LAST_INSERT_ID(id) as id FROM etiqueta ORDER BY id DESC LIMIT 1";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listarinventario($cve_art)
    {
        $sql = "SELECT * FROM inventario_sae WHERE (cve_esp LIKE '".$cve_art."%' OR cve_ing LIKE '".$cve_art."%') AND activo = 1";
        return ejecutarConsulta($sql);
    }

    public function mostrarsobrante($partida)
    {
        $sql = "SELECT * FROM etiqueta WHERE partida = '$partida'";
        return ejecutarConsulta($sql);
    }

    public function listareliminados()
    {
        $sql = "SELECT * FROM etiqueta WHERE activo = 0";
        return ejecutarConsulta($sql);
    }
}
?>