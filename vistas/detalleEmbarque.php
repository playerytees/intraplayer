<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

    require 'header.php';

    if ($_SESSION['embarques']==1) {
?>
    <?php $id_embarque = isset($_GET['folio'])?$_GET['folio']:""; ?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="text-playerytees">Detalle de Embarque</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Orden de Embarque</a></li>
                        <li class="active">Detalle</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="id_embarque" id="id_embarque" value="<?php echo $id_embarque; ?>">
    <div class="content mt-3">
        <div class="row" id="listadodepartidas">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title float-left text-playerytees"> Listado de Partidas </strong>
                        <a data-toggle="modal" href="#myModal">
                            <button type="button" id="btnAgregarPartidas" class="btn btn-playerytees btn-sm float-right">Agregar Partidas</button>
                        </a>
                    </div>
                    <div class="card-body">
                        <table id="tabladepartidas" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th>Partida</th>
                                <th>Estilo</th>
                                <th>Color</th>
                                <th>Talla</th>
                                <th>Piezas</th>
                                <th>Cajas</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <a href="folio_embarque.php" class="btn btn-danger btn-sm float-left">Regresar</a>
                        <button type="button" class="btn btn-playerytees btn-sm float-right" onclick="guardar()">Guardar</button>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content .mt-3 -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Seleccionar Partida</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="tbPartidas" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                    <thead>
                        <th></th>
                        <th>Partida</th>
                        <th>Estilo</th>
                        <th>Color</th>
                        <th>Talla</th>
                        <th>Piezas</th>
                        <th>Cajas</th>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancelar</button>
                <!-- <button type="button" class="btn btn-primary btn-sm">Agregar</button> -->
            </div>
        </div>
    </div>
</div>
<!-- Fin modal -->

<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/detalleEmbarque.js"></script>
<?php
}
ob_end_flush();
?>