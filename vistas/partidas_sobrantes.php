<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';

    if ($_SESSION['embarques']==1) {
?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Sobrantes</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Embarques</a></li>
                        <li class="active">Sobrantes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title float-left"> Sobrantes a embarcar </strong>
                        <a href="sobrantes.php" class="btn btn-danger btn-sm float-right">Regresar</a>
                    </div>
                    <div class="card-body">
                        <table id="tablatemporary" class="table table-bordered table-sm" style="width: 100%;">
                            <thead>
                                <th></th>
                                <th>Partida</th>
                                <th>Estilo</th>
                                <th>Color</th>
                                <th>Talla</th>
                                <th>Piezas</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button id="btn_guardar" type="button" class="btn btn-primary btn-sm" onclick="guardarembarque(event)">Guardar</button>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Sobrantes Pendientes</strong>
                    </div>
                    <div class="card-body">
                        <table id="tablasobrantes" class="table table-bordered table-sm" style="width: 100%;">
                            <thead>
                                <th></th>
                                <th>Partida</th>
                                <th>Estilo</th>
                                <th>Color</th>
                                <th>Talla</th>
                                <th>Piezas</th>
                                <th></th>
                            </thead>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <a href="#" class="btn btn-primary btn-sm">Imprimir</a>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content .mt-3 -->
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/partidas_sobrantes.js"></script>
<?php
}
ob_end_flush();
?>