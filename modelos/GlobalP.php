<?php
    require "../config/ConexionGlobalP.php";

    class GlobalP
    {
        public function __construct()
        {
            
        }

        public function listar()
        {
            $sql = "SELECT INVE01.CVE_ART, INVE01.DESCR, INVE01.LIN_PROD, MULT01.EXIST FROM INVE01 JOIN MULT01 ON INVE01.CVE_ART = MULT01.CVE_ART WHERE MULT01.CVE_ALM = 1";

            return ejecutarConsulta($sql);
        }
    }
?>