<?php
ob_start();
session_start();
date_default_timezone_set("America/Mexico_City");
if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';

    if ($_SESSION['Distribuidores']==1) {
?>
<style>
#tabladistribuidores td:nth-child(1){
    text-align: left;
    white-space: nowrap;
}
</style>
<div class="breadcrumbs">
    <div class="col-sm-12">
        <div class="page-header">
            <div class="page-title">
                <ol class="breadcrumb text-playerytees">
                    <li><a href="ventasGral.php">Ventas</a></li>
                    <li class="active">Distribuidores</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <!-- <div class="card-header pb-0">
                    <strong class="card-title text-playerytees float-left">Distribuidores</strong>
                </div> --> <!-- .card-header -->
                <div class="card-body card-block">
                    <div class="form-inline">
                        <div class="form-group">
                            <label class="form-control-label mr-1">Seleccionar Fecha:</label>
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="date" class="form-control form-control-sm" id="fecha" name="fecha" required value="<?= date("Y-m-d")?>">
                                <button type="button" class="btn btn-playerytees" id="btn-ver">Ver</button>
                            </div>
                        </div>
                    </div>
                    <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tabladistribuidores" style="width: 100%;">
                        <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                            <tr class="">
                                <th>SUCURSAL</th>
                                <th>VENTA DIA</th>
                                <th>VENTA ACUMULADA MES</th>
                                <th>META</th>
                                <th>ALCANCE</th>
                                <th>AÑO ANTERIOR</th>
                                <th>ALCANCE</th>
                                <th>PAGO</th>
                                <th>VENCIDO</th>
                                <th>ACUMULADO</th>
                            </tr>
                        </thead>
                        <tbody style="font-size: 14px; text-align: right;"></tbody>
                    </table>
                </div> <!-- card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-12 -->
    </div> <!-- .row -->

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-four">
                        <div class="stat-icon dib">
                            <i class="fa fa-dollar text-success"></i>
                        </div> <!-- .stat-icon -->
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-heading">Venta Diaria</div>
                                <div class="stat-text" id="totalDailySales"></div>
                            </div>
                        </div> <!-- .stat-content -->
                    </div> <!-- .stat-widget-four -->
                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-3 -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-four">
                        <div class="stat-icon dib">
                            <i class="fa fa-dollar text-success"></i>
                        </div> <!-- .stat-icon -->
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-heading" style="font-size: 19px;">Venta Mensual</div>
                                <div class="stat-text" id="totalMonthlySales"></div>
                            </div>
                        </div> <!-- .stat-content -->
                    </div> <!-- .stat-widget-four -->
                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-3 -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-four">
                        <div class="stat-icon dib">
                            <i class="fa fa-dollar text-success"></i>
                        </div> <!-- .stat-icon -->
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-heading" style="font-size: 19px;">Total Pago</div>
                                <div class="stat-text" id="totalPago"></div>
                            </div>
                        </div> <!-- .stat-content -->
                    </div> <!-- .stat-widget-four -->
                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-3 -->
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="stat-widget-four">
                        <div class="stat-icon dib">
                            <i class="fa fa-line-chart text-info"></i>
                        </div> <!-- .stat-icon -->
                        <div class="stat-content">
                            <div class="text-left dib">
                                <div class="stat-heading">Alcance Total</div>
                                <div class="stat-text" id="AlcanceTotal"></div>
                            </div>
                        </div> <!-- .stat-content -->
                    </div> <!-- .stat-widget-four -->
                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-3 -->
    </div> <!-- .row -->

</div> <!-- .content -->
<?php
    }
    else {
        require "noacceso.php";
    }
    require "footer.php";
?>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script src="scripts/distribuidoresGral.js"></script>
<?php
}
ob_end_flush();
