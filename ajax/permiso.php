<?php
require_once "../modelos/Permiso.php";

$permiso = new Permiso();

switch ($_GET['opcion']) {
    case 'listar':
        $response = $permiso->listar();

        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->nombre
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    
    default:
        echo "No Existe la opcion seleccionada";
        break;
}
?>