<?php
require_once "../modelos/Etiqueta.php";

$etiqueta = new Etiqueta();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$barcode = isset($_POST['barcode'])?limpiarCadena($_POST['barcode']):"";
$cve_art = isset($_POST['cve_art'])?limpiarCadena($_POST['cve_art']):"";
$partida = isset($_POST['partida'])?limpiarCadena($_POST['partida']):"";
$estilo = isset($_POST['estilo'])?limpiarCadena($_POST['estilo']):"";
$talla = isset($_POST['talla'])?limpiarCadena($_POST['talla']):"";
$color = isset($_POST['color'])?limpiarCadena($_POST['color']):"";
$pza_x_caja = isset($_POST['pza_x_caja'])?limpiarCadena($_POST['pza_x_caja']):"";
$pzas_caja = isset($_POST['pzas_caja'])?limpiarCadena($_POST['pzas_caja']):"";
$sobrante = isset($_POST['sobrante'])?limpiarCadena($_POST['sobrante']):"";
$total_rem = isset($_POST['total_rem'])?limpiarCadena($_POST['total_rem']):"";
$cajas = isset($_POST['cajas'])?limpiarCadena($_POST['cajas']):"";

switch ($_GET['opcion']) {
    case 'guardar':
        $partida = strtoupper($partida);
        $response = $etiqueta->insertar($barcode, $cve_art, $partida, $estilo, $talla, $color, $pza_x_caja, $pzas_caja, $sobrante, $total_rem, $cajas);
            echo $response;
        break;
    case 'editar':
        $response = $etiqueta->editar();
        echo $response ? "Etiqueta actualizada" : "Etiqueta no se pudo actualizar";
        break;
    case 'desactivar':
        $response = $etiqueta->desactivar($id);
        echo $response ? "Etiqueta Eliminada" : "Etiqueta no se pudo eliminar";
        break;
    case 'activar':
        $response = $etiqueta->activar($id);
        echo $response ? "Etiqueta activada" : "Etiqueta no se pudo activar";
        break;
    case 'mostrar':
        $response = $etiqueta->mostrar($barcode);
        echo json_encode($response);
        break;
    case 'mostrarfile':
        $datos_etiquetas = file_get_contents('C:\intraplayer\data\json\etiquetas.json');

        $json_etiquetas = json_decode($datos_etiquetas, true);
        $data = array();
        $pos = 0; $flag = false;

        for($i = 0; $i < count($json_etiquetas); $i++)
        {
            if($json_etiquetas[$i]['barcode'] == $barcode)
            {
                $pos = $i;
                $flag = true;
            }
        }

        if ($flag) {
            echo json_encode($json_etiquetas[$pos]);
        } else {
            echo "null";
        }
        break;
    case 'mostrarinventario':
        $response = $etiqueta->mostrarinventario($cve_art);
        echo json_encode($response);
        break;
    case 'ultimoid':
        $response = $etiqueta->ultimoid();
        echo json_encode($response);
        break;
    case 'listar':
        $response = $etiqueta->listar();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => '<b class="text-playerytees">'.$reg->id.'</b>',
                "1" => $reg->barcode,
                "2" => $reg->partida,
                "3" => $reg->cve_art,
                "4" => $reg->total_rem,
                "5" => $reg->pzas_caja,
                "6" => $reg->sobrante,
                "7" => $reg->cajas,
                "8" => date_format(date_create($reg->fecha), 'd/m/Y'),
                "9" => '<a href="#" title="Imprimir" onclick=\'imprimir("reimprimir.php?id='.$reg->id.'")\'><i class="fa fa-print mr-2"></i></a><a href="#" onclick="desactivar('.$reg->id.')"><i class="fa fa-trash-o"></i></a>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listarinventario':
        $response = $etiqueta->listarinventario($cve_art);
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->cve_esp,
                "1" => $reg->cve_ing,
            );
        }
        
        echo json_encode($data);
        break;
    case 'mostrarsobrante':
        $response = $etiqueta->mostrarsobrante($partida);
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->partida,
                "1" => $reg->estilo,
                "2" => $reg->color,
                "3" => $reg->talla,
                "4" => $reg->sobrante
            );
        }
        echo json_encode($data);
        break;
        case 'listareliminados':
            $response = $etiqueta->listareliminados();
            $data = array();

            while ($reg=$response->fetch_object()) {
                $data[] = array(
                    "0" => $reg->id,
                    "1" => $reg->barcode,
                    "2" => $reg->partida,
                    "3" => $reg->cve_art,
                    "4" => $reg->total_rem,
                    "5" => $reg->pzas_caja,
                    "6" => $reg->sobrante,
                    "7" => $reg->cajas,
                    "8" => date_format(date_create($reg->fecha), 'd/m/Y'),
                    "9" => ''
                );
            }
            $results = array(
                "sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($results);
        break;
    default:
        echo "No se encontro la opcion";
        break;
}
?>