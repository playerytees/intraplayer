<?php
require_once "../modelos/VentasMarca.php";
session_start();
$ventas = new VentasMarca();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$empresa = isset($_POST["empresa"])?htmlspecialchars(trim($_POST["empresa"])):"";
$start_date = isset($_POST["start_date"])?htmlspecialchars(trim($_POST["start_date"])):"";
$end_date = isset($_POST["end_date"])?htmlspecialchars(trim($_POST["end_date"])):"";
$almacen = isset($_POST["almacen"])?($_POST["almacen"]):"";
$marca = isset($_POST["marca"])?htmlspecialchars((trim($_POST["marca"]))):"";

switch ($_GET["opcion"]) {
    case 'listar':
        if (empty($almacen)) {
            $almacen = array("'-1'");
        }
        $almacenes = implode( ",",array_filter($almacen) );

        $result = $ventas->listar( $empresa, "'$start_date'", "'$end_date'", $almacenes, "$marca" );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => date_format(date_create(utf8_encode($reg->DocDate)), 'd/m/y'),
                '1' => $reg->CardCode,
                '2' => utf8_encode($reg->CardName),
                '3' => $reg->WhsCode,
                '4' => $reg->ItemCode,
                '5' => "<div title='".utf8_encode($reg->Dscription)."' style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width:250px;'>".utf8_encode($reg->Dscription)."</div>",
                '6' => number_format($reg->Quantity, 0, ".", ","),
                '7' => $reg->GroupName,
                '8' => $reg->DocNum,
                '9' => $reg->FirmName,
                '10' => $reg->ItmsGrpNam,
                '11' => $reg->U_Color,
                '12' => $reg->U_Talla,
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);

        break;

    case 'listarAlmacen':
        $result = $ventas->listarAlmacen( $empresa );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => utf8_encode($reg->WhsCode),
                '1' => utf8_encode($reg->WhsName)
            );
        }

        echo json_encode($data);

        break;

    case 'listarMarca':
        $result = $ventas->listarMarca( $empresa );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => utf8_encode($reg->FirmCode),
                '1' => utf8_encode($reg->FirmName)
            );
        }

        echo json_encode($data);

        break;

    default:
        echo "No se encontro la opcion";
        break;
}