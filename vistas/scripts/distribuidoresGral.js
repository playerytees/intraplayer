var tabla;

function init() {
    $(".loader").hide();
    listar( $("#fecha").val() );
}

function listar(fecha) {
    tabla = $("#tabladistribuidores").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "initComplete": function() {
            sumaColumnas();
        },
        "ajax": {
            url: "../ajax/distribuidoresGral.php?opcion=listar",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "asc"]]
    }).DataTable();
}

$("#btn-ver").click(function (e) { 
    e.preventDefault();
    listar($("#fecha").val());
});

function sumaColumnas() {
    $("#totalDailySales").text("$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 1 ).data().sum().toFixed(2) ) );
    $("#totalMonthlySales").text("$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 2 ).data().sum().toFixed(2) ) );
    $("#AlcanceTotal").text( ((tabla.column( 2 ).data().sum() * 100)/tabla.column( 3 ).data().sum()).toFixed()+"%" );
    $("#totalPago").text( "$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 7 ).data().sum().toFixed(2) ) );
}

init();