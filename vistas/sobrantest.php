<?php require 'header.php'; ?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Sobrantes</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Embarque</a></li>
                        <li class="active">Sobrantes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title float-left">Buscar Sobrantes</strong>
                    </div>
                    <div class="card-body">

                        <div class="row">
                            <div class="form-group col-md-3">
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                    <input class="form-control form-control-sm text-uppercase" id="busqueda" maxlength="15">
                                    <button type="button" id="btn_buscar" class="btn btn-primary btn-sm ml-1" onclick="buscar()">Buscar</button>
                                    <button type="button" class="btn btn-primary btn-sm ml-1" onclick="nuevo()">Nuevo</button>
                                </div>
                            </div>
                            <div class="col-md-9"></div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                                <button type="button" class="btn btn-primary btn-sm mb-1 d-none" id="btn_agregar" onclick="agregarinfo()">Agregar</button>
                                <button type="button" class="btn btn-primary btn-sm d-none" id="btn_imprimir" onclick="imprimir()">Imprimir</button>
                                <div id="resultadoBusqueda" style="height: 380px; overflow: auto;"></div>
                            </div>
                            <div class="col-12 col-md-12 col-sm-12 col-lg-6">
                                <div id="print_table">
                                    <table id="tabla_sobrante" width="600" height="380" style="font-family: Arial; text-align: center;" frame=box>
                                        <tr>
                                            <td rowspan="2" colspan="2" style="background-image: url(../public/images/logo_gris.png);background-repeat: no-repeat;background-position: center;background-size: contain;"><!-- <img src="../public/images/logo_gris.png" alt="Logo Playerytees" width="100"> --></td>
                                            <td colspan="3"> <img id="barcode_sobrante"> </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">SOBRANTES PRIMERAS</td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td>Partida</td>
                                            <td>Color</td>
                                            <td>Estilo</td>
                                            <td>Talla</td>
                                            <td>Piezas</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php require 'footer.php'; ?>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery.PrintArea.js"></script>
<script src="scripts/sobrantest.js"></script>