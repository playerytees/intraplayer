<?php
require_once "../modelos/Embarque.php";

$embarque = new Embarque();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";

$barcode = isset($_POST['barcode'])?limpiarCadena($_POST['barcode']):"";
$partida = isset($_POST['partida'])?limpiarCadena($_POST['partida']):"";
$estilo = isset($_POST['estilo'])?limpiarCadena($_POST['estilo']):"";
$color = isset($_POST['color'])?limpiarCadena($_POST['color']):"";
$talla = isset($_POST['talla'])?limpiarCadena($_POST['talla']):"";
$piezas = isset($_POST['piezas'])?limpiarCadena($_POST['piezas']):"";
$cajas = isset($_POST['cajas'])?limpiarCadena($_POST['cajas']):"";
$maxcajas = isset($_POST['maxcajas'])?limpiarCadena($_POST['maxcajas']):"";
$partidalimpia = isset($_POST['partidalimpia'])?limpiarCadena($_POST['partidalimpia']):"";
$codigo = isset($_POST['codigo'])?limpiarCadena($_POST['codigo']):"";

switch ($_GET['opcion']) {
    case 'guardarpartidas':
        $datosTabla = json_decode($_POST['tablapartidas'], true);
 
        foreach($datosTabla as $key => $value)
        {
            $piezas = $value['Piezas'] / $value['Cajas'];
            $response = $embarque->insertartemp($value['barcode'], $value['Partida'], $value['Estilo'], $value['Color'], $value['Talla'], $piezas, $value['Cajas'], $value['partidalimpia'], $value['Codigo']);
        }
        echo $response;
        break;
    case 'agregarcajas':
        $response = $embarque->consultarexistencias($id);
        if($cajas > $maxcajas){
            echo "No se puede exceder el numero de cajas";
        } else if ($response['cajas'] == 0) {
            echo "No puede agregar mas cajas";
        } else {
            $response = $embarque->insertartemporary($barcode, $partida, $estilo, $color, $talla, $piezas, $cajas, $partidalimpia, $codigo);
            $embarque->actualizartemp($id, $cajas);
            echo $response ? "Agregado correctamente" : "Las cajas no se pudieron agregar";
        }
        break;
    case 'quitarcajas':
        /*if ($cajas > $maxcajas) {
            echo "No se puede exceder el numero de cajas";
        } else { */
            $response = $embarque->insertartemp($barcode, $partida, $estilo, $color, $talla, $piezas, $cajas, $partidalimpia, $codigo);
            $embarque->actualizartemporary($id, $cajas);
            echo $response ? "Las cajas se retiraron correctamente" : "Las cajas no se pudieron retirar";
       // }
        break;
    case 'listarpartidas':
        $response = $embarque->listartemp();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => "<i style='cursor: pointer; color: #28a745; text-align: center;' class='fa fa-plus-circle' onclick=\"javascript:agregarcajas('$reg->id','$reg->barcode','$reg->partida','$reg->estilo','$reg->color','$reg->talla','$reg->pza_x_caja','$reg->cajas','$reg->partidalimpia','$reg->codigo')\"></i>",
                "1" => $reg->partida,
                "2" => $reg->estilo,
                "3" => $reg->color,
                "4" => $reg->talla,
                "5" => $reg->piezas,
                "6" => $reg->cajas,
                "7" => '<a href="#" onclick="desactivar('.$reg->id.')"><i class="fa fa-trash-o"></i></a>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listartemporary':
        $response = $embarque->listartemporary();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => "<i style='cursor: pointer; color: #dc3545;' class='fa fa-minus-circle' onclick=\"quitarcajas('$reg->id','$reg->barcode','$reg->partida','$reg->estilo','$reg->color','$reg->talla','$reg->pza_x_caja','$reg->cajas','$reg->partidalimpia','$reg->codigo')\"></i>",
                "1" => $reg->partida,
                "2" => $reg->estilo,
                "3" => $reg->color,
                "4" => $reg->talla,
                "5" => $reg->piezas,
                "6" => $reg->cajas
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'desactivar':
        $response = $embarque->desactivartemp($id);
        echo $response ? "Partida Eliminada" : "Partida no se pudo eliminar";
        break;
    default:
        echo "No se encontro la opcion";
        break;
}
?>