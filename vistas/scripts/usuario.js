var tabla;

function init(){
    mostrarform(false);
    listar();
 
    $("#formulario").on("submit",function(e)
    {
        guardaryeditar(e);
    });

    //Mostramos los permisos
    $.post("../ajax/usuario.php?opcion=permisos&id=",function(r){
            $("#permisos").html(r);
    });

    $(".loader").hide();
}

function limpiar()
{
    $("#id_usuario").val("");
    $("#nombreUsuario").val("");
    $("#AccessUser").val("");
    $("#AccessPass").val("");
    $("#emailRegistro").val("");
    $( "input[name$='permiso[]']" ).removeAttr("checked");
}

function mostrarform(flag)
{
    limpiar();
    if (flag)
    {
        $("#listadoregistros").hide();
        $("#formularioregistros").show();
        $("#btnGuardar").prop("disabled",false);
        $("#btnagregar").hide();
    }
    else
    {
        $("#AccessPass").attr("required", "true");
        $("#listadoregistros").show();
        $("#formularioregistros").hide();
        $("#btnagregar").show();
    }
}

function cancelarform()
{
    limpiar();
    mostrarform(false);
}

function listar()
{
    tabla=$('#tbllistado').dataTable(
    {
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Blfrtip',
        buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
                ],
        "ajax":
                {
                    url: '../ajax/usuario.php?opcion=listar',
                    type : "get",
                    dataType : "json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[ 0, "desc" ]]
    }).DataTable();
}
 
function guardaryeditar(e)
{
    e.preventDefault();
    $("#btnGuardar").prop("disabled",true);
    var formData = new FormData($("#formulario")[0]);
 
    $.ajax({
        url: "../ajax/usuario.php?opcion=guardaryeditar",
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
 
        success: function(datos)
        {                    
            console.log(datos);
            if(datos == 1) {
                $.alert({type: 'green',title: 'Correcto',content: 'Usuario guardado correctamente'});
            } else {
                $.alert({type: 'orange',title: 'Advertencia',content: 'No se pudieron guardar todos los datos del usuario'});
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
 
    });
    limpiar();
}
 
function mostrar(idusuario)
{
    $.post("../ajax/usuario.php?opcion=mostrar",{id_usuario : idusuario}, function(data, status)
    {
        data = JSON.parse(data);
        mostrarform(true);

        $("#id_usuario").val(data.id_usuario);
        $("#nombreUsuario").val(data.nombreUsuario);
        $("#AccessUser").val(data.AccessUser);
        //$("#AccessPass").val(data.AccessPass);
        $("#AccessPass").removeAttr("required");
        $("#emailRegistro").val(data.emailRegistro);
    });
    $.post("../ajax/usuario.php?opcion=permisos&id="+idusuario,function(r){
        r = JSON.parse(r);
        let general = "";
        let sucursales = "";
        let empresas = "";
        let reportes = "";
        let ajustes = "";
        for (let i = 0; i < r.length; i++) {
            if (r[i].descripcion=="General") {
                general+=`<li><input type="checkbox" ${r[i].checked} name="permiso[]" value="${r[i].id}" > ${r[i].nombre} </li>` ;
            }
            if (r[i].descripcion=="Sucursales") {
                sucursales+=`<li><input type="checkbox" ${r[i].checked} name="permiso[]" value="${r[i].id}" /> ${r[i].nombre} </li>` ;
            }
            if (r[i].descripcion=="Empresa") {
                empresas+=`<li><input type="checkbox" ${r[i].checked} name=permiso[] value=${r[i].id} /> ${r[i].nombre} </li>` ;
            }
            if (r[i].descripcion=="Reportes") {
                reportes+=`<li><input type="checkbox" ${r[i].checked} name="permiso[]" value="${r[i].id}" /> ${r[i].nombre} </li>` ;
            }
            if (r[i].descripcion=="Ajustes") {
                ajustes+=`<li><input type="checkbox" ${r[i].checked} name="permiso[]" value="${r[i].id}" /> ${r[i].nombre} </li>` ;
            }
        }
        $("#permisos_general").html(general);
        $("#permisos_sucursales").html(sucursales);
        $("#permisos_empresas").html(empresas);
        $("#permisos_reportes").html(reportes);
        $("#permisos_ajustes").html(ajustes);
    });
}

function desactivar(idusuario)
{
    confirm("¿Está Seguro de desactivar el usuario?", function(result){
        if(result)
        {
            $.post("../ajax/usuario.php?opcion=desactivar", {id_usuario : idusuario}, function(e){
                alert(e);
                tabla.ajax.reload();
            }); 
        }
    })
}

function activar(idusuario)
{
    confirm("¿Está Seguro de activar el Usuario?", function(result){
        if(result)
        {
            $.post("../ajax/usuario.php?opcion=activar", {id_usuario : idusuario}, function(e){
                alert(e);
                tabla.ajax.reload();
            }); 
        }
    })
}
 
init();