<table frame=box style="width: 100%; height: 100%; font-family: Arial; border-collapse: separate; border-spacing: 0;">
            <tr>
                <td colspan="2"><img src="../public/images/Playerytees_black.png" width="100" alt="" style="margin-left: 10px;"></td>
                <td colspan="2"></td>
                <td colspan="2" valign="TOP"><img class="code"/></td>

                <td colspan="2" style="border-left: 1px solid;"><img src="../public/images/Playerytees_black.png" width="100" alt="" style="margin-left: 10px;"></td>
                <td colspan="2"></td>
                <td colspan="2" valign="TOP"><img class="code"/></td>
            </tr>
            <tr>
                <td></td>
                <td style="border-top: 1px solid;"></td>
                <td style="border-top: 1px solid;"><span>QTY:</span></td>
                <td style="border-top: 1px solid;"><span>35</span><img class="barcode-qty" style="margin-left: 10px;"/></td>
                <td style="border-top: 1px solid;"></td>
                <td></td>

                <td style="border-left: 1px solid;"></td>
                <td style="border-top: 1px solid;"></td>
                <td style="border-top: 1px solid;"><span>QTY:</span></td>
                <td style="border-top: 1px solid;"><span>35<img class="barcode-qty" style="margin-left: 10px;"></span></td>
                <td style="border-top: 1px solid;"></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td><span>ORDER:</span></td>
                <td><span>FAB-125</span></td>
                <td></td>
                <td></td>

                <td style="border-left: 1px solid;"></td>
                <td></td>
                <td><span>ORDER:</span></td>
                <td><span>FAB-125</span> </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td style="border-bottom: 1px solid;"></td>
                <td style="border-bottom: 1px solid;"><span>DATE:</span></td>
                <td style="border-bottom: 1px solid;"><span>09/06/2018</span></td>
                <td style="border-bottom: 1px solid;"></td>
                <td></td>

                <td style="border-left: 1px solid;"></td>
                <td style="border-bottom: 1px solid;"></td>
                <td style="border-bottom: 1px solid;"><span>DATE:</span></td>
                <td style="border-bottom: 1px solid;"><span>09/06/2018</span></td>
                <td style="border-bottom: 1px solid;"></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="6" style="text-align: center;"> <svg class="barcode"></svg> </td>

                <td colspan="6" style="border-left: 1px solid; text-align: center;"> <svg class="barcode"></svg> </td>
            </tr>
            <tr style="text-align: center;">
                <td colspan="2">STYLE</td>
                <td colspan="2">COLOR</td>
                <td colspan="2">SIZE</td>

                <td colspan="2" style="border-left: 1px solid;">STYLE</td>
                <td colspan="2">COLOR</td>
                <td colspan="2">SIZE</td>
            </tr>
            <tr style="font-weight: bold; font-size: 1.5em; width: 50%; text-align: center;">
                <td colspan="2">6000C</td>
                <td colspan="2">COASTAL BLUE</td>
                <td colspan="2">XXXL</td>

                <td colspan="2" style="border-left: 1px solid;">6000C</td>
                <td colspan="2">COASTAL BLUE</td>
                <td colspan="2">XXXL</td>
            </tr>
        </table>

<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script type="text/javascript">
function init()
{
    JsBarcode(".code", "lbl1234", { width: 1, height: 15, displayValue: false, margin: 0 });

    JsBarcode(".barcode", "6000C-CB-3XL", { width: 1.2, height: 40, textPosition: "bottom", fontSize: 12, font: "Arial" });

    JsBarcode(".barcode-qty", "35", {width: 1, height: 20, displayValue: false, margin: 0});

    document.title = "Nueva Etiqueta";
}

init();
</script>
<style>

</style>