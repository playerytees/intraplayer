<?php
require_once "../modelos/ProximasLlegadas.php";

$proximasllegadas = new ProximasLlegadas();

$idproximasllegadas = isset($_POST['idproximasllegadas'])?limpiarCadena($_POST['idproximasllegadas']):"";
$partida = isset($_POST['partida'])?limpiarCadena($_POST['partida']):"";
$codigo = isset($_POST['codigo'])?limpiarCadena($_POST['codigo']):"";
$estilo = isset($_POST['estilo'])?($_POST['estilo']):"";
$color = isset($_POST['color'])?($_POST['color']):"";
$talla = isset($_POST['talla'])?limpiarCadena($_POST['talla']):"";
$cantidad = isset($_POST['cantidad'])?limpiarCadena($_POST['cantidad']):"";
$fechaentrega = isset($_POST['fechaentrega'])?limpiarCadena($_POST['fechaentrega']):"";
$tblExcel = isset($_POST['tblExcel'])?limpiarCadena($_POST['tblExcel']):"";
$min = isset($_POST['min'])?limpiarCadena($_POST['min']):"";
$max = isset($_POST['max'])?limpiarCadena($_POST['max']):"";

switch ($_GET['opcion']) {
    case 'guardartable':
        $tblJsonDecode = json_decode($_POST['tblExcel'], true);
        foreach ($tblJsonDecode as $key => $value) {
            $clave = explode('-', $value['Codigo']);
            $estilo = isset($clave[0])?$clave[0]:"";
            $color = isset($clave[1])?$clave[1]:"";
            $talla = isset($clave[2])?$clave[2]:"";
            $response = $proximasllegadas->guardartable($value['Partida'], $value['Codigo'], $estilo, $color, $talla, $value['Cantidad'], $value['Fecha']);
        }
        echo $response;
        break;
    case 'agregaryeditar':
        $clave = explode('-', $codigo);
            $estilo = isset($clave[0])?$clave[0]:"";
            $color = isset($clave[1])?$clave[1]:"";
            $talla = isset($clave[2])?$clave[2]:"";
        if (empty($idproximasllegadas)) {
            $response=$proximasllegadas->agregar($partida,$codigo,$estilo,$color,$talla,$cantidad,$fechaentrega);
            echo $response;
        } else {
            $response=$proximasllegadas->editar($idproximasllegadas,$partida,$codigo,$estilo,$color,$talla,$cantidad,$fechaentrega);
            echo $response;
        }
        break;
    case 'desactivar':
        $response = $proximasllegadas->desactivar($idproximasllegadas);
        echo $response;
        break;
    case 'activar':
        $response = $proximasllegadas->activar($idproximasllegadas);
        echo $response;
        break;
    case 'mostrar':
        $response = $proximasllegadas->mostrar($idproximasllegadas);
        echo json_encode($response);
        break;
    case 'listar':
        $response=$proximasllegadas->listar();
        $data=array();

        while ($reg=$response->fetch_object()) {
            $data[]=array(
                "0" => $reg->idproximasllegadas,
                "1" => $reg->partida,
                "2" => $reg->codigo,
                "3" => '<div class="text-right">'.number_format($reg->cantidad, 0, ".", ",").'</div>',
                "4" => $reg->fechaentrega,
                "5" => '<div class="text-center"><a href="#" onclick="mostrar('.$reg->idproximasllegadas.')" ><i class="fa fa-edit"></i></a></div>',
                "DT_RowId" => 'row_'.$reg->idproximasllegadas
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'estiloColor':
        if (empty($estilo)) {
            $estilos = '';
        } else  {
            $estilos = implode(",", array_filter($estilo));
        }
        if (empty($color)) {
            $colores = '';
        } else {
            $colores = implode(",", array_filter($color));
        }

        $getPartidaEmbarque = $proximasllegadas->getPartidaEmbarque()->fetch_all(MYSQLI_ASSOC);

        for ($i=0; $i < count($getPartidaEmbarque); $i++) { 
            $partida_array = explode('-', $getPartidaEmbarque[$i]['partida']);
            $partida_segunda_pos = (isset($partida_array[1]))?$partida_array[1]:"";
            $partida_ultima_pos = (isset($partida_array[2]))?'-'.$partida_array[2]:"";
            $getPartidaEmbarque[$i]['partida'] = $partida_segunda_pos.$partida_ultima_pos;
        }

        if ($max == "" || $min == "") {
            $response=$proximasllegadas->filtrarEstiloColor($estilos,$colores);
            $groupEstiloColor=$proximasllegadas->groupEstiloColor($estilos,$colores);
        } else {
            $response=$proximasllegadas->rangoFecha($estilos,$colores,"'$min'","'$max'");
            $groupEstiloColor=$proximasllegadas->groupRangoFecha($estilos,$colores,"'$min'","'$max'");
        }
        $data=array();

        $arregloTallas = array("0","2","3","3M","4","5","6","6M","7","8","9","9M","10","11","12","12M","13","14","15","16","17","18","19","21","23","25","27","28","30","32","34","36","38","40","42","44","46","48","50","XS","S","M","L","XL","2XL","3XL","4XL","5XL","6XL","UNI");

        $Fechas[0] = "FECHA";
        $Partidas[0] = "PARTIDA";
        $Estilos[0] = "ESTILO";
        $Colores[0] = "COLOR";
        $Tallas[0] = "TALLA";
        $numRows = 0;
        $dataBD[][] = "";

        while ($reg=$groupEstiloColor->fetch_object()) {
                array_push($Fechas, $reg->fechaentrega);
                array_push($Partidas, $reg->partida);
                array_push($Estilos, $reg->estilo);
                array_push($Colores, $reg->color);
        }

        while ($reg=$response->fetch_object()) {
                $dataBD[$numRows][0] = $reg->fechaentrega;
                $dataBD[$numRows][1] = $reg->partida;
                $dataBD[$numRows][2] = $reg->estilo;
                $dataBD[$numRows][3] = $reg->color;
                $dataBD[$numRows][4] = $reg->cantidad;
                $dataBD[$numRows][5] = $reg->talla;

                if (!in_array($reg->talla, $Tallas)) {
                    array_push($Tallas, $reg->talla);
                }

                $numRows++;
        }

        $matriz[0][0] = "Fecha";
        $matriz[0][1] = "Partida";
        $matriz[0][2] = "Estilo";
        $matriz[0][3] = "Color";

        $colMat = 4;

        // Llenar las Tallas si existen
        for ($i=0; $i < count($arregloTallas); $i++) { 
            if (in_array($arregloTallas[$i], $Tallas)) {
                $matriz[0][$colMat] = $arregloTallas[$i];
                $colMat++;
            }
        }

        $matriz[0][$colMat] = "Total";
        $colMat++;

        for ($j=1; $j < count($Estilos); $j++) { 
            $matriz[$j][0] = $Fechas[$j];
            $matriz[$j][1] = $Partidas[$j];
            $matriz[$j][2] = $Estilos[$j];
            $matriz[$j][3] = $Colores[$j];
        }

        for ($rows=1; $rows < count($Estilos); $rows++) { 
            for ($cols=4; $cols < $colMat; $cols++) { 
                $matriz[$rows][$cols] = 0;
            }
        }

        // Recorrer para colocar el num de pzas
        for ($pbd=0; $pbd < $numRows; $pbd++) { // filas BD
            for ($rel=1; $rel < count($Estilos); $rel++) { // filas matriz
                if ($dataBD[$pbd][0] == $Fechas[$rel] AND $dataBD[$pbd][1] == $Partidas[$rel] AND $dataBD[$pbd][2] == $Estilos[$rel] AND $dataBD[$pbd][3] == $Colores[$rel]) {
                    for ($cols=4; $cols < $colMat; $cols++) { // columnas matriz
                        if ($dataBD[$pbd][5] == $matriz[0][$cols]) { //Tallas
                            $matriz[$rel][$cols] = number_format($dataBD[$pbd][4], 0, ".", ",");
                            $matriz[$rel][$colMat-1] += $dataBD[$pbd][4];
                        }
                    }
                }
            }
        }

        $totalCol = array('','','','TOTAL:');
        for ($k=4; $k < $colMat; $k++) { 
            $sum = 0;
            foreach ($matriz as $key => $value) {
                if($key != 0) {
                    $sum += (!is_int($value[$k]))?(intval(str_replace(',', '', $value[$k]))):$value[$k];
                }
            }
            array_push($totalCol, $sum);
        }

        array_push($matriz, $totalCol);

        echo "<thead style='background-color: rgba(6, 78, 125, 0.88); color: #fff;'>
        <tr>";
            for ($fm=0; $fm < 1; $fm++) { 
                for ($rm=0; $rm < $colMat; $rm++) { 
                    echo "<th>".$matriz[$fm][$rm]."</th>";
                }
            }
        echo "</tr>
            </thead>
            <tbody style='text-align: right;'>";
                for ($fm=1; $fm < count($Estilos); $fm++) { 
                    echo "<tr>";
                        for ($rm=0; $rm < $colMat; $rm++) { 
                            echo "<td>".$matriz[$fm][$rm]."</td>";
                        }
                    echo "</tr>";
                }
        echo "</tbody>";
        echo "<tfoot style='text-align: right;font-weight:bold;'>";
            for ($fm=count($Estilos); $fm < count($Estilos)+1; $fm++) { 
                echo "<tr>";
                    for ($rm=0; $rm < $colMat; $rm++) { 
                        echo "<td>".$matriz[$fm][$rm]."</td>";
                    }
                echo "</tr>";
            }
        echo "</tfoot>";
        break;
    default:
        # code...
        break;
}
