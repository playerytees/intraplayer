var tabla;

function init()
{
    listar();
    mostrar();
    mostrarform(false);
    $(".loader").hide();

    $("#file-inventario").change(function () {
        $("#btn_guardar_inve").prop("disabled", this.files.length == 0);
    });
}

function limpiar()
{
    $("#file-inventario").val("");
    $("#contenedor_inve").html("");
}

function mostrarform(flag)
{
    limpiar();
    if (flag) {
        $("#listado_resurtido").show();
        $("#div_inventario").hide();
        $("#temp_inventario").hide();
    } else {
        $("#listado_resurtido").hide();
        $("#div_inventario").show();
        $("#temp_inventario").show();
    }
}

function cancelarform()
{
    limpiar();
    mostrarform(false);
}

function listar()
{
    tabla = $("#tabla_inventario").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Bfrtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/inventario.php?opcion=listar",
            type: "POST",
            data: {id_sucursal: $("#idsucursal").val()},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function guardar() {
    var id_sucursal = $("#idsucursal").val();
    var tablaToJSON = $("#tabla_inventario_temp").tableToJSON();
    var tablaObt = JSON.stringify(tablaToJSON);
    $.ajax({
        type: "POST",
        url: "../ajax/inventario.php?opcion=guardar",
        data: { id_sucursal: id_sucursal, tabla_inve: tablaObt },
        beforeSend: function () {
            $(".loader").show();
        },
        success: function (response) {
            console.log(response);
            if (response == 1) {
                $.alert({ type: 'green', title: 'Correcto', content: 'Existencias guardado correctamente' });
                tabla.ajax.reload();
                mostrarform(true);
                $(".loader").hide();
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'Existencias no se pudo guardar <br> Intente Nuevamente' });
                $(".loader").hide();
            }
        }
    });
    limpiar();
}

function guardarresurtido()
{
    var arreglo = [];
    tabla.data().each(function(d){
        arreglo.push({"manufactura":d[0],"personalizado":d[1], "linea":d[2], "minimo": d[3], "existencias": d[4] , "faltante": d[5], "surtir": d[6], "num_piezas": d[7], "piezas_surtir": d[8], "exis_cedis":d[9], "dif_cedis":d[10]});
     });
    
    var id_sucursal = $("#idsucursal").val();
    var tablaObt = JSON.stringify(arreglo);
    $.ajax({
        type: "POST",
        url: "../ajax/resurtido.php?opcion=guardar",
        data: {id_sucursal: id_sucursal, tabla_resurtido: tablaObt},
        beforeSend: function(){
            $(".loader").show();
        },
        success: function (response) {
            console.log(response);
            if (response == 1) {
                $.alert({type: 'green',title: 'Correcto',content: 'Resurtido calculado correctamente'});
                $(".loader").hide();
                window.location.href = "resurtido.php?sucursal=" + id_sucursal;
            } else {
                $.alert({type: 'red',title: 'Error',content: 'Resurtido no se pudo guardar <br> Intente Nuevamente'});
                $(".loader").hide();
            }
        }
    });
}

function mostrar()
{
    $.post("../ajax/sucursales.php?opcion=mostrar", { id: $("#idsucursal").val() },
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            $("#spansucursal").html(" / " + data.descr)
        }
    );
}

$("#file-inventario").change(function (e) {
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 100000) {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no debe superar 1MB' });
        this.value = '';
        this.files[0].name = '';
    } else if (fileName.split('.').pop() != "xlsx") {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no tiene la extensión adecuada' });
        this.value = '';
        this.files[0].name = '';
    } else {
        var reader = new FileReader();

        reader.readAsArrayBuffer(e.target.files[0]);

        reader.onload = function (e) {
            var data = new Uint8Array(reader.result);
            var wb = XLSX.read(data, { type: 'array' });
            if (wb.SheetNames[0] == 'existencias') {
                var htmlstr = XLSX.write(wb, { sheet: 'existencias', type: 'binary', bookType: 'html' });
                $("#contenedor_inve")[0].innerHTML += htmlstr;

                $("#contenedor_inve > table").attr({ 'id': 'tabla_inventario_temp' });
                $("#tabla_inventario_temp").addClass('table table-bordered table-sm');
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'El archivo no contiene el nombre correcto' });
                $("#file-catalogo").val('');
            }
        }
    }
});

init();