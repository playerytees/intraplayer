<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['resurtido']==1) {
?>
    <?php $idsucursal = isset($_GET['sucursal'])?$_GET['sucursal']:""; ?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title text-playerytees">
                    <h1 style="float: left;">Catalogo </h1> <span id ="spansucursal" style="float: right;padding: 15px 0; margin-left: 5px;"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Resurtido</a></li>
                        <li><a href="sucursales.php">Sucursales</a></li>
                        <li class="active">Catalogo</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn" id="listadoCatalogo">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title d-sm-inline text-playerytees">Mostrar Catalogo</strong>
                            <button id="btnagregar" class="btn btn-playerytees btn-sm float-sm-right" type="button" onclick="mostrarform(true)">Nuevo Catalogo</button>
                        </div> <!-- .card-header -->
                        <div class="card-body">
                        <input type="hidden" name="idsucursal" id="idsucursal" value="<?php echo $idsucursal; ?>">
                            <table id="tabla_catalogo" class="table table-bordered table-sm table-hover table-responsive-sm" style="width: 100%;">
                                <thead>
                                    <th>Clave manufactura</th>
                                    <th>Clave Personalizado</th>
                                    <th>Sugerido/Minimo</th>
                                    <th>Sucursal</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div> <!-- .card-body -->
                        <div class="card-footer">
                        <a href="sucursales.php" class="btn btn-danger">Regresar</a>
                    </div> <!-- .card-footer -->
                    </div> <!-- .card -->
                </div> <!-- .col-md-12 -->
            </div> <!-- .row -->
        </div> <!-- .animated .fadeIn -->
        <div class="row" id="agregar_catalogo">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <b>Agregar Catalogo </b>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <form name="form_catalogo" id="form_catalogo" method="POST" class="form-horizontal">
                            <div class="row form-group">
                                <div class="col col-md-3">
                                    <label for="file-catalogo" class=" form-control-label">Seleccionar Catalogo</label>
                                </div>
                                <div class="col-12 col-md-9">
                                    <input type="file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" id="file-catalogo" name="file-catalogo" class="form-control-file">
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row" id="temp_catalogo">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Mostrar Catalogo</strong>
                    </div>
                    <div class="card-body">
                        <div id="contenedor_catalogo" style="overflow: auto;
    height: 300px;"></div>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-primary" id="btn_guardar_catalog" onclick="guardar()" disabled="disabled">Guardar</button>
                        <button type="button" class="btn btn-danger" onclick="cancelarform()">Cancelar</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content -->
<?php   
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="../public/assets/js/xlsx.full.min.js"></script>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/catalogo.js"></script>
<?php
}
ob_end_flush();
?>