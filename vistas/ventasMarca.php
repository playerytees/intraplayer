<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['ventasMarca']==1) {
?>
<style>
#tblVentasMarca_filter{
    display: inline-block;
    float: right;
}
</style>
    <div class="content mt-2">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Ventas por Marca</strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <buttton type="butoon" class="btn btn-outline-success btn-sm active" id="btnDPT">Punto Textil</buttton>
                        <buttton type="butoon" class="btn btn-outline-primary btn-sm" id="btnGP">Global playerytees</buttton>
                    </div>
                </div>
                <div class="row">
                <form action="" class="w-100" method="POST" style="display:flex; align-items:center;">
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="input_start_date" class="control-label mb-1 text-playerytees">Fecha Inicio:</label>
                            <input type="hidden" name="input_empresa" id="input_empresa" value="DPT_BD">
                            <input type="date" name="input_start_date" id="input_start_date" class="form-control" required />
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="input_end_date" class="control-label mb-1 text-playerytees">Fecha Fin:</label>
                            <input type="date" name="input_end_date" id="input_end_date" class="form-control" required />
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="select_marca" class="control-label mb-1 text-playerytees">Marca:</label>
                            <select name="select_marca" id="select_marca" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar marca...">
                            </select>
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-2">
                        <div class="form-group">
                            <label for="select_sucursal" class="control-label mb-1 text-playerytees">Almacen:</label>
                            <select name="select_sucursal[]" id="select_sucursal" class="form-control form-control-chosen d-none" data-placeholder="Todos..." multiple="multiple" required></select>
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-12 col-xl-1">
                        <button type="submit" id="btnBuscar" class="btn btn-playerytees mb-2">BUSCAR &nbsp;<i class="fa fa-search"></i></button>
                    </div>
                </form>
                </div> <!-- .row -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body" id="contentTblVentas">
                                <table class='table table-sm table-striped table-bordered table-responsive-md' id="tblVentasMarca" style="width: 100%;">
                                    <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Cliente</th>
                                            <th>Nombre Cliente</th>
                                            <th>Almacen</th>
                                            <th>Articulo</th>
                                            <th>Descripcion</th>
                                            <th>Cantidad</th>
                                            <th>Grupo</th>
                                            <th>Documento</th>
                                            <th>Marca</th>
                                            <th>Estilo</th>
                                            <th>Color</th>
                                            <th>Talla</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 14px;"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- .row -->

            </div> <!-- card-body -->
        </div> <!-- .card -->
    </div> <!-- .content .mt-3 -->

<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="scripts/ventasMarca.js"></script>
<?php
}
ob_end_flush();
?>