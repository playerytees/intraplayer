<?php

require "../config/Conexion.php";

class Ventas
{
    public function __construct()
    {

    }

    public function insertar($meta, $mes, $anio, $sucursal)
    {
        $sql = "INSERT INTO presupuesto (meta, mes, anio, sucursal, modified, created) VALUES ('$meta','$mes','$anio','$sucursal', now(), now())";

        return ejecutarConsulta($sql);
    }

    public function listar()
    {
        $sql = "SELECT meta, mes, anio, sucursal FROM presupuesto";

        return ejecutarConsulta($sql);
    }

    public function consultarSiExiste($mes,$anio,$sucursal)
    {
        $sql = "SELECT * FROM presupuesto WHERE mes = '$mes' AND anio = '$anio' AND sucursal = '$sucursal'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function mesActual($anio, $mes)
    {
        $sql = "SELECT sucursal, meta_actual meta FROM presupuesto WHERE anio=".$anio." AND mes=".$mes."";

        return ejecutarConsulta($sql);
    }

    public function mesAnterior($anio, $mes)
    {
        $sql = "SELECT sucursal, meta_anterior meta FROM presupuesto WHERE anio=".$anio." AND mes=".$mes."";

        return ejecutarConsulta($sql);
    }

    public function reglaD3($meta, $ventasAct)
    {
        $meta = floatval($meta);
        $ventasAct = floatval($ventasAct);
        if($ventasAct > 0 && $meta > 0)
        {
            $resultado = (($ventasAct*100)/$meta);
            return round($resultado, 2);
        } else {
            return 0;
        }
    }

    function bussiness_days($begin_date, $end_date, $type = 'array') {
        $date_1 = date_create($begin_date);
        $date_2 = date_create($end_date);
        if ($date_1 > $date_2) return FALSE;
        $bussiness_days = array();
        while ($date_1 <= $date_2) {
            $day_week = $date_1->format('w');
            if ($day_week > 0 && $day_week < 6) {
                $bussiness_days[$date_1->format('Y-m')][] = $date_1->format('d');
            }
            date_add($date_1, date_interval_create_from_date_string('1 day'));
        }
        if (strtolower($type) === 'sum') {
            array_map(function($k) use(&$bussiness_days) {
                $bussiness_days[$k] = count($bussiness_days[$k]);
            }, array_keys($bussiness_days));
        }
        return $bussiness_days;
    }
}
