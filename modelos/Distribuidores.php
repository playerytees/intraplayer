<?php
    require "../config/ConexionHana.php";

    class Distribuidores extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listar($empresa)
        {
            try {
                $sql = 'SELECT
                    SUBSTRING(TO_NVARCHAR("AliasName"), 1, 100) "Nombre",
                    SUM("CreditLine") "LimiteCredito",
                    SUM("Balance") "Saldo",
                    SUM("CreditLine") - SUM("Balance") "Disponible"
                FROM
                    "'.$empresa.'"."OCRD"
                WHERE
                    "GroupCode" = \'102\'
                AND
                    "AliasName" IS NOT NULL
                GROUP BY TO_NVARCHAR("AliasName");';
            
            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventas($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUBSTRING(TO_NVARCHAR(T1."AliasName"), 1, 100) "Nombre",
                    SUM(T0."DocTotal") "Monto"
                FROM "'.$empresa.'"."OINV" T0
                INNER JOIN "'.$empresa.'"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE T0."CANCELED" = \'N\'
                AND T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                AND T1."GroupCode" = \'102\'
                GROUP BY TO_NVARCHAR(T1."AliasName");';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devoluciones($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUBSTRING(TO_NVARCHAR(T1."AliasName"), 1, 100) "Nombre",
                    SUM(T0."DocTotal") "Monto"
                FROM
                    "GP_BD"."ORIN" T0
                INNER JOIN "GP_BD"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE
                    T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	T0."CANCELED" = \'N\'
                AND T1."GroupCode" = \'102\'
                GROUP BY TO_NVARCHAR(T1."AliasName");';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventaDiariaTotal($fecha, $empresa)
        {
            try {
                $sql = 'SELECT
                    \'DIS102\' "Sucursal",
                    SUM(A."U_SO1_TOTALNETO") "Monto"
                FROM
                    "'.$empresa.'"."@SO1_01VENTA" A
                LEFT JOIN "'.$empresa.'"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN (\'S\', \'N\')
                AND C."GroupCode" = \'102\';';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventaMensualTotal($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                \'DIS102\' "Sucursal",
                SUM(A."U_SO1_TOTALNETO") "Monto"
            FROM
                "'.$empresa.'"."@SO1_01VENTA" A
            LEFT JOIN "'.$empresa.'"."OCRD" C
            ON (A."U_SO1_CLIENTE" = C."CardCode")
            WHERE
            	A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
            AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
            AND A."U_SO1_SUCURSAL" IN (\'CE1\')
            AND A."U_SO1_STATUS" NOT IN (\'S\', \'N\')
            AND C."GroupCode" = \'102\';';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionDiariaTotal($fecha, $empresa)
        {
            try {
                $sql = 'SELECT
                \'DIS102\' "Sucursal",
                SUM(A."U_SO1_TOTALNETO") "Monto"
            FROM
                "'.$empresa.'"."@SO1_01DEVOLUCION" A
            LEFT JOIN "'.$empresa.'"."OCRD" C
            ON (A."U_SO1_CLIENTE" = C."CardCode")
            WHERE
                A."U_SO1_FECHA" = '.$date.'
            AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
            AND C."GroupCode" = \'102\';';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionMensualTotal($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                \'DIS102\' "Sucursal",
                SUM(A."U_SO1_TOTALNETO") "Monto"
            FROM
                "'.$empresa.'"."@SO1_01DEVOLUCION" A
            LEFT JOIN "'.$empresa.'"."OCRD" C
            ON (A."U_SO1_CLIENTE" = C."CardCode")
            WHERE
                A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
            AND	A."U_SO1_SUCURSAL" IN (\'CE1\')
            AND C."GroupCode" = \'102\';';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function pagos($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                SUBSTRING(TO_NVARCHAR(T1."AliasName"), 1, 100) "Nombre",
                SUM(T0."DocTotal") "Pago"
            FROM
                "'.$empresa.'"."ORCT" T0
            INNER JOIN
                "'.$empresa.'"."OCRD" T1 ON T0."CardCode" = T1."CardCode"
            WHERE
                T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
            AND
                T1."GroupCode" = \'102\'
            GROUP BY TO_NVARCHAR(T1."AliasName");';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function saldoVencido($date, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUBSTRING(TO_NVARCHAR(T1."AliasName"), 1, 100) "Nombre",
                    IFNULL(SUM(T0."DocTotal"), 0) - IFNULL(SUM(T0."PaidToDate"), 0) "saldoVencido"
                FROM "'.$empresa.'"."OINV" T0
                INNER JOIN "'.$empresa.'"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE T0."CANCELED" = \'N\'
                AND T1."GroupCode" = \'102\'
                AND T1."validFor" = \'Y\'
                AND T0."DocStatus" = \'O\'
                AND T0."DocDueDate" < '.$date.'
                AND "AliasName" IS NOT NULL
                GROUP BY TO_NVARCHAR(T1."AliasName");';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarCancelaciones($start_date, $end_date, $empresa)
        {
            try {
                $sql = '';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarPagos($start_date, $end_date, $empresa)
        {
            try {
                $sql = '';

            $stmt = $this->connect()->prepare($sql);

            $stmt->execute();

            return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
