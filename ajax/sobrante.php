<?php
require_once "../modelos/Sobrante.php";

$sobrante = new Sobrante();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$barcode = isset($_POST['barcode'])?limpiarCadena($_POST['barcode']):"";
$partida = isset($_POST['partida'])?limpiarCadena($_POST['partida']):"";

switch ($_GET['opcion']) {
    case 'guardar':
        $datosarreglo = json_decode($_POST['arreglotag'], true);

        $idsobrantenew = $sobrante->insertar($barcode);

        if (!$idsobrantenew) {
            echo "Imprimiendo...";
        } else {
            foreach ($datosarreglo as $key => $value) {
                $response = $sobrante->insertardetalle_sobrante($value['piezas'], $value['idetiqueta'], $idsobrantenew);
            }
        }

        echo $response ? "Guardado Correctamente" : "No se ha podido guardar";
        break;
    case 'listar':
        $response = $sobrante->listar();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->id,
                "1" => $reg->barcode,
                "2" => $reg->fecha,
                "3" => $reg->modified,
                "4" => $reg->created
                # id, barcode, fecha, modified, created
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listaretiqueta':
        $response = $sobrante->listaretiqueta();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->id,
                "1" => $reg->partida,
                "2" => $reg->estilo,
                "3" => $reg->color,
                "4" => $reg->talla,
                "5" => $reg->sobrante,
                "6" => '<input type="checkbox" name="checkboxid[]" value='.$reg->id.'>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'agregarinfo':
        $response = $sobrante->mostrarinfo($id);
        echo json_encode($response);
        break;
    case 'ultimoid':
        $response = $sobrante->ultimoid();
        echo json_encode($response);
        break;
    case 'buscar':
        $response = $sobrante->buscar($partida);
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => $reg->partida,
                "1" => $reg->estilo,
                "2" => $reg->color,
                "3" => $reg->talla,
                "4" => $reg->sobrante,
                "5" => '<input type="checkbox" name="checkboxid[]" value='.$reg->id.'>'
            );
        }

        echo json_encode($data);
        break;
    case 'mostrar':
        $response = $sobrante->mostrar($barcode);
        echo json_encode($response);
        break;
    case 'guardarsobrantes':
        $datosTabla = json_decode($_POST['tablasobrantes'], true);

        foreach ($datosTabla as $key => $value) {
            $response = $sobrante->actualizarsobrante($value['id'], 'P');
        }
        echo $response;
        break;
    case 'listarsobrantes':
        $response = $sobrante->listarsobrantes('P');
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => "<i style='cursor: pointer; color: #28a745; text-align: center;' class='fa fa-plus-circle' onclick=\"javascript:agregarsobrantes('$reg->id')\"></i>",
                "1" => $reg->partida,
                "2" => $reg->estilo,
                "3" => $reg->color,
                "4" => $reg->talla,
                "5" => $reg->piezas,
                "6" => ''
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listartemporary':
        $response = $sobrante->listarsobrantes('T');
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => "<i style='cursor: pointer; color: #dc3545;' class='fa fa-minus-circle' onclick=\"javascript:quitarsobrantes('$reg->id')\"></i>",
                "1" => $reg->partida,
                "2" => $reg->estilo,
                "3" => $reg->color,
                "4" => $reg->talla,
                "5" => $reg->piezas,
                "6" => ''
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'agregarsobrante':
        $response = $sobrante->actualizarsobrante($id, 'T');
        echo $response ? "Agregado Correctamente" : "No se ha podido agregar";
        break;
    case 'quitarsobrante':
        $response = $sobrante->actualizarsobrante($id, 'P');
        echo $response ? "Quitado Correctamente" : "No se ha podido quitar";
        break;
    default:
        echo "No se encontro la opcion";
        break;
}
?>