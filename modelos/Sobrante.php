<?php
    require "../config/Conexion.php";
    date_default_timezone_set("America/Mexico_City");

    class Sobrante
    {
        public function __construct()
        {

        }

        public function insertar($barcode)
        {
            $sql = "INSERT INTO sobrante VALUES (NULL, '$barcode', 'NULL', now(), 'A', now(), now())";

            return ejecutarConsulta_retornarID($sql);
        }

        public function insertardetalle_sobrante($piezas, $idetiqueta, $idsobrante)
        {
            $sql = "INSERT INTO detalle_sobrante (piezas, id_etiqueta, id_sobrante, modified, created) VALUES ('$piezas', '$idetiqueta', '$idsobrante', now(), now())";

            return ejecutarConsulta($sql);
        }

        public function mostrar($barcode)
        {
            $sql = "SELECT s.id, s.barcode, GROUP_CONCAT(e.partida) AS partida, GROUP_CONCAT(e.color) AS color, GROUP_CONCAT(e.estilo) AS estilo, GROUP_CONCAT(e.talla) AS talla, GROUP_CONCAT(ds.piezas) AS piezas FROM etiqueta AS e JOIN detalle_sobrante AS ds ON e.id = ds.id_etiqueta JOIN sobrante AS s ON s.id = ds.id_sobrante WHERE s.barcode = '$barcode' GROUP BY s.barcode";

            return ejecutarConsultaSimpleFila($sql);
        }

        public function listar()
        {
            $sql = "SELECT * FROM sobrante";

            return ejecutarConsulta($sql);
        }

        function listaretiqueta()
        {
            $sql = "SELECT * FROM etiqueta WHERE activo = 1";
            return ejecutarConsulta($sql);
        }

        public function mostrarinfo($id)
        {
            $sql = "SELECT * FROM etiqueta WHERE id = '$id'";
            return ejecutarConsultaSimpleFila($sql);
        }

        public function ultimoid()
        {
            $sql = "SELECT LAST_INSERT_ID(id) as id FROM sobrante ORDER BY id DESC LIMIT 1";
            return ejecutarConsultaSimpleFila($sql);
        }

        public function buscar($partida)
        {
            $sql = "SELECT * FROM etiqueta WHERE partida LIKE '%".$partida."%' AND fecha >= DATE_SUB(CURDATE(), INTERVAL 30 DAY)";
            return ejecutarConsulta($sql);
        }

        public function actualizarsobrante($id, $estado)
        {
            $sql = "UPDATE sobrante SET estado = '$estado', modified = now() WHERE id = '$id'";
            return ejecutarConsulta($sql);
        }

        public function listarsobrantes($estado)
        {
            $sql = "SELECT s.id, s.barcode, GROUP_CONCAT(e.partida) AS partida, GROUP_CONCAT(e.color) AS color, GROUP_CONCAT(e.estilo) AS estilo, GROUP_CONCAT(e.talla) AS talla, GROUP_CONCAT(ds.piezas) AS piezas FROM etiqueta AS e JOIN detalle_sobrante AS ds ON e.id = ds.id_etiqueta JOIN sobrante AS s ON s.id = ds.id_sobrante WHERE s.estado = '$estado' GROUP BY s.barcode";

            return ejecutarConsulta($sql);
        }
    }

?>