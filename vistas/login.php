<?php
ob_start();
session_start();

if (isset($_SESSION['nombre'])) {
    header("Location: index.php");
} else {
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Inicio de Sesion</title>
    <meta name="description" content="Inicio de Sesion">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- <link rel="apple-touch-icon" href="apple-icon.png"> -->
    <link rel="shortcut icon" href="../public/images/logo_playerytees_oficial.png"/>

    <link rel="stylesheet" href="../public/assets/css/normalize.css">
    <link rel="stylesheet" href="../public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../public/assets/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="../public/assets/css/themify-icons.css"> -->
    <!-- <link rel="stylesheet" href="../public/assets/css/flag-icon.min.css"> -->
    <link rel="stylesheet" href="../public/assets/css/cs-skin-elastic.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
    <link rel="stylesheet" href="../public/assets/scss/style.css">
    <link rel="stylesheet" href="../public/assets/css/toastr.min.css">
    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" href="../public/assets/css/estilos.css">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->

</head>
<body class="bg-white">
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="#">
                        <img class="align-content" src="../public/images/LogoGrande.png" alt="Logo Playerytees" width="300">
                    </a>
                </div>
                <div class="login-form">
                    <form method="post" id="formAcceso">
                        <div class="form-group">
                            <label>Usuario</label>
                            <input type="text" class="form-control" id="username" name="username" required>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" id="password" name="password" required>
                        </div>
<?php /*
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Remember Me
                            </label>
                            <label class="pull-right">
                                <a href="#">Forgotten Password?</a>
                            </label>
                        </div>
*/ ?>
                        <button type="submit" name="btn_login" class="btn btn-playerytees btn-flat m-b-30 m-t-30">Iniciar Sesion</button>

                        <div class="social-login-content d-none">
                            <div class="social-button">
                                <button type="button" class="btn social facebook btn-flat btn-addon mb-3"><i class="ti-facebook"></i>Sign in with facebook</button>
                                <button type="button" class="btn social twitter btn-flat btn-addon mt-2"><i class="ti-twitter"></i>Sign in with twitter</button>
                            </div>
                        </div>
                        <div class="register-link m-t-15 text-center mt-3">
                            <p>Contestar Encuesta <a href="https://forms.gle/SeY1C5Qo4xDHcb7J8" target="_blank">Aquí</a></p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
    <script src="../public/assets/js/popper.min.js"></script>
    <script src="../public/assets/js/plugins.js"></script>
    <script src="../public/assets/js/main.js"></script>
    <script src="../public/assets/js/toastr.min.js"></script>
    <script src="scripts/login.js"></script>
</body>
</html>
<?php
}
ob_end_flush();
?>