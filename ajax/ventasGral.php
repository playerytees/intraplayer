<?php
require_once "../modelos/Ventas.php";
session_start();
$ventas = new Ventas();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$meta = isset($_POST["txtmonto"])?limpiarCadena($_POST["txtmonto"]):"";
$mes = isset($_POST["selectmes"])?limpiarCadena($_POST["selectmes"]):"";
$anio = isset($_POST["selectanio"])?limpiarCadena($_POST["selectanio"]):"";
$sucursal = isset($_POST["selectsucursal"])?limpiarCadena($_POST["selectsucursal"]):"";
$fecha = isset($_POST["fecha"])?limpiarCadena($_POST["fecha"]):"";
switch ($_GET["opcion"]) {
    case 'guardar':
        $exist = $ventas->consultarSiExiste($mes, $anio, $sucursal);
        if ($exist) {
            echo false;
        } else {
            $response = $ventas->insertar($meta, $mes, $anio, $sucursal);

            echo $response;
        }
        break;
    case 'listar':
        require_once "../modelos/VentasGralSap.php";
        $ventasSap = new VentasGralSap();
        $data = array();
        $fechaForSap = str_replace("-", "", $fecha);
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0].$fechaArray[1]."01";

        $Permisos = array(
            ($_SESSION['5febrero'])?"'FEB'":"",
            ($_SESSION['cuernavaca'])?"'CUE'":"",
            ($_SESSION['hermosillo'])?"'HE7'":"",
            ($_SESSION['isabel'])?"'ISA'":"",
            ($_SESSION['cdmx'])?"'IS2'":"",
            ($_SESSION['lorey'])?"'LOR'":"",
            ($_SESSION['monterrey'])?"'MO5'":"",
            ($_SESSION['noria'])?"'NO4'":"",
            ($_SESSION['oaxaca'])?"'OA6'":"",
            ($_SESSION['oaxaca2'])?"'OA2'":"",
            ($_SESSION['pachuca'])?"'PAC'":"",
            ($_SESSION['sanluis'])?"'SL2'":"",
            ($_SESSION['CedisDPT'])?"'CE1'":""
        );

        if (empty(implode( ",",array_filter($Permisos) ) )) {
            $sucursales = array();
        } else {
            $sucursales = $ventasSap->listarSucursales( implode( ",",array_filter($Permisos) ) );
        }
        $ventasMensuales = $ventasSap->ventasMensuales("'$inicioMes'", "'$fechaForSap'");
        $ventasDiarias = $ventasSap->ventasDiarias("'$fechaForSap'");
        for ($i=0; $i < count($sucursales); $i++) { 
            if(in_array($sucursales[$i]->Codigo, array_column($ventasDiarias,'Sucursal'))){

            } else {
                array_push($ventasDiarias, (object)array('Monto' => '0', 'Sucursal' => $sucursales[$i]->Codigo));
            }

            if(in_array($sucursales[$i]->Codigo, array_column($ventasMensuales,'Sucursal'))){

            } else {
                array_push($ventasMensuales, (object)array('Monto' => '0', 'Sucursal' => $sucursales[$i]->Codigo));
            }
        }

        if ($_SESSION['CEDIS']) {
            $sucursalesGP = $ventasSap->listarSucursalesGP();
            array_push($sucursales, empty($sucursalesGP) ? (object)array('Codigo' => 'CE1', 'Nombre' => 'CEDIS (GP)') : $sucursalesGP[0]);

            //$posVM = array_search( 'CE1', array_column($ventasMensuales, 'Sucursal') );

            $ventasDiariasGP = $ventasSap->ventasDiariasGP("'$fechaForSap'");
            array_push($ventasDiarias, empty($ventasDiariasGP) ? (object)array('Monto' => '0', 'Sucursal' => 'CE1') : $ventasDiariasGP[0]);

            //$posVD = array_search( 'CE1', array_column($ventasDiarias, 'Sucursal') );

            $ventasMensualesGP = $ventasSap->ventasMensualesGP("'$inicioMes'","'$fechaForSap'");
            array_push($ventasMensuales,empty($ventasMensualesGP) ? (object)array('Monto' => '0', 'Sucursal' => 'CE1') : $ventasMensualesGP[0]);
            //$ventasDiarias[$posVD]->Monto = floatval($ventasDiarias[$posVD]->Monto) + floatval($ventasDiariasGP);

            //$ventasMensuales[$posVM]->Monto = floatval($ventasMensuales[$posVM]->Monto) + floatval($ventasMensualesGP);
        }

        if ($_SESSION['Distribuidores']) {
            array_push($sucursales, (object)array('Codigo' => 'DIS102', 'Nombre' => 'DISTRIBUIDORES'));

            $ventasDiariasDistribuidores = $ventasSap->ventasDiariasDistribuidores("'$fechaForSap'", 'GP_BD');

            $ventasDiariasDistribuidoresDPT = $ventasSap->ventasDiariasDistribuidores("'$fechaForSap'", 'DPT_BD');

            if (!empty($ventasDiariasDistribuidoresDPT)) {
                $ventasDiariasDistribuidores[0]->Monto = floatval($ventasDiariasDistribuidores[0]->Monto) + floatval($ventasDiariasDistribuidoresDPT[0]->Monto);
            }

            array_push($ventasDiarias, empty($ventasDiariasDistribuidores) ? (object)array('Monto' => '0', 'Sucursal' => 'DIS102') : $ventasDiariasDistribuidores[0] );

            $ventasMensualesDistribuidores = $ventasSap->ventasMensualesDistribuidores("'$inicioMes'","'$fechaForSap'", 'GP_BD');

            $ventasMensualesDistribuidoresDPT =  $ventasSap->ventasMensualesDistribuidores("'$inicioMes'","'$fechaForSap'", 'DPT_BD');

            if (!empty($ventasMensualesDistribuidoresDPT)) {
                $ventasMensualesDistribuidores[0]->Monto = floatval($ventasMensualesDistribuidores[0]->Monto) + floatval($ventasMensualesDistribuidoresDPT[0]->Monto);
            }

            array_push($ventasMensuales, empty($ventasMensualesDistribuidores) ? (object)array('Monto' => '0', 'Sucursal' => 'DIS102') : $ventasMensualesDistribuidores[0]);
        }

        $metaActual = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metaAnterior = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        /* if (!empty($metaActual) OR !empty($metaAnterior)) {
            // Buscar la meta de DISTRIBUIDORES
            $posMetaActual = array_search( 'DIS102', array_column($metaActual, 'sucursal') );
            $posMetaAnterior = array_search( 'DIS102', array_column($metaAnterior, 'sucursal') );
            // Buscar la meta de CEDIS
            $posMetaActualCE = array_search( 'CE1', array_column($metaActual, 'sucursal') );
            $posMetaAnteriorCE = array_search( 'CE1', array_column($metaAnterior, 'sucursal') );
            // Sumar las metas de CEDIS y DISTRIBUIDORES
            $metaActual[$posMetaActualCE]['meta'] = floatval($metaActual[$posMetaActualCE]['meta']) + floatval($metaActual[$posMetaActual]['meta']);
            $metaAnterior[$posMetaAnteriorCE]['meta'] = floatval($metaAnterior[$posMetaAnteriorCE]['meta']) + floatval($metaAnterior[$posMetaAnterior]['meta']);
        } */

        for ($i=0; $i < count($sucursales); $i++) { 
            // Indice Venta Diaria Hana
            $indiceVD = array_search($sucursales[$i]->Codigo, array_column($ventasDiarias, 'Sucursal'));
            $montoVD = ($indiceVD === false)?"000.00":$ventasDiarias[$indiceVD]->Monto;
            // Indice Venta Mensual Hana
            $indiceVM = array_search($sucursales[$i]->Codigo, array_column($ventasMensuales, 'Sucursal'));
            $montoVM = ($indiceVM === false)?"000.00":$ventasMensuales[$indiceVM]->Monto;
            // Indice Meta Mes Año Actual MySQL
            $indiceMetaAct = array_search($sucursales[$i]->Codigo, array_column($metaActual, 'sucursal'));
            $metaMesAct = ($indiceMetaAct === false)?"000.00":$metaActual[$indiceMetaAct]["meta"];
            // Indice Meta Mes Año Anterior MySQL
            $indiceMetaAnt = array_search($sucursales[$i]->Codigo, array_column($metaAnterior, 'sucursal'));
            $metaMesAnt = ($indiceMetaAnt === false)?"000.00":$metaAnterior[$indiceMetaAnt]["meta"];
            $porcentajeMetaAct = ($ventas->reglaD3($metaMesAct, $montoVM)>=100)?'<div class="progress"><div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAct, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAct, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAct, $montoVM).'%</div></div>':'<div class="progress"><div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAct, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAct, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAct, $montoVM).'%</div></div>';

            $porcentajeMetaAnt = ($ventas->reglaD3($metaMesAnt, $montoVM)>=100)?'<div class="progress"><div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAnt, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAnt, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAnt, $montoVM).'%</div></div>':'<div class="progress"><div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAnt, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAnt, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAnt, $montoVM).'%</div></div>';

            $url = ($sucursales[$i]->Codigo == "DIS102") ? "<a href='../vistas/distribuidoresGral.php' class='text-playerytees'><u>".$sucursales[$i]->Nombre."</u></a>" : $sucursales[$i]->Nombre;

            $data[] = array(
                '0' => $url,
                '1' => number_format($montoVD, 2, ".", ","),
                '2' => number_format($montoVM, 2, ".", ","),
                '3' => number_format($metaMesAct, 2, ".", ","),
                '4' => $porcentajeMetaAct,
                '5' => number_format($metaMesAnt, 2, ".", ","),
                '6' => $porcentajeMetaAnt
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listarSucursales':
        require_once "../modelos/VentasGralSap.php";
        $ventasSap = new VentasGralSap();

        $Permisos = array("'FEB'","'CE1'","'CUE'","'HE7'","'ISA'","'IS2'","'LOR'","'MO5'","'NO4'","'OA2'","'PAC'","'SL2'");

        $sucursales = $ventasSap->listarSucursales( implode( ",", array_filter($Permisos) ) );
        $data = array();

        foreach ($sucursales as $sucursal) {
            $data[] = array(
                '0' => $sucursal->Codigo,
                '1' => $sucursal->Nombre
            );
        }
        
        /* foreach ($ventasSap->listarSucursalesGP() as $sucursalGP) {
            array_push($data, array($sucursalGP->Codigo, $sucursalGP->Nombre));
        } */

        echo json_encode($data);
        break;
    case 'ventasMensuales':
        require_once "../modelos/VentasGralSap.php";
        $ventasSap = new VentasGralSap();
        $response = $ventasSap->ventasMensuales();

        var_dump($response);
        break;
    case 'grafica':
        require_once "../modelos/VentaSap.php";
        $ventaSap = new VentaSap();
        $data = array();
        $fechaActual = $fecha;
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0].$fechaArray[1]."01";

        $Permisos = array("'FEB'", "'CUE'", "'HE07'", "'ISA'", "'ISA2'", "'LOR'", "'MO05'", "'NO04'", "'OA2'", "'PAC'", "'SL02'", "'SLP'");

        $sucursales = $ventaSap->listarSucursal( implode( ",", array_filter($Permisos) ), DPT );

        $ventasMensuales = $ventaSap->groupBy( $ventaSap->ventas("'$inicioMes'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');

        $devolucionesMensuales = $ventaSap->groupBy( $ventaSap->devoluciones("'$inicioMes'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');

        $ventasDiarias = $ventaSap->groupBy( $ventaSap->ventas("'$fechaActual'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');

        if (!empty($devolucionesMensuales)) {
            for ($i=0; $i < count($ventasMensuales); $i++) { 
                for ($j=0; $j < count($devolucionesMensuales); $j++) {
                    if ($ventasMensuales[$i]['Sucursal'] == $devolucionesMensuales[$j]['Sucursal']) {
                        $ventasMensuales[$i]['Monto'] = $ventasMensuales[$i]['Monto'] - $devolucionesMensuales[$j]['Monto'];
                    }
                }
            }
        }

        $metaActual = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metaAnterior = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        for ($i=0; $i < count($sucursales); $i++) { 
            // Indice Venta Diaria Hana
            $indiceVD = array_search($sucursales[$i]->Codigo, array_column($ventasDiarias, 'Sucursal'));
            $montoVD = ($indiceVD === false)?"000.00":$ventasDiarias[$indiceVD]['Monto'];
            // Indice Venta Mensual Hana
            $indiceVM = array_search($sucursales[$i]->Codigo, array_column($ventasMensuales, 'Sucursal'));
            $montoVM = ($indiceVM === false)?"000.00":$ventasMensuales[$indiceVM]['Monto'];
            // Indice Meta Mes Año Actual MySQL
            $indiceMetaAct = array_search($sucursales[$i]->Codigo, array_column($metaActual, 'sucursal'));
            $metaMesAct = ($indiceMetaAct === false)?"000.00":$metaActual[$indiceMetaAct]["meta"];
            // Indice Meta Mes Año Anterior MySQL
            $indiceMetaAnt = array_search($sucursales[$i]->Codigo, array_column($metaAnterior, 'sucursal'));
            $metaMesAnt = ($indiceMetaAnt === false)?"000.00":$metaAnterior[$indiceMetaAnt]["meta"];

            $data[] = array(
                '0' => $sucursales[$i]->Nombre,
                '1' => $ventas->reglaD3($metaMesAct, $montoVM),
                '2' => $ventas->reglaD3($metaMesAnt, $montoVM)
            );
        }

        echo json_encode($data);
        break;
    case 'ventas':
        require_once "../modelos/VentaSap.php";
        $ventaSap = new VentaSap();
        $fechaActual = $fecha;
        $fechaArray = explode("-", $fechaActual);
        $inicioMes = $fechaArray[0].'-'.$fechaArray[1].'-'.'01';

        $Permisos = array(
            ($_SESSION['CedisDPT'])?"'CE01'":"",
            ($_SESSION['cuernavaca'])?"'CUE'":"",
            ($_SESSION['5febrero'])?"'FEB'":"",
            ($_SESSION['hermosillo'])?"'HE07'":"",
            ($_SESSION['isabel'])?"'ISA'":"",
            ($_SESSION['cdmx'])?"'ISA2'":"",
            ($_SESSION['lorey'])?"'LOR'":"",
            ($_SESSION['monterrey'])?"'MO05'":"",
            ($_SESSION['noria'])?"'NO04'":"",
            ($_SESSION['oaxaca'])?"'OA06'":"",
            ($_SESSION['oaxaca2'])?"'OA2'":"",
            ($_SESSION['pachuca'])?"'PAC'":"",
            ($_SESSION['sanluis'])?"'SL02'":"",
            ($_SESSION['sanluis2'])?"'SLP'":"",
        );

        if (empty(implode( ",",array_filter($Permisos) ) )) {
            $sucursales = array();
        } else {
            $sucursales = $ventaSap->listarSucursal( implode( ",",array_filter($Permisos) ), DPT );
        }

        $ventasMensualesDPT = $ventaSap->groupBy( $ventaSap->ventas("'$inicioMes'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');
        $ventasDiariasDPT = $ventaSap->groupBy( $ventaSap->ventas("'$fechaActual'", "'$fechaActual'", DPT), "Sucursal", "Monto");

        $devolucionesMensualesDPT = $ventaSap->groupBy( $ventaSap->devoluciones("'$inicioMes'", "'$fechaActual'", DPT), 'Sucursal', 'Monto');
        $devolucionesDiariasDPT = $ventaSap->groupBy( $ventaSap->devoluciones("'$fechaActual'", "'$fechaActual'", DPT), "Sucursal", "Monto");

        foreach ($sucursales as $sucursal) {
            if ($sucursal->Codigo == 'CE01') {
                $sucursal->Nombre = 'CEDIS (PT)';
            }
            if (!in_array($sucursal->Codigo, array_column($ventasMensualesDPT, 'Sucursal'))) {
                array_push($ventasMensualesDPT, array('Sucursal' => $sucursal->Codigo, 'Monto' => 0));
            }
            if (!in_array($sucursal->Codigo, array_column($ventasDiariasDPT, 'Sucursal'))) {
                array_push($ventasDiariasDPT, array('Sucursal' => $sucursal->Codigo, 'Monto' => 0));
            }
        }

        if (!empty($devolucionesMensualesDPT)) {
            for ($i=0; $i < count($ventasMensualesDPT); $i++) { 
                for ($j=0; $j < count($devolucionesMensualesDPT); $j++) {
                    if ($ventasMensualesDPT[$i]['Sucursal'] == $devolucionesMensualesDPT[$j]['Sucursal']) {
                        $ventasMensualesDPT[$i]['Monto'] = $ventasMensualesDPT[$i]['Monto'] - $devolucionesMensualesDPT[$j]['Monto'];
                    }
                }
            }
        }

        if (!empty($devolucionesDiariasDPT)) {
            for ($i=0; $i < count($ventasDiariasDPT); $i++) { 
                for ($j=0; $j < count($devolucionesDiariasDPT); $j++) { 
                    if ($ventasDiariasDPT[$i]['Sucursal'] == $devolucionesDiariasDPT[$j]['Sucursal']) {
                        $ventasDiariasDPT[$i]['Monto'] = $ventasDiariasDPT[$i]['Monto'] - $devolucionesDiariasDPT[$j]['Monto'];
                    }
                }
            }
        }

        if ($_SESSION['CEDIS']==1) {
            $sucursalesGP = $ventaSap->listarSucursal("'CE01'", GP);
            if ($sucursalesGP[0]->Codigo == 'CE01') {
                $sucursalesGP[0]->Codigo = 'CE01GP';
                $sucursalesGP[0]->Nombre = 'CEDIS (GP)';
            }
            array_push($sucursales, $sucursalesGP[0]);

            $ventasDiariasGP = $ventaSap->groupBy( $ventaSap->ventas("'$fechaActual'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            if (empty($ventasDiariasGP)) {
                array_push($ventasDiariasDPT, array('Sucursal' => 'CE01GP', 'Monto' => 0));
            } else {
                if (!empty($devolucionesDiariasGP)) {
                    if ($ventasDiariasGP[0]['Sucursal'] == $devolucionesDiariasGP[0]['Sucursal']) {
                        $ventasDiariasGP[0]['Monto'] = $ventasDiariasGP[0]['Monto'] - $devolucionesDiariasGP[0]['Monto'];
                    }
                }
                if ($ventasDiariasGP[0]['Sucursal'] == 'CE01') {
                    $ventasDiariasGP[0]['Sucursal'] = 'CE01GP';
                }
                array_push($ventasDiariasDPT, $ventasDiariasGP[0]);
            }

            $ventasMensualesGP =  $ventaSap->groupBy( $ventaSap->ventas("'$inicioMes'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            $devolucionesMensualesGP = $ventaSap->groupBy( $ventaSap->devoluciones("'$inicioMes'", "'$fechaActual'", GP), 'Sucursal', 'Monto');

            if (empty($ventasMensualesGP)) {
                array_push($ventasMensualesDPT, array('Sucursal' => 'CE01GP', 'Monto' => 0));
            } else {
                if (!empty($devolucionesMensualesGP)) {
                    if ($ventasMensualesGP[0]['Sucursal'] == $devolucionesMensualesGP[0]['Sucursal']) {
                        $ventasMensualesGP[0]['Monto'] = $ventasMensualesGP[0]['Monto'] - $devolucionesMensualesGP[0]['Monto'];
                    }
                }
                if ($ventasMensualesGP[0]['Sucursal'] == 'CE01') {
                    $ventasMensualesGP[0]['Sucursal'] = 'CE01GP';
                }
                array_push($ventasMensualesDPT, $ventasMensualesGP[0]);
            }
        }

        if ($_SESSION['Distribuidores']==1) {
            array_push($sucursales, (object)array('Codigo' => 'DIS102', 'Nombre' => 'DISTRIBUIDORES'));

            $VentasDiariasDistribuidoresGP = $ventaSap->ventasDistribuidores("'$fechaActual'", "'$fechaActual'", GP);

            $ventasDiariasDistribuidoresDPT = $ventaSap->ventasDistribuidores("'$fechaActual'", "'$fechaActual'", DPT);

            $sumaVentaDiaDistr = floatval($VentasDiariasDistribuidoresGP[0]->Monto) + floatval($ventasDiariasDistribuidoresDPT[0]->Monto);

            $devolucionesDiariaDistribuidoresGP = $ventaSap->devolucionesDistribuidores("'$fechaActual'", "'$fechaActual'", GP);

            $devolucionesDiariaDistribuidoresDPT = $ventaSap->devolucionesDistribuidores("'$fechaActual'", "'$fechaActual'", DPT);

            $sumaDevolucionDiaDistr = floatval($devolucionesDiariaDistribuidoresGP[0]->Monto) + floatval($devolucionesDiariaDistribuidoresDPT[0]->Monto);

            array_push($ventasDiariasDPT, array('Sucursal' => 'DIS102', 'Monto' => $sumaVentaDiaDistr - $sumaDevolucionDiaDistr));

            $ventasMensualesDistribuidoresGP = $ventaSap->ventasDistribuidores("'$inicioMes'", "'$fechaActual'", GP);

            $ventasMensualesDistribuidoresDPT =  $ventaSap->ventasDistribuidores("'$inicioMes'", "'$fechaActual'", DPT);

            $sumaVentaMesDistr = floatval($ventasMensualesDistribuidoresGP[0]->Monto) + floatval($ventasMensualesDistribuidoresDPT[0]->Monto);

            $devolucionesMensualesDistribuidoresGP = $ventaSap->devolucionesDistribuidores("'$inicioMes'", "'$fechaActual'", GP);

            $devolucionesMensualesDistribuidoresDPT = $ventaSap->devolucionesDistribuidores("'$inicioMes'", "'$fechaActual'", DPT);

            $sumaDevolucionMesDistr = floatval($devolucionesMensualesDistribuidoresGP[0]->Monto) + floatval($devolucionesMensualesDistribuidoresDPT[0]->Monto);

            array_push($ventasMensualesDPT, array('Sucursal' => 'DIS102', 'Monto' => $sumaVentaMesDistr - $sumaDevolucionMesDistr));
        }

        $metasActuales = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metasAnteriores = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        
        $data = array();
        foreach ($sucursales as $sucursal) {
            foreach ($ventasDiariasDPT as $ventaDiariaDPT) {
                if ($sucursal->Codigo == $ventaDiariaDPT['Sucursal']) {
                    $sucursal->Diaria = $ventaDiariaDPT['Monto'];
                }
            }

            foreach ($ventasMensualesDPT as $ventaMensualDPT) {
                if ($sucursal->Codigo == $ventaMensualDPT['Sucursal']) {
                    $sucursal->Mensual = $ventaMensualDPT['Monto'];
                }
            }

            foreach ($metasActuales as $metaActual) {
                if ($sucursal->Codigo == $metaActual['sucursal']) {
                    $sucursal->metaActual = floatval($metaActual['meta']);
                }
            }

            foreach ($metasAnteriores as $metaAnterior) {
                if ($sucursal->Codigo == $metaAnterior['sucursal']) {
                    $sucursal->metaAnterior = floatval($metaAnterior['meta']);
                }
            }

            $porcentajeMetaActual = ($ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual)>=100)?'<div class="progress"><div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual).'%" aria-valuenow="'.$ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual).'%</div></div>':'<div class="progress"><div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual).'%" aria-valuenow="'.$ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3(isset($sucursal->metaActual)?$sucursal->metaActual:0, $sucursal->Mensual).'%</div></div>';

            $porcentajeMetaAnterior = ($ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual)>=100)?'<div class="progress"><div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual).'%" aria-valuenow="'.$ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual).'%</div></div>':'<div class="progress"><div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual).'%" aria-valuenow="'.$ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3(isset($sucursal->metaAnterior)?$sucursal->metaAnterior:0, $sucursal->Mensual).'%</div></div>';

            $url = ($sucursal->Codigo == "DIS102") ? "<a href='javascript:void(0);' onclick='mostrarDistrGeneral(true)' class='text-playerytees'><u>".$sucursal->Nombre."</u></a>" : $sucursal->Nombre;

            $data[] = array(
                '0' => $url,
                '1' => number_format($sucursal->Diaria, 2, '.', ','),
                '2' => number_format($sucursal->Mensual, 2, '.', ','),
                '3' => isset($sucursal->metaActual)?number_format($sucursal->metaActual, 2, '.', ','):0,
                '4' => $porcentajeMetaActual,
                '5' => isset($sucursal->metaAnterior)?number_format($sucursal->metaAnterior, 2, '.', ','):0,
                '6' => $porcentajeMetaAnterior
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        echo "No se encontro la opcion";
        break;
}