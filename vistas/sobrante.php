<?php require "header.php"; ?>
    <style>
        @media print {
            #cantsob{
                font-size: 16px;
            }
        }
    </style>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Sobrante</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right">
                        <li><a href="#">Etiqueta</a></li>
                        <li class="active">Sobrante</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="col-lg-12" id="divetiqueta">
            <div class="card">
                <div class="card-header">
                    <strong class="float-left">Nueva Etiqueta</strong>
                    <button type="button" class="btn btn-default btn-sm float-right" onclick="mostrartabla(true)" id="btn_ver">Ver Sobrantes</button>
                    <button type="button" class="btn btn-default btn-sm float-right" onclick="mostrartabla(false)" id="btn_ocultar">Regresar</button>
                </div>
                <div class="card-body">
                    <div id="etiquetasobrantes">
                        <div class="col-md-6">
                            <table id="listaretiqueta" class="table table-bordered table-sm" style="width: 100%;">
                                <thead>
                                    <th>Num.</th>
                                    <th>Partida</th>
                                    <th>Estilo</th>
                                    <th>Color</th>
                                    <th>Talla</th>
                                    <th>Sobrante</th>
                                    <th>Acciones</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <button type="button" onclick="agregarinfo()" class="btn btn-sm btn-info" id="btn-agregar">Agregar</button>
                            <button type="button" class="btn btn-sm btn-info" onclick="imprimir()" id="btn-imprimir">Imprimir</button>
                            <button type="button" class="btn btn-sm btn-info" onclick="nuevo()">Nuevo</button>
                        </div>
                        
                        <div id="printetiqueta" class="col-md-6">
                            <table id="tabla_sobrante" width="600" height="380" style="font-family: Arial; text-align: center;" frame=box>
                                <tr>
                                    <td rowspan="2" colspan="2" style="background-image: url(../public/images/logo_gris.png);background-repeat: no-repeat;background-position: center;background-size: contain;"><!-- <img src="../public/images/logo_gris.png" alt="Logo Playerytees" width="100"> --></td>
                                    <td colspan="3"> <img id="barcode_sobrante"> </td>
                                </tr>
                                <tr>
                                    <td colspan="3">SOBRANTES PRIMERAS</td>
                                </tr>
                                <tr style="font-weight: bold;">
                                    <td>Partida</td>
                                    <td>Color</td>
                                    <td>Estilo</td>
                                    <td>Talla</td>
                                    <td>Piezas</td>
                                </tr>
                            </table>
                        </div> <!-- #printetiqueta -->
                    </div> <!-- #etiquetasobrantes -->

                    <div id="divlistarsobrante">
                        <table id="listarsobrante" class="table table-bordered table-sm" style="width: 100%;">
                            <thead>
                                <th>Num.</th>
                                <th>Codigo</th>
                                <th>Fecha</th>
                                <th>Modificado</th>
                                <th>Creado</th>
                            </thead>
                        </table>
                    </div> <!-- #divlistarsobrante -->
                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-12 -->
    </div> <!-- .content .mt-3 -->
<?php require "footer.php"; ?>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery.PrintArea.js"></script>
<script src="scripts/sobrante.js"></script>