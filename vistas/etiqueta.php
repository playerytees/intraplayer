<?php
ob_start();
session_start();

if(!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';

    if ($_SESSION['embarques']==1) {

    date_default_timezone_set("America/Mexico_City");
?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="text-playerytees">Etiquetas</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Embarques</a></li>
                        <li class="active">Etiqueta</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

        <div class="content mt-3">
            <div class="row" id="divetiqueta">
                <div class="col-12 col-lg-5 col-xl-4">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Nueva Etiqueta</strong>
                        </div> <!-- .card-header -->
                        <form action="" method="post" class="" id="formtag">
                            <div class="card-body card-block">
                                <div class="form-group">
                                    <label for="cve_art" class="form-control-label text-playerytees">Clave de producto:</label>
                                    <input id="cve_art" name="cve_art" class="form-control text-uppercase" type="text" maxlength="18" required>
                                </div>
                                <div class="form-group">
                                    <label for="partida" class="form-control-label text-playerytees">No. de orden:</label>
                                    <input id="partida" name="partida" class="form-control text-uppercase" type="text" maxlength="15" required>
                                </div>
                                <div class="form-group">
                                    <label for="total_rem" class="form-control-label text-playerytees">Total de piezas:</label>
                                    <input id="total_rem" name="total_rem" class="form-control" type="text" pattern="\d*" maxlength="4" required title="Solo Números">
                                </div>
                                <div class="alert alert-primary mb-1" role="alert" id="etiquetas_imprimir"></div>
                                <div class="alert alert-primary mb-0" role="alert" id="sobrantes_imprimir"></div>
                            </div> <!-- .card-body -->
                            <div class="card-footer">
                                <div class="form-group mb-1">
                                    <button type="submit" class="btn btn-playerytees btn-sm">Agregar</button>
                                    <button type="button" class="btn btn-playerytees btn-sm" id="btn_imprimir" onclick="guardar()">Imprimir</button>
                                    <button type="button" class="btn btn-playerytees btn-sm" id="btn_imprimir_chrome" onclick="guardar()">Imprimir</button>
                                    <button type="button" class="btn btn-playerytees btn-sm" id="btn_nueva" onclick="nuevaetiqueta()">Nueva</button>
                                </div>
                            </div> <!-- .card-footer -->
                        </form> <!-- form -->
                    </div> <!-- .card -->
                </div> <!-- col-12 -->

                <div class="col-12 col-lg-7 col-xl-5">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title float-left">Etiqueta</strong>
                            <button type="button" class="btn btn-sm float-right btn-playerytees" onclick="mostrartabla(true)" id="btnlistadotag">Listado de Etiquetas</button>
                        </div> <!-- .card-header -->
                        <div class="card-body card-block">
                            <input type="hidden" name="barcode" class="barcode" value="">
                            <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%; height: 20rem;">
                                <tr style="border-top: 1px solid;">
                                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">STYLE:</td>
                                    <td style="text-align: center; font-size: 25px"><b><span class="estilo"> </span></b></td>
                                    <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; text-align: center;" rowspan="6">
                                        <div style="writing-mode: tb-rl; transform: rotate(180deg); width: 15px;">
                                            <span style="white-space: nowrap;">Order <span class="partida"> </span> </span>
                                        </div>
                                    </td>
                                    <td style="text-align: center; font-weight: bold;" class="d-none d-sm-table-cell">STYLE:</td>
                                    <td style="text-align: center; font-size: 25px" class="d-none d-sm-table-cell"><b><span class="estilo"> </span></b></td>
                                    <td nowrap style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; text-align: center;" class="d-none d-sm-table-cell" rowspan="6">
                                        <div style="writing-mode: tb-rl; transform: rotate(180deg); width: 15px;">
                                            <span style="white-space: nowrap;">Order <span class="partida"> </span></span>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">SIZE:</td>
                                    <td style="text-align: center; font-size: 25px"><b><span class="talla"> </span></b></td>
                                    <td style="text-align: center; font-weight: bold;" class="d-none d-sm-table-cell">SIZE:</td>
                                    <td style="text-align: center; font-size: 25px" class="d-none d-sm-table-cell"><b><span class="talla"> </span></b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">COLOR:</td>
                                    <td style="text-align: center;"><b><span class="color" style="font-size: 25px;"> </span></b></td>
                                    <td style="text-align: center; font-weight: bold;" class="d-none d-sm-table-cell">COLOR:</td>
                                    <td style="text-align: center;"><b><span class="color d-none d-sm-table-cell" style="font-size: 25px;"> </span></b></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">QUANTITY:</td>
                                    <td style="text-align: center; font-size: 30px"><b><span class="pza_x_caja"> </span></b></td>
                                    <td style="text-align: center; font-weight: bold;" class="d-none d-sm-table-cell">QUANTITY:</td>
                                    <td style="text-align: center; font-size: 30px" class="d-none d-sm-table-cell"><b><span class="pza_x_caja"> </span></b></td>
                                </tr>
                                <tr style="text-align: center; border-left: 1px solid;">
                                    <td colspan="2" valign="bottom" style="padding: 0;"><img class="cve_art_barcode"></td>
                                    <td colspan="2" valign="bottom" style="padding: 0" class="d-none d-sm-table-cell"><img class="cve_art_barcode"></td>
                                </tr>
                                <tr style="font-size: 12px; background: black; color: white; height: 10px;">
                                    <td valign="bottom">Packing</td>
                                    <td valign="bottom" style="font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
                                    <td valign="bottom" style="text-align: right" class="d-none d-sm-table-cell">Packing</td>
                                    <td valign="bottom" style="text-align: right; font-weight: bold;" class="d-none d-sm-table-cell"><?php echo date("d/m/Y"); ?></td>
                                </tr>
                            </table>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-12 -->
            </div> <!-- .row #divetiqueta -->

            <!-- <div class="animated fadeIn">
                <div class="row"> -->
                    <div class="col-md-12" id="listadoEtiquetas">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title d-sm-inline text-playerytees">Mostrar Etiquetas</strong>
                                <?php if($_SESSION['usuarios']==1): ?>
                                    <span class="float-sm-right text-playerytees"> <input type="checkbox" id="mostrareliminados" name="mostrareliminados" /> Mostrar Eliminados</span>
                                <?php endif; ?>
                            </div> <!-- .card-header -->
                            <div class="card-body">
                                    <div id="div_tabla_etiqueta">
                                        <table id="tabla_etiqueta" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                                            <thead>
                                                <th>Num.</th>
                                                <th>Código</th>
                                                <th>Partida</th>
                                                <th>Clave Articulo</th>
                                                <th>Total</th>
                                                <th>Piezas/Caja</th>
                                                <th>Sobrante</th>
                                                <th>Num Cajas</th>
                                                <th>Fecha Impresión</th>
                                                <th>Acciones</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div id="div_tabla_eliminados">
                                        <table id="tabla_eliminados" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                                            <thead>
                                                <th>Num.</th>
                                                <th>Código</th>
                                                <th>Partida</th>
                                                <th>Clave Articulo</th>
                                                <th>Total</th>
                                                <th>Piezas/Caja</th>
                                                <th>Sobrante</th>
                                                <th>Num Cajas</th>
                                                <th>Fecha Impresión</th>
                                                <th>Acciones</th>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                            </div> <!-- .card-body -->
                            <div class="card-footer">
                                <button type="button" class="btn btn-danger btn-sm" onclick="mostrartabla(false)"> Regresar </button>
                            </div>
                        </div> <!-- .card -->
                    </div> <!-- .col-md-12 -->
                <!-- </div> --> <!-- .row -->
            <!-- </div> --> <!-- .animated .fadeIn -->
        </div> <!-- .content -->
        <div class="row">
            <div class="col-md-4 col-xs-6" style="position:fixed; bottom: 0; right: 0;" id="divBuscarxcodigo">
                <button class="btn btn-playerytees" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Buscar por c&oacute;digo
                </button>
                <div class="collapse mt-2" id="collapseExample" style="background-color: rgb(239, 243, 245);">
                    <div style="padding: 20px; border-radius: 4px;">
                        <div class="form-inline">
                            <div class="form-group">
                                <label for="txtBuscraXProd" class="text-playerytees"><b>Clave de producto :</b></label>
                                <input type="text" class="form-control ml-2 text-uppercase" id="txtBuscraXProd" placeholder="????-???-???" autocomplete="off" >
                            </div>
                        </div>
                        <br/>
                        <div class="scroll" id="scrollIDCopy" style="overflow: auto; height: 200px;"></div>
                    </div>
                </div>
            </div>
        </div>
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script src="../public/assets/js/jquery.PrintArea.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="../public/assets/js/toastr.min.js"></script>
<script src="scripts/etiqueta.js"></script>
<?php
}
ob_end_flush();
?>