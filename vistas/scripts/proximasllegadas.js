var tabla;
// var selected = [];
function init() {
   $(".loader").hide();
   $("#fileExcel").change(function (e) { 
       e.preventDefault();
        $("#btnGuardar").prop("disabled", this.files.length == 0);
   });
   listar();
}

function limpiar() {
    $("#fileExcel").val("");
    $("#contentExcel").text("");

    $("#idproximasllegadas").val("");
    $("#partida").val("");
    $("#codigo").val("");
    $("#cantidad").val("");
    $("#fechaentrega").val("");
}

function guardartable() {
    var tablaToJSON = $("#tblExcel").tableToJSON();
    var tablaObj = JSON.stringify(tablaToJSON);
    $(".loader").show();
    $.post("../ajax/proximasllegadas.php?opcion=guardartable", {tblExcel: tablaObj},
        function (data, textStatus, jqXHR) {
            $(".loader").hide();
            console.log(data);
            if (data) {
                $.alert({ type: 'green', title: 'Correcto', content: 'Guardado correctamente' });
            } else {
                $.alert({ type: 'red', title: 'Error', content: 'No guardardo <br> Intente nuevamente' });
            }
            tabla.ajax.reload();
            limpiar();
        }
    );
}

$("#fileExcel").change(function (e) { 
    e.preventDefault();
    var fileName = this.files[0].name;
    var fileSize = this.files[0].size;

    if (fileSize > 100000) {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no debe superar 1MB' });
        this.value = '';
        this.files[0].name = '';
    } else if (fileName.split('.').pop() != "xlsx") {
        $.alert({ type: 'red', title: 'Error', content: 'El archivo no tiene la extensión adecuada' });
        this.value = '';
        this.files[0].name = '';
    } else {
        var reader = new FileReader();

        reader.readAsArrayBuffer(e.target.files[0]);

        reader.onload = function (e) {
            var data = new Uint8Array(reader.result);
            var wb = XLSX.read(data, { type: 'array' });
            //console.log(wb);
            if (wb.SheetNames[0]) {
                var htmlstr = XLSX.write(wb, {type: 'binary', bookType: 'html' });
                $("#contentExcel")[0].innerHTML += htmlstr;

                $("#contentExcel > table").attr({ 'id': 'tblExcel' });
                $("#tblExcel").addClass('table table-bordered table-sm table-responsive-sm');
            }
        }
    }
});

function listar() {
    tabla=$("#tblPartidas").DataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: 'Blfrtip',
        buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdf'
                ],
        "ajax":
                {
                    url: '../ajax/proximasllegadas.php?opcion=listar',
                    type : "get",
                    dataType : "json",
                    error: function(e){
                        console.log(e.responseText);
                    }
                },
        /* "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('table-active');
            }
        }, */
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[ 0, "desc" ]]
    });
}

function guardaryeditar() {
    $.confirm({
        title: 'Partidas',
        content: '' +
            `<form action="" class="formName">
                <div class="form-group">
                    <label for="partida">Partida:</label>
                    <input type="text" name="partida" id="partida" class="form-control" />
                    <input type="hidden" name="idproximasllegadas" id="idproximasllegadas" class="form-control"/>
                </div>
                <div class="form-group">
                    <label for="codigo">Codigo</label>
                    <input type="text" name="codigo" id="codigo" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="cantidad">Cantidad</label>
                    <input type="number" name="cantidad" id="cantidad" class="form-control" />
                </div>
                <div class="form-group">
                    <label for="fechaentrega">Fecha entrega:</label>
                    <input type="date" name="fechaentrega" id="fechaentrega" class="form-control" />
                </div>
            </form>`,
        buttons: {
            formSubmit: {
                text: 'Guardar',
                btnClass: 'btn-blue',
                action: function () {
                    var id = this.$content.find("#idproximasllegadas").val();
                    var partida = this.$content.find("#partida").val();
                    var codigo = this.$content.find("#codigo").val();
                    var cantidad = this.$content.find("#cantidad").val();
                    var fechaentrega = this.$content.find('#fechaentrega').val();
                    if ( partida == "" ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Se requiere una partida' });
                        return false;
                    }
                    if ( codigo == "" ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Introduzca un codigo' });
                        return false;
                    }
                    if ( cantidad == "" ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Introduzca la cantidad' });
                        return false;
                    }
                    if ( !fechaentrega ) {
                        $.alert({ type: 'red', title: 'Error', content: 'Fecha invalida' });
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: "../ajax/proximasllegadas.php?opcion=agregaryeditar",
                        data: {
                            idproximasllegadas: id,
                            partida: partida,
                            codigo: codigo,
                            cantidad: cantidad,
                            fechaentrega: fechaentrega
                        },
                        success: function (response) {
                            console.log(response);
                            if (response == 1) {
                                $.alert({ type: 'green', title: 'Correcto', content: 'Guardado correctamente' });
                            } else {
                                $.alert({ type: 'red', title: 'Error', content: 'No se ha podido guardar' });
                            }
                            limpiar();
                            tabla.ajax.reload();
                        }
                    });
                }
            },
            cancelar: function () {
                limpiar();
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // si el usuario envía el formulario presionando Enter en el campo.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // hacer referencia al botón y hacer clic en él
            });            
        }
    });
}

function mostrar(id) {
    guardaryeditar();
    setTimeout(function () {
        $.post("../ajax/proximasllegadas.php?opcion=mostrar", {idproximasllegadas : id},
            function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                $("#idproximasllegadas").val(data.idproximasllegadas);
                $("#partida").val(data.partida);
                $("#codigo").val(data.codigo);
                $("#cantidad").val(data.cantidad);
                $("#fechaentrega").val(data.fechaentrega);
            }
        );
    }, 300);
}

/* $('#tblPartidas tbody').on('click', 'tr', function () {
    var id = this.id;
    var index = $.inArray(id, selected);

    if ( index === -1 ) {
        selected.push( id );
    } else {
        selected.splice( index, 1 );
    }

    $(this).toggleClass('table-active');
} ); */

init();