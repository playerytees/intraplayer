<?php
ob_start();
session_start();
date_default_timezone_set("America/Mexico_City");
if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['existencias']==1 OR $_SESSION['Distribuidores']==1) {
?>
<style>
#tblGeneral td:nth-child(1), #tblDetalle td:nth-child(1),
#tblDistrGeneral td:nth-child(1), #tblDistrDetalle td:nth-child(1),
#tblPagos td:nth-child(1) {
    white-space: nowrap;
    text-align: left;
}
</style>

<div class="content mt-3">
    <div class="card">
        <div class="card-header">
            <string class="card-title">VENTAS</string>
        </div>
        <div class="card-body">
            <div class="form-inline">
                <div class="form-group">
                    <label class="form-control-label mr-1">Seleccionar Fecha:</label>
                    <div class="input-group">
                        <input type="date" class="form-control form-control-sm" id="fecha" name="fecha" required value="<?= date("Y-m-d")?>">
                        <button type="button" class="btn btn-playerytees" id="btn-ver">Ver</button>
                    </div>
                </div>
            </div>
            <div class="default-tab mt-2">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active show" id="nav-general-tab" data-toggle="tab" href="#nav-general" role="tab" aria-controls="nav-general" aria-selected="true">General</a>
                        <a class="nav-item nav-link" id="nav-detalle-tab" data-toggle="tab" href="#nav-detalle" role="tab" aria-controls="nav-detalle" aria-selected="false">Detalle</a>
                        <?php if ($_SESSION['pagos']==1): ?>
                        <a class="nav-item nav-link" id="nav-pagos-tab" data-toggle="tab" href="#nav-pagos" role="tab" aria-controls="nav-pagos" aria-selected="false">Distribuidores</a>
                        <?php endif; ?>
                    </div>
                </nav>
                <div class="tab-content pl-3 pt-2" id="nav-tabContent">

                    <div class="tab-pane fade active show" id="nav-general" role="tabpanel" aria-labelledby="nav-general-tab">
                        <table class='table table-sm table-striped table-bordered table-responsive-sm' id="tblGeneral" style="width: 100%;">
                            <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                <tr>
                                    <th>SUCURSAL</th>
                                    <th>VENTA DIA</th>
                                    <th>VENTA ACUMULADA MES</th>
                                    <th>META</th>
                                    <th>ALCANCE</th>
                                    <th>AÑO ANTERIOR</th>
                                    <th>ALCANCE</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 14px; text-align: right;"></tbody>
                            <tfoot style="font-size: 14px; text-align: right;">
                                <tr>
                                    <th class="text-left"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        
                        <a id="linkRegresarGeneral" href="javascript:void(0);" onclick="mostrarDistrGeneral(false)">Regresar</a>
                        <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tblDistrGeneral" style="width: 100%;">
                            <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                <tr>
                                    <th>SUCURSAL</th>
                                    <th>VENTA DIA</th>
                                    <th>VENTA ACUMULADA MES</th>
                                    <th>META</th>
                                    <th>ALCANCE</th>
                                    <th>AÑO ANTERIOR</th>
                                    <th>ALCANCE</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 14px; text-align: right;"></tbody>
                            <tfoot style="font-size: 14px; white-space: nowrap; text-align: right;">
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div style="width: 100%; overflow: auto;">
                            <div id="chartContentGeneral" style="width: 800px; height: 400px; margin-left: auto; margin-right: auto;">
                                <canvas id="graficaGeneral"></canvas>
                            </div>
                        </div>
                    </div> <!-- .tab-pane -->

                    <div class="tab-pane fade" id="nav-detalle" role="tabpanel" aria-labelledby="nav-detalle-tab">
                        <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tblDetalle" style="width: 100%;">
                            <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                <tr>
                                    <th>SUCURSAL</th>
                                    <th>VENTA DIA</th>
                                    <th>VENTA ACUMULADA MES</th>
                                    <th>PROM DIARIO</th>
                                    <th>TENDENCIA</th>
                                    <th>AÑO ANTERIOR</th>
                                    <th>DIF VS A.A</th>
                                    <th>% VS A.A</th>
                                    <th>CUOTA</th>
                                    <th>DIF VS CUOTA</th>
                                    <th>% VS CUOTA</th>
                                    <th>PROM NECESARIO</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 14px; text-align: right;"></tbody>
                            <tfoot style="font-size: 14px; white-space: nowrap; text-align: right;">
                                <tr>
                                    <th class="text-left"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>

                        <a id="linkRegresarDetalle" href="javascript:void(0);" onclick="mostrarDistrDetalle(false)">Regresar</a>
                        <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tblDistrDetalle" style="width: 100%;">
                            <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                <tr>
                                    <th>NOMBRE</th>
                                    <th>VENTA DIA</th>
                                    <th>VENTA ACUMULADA MES</th>
                                    <th>PROM DIARIO</th>
                                    <th>TENDENCIA</th>
                                    <th>AÑO ANTERIOR</th>
                                    <th>DIF VS A.A</th>
                                    <th>% VS A.A</th>
                                    <th>CUOTA</th>
                                    <th>DIF VS CUOTA</th>
                                    <th>% VS CUOTA</th>
                                    <th>PROM NECESARIO</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 14px; text-align: right;"></tbody>
                            <tfoot style="font-size: 14px; white-space: nowrap; text-align: right;">
                                <tr>
                                    <th class="text-left"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div style="width: 100%; overflow: auto;">
                            <div id="chartContentDetalle" style="width: 800px; height: 400px; margin-left: auto; margin-right: auto;">
                                <canvas id="graficaDetalle"></canvas>
                            </div>
                        </div>
                    </div> <!-- .tab-pane -->
                    <?php if($_SESSION['pagos']==1): ?>
                    <div class="tab-pane fade" id="nav-pagos" role="tabpanel" aria-labelledby="nav-pagos-tab">
                        <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tblPagos" style="width: 100%;">
                            <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                <tr>
                                    <th>CLIENTE</th>
                                    <th>ACUMULADO MES</th>
                                    <th>LIMITE CREDITO</th>
                                    <th>SALDO</th>
                                    <th>DISPONIBLE</th>
                                    <th>SALDO VENCIDO</th>
                                    <th>PAGOS</th>
                                </tr>
                            </thead>
                            <tbody style="font-size: 14px; text-align: right;"></tbody>
                            <tfoot style="font-size: 14px; white-space: nowrap; text-align: right;">
                                <tr>
                                    <th class="text-left"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div> <!-- .tab-pane -->
                    <?php endif; ?>
                </div> <!-- .tab-content -->
            </div> <!-- .default-tab -->
        </div> <!-- .card-body -->
    </div> <!-- .card -->
</div> <!-- .content -->
<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script type="text/javascript" src="scripts/VentasTienda.js"></script>
<?php
}
ob_end_flush();
?>