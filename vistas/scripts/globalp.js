var tabla;

function init()
{
    listar();
    $(".loader").hide();
}

function listar()
{
    tabla = $("#tabla_globalp").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "Bfrtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/globalp.php?opcion=listar",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 10,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

init();