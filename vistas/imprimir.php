<?php require 'header.php'; date_default_timezone_set("America/Mexico_City"); ?>

<style>
    _:-ms-lang(x), _:-webkit-full-screen, .ms-height
    {
        height: 300px;
        margin-bottom: 30px;
    }
    @media screen\0 {
        .ms-height{
            height: 300px;
            margin-bottom: 30px;
        }
    }
</style>
<div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1 class="text-playerytees">Nueva Etiqueta</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right text-playerytees">
                            <li><a href="#">Imprimir</a></li>
                            <li class="active">Etiqueta</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

<div class="container text-center">
    <div id="mensaje_aceptar" class="alert d-none" role="alert"></div>
    <div id="mensaje_guardar" class="alert d-none" role="alert"></div>
</div>

<div class="container mt-3 d-flex">
    <div id="print_area" class="ms-height">
        <table style="width: 600px; height: 372px; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
            <tr style="border-bottom: 1px solid;">
                <td colspan="3"></td>
                <td colspan="3" align="right"></td>
            </tr>
            <tr>
                <td style="text-align: center; border-left: 1px solid; font-weight: bold; width: 100px;">STYLE:</td>
                <td style="text-align: center; font-size: 25px"><b><span class="labelStyle"> </span></b></td>
                <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 372px; text-align: center;" rowspan="6">
                    <div style="writing-mode: tb-rl; transform: rotate(180deg);">
                        <span>Order</span> <span class="labelOrden"> </span>
                    </div>
                </td>
                <td style="text-align: center; font-weight: bold; width: 100px;">STYLE:</td>
                <td style="text-align: center; font-size: 25px"><b><span class="labelStyle" id="labelStyle"> </span></b></td>
                <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 372px; text-align: center;" rowspan="6">
                    <div style="writing-mode: tb-rl; transform: rotate(180deg);">
                        <span>Order </span><span class="labelOrden"> </span>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align: center; border-left: 1px solid; font-weight: bold;">SIZE:</td>
                <td style="text-align: center; font-size: 25px"><b><span class="labelSize"> </span></b></td>
                <td style="text-align: center; font-weight: bold;">SIZE:</td>
                <td style="text-align: center; font-size: 25px"><b><span class="labelSize"> </span></b></td>
            </tr>
            <tr>
                <td style="text-align: center; border-left: 1px solid; font-weight: bold;">COLOR:</td>
                <td style="text-align: center;"><b><span class="labelColor" style="font-size: 25px;"> </span></b></td>
                <td style="text-align: center; font-weight: bold;">COLOR:</td>
                <td style="text-align: center;"><b><span class="labelColor" style="font-size: 25px;"> </span></b></td>
            </tr>
            <tr>
                <td style="text-align: center; border-left: 1px solid; font-weight: bold;">QUANTITY:</td>
                <td style="text-align: center; font-size: 30px"><b><span class="labelQty"> </span></b></td>
                <td style="text-align: center; font-weight: bold;">QUANTITY:</td>
                <td style="text-align: center; font-size: 30px"><b><span class="labelQty"> </span></b></td>
            </tr>
            <tr style="text-align: center; border-left: 1px solid;">
                <td colspan="2" valign="bottom" style="padding: 0; height: 50px;"><svg class="barcode"></svg></td>
                <td colspan="2" valign="bottom" style="padding: 0"><svg class="barcode"></svg></td>
            </tr>
            <tr style="font-size: 12px; background: black; color: white; height: 10px;">
                <td valign="bottom">Packing</td>
                <td valign="bottom" id="labelFecha" style="font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
                <td valign="bottom" style="text-align: right">Packing</td>
                <td valign="bottom" style="text-align: right; font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
            </tr>
        </table>
    </div>
    <div class="ml-3">
        <form id="form_label">
            <div class="form-group">
                <label for="input_cve_ref" style="font-weight: bold; color: rgb(31, 78, 120);">Clave de Producto:</label>
                <input type="text" class="form-control text-uppercase" id="input_cve_ref" name="input_cve_ref" maxlength="18">
            </div>
            <div class="form-group">
                <label for="input_orden" style="font-weight: bold; color: rgb(31, 78, 120);">No. de Orden:</label>
                <input type="text" class="form-control text-uppercase" id="input_orden" name="input_orden" maxlength="15">
            </div>
            <input type="hidden" id="input_id_etiqueta" name="input_id_etiqueta">
            <input type="button" class="btn btn-info btn-sm" style="background-color: rgb(31, 78, 120); border-color: rgb(31, 78, 120); font-weight: bold;" id="btn_print_area" value="Imprimir" onclick="imprimir()">
        </form>
        <div id="mensaje" class="alert alert-danger mt-1 text-center" role="alert">Datos Incorrectos</div>
    </div>
</div>

<div class="row">
    <div class="col-md-4 col-xs-6" style="position:fixed; bottom: 0; right: 0;">
        <button class="btn btn-playerytees" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
            <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Buscar por c&oacute;digo
        </button>
        <div class="collapse mt-2" id="collapseExample">
            <div style="padding: 20px; border-radius: 4px;">
                <div class="form-inline">
                    <div class="form-group">
                        <label for="txtBuscraXProd" class="text-playerytees"><b>Clave de producto :</b></label>
                        <input type="text" class="form-control ml-2 text-uppercase" id="txtBuscraXProd" placeholder="????-???-???" autocomplete="off" >
                    </div>
                </div>
                <br/>
                <div class="scroll" id="scrollIDCopy" style="overflow: auto; height: 200px;"></div>
            </div>
        </div>
    </div>
</div>
<?php require 'footer.php'; ?>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script src="../public/assets/js/jquery.PrintArea.js"></script>
<script src="../public/assets/js/toastr.min.js"></script>
<script src="scripts/imprimir.js"></script>