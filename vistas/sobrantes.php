<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';

    if ($_SESSION['embarques']==1) {
?>
<div class="content mt-1">
    <div class="row">
        <div class="col-lg-12">
            <div class="card" style="margin-bottom: .5em;">
                <div class="card-header" style="padding-bottom: 0;">
                    <strong class="card-title float-left" style="margin-bottom: .5em">Escanear Sobrantes</strong>
                    <a href="partidas_sobrantes.php" class="btn btn-primary btn-sm float-right">Mostrar Sobrantes</a>
                </div>
                <div class="card-body" style="padding-bottom: 0;">
                    <form class="form-horizontal">
                        <div class="row form-group">
                            <div class="col-md-3"></div>
                            <div class="col col-md-2">
                                <label for="barcodesob" class="form-control-label" style="font-size: 14px">Codigo de Sobrante:</label>
                            </div>
                            <div class="col-12 col-md-2">
                                <input type="text" id="barcodesob" name="barcodesob" class="form-control form-control-sm" maxlength="7" autofocus onkeypress="return pulsarenter(event)">
                            </div>
                            <div class="col-md-2">
                                <button type="button" id="btnverificar" class="btn btn-primary btn-sm" onclick="mostrarsobrante()">Verificar</button>
                            </div>
                            <div class="col-md-3">
                                <div id="message-sku"></div>
                            </div>
                        </div>
                    </form>
                </div> <!-- .card-body -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-12 -->
    </div><!-- .row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header" style="padding-bottom: 0;">
                    <strong class="card-title">Partidas</strong>
                </div>
                <div class="card-body">
                    <table id="table_sob" class="table table-bordered table-sm" style="width: 100%;">
                        <thead>
                            <th class="d-none">barcode</th>
                            <th class="d-none">id</th>
                            <th>Partida</th>
                            <th>Estilo</th>
                            <th>Color</th>
                            <th>Talla</th>
                            <th>Piezas</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div> <!-- .card-body -->
                <div class="card-footer">
                    <button type="button" class="btn btn-primary btn-sm" onclick="guardarsobrantes()">Guardar</button>
                </div>
            </div> <!-- .card -->
        </div> <!-- .col-lg-12 -->
    </div> <!-- .row -->
</div> <!-- .content -->
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/sobrantes.js"></script>
<?php
}
ob_end_flush();
?>