var tblGeneral;
var tblDetalle;
var tblDistrGeneral;
var tblDistrDetalle;
var tblPagos;
var myChartGeneral;
var myChartDetalle;

function init() {
    $(".loader").hide();
    listarGeneral($("#fecha").val());
    listarDetalle($("#fecha").val());
    listarDistrGeneral($("#fecha").val());
    listarDistrDetalle($("#fecha").val());
    mostrarDistrGeneral(false);
    mostrarDistrDetalle(false);
    graficaGeneral();
    addDataGeneral();
    graficaDetalle();
    addDataDetalle();
    listarPagos($("#fecha").val());
}

function mostrarDistrGeneral(flag) {
    if (flag) {
        $("#linkRegresarGeneral").show();
        $("#tblDistrGeneral").show();
        $("#tblGeneral").hide();
        $("#chartContentGeneral").hide();
    } else {
        $("#linkRegresarGeneral").hide();
        $("#tblDistrGeneral").hide();
        $("#tblGeneral").show();
        $("#chartContentGeneral").show();
    }
}

function mostrarDistrDetalle(flag) {
    if (flag) {
        $("#linkRegresarDetalle").show();
        $("#tblDistrDetalle").show();
        $("#tblDetalle").hide();
        $("#chartContentDetalle").hide();
    } else {
        $("#linkRegresarDetalle").hide();
        $("#tblDistrDetalle").hide();
        $("#tblDetalle").show();
        $("#chartContentDetalle").show();
    }
}

function listarGeneral(fecha) {
    tblGeneral = $("#tblGeneral").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            TotalGeneral( this.api() );
        },
        "initComplete": function() {
            // sumaColumnas();
        },
        "ajax": {
            url: "../ajax/ventasGral.php?opcion=ventas",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarDetalle(fecha) {
    tblDetalle = $("#tblDetalle").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            TotalDetalle( this.api() );
        },
        "initComplete": function(settings, json) {
            // sumaColumnas();
        },
        "ajax": {
            url: "../ajax/ventas.php?opcion=ventas",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function TotalGeneral(api) {
    $( api.column( 0 ).footer() ).html("TOTAL:");
    $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblGeneral.column( 1 ).data().sum().toFixed(2) ) );
    $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblGeneral.column( 2 ).data().sum().toFixed(2) ) );
    $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblGeneral.column( 3 ).data().sum().toFixed(2) ) );
    $( api.column( 4 ).footer() ).html( ((tblGeneral.column( 2 ).data().sum() * 100)/tblGeneral.column( 3 ).data().sum()).toFixed()+"%" );
    $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblGeneral.column( 5 ).data().sum().toFixed(2) ) );
    $( api.column( 6 ).footer() ).html( ((tblGeneral.column( 2 ).data().sum() * 100)/tblGeneral.column( 5 ).data().sum()).toFixed()+"%" );
}

function TotalDetalle(api) {
    var diasHabiles;
    var diasTranscurridos;
    var diasRestantes;

    var prom_diario;
    var tendencia;
    var difVsAA;
    var PorcentajeVsAA;
    var difVsCuota;
    var PorcentajeVsCuota;
    var prom_necesario;

    $( api.column( 0 ).footer() ).html("TOTAL:");
    $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDetalle.column( 1 ).data().sum().toFixed(2) ) );
    $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDetalle.column( 2 ).data().sum().toFixed(2) ) );

    $.post("../ajax/ventas.php?opcion=bussiness_days", {fecha: $("#fecha").val()},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);

            diasHabiles = data[0][0];
            diasTranscurridos = data[0][1];
            diasRestantes = data[0][2];

            prom_diario = tblDetalle.column( 2 ).data().sum().toFixed(2) / diasTranscurridos;
            tendencia = prom_diario * diasHabiles;
            difVsAA = tendencia - tblDetalle.column( 5 ).data().sum().toFixed(2);
            PorcentajeVsAA = difVsAA / tblDetalle.column( 5 ).data().sum().toFixed(2);
            difVsCuota = tendencia - tblDetalle.column( 8 ).data().sum().toFixed(2);
            PorcentajeVsCuota = difVsCuota / tblDetalle.column( 8 ).data().sum().toFixed(2);
            prom_necesario = ( tblDetalle.column( 8 ).data().sum().toFixed(2) - tblDetalle.column( 2 ).data().sum().toFixed(2) ) / diasRestantes;

            $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_diario ) );
            $( api.column( 4 ).footer() ).html( new Intl.NumberFormat("en-US").format( tendencia ) );
            $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDetalle.column( 5 ).data().sum().toFixed(2) ) );
            $( api.column( 6 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsAA ) );
            $( api.column( 7 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsAA*100 ) );
            $( api.column( 8 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDetalle.column( 8 ).data().sum().toFixed(2) ) );
            $( api.column( 9 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsCuota ) );
            $( api.column(10 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsCuota*100 ) );
            $( api.column(11 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_necesario ) );
        }
    );
}

function listarDistrGeneral(fecha) {
    tblDistrGeneral = $("#tblDistrGeneral").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            TotalDistrGeneral( this.api() );
        },
        "initComplete": function() {
            // sumaColumnas();
        },
        "ajax": {
            url: "../ajax/distribuidoresGral.php?opcion=listar",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "asc"]]
    }).DataTable();
}

function listarDistrDetalle(fecha) {
    tblDistrDetalle = $("#tblDistrDetalle").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            TotalDistrDetalle( this.api() );
        },
        "ajax": {
            url: "../ajax/distribuidores.php?opcion=listar",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "asc"]]
    }).DataTable();
}

function TotalDistrGeneral(api) {
    $( api.column( 0 ).footer() ).html("TOTAL:");
    $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrGeneral.column( 1 ).data().sum().toFixed(2) ) );
    $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrGeneral.column( 2 ).data().sum().toFixed(2) ) );
    $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrGeneral.column( 3 ).data().sum().toFixed(2) ) );
    $( api.column( 4 ).footer() ).html( ((tblDistrGeneral.column( 2 ).data().sum() * 100)/tblDistrGeneral.column( 3 ).data().sum()).toFixed()+"%" );
    $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrGeneral.column( 5 ).data().sum().toFixed(2) ) );
    $( api.column( 6 ).footer() ).html( ((tblDistrGeneral.column( 2 ).data().sum() * 100)/tblDistrGeneral.column( 5 ).data().sum()).toFixed()+"%" );
}

function TotalDistrDetalle(api) {
    var diasHabiles;
    var diasTranscurridos;
    var diasRestantes;

    var prom_diario;
    var tendencia;
    var difVsAA;
    var PorcentajeVsAA;
    var difVsCuota;
    var PorcentajeVsCuota;
    var prom_necesario;

    $( api.column( 0 ).footer() ).html("TOTAL:");
    $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrDetalle.column( 1 ).data().sum().toFixed(2) ) );
    $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrDetalle.column( 2 ).data().sum().toFixed(2) ) );

    $.post("../ajax/ventas.php?opcion=bussiness_days", {fecha:  $("#fecha").val()}, function (data, textStatus, jqXHR) {
        data = JSON.parse(data);

        diasHabiles = data[0][0];
        diasTranscurridos = data[0][1];
        diasRestantes = data[0][2];

        prom_diario = tblDistrDetalle.column( 2 ).data().sum().toFixed(2) / diasTranscurridos;
        tendencia = prom_diario * diasHabiles;
        difVsAA = tendencia - tblDistrDetalle.column( 5 ).data().sum().toFixed(2);
        PorcentajeVsAA = difVsAA / tblDistrDetalle.column( 5 ).data().sum().toFixed(2);
        difVsCuota = tendencia - tblDistrDetalle.column( 8 ).data().sum().toFixed(2);
        PorcentajeVsCuota = difVsCuota / tblDistrDetalle.column( 8 ).data().sum().toFixed(2);
        prom_necesario = ( tblDistrDetalle.column( 8 ).data().sum().toFixed(2) - tblDistrDetalle.column( 2 ).data().sum().toFixed(2) ) / diasRestantes;

        $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_diario ) );
        $( api.column( 4 ).footer() ).html( new Intl.NumberFormat("en-US").format( tendencia ) );
        $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrDetalle.column( 5 ).data().sum().toFixed(2) ) );
        $( api.column( 6 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsAA ) );
        $( api.column( 7 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsAA*100 ) );
        $( api.column( 8 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblDistrDetalle.column( 8 ).data().sum().toFixed(2) ) );
        $( api.column( 9 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsCuota ) );
        $( api.column(10 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsCuota*100 ) );
        $( api.column(11 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_necesario ) );
    });
}

function graficaGeneral() {
    var ctx = document.getElementById('graficaGeneral').getContext('2d');
    myChartGeneral = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: "",
            datasets: [{
                label: 'Año Actual',
                data: "",
                backgroundColor: 'rgba(8, 151, 20, 0.8)',
                borderColor: 'rgba(8, 151, 20, 1)',
                borderWidth: 1
            },
            {
                label: 'Año Anterior',
                data: "",
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                        // max: 100,
                        callback: function(value) {
                            return value + "%"
                        }
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Porcentaje"
                    }
                }]
            }
        }
    });
}

function addDataGeneral() {
    var etiquetas = [];
    var lblAnioActual = [];
    var lblAnioAnterior = [];
    $(".loading").show();
    $("#btn-ver").prop("disabled", true);
   $.post("../ajax/ventasGral.php?opcion=grafica", {fecha: $("#fecha").val()},
        function (response) {
            $(".loading").hide();
            $("#btn-ver").prop("disabled", false);
            response = JSON.parse(response);
            etiquetas.length=0;
            lblAnioActual.length=0;
            lblAnioAnterior.length=0;
            for (let i = 0; i < response.length; i++) {
                etiquetas.push(response[i][0]);
                lblAnioActual.push(response[i][1]);
                lblAnioAnterior.push(response[i][2]);
            }
            myChartGeneral.data.labels = etiquetas;
            myChartGeneral.data.datasets[0].data = lblAnioActual;
            myChartGeneral.data.datasets[1].data = lblAnioAnterior;
            myChartGeneral.update();
        }
    );
}

function graficaDetalle() {
    var ctx = document.getElementById('graficaDetalle').getContext('2d');
    myChartDetalle = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: "",
            datasets: [{
                label: 'Tendencia',
                data: "",
                backgroundColor: 'rgba(8, 151, 20, 0.8)',
                borderColor: 'rgba(8, 151, 20, 1)',
                borderWidth: 1
            },
            {
                label: 'Año Anterior',
                data: "",
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            },
            {
                label: 'Cuota',
                data: "",
                fill: false,
                backgroundColor: 'rgba(255, 0, 0, 0.5)',
                borderColor: 'rgba(255, 0, 0, 0.5)',
                type: 'line'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function addDataDetalle() {
    var etiquetas = [];
    var tendencia = [];
    var anioAnterior = [];
    var cuota = [];
    $(".loading").show();
    $("#btn-ver").prop("disabled", true);
   $.post("../ajax/ventas.php?opcion=grafica", {fecha: $("#fecha").val()},
        function (response, textStatus, jqXHR) {
            $(".loading").hide();
            $("#btn-ver").prop("disabled", false);
            response = JSON.parse(response);
            etiquetas.length=0;
            tendencia.length=0;
            anioAnterior.length=0;
            cuota.length = 0;
            for (let i = 0; i < response.length; i++) {
                etiquetas.push(response[i][0]);
                tendencia.push(Number(response[i][1]).toFixed(2));
                anioAnterior.push(Number(response[i][2]).toFixed(2));
                cuota.push(Number(response[i][3]).toFixed(2));
            }
            myChartDetalle.data.labels = etiquetas;
            myChartDetalle.data.datasets[0].data = tendencia;
            myChartDetalle.data.datasets[1].data = anioAnterior;
            myChartDetalle.data.datasets[2].data = cuota;
            myChartDetalle.update();
        }
    );
}


function listarPagos(fecha) {
    tblPagos = $("#tblPagos").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            TotalPagos( this.api() );
        },
        "initComplete": function() {
            // sumaColumnas();
        },
        "ajax": {
            url: "../ajax/distribuidoresGral.php?opcion=listarPagos",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function TotalPagos(api) {
    $( api.column( 0 ).footer() ).html("TOTAL:");
    $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblPagos.column( 1 ).data().sum().toFixed(2) ) );
    $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblPagos.column( 2 ).data().sum().toFixed(2) ) );
    $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblPagos.column( 3 ).data().sum().toFixed(2) ) );
    $( api.column( 4 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblPagos.column( 4 ).data().sum().toFixed(2) ) );
    $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblPagos.column( 5 ).data().sum().toFixed(2) ) );
    $( api.column( 6 ).footer() ).html( new Intl.NumberFormat("en-US").format( tblPagos.column( 6 ).data().sum().toFixed(2) ) );
}

$("#fecha").keypress(function(event) { 
    if (event.keyCode === 13) { 
        $("#btn-ver").click(); 
    } 
}); 

$("#btn-ver").click(function (e) { 
    e.preventDefault();
    tblGeneral.clear().draw();
    tblDetalle.clear().draw();

    tblDistrGeneral.clear().draw();
    tblDistrDetalle.clear().draw();

    tblPagos.clear().draw();

    listarGeneral($("#fecha").val());
    listarDetalle($("#fecha").val());

    listarDistrGeneral($("#fecha").val());
    listarDistrDetalle($("#fecha").val());

    listarPagos($("#fecha").val());

    addDataGeneral();
    addDataDetalle();
});

init();