<?php
    require "../config/ConexionHana.php";

    class VentasArticulos extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listar($empresa, $start_date, $end_date, $almacen)
        {
            try {
                $sql = 'SELECT
                    A."Name" "FolioR1",
                    A."U_SO1_FECHA" "FechaCreacion",
                    A."U_SO1_COMENTARIO" "Comentarios",
                    B."U_SO1_NUMEROARTICULO" "Articulo",
                    T1."U_Talla" "Talla",
                    T1."U_Color" "Color",
                    T2."ItmsGrpNam" "Linea",
                    B."U_SO1_PBSD" "PrecioUnitario",
                    B."U_SO1_LISTAPRECIO" "ListadePrecio",
                    B."U_SO1_IMPORTENETO" "Total",
                    B."U_SO1_CANTIDAD" "Cantidad",
                    T0."CardCode" "CodigoCliente",
                    T0."CardName" "NombreCliente",
                    T4."SlpName" "Vendedor",
                    C."Name" "Almacen"
                FROM "'.$empresa.'"."@SO1_01VENTA" A
                JOIN "'.$empresa.'"."@SO1_01VENTADETALLE" B ON (A."Name" =  B."U_SO1_FOLIO")
                JOIN "'.$empresa.'"."OCRD" T0 ON A."U_SO1_CLIENTE" = T0."CardCode"
                JOIN "'.$empresa.'"."OITM" T1 ON B."U_SO1_NUMEROARTICULO" = T1."ItemCode"
                JOIN "'.$empresa.'"."OITB" T2 on T2."ItmsGrpCod" = T1."ItmsGrpCod"
                JOIN "'.$empresa.'"."OMRC" T3 on T3."FirmCode" = T1."FirmCode"
                join "'.$empresa.'"."OSLP" T4 ON A."U_SO1_VENDEDOR" = T4."SlpCode"
                JOIN "'.$empresa.'"."@SO1_01SUCURSAL" C ON A."U_SO1_SUCURSAL" = C."Code"
                WHERE A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND C."Code" IN ('.$almacen.')
                AND "U_SO1_TIPO" IN (\'CA\',\'CR\');';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function Sucursales($empresa)
        {
            try {
                $sql = 'SELECT "Code", "Name" FROM "'.$empresa.'"."@SO1_01SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
