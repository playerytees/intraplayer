var tabla, listarsob;
var arreglotag = [];
function init()
{
    listaretiqueta();
    $(".loader").hide();
    $("#btn-imprimir").prop("disabled", true);
    barcode();
    listar();
    mostrartabla(false);
}

function mostrartabla(flag)
{
    if (flag) {
        $("#divlistarsobrante").show();
        $("#btn_ver").hide();
        $("#btn_ocultar").show();
        $("#etiquetasobrantes").hide();
    } else {
        $("#divlistarsobrante").hide();
        $("#btn_ver").show();
        $("#btn_ocultar").hide();
        $("#etiquetasobrantes").show();
    }
}

function listar()
{
    listarsob = $("#listarsobrante").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "frtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/sobrante.php?opcion=listar",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 5,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

function listaretiqueta()
{
    tabla = $("#listaretiqueta").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "frtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/sobrante.php?opcion=listaretiqueta",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 5,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

function agregarinfo()
{
    $("input:checkbox:checked").each(function() {
        $.post("../ajax/sobrante.php?opcion=agregarinfo", {id:$(this).val()},
            function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                var tabla = `<tr class="trdata">
                    <td>${data.partida}</td>
                    <td>${data.color}</td>
                    <td>${data.estilo}</td>
                    <td>${data.talla}</td>
                    <td><input style="width: 30px; border:none;" name="cantsob[]" id="cantsob" type="text" value=${data.sobrante}></td>
                </tr>`;

                $("#tabla_sobrante").append(tabla);

                $("#btn-imprimir").prop("disabled", false);

                arreglotag.push({barcode: data.barcode, partida: data.partida, color: data.color, estilo: data.estilo, talla: data.talla, sobrante: 0});
            }
        );
    });
}

function imprimir()
{
    $("#printetiqueta").printArea();

    let i = 0;
    $("input[name='cantsob[]'").each(function() {
        arreglotag[i].sobrante = $(this).val();
        i++;
    });

    guardar();

    $("#btn-agregar").prop("disabled", true);
    $("input[name='cantsob[]'").attr("readonly", true);
}

function nuevo()
{
    $(".trdata").remove();
    $('input:checkbox').removeAttr('checked');
    arreglotag.length = 0;
    barcode();
    $("#btn-imprimir").prop("disabled", true);
}

function guardar()
{
    $.ajax({
        type: "POST",
        url: "../ajax/sobrante.php?opcion=guardar",
        data: {"barcode": $("#barcode_sobrante").attr('alt'), "arreglotag": JSON.stringify(arreglotag)},
        success: function (response) {
            listarsob.ajax.reload();
            console.log(response);
            alert(response);
        }
    });
}

function barcode()
{
    $.ajax({
        type: "POST",
        url: "../ajax/sobrante.php?opcion=ultimoid",
        success: function (response) {
            response = JSON.parse(response);
            let proximoid = parseInt(response.id) + 1;
            console.log(proximoid);
            $("#barcode_sobrante").attr("alt", "sob"+proximoid);
            JsBarcode("#barcode_sobrante", "sob"+proximoid, { width: 1.5, height: 23, displayValue: false, margin: 0 });
        }
    });
}

init();