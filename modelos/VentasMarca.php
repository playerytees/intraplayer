<?php
    require "../config/ConexionHana.php";

    class VentasMarca extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listar($empresa, $start_date, $end_date, $almacen, $marca)
        {
            try {
                $sqlmarca = ($almacen == "'-1'") ? "":' AND T1."WhsCode" IN ('.$almacen.') ';
                $sql = 'SELECT
                    T0."DocDate",
                    T0."CardCode",
                    T0."CardName",
                    T1."WhsCode",
                    T1."ItemCode",
                    T1."Dscription",
                    T1."Quantity",
                    T0."CANCELED",
                    T3."GroupName",
                    T0."DocNum",
                    T5."FirmName",
                    T6."ItmsGrpNam",
                    T4."U_Color",
                    T4."U_Talla"
                FROM "'.$empresa.'"."OINV" T0
                INNER JOIN "'.$empresa.'"."INV1" T1 ON T0."DocEntry" = T1."DocEntry"
                INNER JOIN "'.$empresa.'"."OCRD" T2 ON T0."CardCode" = T2."CardCode"
                INNER JOIN "'.$empresa.'"."OCRG" T3 ON T2."GroupCode" = T3."GroupCode"
                INNER JOIN "'.$empresa.'"."OITM" T4 ON T1."ItemCode" = T4."ItemCode"
                INNER JOIN "'.$empresa.'"."OMRC" T5 ON T4."FirmCode" = T5."FirmCode"
                INNER JOIN "'.$empresa.'"."OITB" T6 ON T4."ItmsGrpCod" = T6."ItmsGrpCod"
                WHERE T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                '.$sqlmarca.'
                AND T5."FirmCode" = '.$marca.';';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarAlmacen($empresa)
        {
            try {
                $sql = 'SELECT "WhsCode", "WhsName" FROM "'.$empresa.'"."OWHS" WHERE "WhsCode" NOT IN (\'CO10\', \'DE09\', \'MP09\', \'OA06\', \'QU03\', \'R01\', \'TR08\', \'TR09\', \'TR10\', \'EX07\', \'R10\');';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarMarca($empresa)
        {
            try {
                $sql = 'SELECT "FirmCode", "FirmName" FROM "'.$empresa.'"."OMRC" WHERE "DataSource" = \'I\';';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
