var arreglotallas = [];
function init()
{
    $(".loader").hide();

    /************************************
    *        Datos Etiqueta Chica       *
    *************************************/
    $("#txtcliente").keyup(function () {
        var value = $(this).val();
        $("#lblcliente").text(value);
        localStorage.setItem("cliente", value);
    });

    $("#txtetiqueta").keyup(function () {
        var value = $(this).val();
        $("#lbletiqueta").text(value);
        localStorage.setItem("etiqueta", value);
    });

    $("#txtcomposicion").keyup(function () { 
        var value = $(this).val();
        $("#lblcomposicion").text(value);
        localStorage.setItem("composicion", value);
    });

    $("#txttalla").keyup(function () {
        var value = $(this).val();
        $("#lbltalla").text(value);
        localStorage.setItem("talla", value);
    });

    $("#txtcant").keyup(function () {
        var value = $(this).val();
        $("#lblcant").text( new Intl.NumberFormat("en-US").format( value ) );
        localStorage.setItem("cantidad", value);
    }); // .keyup()

    $("#txtfolio-ch").keyup(function (e) { 
        var value = $(this).val();
        $("#lblfolio-ch").text(value);
        localStorage.setItem("folio-ch", value);
    });

    $("#txt-oc-ch").keyup(function (e) { 
        var value = $(this).val();
        $("#lbl-oc-ch").text(value);
        localStorage.setItem("oc-ch", value);
    });

    /************************************
    *        Datos Etiqueta Grande      *
    *************************************/

    $("#txtcliente01").keyup(function (e) { 
        var value = $(this).val();
        $("#lblcliente01").text(value);
        localStorage.setItem("cliente01", value);
    });

    $("#txtetiqueta01").keyup(function (e) { 
        var value = $(this).val();
        $("#lbletiqueta01").text(value);
        localStorage.setItem("etiqueta01", value);
    });

    $("#txtcomposicion01").keyup(function (e) { 
        var value = $(this).val();
        $("#lblcomposicion01").text(value);

        localStorage.setItem("composicion01", value);
    });

	$("#txtfolio").keyup(function (e) { 
        var value = $(this).val();
        $("#lblfolio").text(value);

        localStorage.setItem("folio", value);
    });

    /************************************
    *      Datos Etiqueta 5 X 2.5 cm     *
    *************************************/

    $("#txtnombre03").keyup(function (e) { 
        $("#lblnombre03").text($(this).val());

        localStorage.setItem("nombre03", $(this).val());
    });

    $("#txtmaquina03").keyup(function (e) { 
        $("#lblmaquina03").text($(this).val());

        localStorage.setItem("maquina03", $(this).val());
    });
    
    $("#txtfecha03").keyup(function (e) { 
        $("#lblfecha03").text($(this).val());

        localStorage.setItem("fecha03", $(this).val());
    });

    $("#txtfolio03").keyup(function (e) { 
        $("#lblfolio03").text($(this).val());

        localStorage.setItem("folio03", $(this).val());
    });

    $("#txtrollo03").keyup(function (e) {
        $("#lblrollo03").text($(this).val());

        localStorage.setItem("rollo03", $(this).val());
    });

    $("#txtcantidad03").keyup(function (e) {
        $("#lblcantidad03").text($(this).val());

        localStorage.setItem("cantidad03", $(this).val());
    });

    /************************************
    *   Datos Etiqueta 2.5cm X 2.5cm    *
    *************************************/
   $("#txtcliente04").keyup(function (e) { 
       $("#lblcliente04").text($(this).val());
       
       localStorage.setItem("cliente04", $(this).val());
   });

   $("#txtitem04").keyup(function (e) { 
        $("#lblitem04").text($(this).val());
        
        localStorage.setItem("item04", $(this).val());
    });

    $("#txtoc04").keyup(function (e) { 
        $("#lbloc04").text($(this).val());
        
        localStorage.setItem("oc04", $(this).val());
    });

    $("#txtcantidad04").keyup(function (e) { 
        $("#lblcantidad04").text($(this).val());
        
        localStorage.setItem("cantidad04", $(this).val());
    });

    $("#txtfecha04").keyup(function (e) { 
        $("#lblfecha04").text($(this).val());
        
        localStorage.setItem("fecha04", $(this).val());
    });

    /************************************
    *        Etiqueta Chica             *
    *************************************/

    $("#txtcliente").val( localStorage.getItem("cliente") );
    $("#txtetiqueta").val( localStorage.getItem("etiqueta") );
    $("#txtcomposicion").val( localStorage.getItem("composicion") );
    $("#txttalla").val( localStorage.getItem("talla") );
    $("#txtcant").val( localStorage.getItem("cantidad") );
    $("#txtfolio-ch").val( localStorage.getItem("folio-ch") );
    $("#txt-oc-ch").val( localStorage.getItem("oc-ch") );

    $("#lblcliente").text( localStorage.getItem("cliente") );
    $("#lbletiqueta").text( localStorage.getItem("etiqueta") );
    $("#lblcomposicion").text( localStorage.getItem("composicion") );
    $("#lbltalla").text( localStorage.getItem("talla") );
    $("#lblcant").text( new Intl.NumberFormat("en-US").format( localStorage.getItem("cantidad") ) );
    $("#lblfolio-ch").text( localStorage.getItem("folio-ch") );
    $("#lbl-oc-ch").text( localStorage.getItem("oc-ch") );

    /************************************
    *        Datos Etiqueta Grande      *
    *************************************/

    $("#txtcliente01").val( localStorage.getItem("cliente01") );
    $("#txtetiqueta01").val( localStorage.getItem("etiqueta01") );
    $("#txtcomposicion01").val( localStorage.getItem("composicion01") );
    $("#txttalla01").val( localStorage.getItem("talla01") );
    $("#txtnumCajones").val( localStorage.getItem("numCajones") );
    $("#txtetiquetasxcajon").val( localStorage.getItem("etiquetasxcajon") );
    $("#txtfolio").val( localStorage.getItem("folio") );

    $("#lblcliente01").text( localStorage.getItem("cliente01") );
    $("#lbletiqueta01").text( localStorage.getItem("etiqueta01") );
    $("#lblcomposicion01").text( localStorage.getItem("composicion01") );
    $("#lblfecha").text(Fecha());
    $("#lblhora").text(Hora());
    $("#lblfolio").text(localStorage.getItem("folio"));

    /************************************
    *     Datos Etiqueta 5 X 2.5 cm     *
    *************************************/

    $("#txtnombre03").val(localStorage.getItem("nombre03"));
    $("#txtmaquina03").val(localStorage.getItem("maquina03"));
    $("#txtfecha03").val(localStorage.getItem("fecha03"));
    $("#txtfolio03").val(localStorage.getItem("folio03"));
    $("#txtrollo03").val(localStorage.getItem("rollo03"));
    $("#txtcantidad03").val(localStorage.getItem("cantidad03"));

    $("#lblnombre03").text(localStorage.getItem("nombre03"));
    $("#lblmaquina03").text(localStorage.getItem("maquina03"));
    $("#lblfecha03").text(localStorage.getItem("fecha03"));
    $("#lblfolio03").text(localStorage.getItem("folio03"));
    $("#lblrollo03").text(localStorage.getItem("rollo03"));
    $("#lblcantidad03").text(localStorage.getItem("cantidad03"));

    /************************************
    *     Etiqueta 2.5cm X 2.5cm        *
    *************************************/

    $("#txtcliente04").val(localStorage.getItem("cliente04"));
    $("#txtitem04").val(localStorage.getItem("item04"));
    $("#txtoc04").val(localStorage.getItem("oc04"));
    $("#txtcantidad04").val(localStorage.getItem("cantidad04"));
    $("#txtfecha04").val(localStorage.getItem("fecha04"));

    $("#lblcliente04").text(localStorage.getItem("cliente04"));
    $("#lblitem04").text(localStorage.getItem("item04"));
    $("#lbloc04").text(localStorage.getItem("oc04"));
    $("#lblcantidad04").text(localStorage.getItem("cantidad04"));
    $("#lblfecha04").text(localStorage.getItem("fecha04"));
}

function imprimirEtiquetaChica()
{
    var cliente = $("#txtcliente").val();
    var etiqueta = $("#txtetiqueta").val();
    var composicion = $("#txtcomposicion").val();
    var talla = $("#txttalla").val();
    var cantidad = $("#lblcant").text();
    var folioch = $("#txtfolio-ch").val();
    var oc = $("#txt-oc-ch").val();
    window.open("impresiones/etiquetaChica.php?cliente="+cliente+"&etiqueta="+etiqueta+"&composicion="+encodeURI(composicion)+"&talla="+talla+"&cantidad="+cantidad+"&folio="+folioch+"&oc="+encodeURI(oc)+"","_reporte","{opciones}");
}

function imprimirEtiquetaGrande()
{
    var cliente = $("#txtcliente01").val();
    var etiqueta = $("#txtetiqueta01").val();
    var composicion01 = $("#txtcomposicion01").val();
    var fecha = $("#lblfecha").text();
    var hora = $("#lblhora").text();
    var folio = $("#txtfolio").val();
    var sumaNumCajones = 0;
    var sumaEtiquetasxcajon = 0;
    var sumatotal = 0;

    for (var i = 0; i < arreglotallas.length; i++) {
        sumaNumCajones += parseInt(arreglotallas[i].numCajones);
        sumaEtiquetasxcajon += parseInt(arreglotallas[i].etiquetasxcajon);
        sumatotal += parseInt(arreglotallas[i].total);
    }

    window.open("impresiones/etiquetaGrande.php?cliente="+cliente+"&etiqueta="+etiqueta+"&composicion01="+encodeURI(composicion01)+"&fecha="+encodeURI(fecha)+"&hora="+encodeURI(hora)+"&folio="+encodeURI(folio)+"&sumaNumCajones="+sumaNumCajones+"&sumaEtiquetasxcajon="+sumaEtiquetasxcajon+"&sumatotal="+sumatotal+"&talla="+JSON.stringify(arreglotallas)+"","_reporte","{opciones}");
}

function imprimirEtiqueta5x25cm() {
    var nombre = $("#txtnombre03").val();
    var maquina = $("#txtmaquina03").val();
    var fecha = $("#txtfecha03").val();
    var folio = $("#txtfolio03").val();
    var rollo = $("#txtrollo03").val();
    var cantidad = $("#txtcantidad03").val();

    window.open(`impresiones/etiqueta5x25cm.php?nombre=${nombre}&maquina=${maquina}&fecha=${encodeURI(fecha)}&folio=${folio}&rollo=${rollo}&cantidad=${cantidad}`, "_reporte", "{opciones}");
}

function imprimirEtiqueta25cmx25cm() {
    var cliente = $("#txtcliente04").val();
    var item = $("#txtitem04").val();
    var oc = $("#txtoc04").val();
    var cantidad = $("#txtcantidad04").val();
    var fecha = $("#txtfecha04").val();

    window.open(`impresiones/etiqueta25cmx25cm.php?cliente=${encodeURI(cliente)}&item=${encodeURI(item)}&oc=${encodeURI(oc)}&cantidad=${encodeURI(cantidad)}&fecha=${encodeURI(fecha)}`, "_reporte", "{opciones}");
}

function soloNumeros(e)
{
    var key = window.Event ? e.which : e.keyCode 
    return ((key >= 48 && key <= 57) || (key==8)) 
}

function agregartallas()
{
    var talla01 = $("#txttalla01").val();
    var numCajones = $("#txtnumCajones").val();
    var etiquetasxcajon = $("#txtetiquetasxcajon").val();
    var total = parseInt(numCajones) * parseInt(etiquetasxcajon);
    
    var sumaNumCajones = 0;
    var sumaEtiquetasxcajon = 0;
    var sumatotal = 0;

    localStorage.setItem("talla01", talla01);
    localStorage.setItem("numCajones", numCajones);
    localStorage.setItem("etiquetasxcajon", etiquetasxcajon);
    localStorage.setItem("total", total);

    var tabla = `<tr class="trdata">
    <td>${talla01}</td>
    <td>${new Intl.NumberFormat("en-US").format(numCajones)}</td>
    <td>${new Intl.NumberFormat("en-US").format(etiquetasxcajon)}</td>
    <td>${new Intl.NumberFormat("en-US").format(total)}</td>
    </tr>`;

    arreglotallas.push({talla01: talla01, numCajones: numCajones, etiquetasxcajon: etiquetasxcajon, total: total});

    for (var i = 0; i < arreglotallas.length; i++) {
        sumaNumCajones += parseInt(arreglotallas[i].numCajones);
        sumaEtiquetasxcajon += parseInt(arreglotallas[i].etiquetasxcajon);
        sumatotal += parseInt(arreglotallas[i].total);
    }

    $("#lblsumaNumCajones").text(new Intl.NumberFormat("en-US").format(sumaNumCajones));
    $("#lblsumaEtiquetasxcajon").text(new Intl.NumberFormat("en-US").format(sumaEtiquetasxcajon));
    $("#lblsumatotal").text(new Intl.NumberFormat("en-US").format(sumatotal));

    $("#total").before(tabla);
}

function Hora() {
    var momentoActual = new Date();
    var hora = momentoActual.getHours();
    var minuto = momentoActual.getMinutes()<10?"0"+momentoActual.getMinutes():momentoActual.getMinutes();
    var segundo = momentoActual.getSeconds();

    var horaImprimible = hora + ":" + minuto;// + " : " + segundo;
    // document.form_reloj.reloj.value = horaImprimible
    return horaImprimible;
}

function Fecha() {
    var date =  new Date();

    var year = date.getFullYear();
    var month = (date.getMonth() + 1)<10?"0"+(date.getMonth() + 1):(date.getMonth() + 1);
    var day = date.getDate();

    return day + "/" + month + "/" + year;
}

function dateToYMD(d) {
    var date = new Date(d);
    var d = date.getDate()+1;
    var m = date.getMonth() + 1; //Month from 0 to 11
    var y = date.getFullYear();
    return (d <= 9 ? '0' + d : d) + '/' + (m<=9 ? '0' + m : m) + '/' + y;
}

init();