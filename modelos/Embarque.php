<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");
if (!isset($_SESSION)) { session_start(); }
Class Embarque
{
    private $tabla;
    private $tablatemporary;
    public function __construct()
    {   
        $this->tabla = "Sin_Permiso";
        if ($_SESSION['user'] == 'embarques2') {
            $this->tabla = "embarque_pendiente2";
        }
        if ($_SESSION['user'] == 'Embarques') {
            $this->tabla = "embarque_pendiente";
        }
        $this->tablatemporary = ($_SESSION['user'] == 'embarques2') ? "temporary2" : "temporary";
    }

    public function insertartemp($barcode, $partida, $estilo, $color, $talla, $piezas, $cajas, $partidalimpia, $codigo)
    {
        $sql = "INSERT INTO $this->tabla VALUES (null, '$barcode', '$partida', '$estilo', '$color', '$talla', '$piezas', '$cajas', 1, now(), now(), '".$_SESSION["user"]."', '$partidalimpia', '$codigo') ON DUPLICATE KEY UPDATE cajas=cajas+'$cajas'";
        return ejecutarConsulta($sql);
    }

    public function editartemp($id)
    {
        $sql = "UPDATE $this->tabla SET campo = 'campo', modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function desactivartemp($id)
    {
        $sql = "UPDATE $this->tabla SET cajas = 0, modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function activartemp($id)
    {
        $sql = "UPDATE $this->tabla SET estado = 1, modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function listartemp(){
        $sql = "SELECT id, barcode, partida, estilo, color, talla, (piezas*cajas) AS piezas, piezas AS pza_x_caja, cajas, partidalimpia, codigo FROM $this->tabla WHERE cajas > 0 AND sesion = '".$_SESSION["user"]."'";
        return ejecutarConsulta($sql);
    }

    public function actualizartemp($id, $cajas)
    {
        $sql = "UPDATE $this->tabla SET cajas = cajas - '$cajas', modified = now() WHERE id = '$id'";
        return ejecutarConsulta($sql);
    }

    public function insertartemporary($barcode, $partida, $estilo, $color, $talla, $piezas, $cajas, $partidalimpia, $codigo)
    {
        $sql = "INSERT INTO $this->tablatemporary VALUES (NULL, '$barcode', '$partida', '$estilo', '$color', '$talla', '$piezas', '$cajas', now(), now(), '".$_SESSION["user"]."', '$partidalimpia', '$codigo') ON DUPLICATE KEY UPDATE cajas=cajas+'$cajas', modified = now()";
        return ejecutarConsulta($sql);
    }

    public function listartemporary()
    {
        $sql = "SELECT id, barcode, partida, estilo, color, talla, (piezas*cajas) AS piezas, piezas AS pza_x_caja, cajas, partidalimpia, codigo FROM $this->tablatemporary WHERE cajas > 0 AND sesion = '".$_SESSION["user"]."'";
        return ejecutarConsulta($sql);
    }

    public function actualizartemporary($id, $cajas)
    {
        $sql = "UPDATE $this->tablatemporary SET cajas = cajas - '$cajas', modified = now() WHERE id = '$id'";
        return ejecutarConsulta($sql);
    }

    public function consultarexistencias($id)
    {
        $sql = "SELECT cajas FROM $this->tabla WHERE id = '$id'";
        return ejecutarConsultaSimpleFila($sql);
    }
}
?>