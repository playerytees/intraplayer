<table style="width: 100%; height: 100%;">
    <tr>
        <td>
            <table style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; font-size:20px;">
            <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Tono</td>
                    <td style="font-weight: bold;">Nombre:</td>
                    <td class="lblnombre03" style="border-bottom: 1px solid #000; min-width: 180px;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td style="width:90px;">Definición</td>
                    <td style="font-weight: bold;">Máquina:</td>
                    <td class="lblmaquina03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Anclaje</td>
                    <td style="font-weight: bold;">Fecha:</td>
                    <td class="lblfecha03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Reverso</td>
                    <td style="font-weight: bold;">No. Folio:</td>
                    <td class="lblfolio03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Desfase</td>
                    <td style="font-weight: bold;">No. Rollo:</td>
                    <td class="lblrollo03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold;">Talla:_______ <!-- <hr style="width: 90px; border-bottom: 1px solid #000;"> --></td>
                    <td style="font-weight: bold; padding-top: 7px; width:90px;">Cantidad:</td>
                    <td class="lblcantidad03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr style="padding-top: 7px;">
                    <td colspan="2" style="font-weight: bold;">Composición:</td>
                    <td colspan="2" style="border-bottom: 1px solid #000;"></td>
                </tr>
            </table>
        </td>
        <td>&nbsp;</td>
        <td>
            <table  style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; margin-left: 25px; font-size:20px;">
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Tono</td>
                    <td style="font-weight: bold;">Nombre:</td>
                    <td class="lblnombre03" style="border-bottom: 1px solid #000; min-width: 180px;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td style="width:90px;">Definición</td>
                    <td style="font-weight: bold;">Máquina:</td>
                    <td class="lblmaquina03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Anclaje</td>
                    <td style="font-weight: bold;">Fecha:</td>
                    <td class="lblfecha03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Reverso</td>
                    <td style="font-weight: bold;">No. Folio:</td>
                    <td class="lblfolio03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td align="center"><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></td>
                    <td>Desfase</td>
                    <td style="font-weight: bold;">No. Rollo:</td>
                    <td class="lblrollo03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr>
                    <td colspan="2" style="font-weight: bold;">Talla:_______ <!-- <hr style="width: 90px; border-bottom: 1px solid #000;"> --></td>
                    <td style="font-weight: bold; padding-top: 7px; width:90px;">Cantidad:</td>
                    <td class="lblcantidad03" style="border-bottom: 1px solid #000;"></td>
                </tr>
                <tr style="padding-top: 7px;">
                    <td colspan="2" style="font-weight: bold;">Composición:</td>
                    <td colspan="2" style="border-bottom: 1px solid #000;"></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script src="../../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script>
function init() {
    var nombre = getParameterByName("nombre");
    var maquina = getParameterByName("maquina");
    var fecha = getParameterByName("fecha");
    var folio = getParameterByName("folio");
    var rollo = getParameterByName("rollo");
    var cantidad = getParameterByName("cantidad");

    $(".lblnombre03").text(nombre);
    $(".lblmaquina03").text(maquina);
    $(".lblfecha03").text(fecha);
    $(".lblfolio03").text(folio);
    $(".lblrollo03").text(rollo);
    $(".lblcantidad03").text(cantidad);

    if (window.print) {
        setTimeout(function(){window.print();window.close();},300); // 500ms = 0.5s - 1000 = 1s
    }
}

function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
init();
</script>