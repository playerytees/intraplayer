<?php
//error_reporting(0);
$enlace = mysqli_connect("localhost", "root", "", "intraplayer");
 
$id_folio = $_GET['folio'];

$queryFolio = mysqli_query($enlace, "SELECT * FROM embarque WHERE id = '$id_folio'");

$arrayFolio = mysqli_fetch_array($queryFolio);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Plantilla</title>
    <script src="../public/assets/js/lib/tableexport/xlsx.core.min.js"></script>
    <script src="../public/assets/js/lib/tableexport/Blob.min.js"></script>
    <script src="../public/assets/js/lib/tableexport/FileSaver.min.js"></script>
    <link rel="stylesheet" href="../public/assets/css/lib/tableexport/tableexport.min.css">
    <style>
        @media print {
            @page { margin: 15px; }
            body{
                font-size: 10px;
            }
            table{
                border-collapse: collapse;
            }
            img{
                width: 200px;
            }
            #btn_excel, .button-default{
                display: none;
            }
        }
    </style>
</head>
<body>
    <table style="width: 100%; font-family: Arial; font-weight: bold; border-collapse: collapse;">
        <tr>
            <td rowspan="3"><img src="../public/images/punto_textil.jpg" alt="Punto Textil" width="250"></td>
            <td rowspan="3">ORIGEN: <br>
            AV. PEDREGAL 210 SECCION 3RA SAN LUIS TEOLOCHOLCO TLAXCALA <br>
            C.P. 90850  RFC  GPL 060609 2K7  TEL.2464616442
            </td>
            <td></td>
            <td>CHOFER:</td>
            <td style="font-weight: normal;"><?php echo $arrayFolio['chofer']; ?></td>
        </tr>
        <tr>

            
            <td>CONFECCIONES PUNTO TEXTIL S.A. DE C.V.</td>
            <td>FOLIO:</td>
            <td style="font-weight: normal;"><?php echo $arrayFolio['id'] ?></td>
        </tr>
        <tr>

            
            <td></td>
            <td>MAQUILA:</td>
            <td style="font-weight: normal;"><?php echo $arrayFolio['maquila']; ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>FECHA:</td>
            <td style="font-weight: normal;"><?php echo $arrayFolio['fecha']; ?></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;"><?php echo ($arrayFolio['estado'] == 0)? "ORDEN DE EMBARQUE" : "LISTA DE EMBARQUE" ?></td>
        </tr>
    </table>
    <br>
<?php
$consulta = "SELECT id, barcode, partida, estilo, color, trim(talla), (piezas*cajas) AS piezas, cajas FROM detalle_embarque WHERE id_embarque = '$id_folio' ORDER BY id ASC";

$sql = mysqli_query($enlace, "SELECT estilo, SUM(piezas*cajas) as suma, SUM(cajas) as cajas, color, partida FROM detalle_embarque WHERE id_embarque = '$id_folio' GROUP BY partida, estilo, color ORDER BY id ASC;");
 
$resultado = mysqli_query($enlace, $consulta);
 
$arregloTallas = array("3","4", "5", "6","7", "8","9", "10","11", "12","13", "14","15","16","17","18","19","21","23","25","27","28","30","32","34","36","38","40","42","44","46","48","50", "XS", "S", "M", "L", "XL", "XXL", "3XL", "4XL", "5XL", "6XL", "UNI");
 
$arrayTallas[0]="talla";
$arraypartida[0]="partida";
$arrayestilo[0]="estilo";
$arraycolor[0]="color";
$arraysuma[0]="suma";
$arraycajas[0]="cajas";

while($data = mysqli_fetch_array($sql))
{
    array_push($arrayestilo, $data[0]);
    array_push($arraysuma, $data[1]);
    array_push($arraycajas, $data[2]);
    array_push($arraycolor, $data[3]);
    array_push($arraypartida, $data[4]);
}

$numRows=0;
$dataBDProx[][]="";

while($dataProx = mysqli_fetch_array($resultado))
{
    $dataBDProx [$numRows][0]= $dataProx[0];//id
    $dataBDProx [$numRows][1]= $dataProx[1];//barcode
    $dataBDProx [$numRows][2]= $dataProx[2];//partida
    $dataBDProx [$numRows][3]= $dataProx[3];//estilo
    $dataBDProx [$numRows][4]= $dataProx[4];//color
    $dataBDProx [$numRows][5]= $dataProx[5];//talla
    $dataBDProx [$numRows][6]= $dataProx[6];//piezas
    $dataBDProx [$numRows][7]= $dataProx[7];//cajas
    if(in_array($dataProx[5], $arrayTallas)){
 
    }else {
        array_push($arrayTallas, $dataProx[5]);
    }
    /* if (in_array($dataProx[2], $arraypartida)) {

    }else {
      array_push($arraypartida, $dataProx[2]);
    } */
    /* if (in_array($dataProx[4], $arraycolor)) {
        # code...
    } else {
        array_push($arraycolor, $dataProx[4]);
    } */

    $numRows++;
}
 
$matriz[0][0] = "";
$matriz[0][1] = "";
$matriz[0][2] = "PARTIDA";
$matriz[0][3] = "ESTILO";
$matriz[0][4] = "COLOR";
$matriz[0][5] = "TOTAL PZAS";
$matriz[0][6] = "CAJAS";
$colMat = 7;
$filMat = 1;

for ($i=0; $i < count($arregloTallas); $i++) {
    if(in_array($arregloTallas[$i], $arrayTallas))
    {
        $matriz[0][$colMat] = $arregloTallas[$i];
        $colMat++;
    }
}

for($tit =1; $tit < count($arraypartida); $tit++)
{
    $matriz[$tit][0] = "";//$dataBDProx[$tit-1][0];//id
    $matriz[$tit][1] = "";//$dataBDProx[$tit-1][1];//barcode
    $matriz[$tit][2] = $arraypartida[$tit];//partida
    $matriz[$tit][3] = $arrayestilo[$tit];//estilo
    $matriz[$tit][4] = $arraycolor[$tit];//color
    $matriz[$tit][5] = $arraysuma[$tit];//total piezas
    $matriz[$tit][6] = $arraycajas[$tit];//total cajas
}

for($rel =1; $rel < count($arraypartida); $rel++)
{
    for($cols =7; $cols < $colMat; $cols++)
    {
    $matriz[$rel][$cols] = "";
    }
}

//RECORRER PARA PONER NUMERO PIEZAS
for($pbd = 0; $pbd < $numRows; $pbd++)//10
{
    for($rel =1; $rel < count($arraypartida); $rel++)
    {
        if($dataBDProx[$pbd][2] == $matriz[$rel][2] AND $dataBDProx[$pbd][3] == $matriz[$rel][3] AND $dataBDProx[$pbd][4] == $matriz[$rel][4])
        {
            for($cols = 7; $cols < $colMat; $cols++)
            {
                if($dataBDProx[$pbd][5] == $matriz[0][$cols])
                {
                    $matriz[$rel][$cols] = $dataBDProx[$pbd][6];
                    //$matriz[$rel][2] += $dataBDProx[$pbd][3];//total suma piezas del row
                }
            }

        }
    }
}

echo "<table style='width: 100%; font-family: Arial; border-collapse: collapse;' border='1' id='OrdenEmbarque'>
              <thead><tr>";
        for($fm = 0; $fm < 1; $fm++)
        {
          for($rm=0; $rm < $colMat; $rm++)
          {
            echo "<th>".$matriz[$fm][$rm]."</th>";
          }
        }
        echo "</tr></thead><tbody>";
        for($fm = 1; $fm < count($arraypartida); $fm++)
        {
          echo "<tr>";
          for($rm=0; $rm < $colMat; $rm++)
          {
            echo "<td>".$matriz[$fm][$rm]."</td>";
          }
          echo "</tr>";
        }
        echo "</tbody></table>";

?>
<br><br><br>
<table style='width: 100%; font-family: Arial; border-collapse: collapse; text-align: center;' id='tabla'>
    <tr>
        <td>RECIBE</td>
        <td>RECIBE</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>CHOFER - FABRICA</td>
        <td>ALMACEN - CEDIS</td>
    </tr>
</table>
<center><button type="button" id="btn_excel">Exportar a Excel</button></center>
</body>
</html>
<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="../public/assets/js/lib/tableexport/tableexport.min.js"></script>
<script>
$("#btn_excel").click(function (e) {
    e.preventDefault();
    $("#OrdenEmbarque").tableExport();
    $(this).prop("disabled", true);
});
</script>