<?php
require_once "../modelos/FolioEmbarque.php";

$folioEmbarque = new FolioEmbarque();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$fecha = isset($_POST['fecha'])?limpiarCadena($_POST['fecha']):"";
$maquila = isset($_POST['maquila'])?limpiarCadena($_POST['maquila']):"";
$chofer = isset($_POST['chofer'])?limpiarCadena($_POST['chofer']):"";
$comentarios = isset($_POST['comentarios'])?limpiarCadena($_POST['comentarios']):"";																					

switch ($_GET['opcion']) {
    case 'guardar':
        $response = $folioEmbarque->insertar($fecha, $maquila, $chofer, $comentarios);

        if ($response == 1) {
            $response = 1;
            $folioEmbarque->deletetemporary();
        } else {
            $response = 0;
        }
        
        echo $response;
        break;
    case 'editar':
        $response = $folioEmbarque->editar($id, $fecha, $maquila, $chofer, $comentarios);
        echo $response ? "Embarque actualizado" : "Embarque NO se pudo actualizar";
        break;
    case 'desactivar':
        $response = $folioEmbarque->desactivar($id);
        echo $response ? "Se ha guardado Correctamente" : "No se ha podido guardar";
        break;
    case 'activar':
        $response = $folioEmbarque->activar($id);
        echo $response ? "embarque activada" : "embarque no se pudo activar";
        break;
    case 'mostrar':
        $response = $folioEmbarque->mostrar($id);
        echo json_encode($response);
        break;
    case 'listar':
        $response = $folioEmbarque->listar();
        $data = array();

        while ($reg=$response->fetch_object()) {
            $data[] = array(
                "0" => '<span data-toogle="tooltip" data-placement="top" title="'.$reg->sesion.'">'.$reg->id.'</span>',
                "1" => date_format(date_create($reg->fecha), 'd/m/Y'),
                "2" => $reg->maquila,
                "3" => $reg->chofer,
                "4" => ($reg->estado == 1 AND $_SESSION['user'] == $reg->sesion)?"<a href='detalleEmbarque.php?folio=".$reg->id."'><i class='fa fa-eye'></i></a><a href='orden.php?folio=".$reg->id."' title='Imprimir' target='_blank'><i class='fa fa-print ml-2'></i></a>":"<a href='orden.php?folio=".$reg->id."' title='Imprimir' target='_blank'><i class='fa fa-print ml-2'></i></a>",
                "5" => "<i class='fa fa-edit mx-1' title='Editar' style='cursor: pointer;' onclick='mostrar(".$reg->id.")'></i>",
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'ultimoid':
        $response = $folioEmbarque->ultimoid();
        echo json_encode($response);
        break;

    default:
        echo "No se encontro la opcion";
        break;
}
?>