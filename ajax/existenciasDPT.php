<?php
require_once '../modelos/ExistenciasDPT.php';
session_start();
$existenciasDPT = new ExistenciasDPT();

$idAlmacen = isset($_POST["idAlmacen"])?$_POST["idAlmacen"]:"";
$idMarca = isset($_POST["idMarca"])?$_POST["idMarca"]:"";
$idEstilo = isset($_POST["idEstilo"])?$_POST["idEstilo"]:"";
$idColor = isset($_POST["idColor"])?$_POST["idColor"]:"";
$talla = isset($_POST["talla"])?$_POST["talla"]:"";
$codigo = isset($_POST["codigo"])?$_POST["codigo"]:"";

switch ($_GET['opcion']) {
    case 'listar':
        if (empty($idColor)) {
            $idColor = array("'-1'");
        }
        if (empty($idEstilo)) {
            $idEstilo = array("'-1'");
        }
        $colores = implode( ",",array_filter($idColor) );
        $estilos = implode(",", array_filter($idEstilo));

        $response = $existenciasDPT->listar("'$idAlmacen'","'$idMarca'", "$estilos", "$colores");
        $estiloDescrColor = $existenciasDPT->estiloDescrColor("'$idAlmacen'","'$idMarca'", "$estilos", "$colores");
        $data = array();
        $arregloTallas = array("0","2","3","3M","4","5","6","6M","7","8","9","9M","10","11","12","12M","13","14","15","16","17","17M","18","19","21","23","24","24M","25","27","28","30","32","34","36","38","40","42","44","46","48","50","XS","S","M","L","XL","2XL","3XL","4XL","5XL","6XL","UNI");

        $Estilos[0]="ESTILO";
        $Descripciones[0]="DESCRIPCION";
        $Colores[0]="COLOR";
        $Tallas[0]="TALLA";
        $numRows = 0;
        $dataBD[][] = "";

        // Llenar estilo, color y descripcion
        while ($reg=$estiloDescrColor->fetch(PDO::FETCH_OBJ)) {
            array_push($Estilos, $reg->estilo);
            array_push($Colores, $reg->color);
            array_push($Descripciones, utf8_encode($reg->descripcion));
        }

        while($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $dataBD[$numRows][0] = $reg->estilo;
            $dataBD[$numRows][1] = utf8_encode($reg->descripcion);
            $dataBD[$numRows][2] = $reg->color;
            $dataBD[$numRows][3] = $reg->talla;
            $dataBD[$numRows][4] = $reg->existencia;

            if (in_array($reg->talla, $Tallas)) {
                # code...
            } else {
                array_push($Tallas, $reg->talla);
            }

            $numRows++;
        }

        $matriz[0][0] = "Estilo";
        $matriz[0][1] = "Descripcion";
        $matriz[0][2] = "Color";

        $colMat = 3;
        $rowMat = 1;

        // Llenar las Tallas si existen
        for ($i=0; $i < count($arregloTallas); $i++) { 
            if (in_array($arregloTallas[$i], $Tallas)) {
                $matriz[0][$colMat] = $arregloTallas[$i];
                $colMat++;
            }
        }

        $matriz[0][$colMat] = "Total";
        $colMat++;

        for ($j=1; $j < count($Estilos); $j++) { 
            $matriz[$j][0] = $Estilos[$j];
            $matriz[$j][1] = $Descripciones[$j];
            $matriz[$j][2] = $Colores[$j];
        }

        for ($rows=1; $rows < count($Estilos); $rows++) { 
            for ($cols=3; $cols < $colMat; $cols++) { 
                $matriz[$rows][$cols] = "0";
            }
        }

        // Recorrer para colocar el num de pzas
        for ($pbd=0; $pbd < $numRows; $pbd++) { // filas BD
            for ($rel=1; $rel < count($Estilos); $rel++) { // filas matriz
                if ($dataBD[$pbd][0] == $Estilos[$rel] AND $dataBD[$pbd][1] == $Descripciones[$rel] AND $dataBD[$pbd][2] == $Colores[$rel]) {
                    for ($cols=3; $cols < $colMat; $cols++) { // columnas matriz
                        if ($dataBD[$pbd][3] == $matriz[0][$cols]) { //Tallas
                            $matriz[$rel][$cols] = number_format($dataBD[$pbd][4], 0, ".", ",");
                            $matriz[$rel][$colMat-1] += $dataBD[$pbd][4];
                        }
                    }
                }
            }
        }

        $totalCol = array('','','TOTAL:');
        for ($k=3; $k < $colMat; $k++) { 
            $sum = 0;
            foreach ($matriz as $key => $value) {
                if($key != 0) {
                    $sum += (!is_int($value[$k]))?(intval(str_replace(',', '', $value[$k]))):$value[$k];
                }
            }
            array_push($totalCol, $sum);
        }

        array_push($matriz, $totalCol);

        echo "<thead style='background-color: rgba(6, 78, 125, 0.88); color: #fff;'>
                <tr>";
                    for ($fm=0; $fm < 1; $fm++) { 
                        for ($rm=0; $rm < $colMat; $rm++) { 
                            echo "<th>".$matriz[$fm][$rm]."</th>";
                        }
                    }
        echo "</tr>
            </thead>
            <tbody style='text-align: right;'>";
                for ($fm=1; $fm < count($Estilos); $fm++) { 
                    echo "<tr>";
                        for ($rm=0; $rm < $colMat; $rm++) { 
                            echo "<td>".$matriz[$fm][$rm]."</td>";
                        }
                    echo "</tr>";
                }
        echo "</tbody>";
        echo "<tfoot style='text-align: right;font-weight:bold;'>";
            for ($fm=count($Estilos); $fm < count($Estilos)+1; $fm++) { 
                echo "<tr>";
                    for ($rm=0; $rm < $colMat; $rm++) { 
                        echo "<td>".((is_int($matriz[$fm][$rm]))?number_format($matriz[$fm][$rm], 0, ".", ","):$matriz[$fm][$rm])."</td>";
                    }
                echo "</tr>";
            }
        echo "</tfoot>";
        break;
    case 'listarMarca':
        $response = $existenciasDPT->listarMarca();
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => $reg->FirmCode,
                '1' => $reg->FirmName
            );
        }

        echo json_encode($data);
        break;
    case 'listarEstilo':
        $response = $existenciasDPT->listarEstilo("'$idAlmacen'", "'$idMarca'");
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => $reg->ItmsGrpCod,
                '1' => utf8_encode($reg->ItmsGrpNam)
            );
        }

        echo json_encode($data);
        break;
    case 'listarColor':
        if (empty($idEstilo)) {
            $idEstilo = array("'-1'");
        }
        $estilos = implode(",", array_filter($idEstilo));
        $response = $existenciasDPT->listarColor("'$idAlmacen'", "'$idMarca'", "$estilos");
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => utf8_encode($reg->color),
                '1' => $reg->codigo
            );
        }

        echo json_encode($data);
        break;
    case 'listarAlmacen':
        $arrayName = array(
            ($_SESSION['5febrero']==1)?"'FEB'":"",
            ($_SESSION['cdmx']==1)?"'ISA2'":"",
            ($_SESSION['cuernavaca']==1)?"'CUE'":"",
            ($_SESSION['hermosillo']==1)?"'HE07'":"",
            ($_SESSION['isabel']==1)?"'ISA'":"",
            ($_SESSION['lorey']==1)?"'LOR'":"",
            ($_SESSION['monterrey']==1)?"'MO05'":"",
            ($_SESSION['oaxaca']==1)?"'OA06'":"",
            ($_SESSION['pachuca']==1)?"'PAC'":"",
            ($_SESSION['noria']==1)?"'NO04'":"",
            ($_SESSION['sanluis']==1)?"'SL02'":"",
            ($_SESSION['oaxaca2']==1)?"'OA2'":"",
            ($_SESSION['CedisDPT']==1 || $_SESSION['DPT']==1)?"'CE01'":"",
            ($_SESSION['sanluis2']==1)?"'SLP'":"",
        );
        $response = $existenciasDPT->listarSucursal(implode(",",array_filter($arrayName)));
        $data = array();

        foreach ($response as $reg) {
            $data[] = array(
                '0' => $reg->WhsCode,
                '1' => $reg->WhsName
            );
        }

        echo json_encode($data);
        break;
    case 'busquedaCodigo':
        $response = $existenciasDPT->busquedaPorCodigo("'$idAlmacen'","'$codigo%'");
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                "0" => $reg->Codigo,
                "1" => utf8_encode($reg->Descripcion),
                "2" => $reg->Existencia
            );
        }

        echo json_encode($data);
        break;
    case 'busquedaAlmacen':
        if (empty($idEstilo)) {
            $estilos = '';
        } else  {
            $estilos = implode(",", array_filter($idEstilo));
        }
        if (empty($idColor)) {
            $colores = '';
        } else {
            $colores = implode(",", array_filter($idColor));
        }
        $response = $existenciasDPT->busquedaPorAlmacen("'$idMarca'", $estilos, $colores);
        $data=array();

        foreach ($response as $reg) {
            $data[]=array(
                "0" => $reg->Codigo,
                "1" => '<div class="text-truncate">'.utf8_encode($reg->Descripcion).'</div>',
                "2" => $reg->Almacen,
                "3" => '<div class="text-right">'.number_format($reg->Existencia, 0, ".", ",").'</div>'
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;    
    case 'listarPorAlmacen':
        if (empty($idColor)) {
            $idColor = array("'-1'");
        }
        if (empty($idEstilo)) {
            $idEstilo = array("'-1'");
        }
        $colores = implode( ",",array_filter($idColor) );
        $estilos = implode(",", array_filter($idEstilo));

        $response = $existenciasDPT->listarPorAlmacen("'$idMarca'", "$estilos", "$colores");
        $estiloDescrColor = $existenciasDPT->estiloDescrColorAlmacen("'$idMarca'", "$estilos", "$colores");
        $data = array();
        $arregloTallas = array("0","2","3","3M","4","5","6","6M","7","8","9","9M","10","11","12","12M","13","14","15","16","17","18","19","21","23","25","27","28","30","32","34","36","38","40","42","44","46","48","50","XS","S","M","L","XL","2XL","3XL","4XL","5XL","6XL","UNI");

        $Almacenes[0]="ALMACEN";
        $Estilos[0]="ESTILO";
        $Descripciones[0]="DESCRIPCION";
        $Colores[0]="COLOR";
        $Tallas[0]="TALLA";
        $numRows = 0;
        $dataBD[][] = "";

        // Llenar estilo, color, descripcion y Almacen
        while ($reg=$estiloDescrColor->fetch(PDO::FETCH_OBJ)) {
            array_push($Almacenes, $reg->almacen);
            array_push($Estilos, $reg->estilo);
            array_push($Colores, $reg->color);
            array_push($Descripciones, utf8_encode($reg->descripcion));
        }

        while($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $dataBD[$numRows][0] = $reg->estilo;
            $dataBD[$numRows][1] = utf8_encode($reg->descripcion);
            $dataBD[$numRows][2] = $reg->color;
            $dataBD[$numRows][3] = $reg->talla;
            $dataBD[$numRows][4] = $reg->existencia;
            $dataBD[$numRows][5] = $reg->almacen;

            if (in_array($reg->talla, $Tallas)) {
                # code...
            } else {
                array_push($Tallas, $reg->talla);
            }

            $numRows++;
        }

        $matriz[0][0] = "Almacen";
        $matriz[0][1] = "Estilo";
        // $matriz[0][2] = "Descripcion";
        $matriz[0][2] = "Color";

        $colMat = 3;
        $rowMat = 1;

        // Llenar las Tallas si existen
        for ($i=0; $i < count($arregloTallas); $i++) { 
            if (in_array($arregloTallas[$i], $Tallas)) {
                $matriz[0][$colMat] = $arregloTallas[$i];
                $colMat++;
            }
        }

        $matriz[0][$colMat] = "Total";
        $colMat++;

        for ($j=1; $j < count($Estilos); $j++) { 
            $matriz[$j][0] = $Almacenes[$j];
            $matriz[$j][1] = $Estilos[$j];
            // $matriz[$j][2] = $Descripciones[$j];
            $matriz[$j][2] = $Colores[$j];
        }

        for ($rows=1; $rows < count($Estilos); $rows++) { 
            for ($cols=3; $cols < $colMat; $cols++) { 
                $matriz[$rows][$cols] = "0";
            }
        }

        // Recorrer para colocar el num de pzas
        for ($pbd=0; $pbd < $numRows; $pbd++) { // filas BD
            for ($rel=1; $rel < count($Estilos); $rel++) { // filas matriz
                if ($dataBD[$pbd][0] == $Estilos[$rel] AND $dataBD[$pbd][2] == $Colores[$rel] AND $dataBD[$pbd][5] == $Almacenes[$rel]) {
                    for ($cols=3; $cols < $colMat; $cols++) { // columnas matriz
                        if ($dataBD[$pbd][3] == $matriz[0][$cols]) { //Tallas
                            $matriz[$rel][$cols] = number_format($dataBD[$pbd][4], 0, ".", ",");
                            $matriz[$rel][$colMat-1] += $dataBD[$pbd][4];
                        }
                    }
                }
            }
        }

        $totalCol = array('','','TOTAL:');
        for ($k=3; $k < $colMat; $k++) { 
            $sum = 0;
            foreach ($matriz as $key => $value) {
                if($key != 0) {
                    $sum += (!is_int($value[$k]))?(intval(str_replace(',', '', $value[$k]))):$value[$k];
                }
            }
            array_push($totalCol, $sum);
        }

        array_push($matriz, $totalCol);

        echo "<thead style='background-color: rgba(6, 78, 125, 0.88); color: #fff;'>
                <tr>";
                    for ($fm=0; $fm < 1; $fm++) { 
                        for ($rm=0; $rm < $colMat; $rm++) { 
                            echo "<th>".$matriz[$fm][$rm]."</th>";
                        }
                    }
        echo "</tr>
            </thead>
            <tbody style='text-align: right;'>";
                for ($fm=1; $fm < count($Estilos); $fm++) { 
                    echo "<tr>";
                        for ($rm=0; $rm < $colMat; $rm++) { 
                            echo "<td>".$matriz[$fm][$rm]."</td>";
                        }
                    echo "</tr>";
                }
        echo "</tbody>";
        echo "<tfoot style='text-align: right;font-weight:bold;'>";
            for ($fm=count($Estilos); $fm < count($Estilos)+1; $fm++) { 
                echo "<tr>";
                    for ($rm=0; $rm < $colMat; $rm++) { 
                        echo "<td>".$matriz[$fm][$rm]."</td>";
                    }
                echo "</tr>";
            }
        echo "</tfoot>";
        break;
    case 'imprimirEtiqueta':
        if (empty($idEstilo)) {
            $estilos = '';
        } else  {
            $estilos = implode(",", array_filter($idEstilo));
        }
        if (empty($idColor)) {
            $colores = '';
        } else {
            $colores = implode(",", array_filter($idColor));
        }

        $colores = implode( ",",array_filter($idColor) );
        $estilos = implode(",", array_filter($idEstilo));

        $response = $existenciasDPT->imprimirEtiqueta("'$idMarca'", "$estilos", "$colores");
        $data=array();

        foreach ($response as $reg) {
            $data[]=array(
                "0" => $reg->ItemCode
            );
        }

        echo json_encode($data);
        break;
    default:
        echo "No existe la opcion seleccionada";
        break;
}