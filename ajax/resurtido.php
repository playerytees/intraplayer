<?php
require_once "../modelos/Resurtido.php";

$resurtido = new Resurtido();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$sku_factory = isset($_POST['sku_factory'])?limpiarCadena($_POST['sku_factory']):"";
$sku_custom = isset($_POST['sku_custom'])?limpiarCadena($_POST['sku_custom']):"";
$sugerido = isset($_POST['sugerido'])?limpiarCadena($_POST['sugerido']):"";
$inventario = isset($_POST['inventario'])?limpiarCadena($_POST['inventario']):"";
$faltante = isset($_POST['faltante'])?limpiarCadena($_POST['faltante']):"";
$propuesto = isset($_POST['propuesto'])?limpiarCadena($_POST['propuesto']):"";
$resurtir = isset($_POST['resurtir'])?limpiarCadena($_POST['resurtir']):"";
$pza_x_caja = isset($_POST['pza_x_caja'])?limpiarCadena($_POST['pza_x_caja']):"";
$existencias = isset($_POST['existencias'])?limpiarCadena($_POST['existencias']):"";
$id_sucursal = isset($_POST['id_sucursal'])?limpiarCadena($_POST['id_sucursal']):"";
$id_resurtidofolio = isset($_POST['id_resurtidofolio'])?limpiarCadena($_POST['id_resurtidofolio']):"";

switch ($_GET['opcion']) {
    case 'guardar':
        $datosTabla = json_decode($_POST['tabla_resurtido'], true);

        $idresurtidonew = $resurtido->idresurtidonew($id_sucursal);
    
        foreach ($datosTabla as $key => $value)
        {
            $response = $resurtido->insertar($value['manufactura'], $value['personalizado'], $value['linea'], $value['minimo'], $value['existencias'], $value['faltante'], $value['surtir'], $value['num_piezas'], $value['piezas_surtir'], $value['exis_cedis'], $value['dif_cedis'], $id_sucursal, $idresurtidonew);
        }

        echo $response;
        break;
    case 'mostrar':
        $response = $resurtido->mostrar($id_resurtidofolio);
        echo json_encode($response);
        break;
    case 'listarfolio':
        $response = $resurtido->listarfolio($id_sucursal);
        $data = array();
        
        while ( $reg=$response->fetch_object() ) {
            $data[] = array(
                '0' => $reg->id,
                '1' => date_format(date_create($reg->created), 'd/m/Y H:i:s'),
                '2' => $reg->descr,
                '3' => '<i class="fa fa-eye mx-1 fa-lg" title="Ver Detalles" style="cursor: pointer;" onclick="listar('.$reg->id.')"></i>',
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listar':
        $response = $resurtido->listar($id_resurtidofolio);
        $data = array();
        
        while ( $reg=$response->fetch_object() ) {
            $data[] = array(
                '0' => $reg->manufactura,
                '1' => $reg->personalizado,
                '2' => $reg->linea,
                '3' => $reg->exist_cedis,
                '4' => $reg->existencias,
                '5' => $reg->piezas_surtir,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'resurtir':
        $response = $resurtido->resurtir($id, $resurtir);
        echo $response ? "Actualizado Correctamente" : "No se pudo actualizar";
        break;
        
    default:
        # code...
        break;
}
?>