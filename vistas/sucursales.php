<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['resurtido']==1) {
?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="text-playerytees">Sucursales</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Resurtido</a></li>
                        <li class="active">Sucursales</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="animated fadeIn" id="listadoSucursales">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title text-playerytees d-sm-inline">Lista de Sucursales</strong>
                            <button id="btnagregar" class="btn btn-playerytees btn-sm float-sm-right" type="button" onclick="mostrarform(true)">Nueva Sucursal</button>
                        </div> <!-- .card-header -->
                        <div class="card-body">
                            <table id="tabla_sucursales" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th>No.</th>
                                <th>Nombre</th>
                                <th>Direccion</th>
                                <th>Encargado</th>
                                <th>Telefono</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-md-12 -->
            </div> <!-- .row -->
        </div> <!-- .animated .fadeIn -->
        <div class="col-lg-12" id="form_reg_sucursal">
            <div class="card">
                <form name="form_sucursal" id="form_sucursal" method="POST" class="form-horizontal">
                <div class="card-header text-playerytees">
                    <b id="title_suc">Nueva Sucursal</b>
                </div> <!-- .card-header -->
                <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="descr" class="form-control-label text-playerytees">Nombre:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="hidden" name="id" id="id">
                                <input id="descr" name="descr" class="form-control" type="text" maxlength="45" required>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="direccion" class=" form-control-label text-playerytees">Direccion:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input id="direccion" name="direccion" class="form-control" type="text" maxlength="60" required>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="encargado" class=" form-control-label text-playerytees">Encargado:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input id="encargado" name="encargado" class="form-control" type="text" maxlength="45">
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="telefono" class=" form-control-label text-playerytees">Telefono:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input id="telefono" name="telefono" class="form-control" type="tel" maxlength="45">
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="telefono" class="form-control-label text-playerytees">Estado:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <select name="estado" id="estado" class="form-control" onchange="activarestado(this)">
                                    <option selected="true" value="A">Activo</option>
                                    <option value="I">Inactivo</option>
                                </select>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group" id="divdescripcion">
                            <div class="col col-md-2">
                                <label for="descripcion" class="form-control-label text-playerytees">Descripcion:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <textarea novalidate name="descripcion" id="descripcion" rows="2" style="max-height: 150px; width: 100%;" maxlength="80" placeholder="Explique porque desea eliminar la sucursal o distribuidor"></textarea>  
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                </div> <!-- .card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-playerytees btn-sm" id="btn_guardar">Guardar</button>
                    <button type="reset" class="btn btn-danger btn-sm" onclick="cancelarform()">Cancelar</button>
                </div> <!-- .card-footer -->
                </form> <!-- .form -->
            </div> <!-- .card -->
        </div> <!-- .col-lg-12 -->
    </div> <!-- .content -->
<?php  
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/sucursales.js"></script>
<?php
}
ob_end_flush();
?>