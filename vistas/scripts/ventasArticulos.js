var tblVentasArticulos;

function init() {
    $(".loader").hide();
    $("#contentTblVentas").hide();
    listarSucursal("DPT_BD");
    $('.form-control-chosen').select2({
        language: "es",
        width: '100%',
        closeOnSelect: false
    });
}

function listar(empresa, start_date, end_date, almacen) {
    tblVentasArticulos = $("#tblVentasArticulos").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Bfrtip",
        buttons: [
            {
            extend: 'excelHtml5',
            title: 'Ventas-Detalle-'+$("#input_start_date").val()+'-a-'+$("#input_end_date").val()
        },
        {
            extend: 'csvHtml5',
            title: 'Ventas-Detalle-'+$("#input_start_date").val()+'-a-'+$("#input_end_date").val()
        }],
        "ajax": {
            url: "../ajax/ventasArticulos.php?opcion=listar",
            type: "POST",
            data: {empresa: empresa, start_date: start_date, end_date: end_date, almacen: almacen},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarSucursal(empresa) {
    $.post("../ajax/ventasArticulos.php?opcion=listarSucursales", {empresa: empresa},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {  
                select += `<option value="'${data[i][0]}'">${data[i][1]}</option>`;
            }
            $("#select_sucursal").append(select);
            $("#select_sucursal").trigger("chosen:updated");
        }
    );
}

$("#btnBuscar").click(function (e) { 
    e.preventDefault();
    if($("#input_start_date").val() != "" && $("#input_end_date").val() != "" && $("#select_sucursal :selected").length != 0 ) {
        listar( $("#input_empresa").val(), $("#input_start_date").val(), $("#input_end_date").val(), $("#select_sucursal").val() );
        $("#contentTblVentas").show();
    }
});

$("#btnDPT").click(function (e) { 
    e.preventDefault();
    $("#select_sucursal").empty();
    listarSucursal("DPT_BD");
    $(this).addClass("active");
    $("#btnGP").removeClass("active");
    $("#input_empresa").val("DPT_BD");
});

$("#btnGP").click(function (e) { 
    e.preventDefault();
    $("#select_sucursal").empty();
    listarSucursal("GP_BD");
    $(this).addClass("active");
    $("#btnDPT").removeClass("active");
    $("#input_empresa").val("GP_BD");
});

init();