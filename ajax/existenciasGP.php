<?php
require_once '../modelos/ExistenciasGP.php';

$existenciasGP = new ExistenciasGP();

$idAlmacen = isset($_POST["idAlmacen"])?$_POST["idAlmacen"]:"";
$idMarca = isset($_POST["idMarca"])?$_POST["idMarca"]:"";
$idEstilo = isset($_POST["idEstilo"])?$_POST["idEstilo"]:"";
$idColor = isset($_POST["idColor"])?$_POST["idColor"]:"";
$talla = isset($_POST["talla"])?$_POST["talla"]:"";
$codigo = isset($_POST["codigo"])?$_POST["codigo"]:"";

switch ($_GET['opcion']) {
    case 'listar':
        if (empty($idColor)) {
            $idColor = array("'-1'");
        }
        if (empty($idEstilo)) {
            $idEstilo = array("'-1'");
        }
        $colores = implode( ",",array_filter($idColor) );
        $estilos = implode(",", array_filter($idEstilo));

        $response = $existenciasGP->listar("'$idAlmacen'","'$idMarca'", "$estilos", "$colores");
        $estiloDescrColor = $existenciasGP->estiloDescrColor("'$idAlmacen'","'$idMarca'", "$estilos", "$colores");
        $arregloTallas = array("0","2","3","3M","4","5","6","6M","7","8","9","9M","10","11","12","12M","13","14","15","16","17","17M","18","19","21","23","24","24M","25","27","28","30","32","34","36","38","40","42","44","46","48","50","XS","S","M","L","XL","2XL","3XL","4XL","5XL","6XL","UNI");

        $arrayEstilo[0]="ESTILO";
        $arrayDescripcion[0]="DESCRIPCION";
        $arrayColor[0]="COLOR";
        $arrayTallas[0]="TALLA";
        $numRows = 0;
        $dataBD[][] = "";

        // Llenar estilo, Color y descripcion
        while ($reg=$estiloDescrColor->fetch(PDO::FETCH_OBJ)) {
            array_push($arrayEstilo, $reg->estilo);
            array_push($arrayColor, $reg->color);
            array_push($arrayDescripcion, utf8_encode($reg->descripcion));
        }

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $dataBD[$numRows][0] = $reg->estilo;
            $dataBD[$numRows][1] = utf8_encode($reg->descripcion);
            $dataBD[$numRows][2] = $reg->color;
            $dataBD[$numRows][3] = $reg->talla;
            $dataBD[$numRows][4] = $reg->Existencia;

            if (in_array($reg->talla, $arrayTallas)) {
                # code...
            } else {
                array_push($arrayTallas, $reg->talla);
            }

            $numRows++;
        }

        $matriz[0][0] = "Estilo";
        $matriz[0][1] = "Descripcion";
        $matriz[0][2] = "Color";

        $colMat = 3;
        $filMat = 1;
        //Llenar Tallas Si existen
        for($item =0; $item < count($arregloTallas); $item++)
        {
            if(in_array($arregloTallas[$item], $arrayTallas))
            {
                $matriz[0][$colMat] = $arregloTallas[$item];
                $colMat++;
            }
        }

        $matriz[0][$colMat] = "Total";
        $colMat++;

        for($tit = 1; $tit < count($arrayEstilo); $tit++)
        {
            $matriz[$tit][0] = $arrayEstilo[$tit];//estilo
            $matriz[$tit][1] = $arrayDescripcion[$tit];//descripcion
            $matriz[$tit][2] = $arrayColor[$tit];//color
        }

        for($rel = 1; $rel < count($arrayEstilo); $rel++)
        {
            for($cols = 3; $cols < $colMat; $cols++)
            {
                $matriz[$rel][$cols] = "0";
            }
        }

        //RECORRER PARA PONER NUM PIEZAS
        for($pbd = 0; $pbd < $numRows; $pbd++)// filas Base de Datos
        {
            for($rel = 1; $rel < count($arrayEstilo); $rel++)//filas
            {
                if($dataBD[$pbd][0] == $arrayEstilo[$rel] AND $dataBD[$pbd][1] == $arrayDescripcion[$rel] AND $dataBD[$pbd][2] == $arrayColor[$rel])
                {
                    for($cols = 3; $cols < $colMat; $cols++)//columnas
                    {
                        if($dataBD[$pbd][3] == $matriz[0][$cols])
                        {
                            $matriz[$rel][$cols] = number_format($dataBD[$pbd][4], 0, ".", ",");
                            $matriz[$rel][$colMat-1] += $dataBD[$pbd][4];
                        }
                    }
                }
            }
        }

        $totalCol = array('','','TOTAL:');
        for ($k=3; $k < $colMat; $k++) { 
            $sum = 0;
            foreach ($matriz as $key => $value) {
                if($key != 0) {
                    $sum += (!is_int($value[$k]))?(intval(str_replace(',', '', $value[$k]))):$value[$k];
                }
            }
            array_push($totalCol, $sum);
        }

        array_push($matriz, $totalCol);

        echo "<thead style='background-color: rgba(6, 78, 125, 0.88); color: #fff;'>
                <tr>";
                    for($fm = 0; $fm < 1; $fm++)
                    {
                        for($rm=0; $rm < $colMat; $rm++)
                        {
                            echo "<th>".$matriz[$fm][$rm]."</th>";
                        }
                    }
        echo "</tr>
            </thead>
            <tbody style='text-align: right;'>";
                for($fm = 1; $fm < count($arrayEstilo); $fm++)
                {
                    echo "<tr>";
                        for($rm=0; $rm < $colMat; $rm++)
                        {
                            echo "<td>".$matriz[$fm][$rm]."</td>";
                        }
                    echo "</tr>";
                }
        echo "</tbody>";
        echo "<tfoot style='text-align: right;font-weight:bold;'>";
            for ($fm=count($arrayEstilo); $fm < count($arrayEstilo)+1; $fm++) { 
                echo "<tr>";
                    for ($rm=0; $rm < $colMat; $rm++) { 
                        echo "<td>".((is_int($matriz[$fm][$rm]))?number_format($matriz[$fm][$rm], 0, ".", ","):$matriz[$fm][$rm])."</td>";
                    }
                echo "</tr>";
            }
        echo "</tfoot>";
        break;
    case 'listarMarca':
        $response = $existenciasGP->listarMarca();
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => $reg->FirmCode,
                '1' => $reg->FirmName
            );
        }

        echo json_encode($data);
        break;
    case 'listarEstilo':
        $response = $existenciasGP->listarEstilo("'$idAlmacen'", "'$idMarca'");
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => $reg->ItmsGrpCod,
                '1' => utf8_encode($reg->ItmsGrpNam)
            );
        }

        echo json_encode($data);
        break;
    case 'listarColor':
        if (empty($idEstilo)) {
            $idEstilo = array("'-1'");
        }
        $estilos = implode(",", array_filter($idEstilo));
        $response = $existenciasGP->listarColor("'$idAlmacen'", "'$idMarca'", "$estilos");
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => utf8_encode($reg->color),
                '1' => $reg->codigo
            );
        }

        echo json_encode($data);
        break;
    case 'listarAlmacen':
        $response = $existenciasGP->listarAlmacen();
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => $reg->WhsCode,
                '1' => $reg->WhsName
            );
        }

        echo json_encode($data);
        break;
    case 'busquedaCodigo':
        $response = $existenciasGP->busquedaPorCodigo("'$idAlmacen'", "'$codigo%'");
        $data = array();

        while ($reg=$response->fetch(PDO::FETCH_OBJ)) {
            $data[] = array(
                '0' => $reg->Codigo,
                '1' => utf8_encode($reg->Descripcion),
                '2' => $reg->Existencia
            );
        }

        echo json_encode($data);
        break;
    case 'busquedaAlmacen':
        $response = $existenciasGP->busquedaPorAlmacen("'$codigo'");
        echo json_encode($response);
        break;
    default:
        echo "No existe la opcion seleccionada";
        break;
}