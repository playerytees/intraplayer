<?php
    require "../config/ConexionHana.php";

    class VentaSap extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listarSucursal($sucursal, $empresa)
        {
            try {
                $sql = 'SELECT
                    "WhsCode" "Codigo",
                    "WhsName" "Nombre"
                FROM
                    "'.$empresa.'"."OWHS"
                WHERE
                    "WhsCode" IN ('.$sucursal.')
                ORDER BY "WhsCode";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventas($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    (SELECT
                        "WhsCode"
                    FROM "'.$empresa.'"."INV1"
                    WHERE "DocEntry" = T0."DocEntry"
                    GROUP BY "WhsCode") AS "Sucursal",
                    T0."DocTotal" + (T0."DpmAmnt" + T0."DpmVat") "Monto"
                FROM
                    "'.$empresa.'"."OINV" T0
                INNER JOIN "'.$empresa.'"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE
                    T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	T0."CANCELED" = \'N\'
                AND T1."GroupCode" != \'102\'
                ORDER BY "Sucursal";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDistribuidores($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    \'DIS102\' "Sucursal",
                    IFNULL(SUM(T0."DocTotal" + T0."DpmAmnt"), 0) "Monto"
                FROM
                    "'.$empresa.'"."OINV" T0
                INNER JOIN "'.$empresa.'"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE
                    T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	T0."CANCELED" = \'N\'
                AND T1."GroupCode" = \'102\';';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devoluciones($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    (SELECT
                        "WhsCode"
                    FROM "'.$empresa.'"."RIN1"
                    WHERE "DocEntry" = T0."DocEntry"
                    GROUP BY "WhsCode") AS "Sucursal",
                    T0."DocTotal" "Monto"
                FROM
                    "'.$empresa.'"."ORIN" T0
                INNER JOIN "'.$empresa.'"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE
                    T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	T0."CANCELED" = \'N\'
                AND T1."GroupCode" != \'102\'
                ORDER BY "Sucursal";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function devolucionesDistribuidores($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    \'DIS102\' "Sucursal",
                    IFNULL(SUM(T0."DocTotal"), 0) "Monto"
                FROM
                    "'.$empresa.'"."ORIN" T0
                INNER JOIN "'.$empresa.'"."OCRD" T1 ON T1."CardCode" = T0."CardCode"
                WHERE
                    T0."DocDate" BETWEEN '.$start_date.' AND '.$end_date.'
                AND	T0."CANCELED" = \'N\'
                AND T1."GroupCode" = \'102\';';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function groupBy($array, $id, $data)
        {
            $ids_claves = [];
            foreach ($array as $arr_clave) {
                $id_clave = $arr_clave[$id];
                if (! in_array($id_clave, $ids_claves)) {
                    $ids_claves[] = $id_clave;
                }
            }

            $claves_unique = [];
            foreach ($ids_claves as $unique_id) {
                $temp     = [];
                $quantity = 0;
                foreach ($array as $arr_clave) {
                    $id_temp = $arr_clave[$id];

                    if ($id_temp === $unique_id) {
                        $temp[] = $arr_clave;
                    }
                }

                $clave = $temp[0];
                $clave[$data] = 0;
                foreach ($temp as $clave_temp) {
                    $clave[$data] += $clave_temp[$data];
                }
                $claves_unique[] = $clave;
            }

            return $claves_unique;
        }
    }
    