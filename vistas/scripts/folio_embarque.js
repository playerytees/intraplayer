var tabla;

function init()
{
    mostrarform(false);
    listar();
    $(".loader").hide();
    $("#form-edit").on("submit", function (e) {
        editar(e);
    });
}

function limpiar() {
    $("#inputid").val("");
    $("#inputfecha").val("");
    $("#inputmaquila").val("");
    $("#inputchofer").val("");
    $("#inputcomentarios").val("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#row-edit").show();
        $("#row-show").hide();
        $("#btnGuardar").prop("disabled", false);
    } else {
        $("#row-show").show();
        $("#row-edit").hide();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function listar()
{
    tabla = $("#tablafolio").dataTable({
        "aProcessing" : true,
        "aServerSide" : true,
        dom : "Blfrtip",
        buttons : [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax":{
            url : "../ajax/folioEmbarque.php?opcion=listar",
            type : "get",
            dataType : "json",
            error : function(e){
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy" : true,
        "iDisplayLength" : 10,
        "order" : [[ 0, "desc" ]]
    }).DataTable();
}

function editar(e) {
    e.preventDefault();
    $("#btnGuardar").prop("disabled", true);
    var formData = new FormData($("#form-edit")[0]);

    $.ajax({
        type: "POST",
        url: "../ajax/folioEmbarque.php?opcion=editar",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            $.alert(response);
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function mostrar(id) {
    $.post("../ajax/folioEmbarque.php?opcion=mostrar", {id:id},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            mostrarform(true);
            $("#mostrarid").text("(Folio "+data.id+")");
            $("#fecha").val(data.fecha);
            $("#maquila").val(data.maquila);
            $("#chofer").val(data.chofer);
            $("#comentarios").val(data.comentarios);
            $("#id").val(data.id);
        }
    );
}

init();