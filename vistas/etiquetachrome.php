<style>
    /* @media print {
        table {
            page-break-after: always;
        }
    } */
</style>
<?php /* for ($i=1; $i <= $_GET['cajas']; $i++): */ ?>
<table style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
    <tr style="border-bottom: 1px solid;">
        <td colspan="3"><img class="barcode" style="margin-left: 15px;"></img></td>
        <td colspan="3" align="right"><img class="barcode" style="margin-right: 15px;"></img></td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold; width: 100px;">STYLE:</td>
        <td style="text-align: center; font-size: 25px"><b><span class="estilo"> </span></b></td>
        <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 355px; text-align: center;" rowspan="6">
            <div style="writing-mode: tb-rl; transform: rotate(180deg); width: 15px;">
                <span style="white-space: nowrap;">Order <span class="partida"> </span> </span> 
            </div>
        </td>
        <td style="text-align: center; font-weight: bold; width: 100px;">STYLE:</td>
        <td style="text-align: center; font-size: 25px"><b><span class="estilo"> </span></b></td>
        <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 355px; text-align: center;" rowspan="6">
            <div style="writing-mode: tb-rl; transform: rotate(180deg); width: 15px;">
                <span style="white-space: nowrap;">Order <span class="partida"> </span> </span>
            </div>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold;">SIZE:</td>
        <td style="text-align: center; font-size: 25px"><b><span class="talla"> </span></b></td>
        <td style="text-align: center; font-weight: bold;">SIZE:</td>
        <td style="text-align: center; font-size: 25px"><b><span class="talla"> </span></b></td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold;">COLOR:</td>
        <td style="text-align: center;"><b><span class="color" style="font-size: 25px;"> </span></b></td>
        <td style="text-align: center; font-weight: bold;">COLOR:</td>
        <td style="text-align: center;"><b><span class="color" style="font-size: 25px;"> </span></b></td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold;">QUANTITY:</td>
        <td style="text-align: center; font-size: 30px"><b><span class="pza_x_caja"> </span></b></td>
        <td style="text-align: center; font-weight: bold;">QUANTITY:</td>
        <td style="text-align: center; font-size: 30px"><b><span class="pza_x_caja"> </span></b></td>
    </tr>
    <tr style="text-align: center; border-left: 1px solid;">
        <td colspan="2" valign="bottom" style="padding: 0; height: 50px;"><img class="cve_art_barcode"></img></td>
        <td colspan="2" valign="bottom" style="padding: 0"><img class="cve_art_barcode"></img></td>
    </tr>
    <tr style="font-size: 12px; background: black; color: white; height: 10px;">
        <td valign="bottom">Packing</td>
        <td valign="bottom" style="font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
        <td valign="bottom" style="text-align: right">Packing</td>
        <td valign="bottom" style="text-align: right; font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
    </tr>
</table>
<?php /* endfor; */ ?>
<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script>
function init()
{
    var estilo = getParameterByName('estilo');
    var talla = getParameterByName('talla');
    var color = getParameterByName('color');
    var pza = getParameterByName('pza');
    var partida = getParameterByName('partida');
    var barcode = getParameterByName('barcode');
    var cve_art = getParameterByName('cve_art');

    $(".estilo").html(estilo);
    $(".talla").html(talla);
    $(".color").html(color);
    $(".pza_x_caja").html(pza);
    $(".partida").html(partida);

    JsBarcode(".barcode", barcode, { width: 1, height: 15, displayValue: false, margin: 0 });
    JsBarcode(".cve_art_barcode", cve_art, { width: 1, height: 30, fontSize: 18, font: "Arial", fontOptions: "bold" });

    if (window.print) {
        setTimeout(function(){window.print();window.close();},300); // 500ms = 0.5s - 1000 = 1s
    }
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

init();
</script>