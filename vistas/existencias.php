<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['DPT']==1 OR $_SESSION['GP']==1) {
?>
<style>
/*.dataTables_filter {
    float: left !important;
 } */

 .dt-buttons {
     float: right !important;
 }
 .stock_table td:nth-child(2) {
    white-space: nowrap;
 }
 .stock_table td:nth-child(1), td:nth-child(2), td:nth-child(3) {
    text-align: left;
 }
 #tblProximasLlegadas td:nth-child(4) {
    text-align: left;
 }
 @media (max-width: 425px) {
.dt-buttons { float: none !important;}	
}
</style>
    <div class="content mt-2">
    <div class="card">
        <div class="card-header">
            <strong class="card-title">Existencias</strong>
        </div>
        <div class="card-body">

            <div class="default-tab">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <?php if($_SESSION['DPT']==1): ?>
                    <a class="nav-item nav-link active show" id="nav-dpt-tab" data-toggle="tab" href="#nav-dpt" role="tab" aria-controls="nav-dpt" aria-selected="true">Punto textil</a>
                    <?php endif; ?>
                    <?php if($_SESSION['GP']==1): ?>
                            <a class="nav-item nav-link <?= ($_SESSION['DPT']==0)?'active show':''; ?>" id="nav-gp-tab" data-toggle="tab" href="#nav-gp" role="tab" aria-controls="nav-gp" aria-selected="false">Global playerytees</a>
                    <?php endif; ?>
                </div>
            </nav>
            <div class="tab-content pl-3 pt-2" id="nav-tabContent">
            <?php if($_SESSION['DPT']==1): ?>
                <div class="tab-pane fade active show" id="nav-dpt" role="tabpanel" aria-labelledby="nav-dpt-tab">

                    <div class="row">
                        <div class="col-12 col-md-6 col-xl-3">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Almacen:</strong>
                                </div> <!-- .card-header -->
                                <div class="card-body text-center p-0">
                                    <select name="select_almacen_dpt" id="select_almacen_dpt" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar almacen...">
                                    </select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-6 col-xl-3">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Marca:</strong>
                                </div> <!-- .card-header-->
                                <div class="card-body text-center p-0">
                                    <select name="select_marca_dpt" id="select_marca_dpt" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar Marca..."></select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-6 col-xl-3">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Estilo:</strong>
                                </div> <!-- .card-header-->
                                <div class="card-body text-center p-0">
                                    <select name="select_estilo_dpt[]" id="select_estilo_dpt" class="form-control form-control-chosen d-none" data-placeholder="TODOS" multiple="multiple">
                                        <!-- <option value="-1">TODOS</option> -->
                                    </select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-6 col-xl-2">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Color:</strong>
                                </div> <!-- .card-header-->
                                <div class="card-body text-center p-0">
                                    <select name="select_color_dpt[]" id="select_color_dpt" class="form-control form-control-chosen d-none" data-placeholder="TODOS" multiple="multiple">
                                        <!-- <option value="-1">TODOS</option> -->
                                    </select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-12 col-xl-1" style="display:flex; align-items:center;">
                            <button type="button" id="btnlistar-dpt" class="btn btn-playerytees mb-2">BUSCAR &nbsp;<i class="fa fa-search"></i></button>
                        </div>
                    </div> <!-- .row -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <strong class="card-title text-playerytees">Existencias</strong>
                                </div> --> <!-- .card-header -->
                                <div class="card-body">
                                    <div id="contenido-dpt">
                                        <div class="loading text-center">
                                            <img src="ajax-loader.gif" alt="loading" /><br/>Un momento, por favor...
                                        </div>
                                    </div>
                                    <table id="stock_table_dpt" class="stock_table table table-sm table-bordered table-striped" style="width: 100%; font-size: 14px;">
                                    </table>
                                    <button id="btn_codigo_dpt" type="button" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#codigoModal">Buscar de Código</button>
                                    <?php if($_SESSION['VentasTotales']==1): ?>
                                    <button id="btn_almacen_dpt" type="button" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#AlmacenModal">Buscar en Almacén</button>
                                    <?php endif; ?>
                                    <button type="button" id="btnproximasllegadas-dpt" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#LlegadasModal">Proximas Llegadas</button>
                                    <?php if($_SESSION['imprimiretiqueta']==1): ?>
                                    <button type="button" id="btnimprimiretiqueta" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#EtiquetaModal">Imprimir Etiqueta</button>
                                    <?php endif; ?>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-12 -->
                    </div> <!-- .row -->

                </div>
                <?php endif; ?>
                <?php if($_SESSION['GP']==1): ?>
                <div class="tab-pane fade <?= ($_SESSION['DPT']==0)?'active show':''; ?>" id="nav-gp" role="tabpanel" aria-labelledby="nav-gp-tab">

                    <div class="row">
                        <div class="col-12 col-md-6 col-xl-3">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Almacen:</strong>
                                </div> <!-- .card-header -->
                                <div class="card-body text-center p-0">
                                    <select name="select_almacen_gp" id="select_almacen_gp" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar almacen...">
                                    </select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-6 col-xl-3">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Marca:</strong>
                                </div> <!-- .card-header-->
                                <div class="card-body text-center p-0">
                                    <select name="select_marca_gp" id="select_marca_gp" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar Marca..."></select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-6 col-xl-3">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Estilo:</strong>
                                </div> <!-- .card-header-->
                                <div class="card-body text-center p-0">
                                    <select name="select_estilo_gp[]" id="select_estilo_gp" class="form-control form-control-chosen d-none" data-placeholder="TODOS" multiple="multiple">
                                        <!-- <option value="-1">TODOS</option> -->
                                    </select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-6 col-xl-2">
                            <div class="card mb-2">
                                <div class="card-header p-1">
                                    <strong class="card-title text-playerytees">Color:</strong>
                                </div> <!-- .card-header-->
                                <div class="card-body text-center p-0">
                                    <select name="select_color_gp[]" id="select_color_gp" class="form-control form-control-chosen d-none" data-placeholder="TODOS" multiple="multiple">
                                        <!-- <option value="-1">TODOS</option> -->
                                    </select>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-3 -->
                        <div class="col-12 col-md-12 col-xl-1"  style="display:flex; align-items:center;">
                            <button type="button" id="btnlistar-gp" class="btn btn-playerytees mb-2">BUSCAR &nbsp;<i class="fa fa-search"></i></button>
                        </div> <!-- .col-1 -->
                    </div> <!-- .row -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <!-- <div class="card-header">
                                    <strong class="card-title text-playerytees">Existencias</strong>
                                </div> --> <!-- .card-header -->
                                <div class="card-body">
                                    <div id="contenido-gp">
                                        <div class="loading text-center">
                                            <img src="ajax-loader.gif" alt="loading" /><br/>Un momento, por favor...
                                        </div>
                                    </div>
                                    <table id="stock_table_gp" class="stock_table table table-sm table-bordered table-striped" style="width: 100%; font-size: 14px;">
                                    </table>
                                    <button id="btn_codigo_gp" type="button" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#codigoModal">Buscar de Código</button>
                                    <?php if($_SESSION['VentasTotales']==1): ?>
                                    <button id="btn_almacen_gp" type="button" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#AlmacenModal">Buscar en Almacén</button>
                                    <?php endif; ?>
                                    <button type="button" id="btnproximasllegadas-gp" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#LlegadasModal">Proximas Llegadas</button>
                                    <?php if($_SESSION['imprimiretiqueta']==1): ?>
                                    <button type="button" id="btnimprimiretiqueta" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#EtiquetaModal">Imprimir Etiqueta</button>
                                    <?php endif; ?>
                                </div> <!-- .card-body -->
                            </div> <!-- .card -->
                        </div> <!-- .col-12 -->
                    </div> <!-- .row -->

                </div>
                <?php endif; ?>
            </div> <!-- .tab-content -->
            </div> <!-- .default-tab -->

        </div> <!-- card-body -->
    </div> <!-- .card -->

        <div class="modal fade" id="codigoModal" tabindex="-1" role="dialog" aria-labelledby="codigoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="codigoModalLabel-codigo">Busqueda de código</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" class="">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Buscar</div>
                                    <input type="text" id="buscarCodigo-dpt" name="buscarCodigo-dpt" class="form-control" onkeypress="return pulsarenter(event)">
                                    <input type="text" id="buscarCodigo-gp" name="buscarCodigo-gp" class="form-control" onkeypress="return pulsarenter(event)">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                </div>
                            </div>
                        </form>
                        <div id="mostrarTabla" style="height: 300px; overflow: auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="AlmacenModal" tabindex="-1" role="dialog" aria-labelledby="codigoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="max-width: 1200px;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="codigoModalLabel-almacen">Busqueda en almacén</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div id="loadingAlmacen">
                            <div class="loading text-center">
                                <img src="ajax-loader.gif" alt="loading" /><br/>Un momento, por favor...
                            </div>
                        </div>
                        <div id="" style="height: auto; overflow: auto">
                            <table id="tblAlmacen" class="table table-bordered table-sm table-striped table-responsive-md w-100 stock_table">
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="LlegadasModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-lg" style="max-width: 1200px;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Proximas Llegadas</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" class="form-inline">
                            <div class="form-group"><label for="min" class="pr-1  form-control-label">De:</label><input type="date" id="min" placeholder="" required="" class="form-control form-control-sm" value="<?=  date("Y-m-d"); ?>"></div>
                            <div class="form-group"><label for="max" class="px-1  form-control-label">A:</label><input type="date" id="max" placeholder="" required="" class="form-control form-control-sm"></div>
                            <div class="form-group"><input type="button" id="btnBuscarFecha_dpt" class="btn-playerytees btn-sm ml-1" value="Buscar"></div>
                            <div class="form-group"><input type="button" id="btnBuscarFecha_gp" class="btn-playerytees btn-sm ml-1" value="Buscar"></div>
                        </form>
                        <div id="loadingProximasLlegadas">
                            <div class="loading text-center">
                                <img src="ajax-loader.gif" alt="loading" /><br/>Un momento, por favor...
                            </div>
                        </div>
                        <table id="tblProximasLlegadas" class="table table-bordered table-sm stock_table" style="width:100%;">
                          <!--  <thead>
                               <tr>
                                    <th>Partida</th>
                                    <th>Clave</th>
                                    <th>Cantidad</th>
                                    <th>Fecha de Entrega</th>
                               </tr>
                           </thead>
                           <tbody></tbody> -->
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="modal fade" id="EtiquetaModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="">Imprimir Etiqueta</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" class="">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="codigo_tag" name="codigo_tag" placeholder="Codigo del articulo" class="form-control" maxlength="20" required onkeypress="return pulsarenter(event)">
                                    <div class="input-group-btn">
                                    </div>
                                </div>
                            </div>
                        </form>
                        <table style="width: 100%; height: 100%; margin-top: 20px;">
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td><img class="codigo"></img></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>&nbsp;</td>
                                <td>
                                    <table>
                                        <tr>
                                            <td><img class="codigo"></img></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <button type="button" id="btnImprimirCodigo" class="btn btn-playerytees btn-sm mt-2 ml-3">Imprimir</button>
                    </div>
                </div>
            </div>
        </div> -->

    </div> <!-- .content .mt-3 -->

<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="scripts/existencias.js"></script>
<?php
}
ob_end_flush();
?>