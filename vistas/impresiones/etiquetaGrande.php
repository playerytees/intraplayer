<table border="1" style="width: 100%; height: 100%; text-align: center; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; font-size: 1.5rem">
    <tr>
        <td colspan="2" style="font-weight: bold; text-align: left; font-size: 22px; width: 35%;">CLIENTE:</td>
        <td colspan="2" id="lblcliente"></td>
    </tr>
    <tr>
        <td colspan="2" style="font-weight: bold; text-align: left; font-size: 22px;">NOMBRE ETIQUETA:</td>
        <td colspan="2" id="lbletiqueta"></td>
    </tr>
    <tr>
        <td colspan="2" style="font-weight: bold; text-align: left; font-size: 22px;">COMPOSICIÓN:</td>
        <td colspan="2" id="lblcomposicion01"></td>
    </tr>
    <tr>
        <td style="font-weight: bold; font-size: 21px;">TALLA</td>
        <td style="font-weight: bold; font-size: 21px;">CAJONES</td>
        <td style="font-weight: bold; font-size: 21px;">ETIQUETAS <br /> POR CAJÓN</td>
        <td style="font-weight: bold; font-size: 21px;">TOTAL</td>
    </tr>
<!--
    <tr>
        <td id="lbltalla">T</td>
        <td id="lblnumCajones">0</td>
        <td id="lbletiquetasxcajon">0</td>
        <td id="lbletiquetasxcaja">0</td>
    </tr>
-->
    <tr style="font-weight: bold; color: #fff; background-color: #000;" id="total">
        <td>TOTAL <br> CAJONES</td>
        <td id="lblsumaNumCajones">0</td>
        <td>TOTAL <br> ETIQUETAS</td>
        <td id="lblsumatotal">0</td>
    </tr>
    <tr>
        <td>Packing:</td>
        <td><span id="lblfecha"></span>&nbsp;<span id="lblhora"></span></td>
        <td>Folio:</td>
        <td id="lblfolio"></td>
    </tr>
</table>
<script src="../../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script>
function init()
{
    var tabla = "";
    var cliente = getParameterByName('cliente');
    var etiqueta = getParameterByName('etiqueta');
    var composicion01 = getParameterByName('composicion01');
    var fecha = getParameterByName('fecha');
    var hora = getParameterByName('hora');
    var folio = getParameterByName('folio');
    var talla = JSON.parse(getParameterByName('talla'));

    var sumaNumCajones = getParameterByName('sumaNumCajones');
    var sumaEtiquetasxcajon = getParameterByName('sumaEtiquetasxcajon');
    var sumatotal = getParameterByName('sumatotal');

    $("#lblcliente").text(cliente);
    $("#lbletiqueta").text(etiqueta);
    $("#lblcomposicion01").text(composicion01);
    $("#lblfecha").text(fecha);
    $("#lblhora").text(hora);
    $("#lblfolio").text(folio);
    for (var i = 0; i < talla.length; i++) {
        tabla += `<tr class="trdata">
        <td>${talla[i].talla01}</td>
        <td>${new Intl.NumberFormat("en-US").format(talla[i].numCajones)}</td>
        <td>${new Intl.NumberFormat("en-US").format(talla[i].etiquetasxcajon)}</td>
        <td>${new Intl.NumberFormat("en-US").format(talla[i].total)}</td>
        </tr>`;
    }

    $("#lblsumaNumCajones").text(new Intl.NumberFormat("en-US").format(sumaNumCajones));
    $("#lblsumaEtiquetasxcajon").text(new Intl.NumberFormat("en-US").format(sumaEtiquetasxcajon));
    $("#lblsumatotal").text(new Intl.NumberFormat("en-US").format(sumatotal));

    $("#total").before(tabla);

	if (window.print) {
        setTimeout(function(){window.print();window.close();},300); // 500ms = 0.5s - 1000 = 1s
    }
}

function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
init();
</script>