<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';

    if($_SESSION['embarques']==1) {
?>
    <div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="text-playerytees">Orden de Embarque</h1>
                </div>
            </div>
        </div>
    </div>
    
    <div class="content mt-3">
        <div class="row" id="row-show">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title text-playerytees">Listado</strong>
                    </div>
                    <div class="card-body">
                        <table id="tablafolio" class="table table-bordered table-sm table-hover table-responsive-sm" style="width: 100%;">
                        <thead>
                            <th>Folio</th>
                            <th>Fecha</th>
                            <th>Maquiladora</th>
                            <th>Chofer</th>
                            <!-- <th>Piezas</th>
                            <th>Cajas</th> -->
                            <th>Acciones</th>
                            <?php if($_SESSION['usuarios']==1): ?>
                            <th>Editar</th>
                            <?php endif; ?>
                        </thead>
                        <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->

        <div class="row" id="row-edit">
            <div class="col-lg-12">
                <div class="card">
                    <form name="form-edit" id="form-edit" method="POST" class="form-horizontal">
                        <div class="card-header">
                            <strong class="card-title text-playerytees">Editar Embarque <span id="mostrarid"></span></strong>
                        </div> <!-- .card-header -->
                        <div class="card-body">
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="fecha" class="form-control-label text-playerytees">Fecha:</label>
                                </div>
                                <div class="col-12 col-md-4">
                                    <input type="hidden" name="id" id="id">
                                    <input type="date" name="fecha" id="fecha" class="form-control" required>
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="maquila" class="form-control-label text-playerytees">Maquiladora:</label>
                                </div>
                                <div class="col-12 col-md-4">
                                    <input type="text" name="maquila" id="maquila" class="form-control" maxlength="50" required>
                                </div>
                                <div class="col-md-6"></div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="chofer" class="form-control-label text-playerytees">Chofer:</label>
                                </div>
                                <div class="col-12 col-md-4">
                                    <input type="text" name="chofer" id="chofer" class="form-control" maxlength="30" required>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-2">
                                    <label for="comentarios" class="form-control-label text-playerytees">Comentarios:</label>
                                </div>
                                <div class="col-12 col-md-4">
                                    <input type="text" name="comentarios" id="comentarios" class="form-control">
                                </div>
                            </div>
                        </div> <!-- .card-body -->
                        <div class="card-footer">
                            <button type="submit" class="btn btn-playerytees btn-sm" id="btnGuardar">Guardar</button>
                            <button type="reset" class="btn btn-danger btn-sm" onclick="cancelarform()">Cancelar</button>
                        </div> <!-- .card-footer -->
                    </form> <!-- .form -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content -->
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/folio_embarque.js"></script>
<?php
}
ob_end_flush();
?>