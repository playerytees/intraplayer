<?php
ob_start();
session_start();

if(!isset($_SESSION['nombre']))
{
    header("Location: login.php");
}
else
{
    require 'header.php';

if ($_SESSION['resurtido']==1) {
?>
<?php $idsucursal = isset($_GET['sucursal'])?$_GET['sucursal']:""; ?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title text-playerytees">
                    <h1 style="float: left;">Reportes </h1> <span id ="spansucursal" style="float: right;padding: 15px 0; margin-left: 5px;"></span>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Resurtido</a></li>
                        <li><a href="sucursales.php">Sucursales</a></li>
                        <li class="active">Reportes</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" name="idsucursal" id="idsucursal" value="<?php echo $idsucursal; ?>">
    <div class="content mt-3">
        <div class="row" id="listadofolio">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-playerytees">
                        <strong class="card-title">Reportes</strong> <span class="spanresurtido"></span>
                    </div> <!-- .card-header -->
                    <div class="card-body">
                        <table id="tablafolio" class="table table-bordered table-sm table-hover table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th>Num</th>
                                <th>Fecha</th>
                                <th>Sucursal/Distribuidor</th>
                                <th>Ver</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <a href="sucursales.php" class="btn btn-danger" >Regresar</a>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row" id="listado_final">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header text-playerytees">
                        <strong class="card-title">Resurtido</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body">
                        <table id="tabla_resurtido" class="table table-bordered table-sm table-hover table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th>Manufactura</th>
                                <th>Personalizado</th>
                                <th>Linea</th>
                                <th>Exis CEDIS</th>
                                <th>Existencias</th>
                                <th>Piezas surtir</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button type="button" class="btn btn-danger" onclick="cancelarform()">Regresar</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content .mt-3 -->
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="scripts/resurtido.js"></script>
<?php
}
ob_end_flush();
?>