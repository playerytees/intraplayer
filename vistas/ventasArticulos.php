<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['ventasArticulos']==1) {
?>
<style>
#tblVentasArticulos_filter{
    display: inline-block;
    float: right;
}
</style>
    <div class="content mt-2">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Ventas Articulos</strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <buttton type="butoon" class="btn btn-outline-success btn-sm active" id="btnDPT">Punto Textil</buttton>
                        <buttton type="butoon" class="btn btn-outline-primary btn-sm" id="btnGP">Global playerytees</buttton>
                    </div>
                </div>
                <div class="row">
                <form action="" class="w-100" method="POST">
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="input_start_date" class="control-label mb-1 text-playerytees">Fecha Inicio:</label>
                            <input type="hidden" name="input_empresa" id="input_empresa" value="DPT_BD">
                            <input type="date" name="input_start_date" id="input_start_date" class="form-control" required />
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="input_end_date" class="control-label mb-1 text-playerytees">Fecha Fin:</label>
                            <input type="date" name="input_end_date" id="input_end_date" class="form-control" required />
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="select_sucursal" class="control-label mb-1 text-playerytees">Almacen:</label>
                            <select name="select_sucursal[]" id="select_sucursal" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar..." multiple="multiple" required></select>
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-12 col-xl-3" style="display:flex; align-items:center;">
                        <button type="submit" id="btnBuscar" class="btn btn-playerytees mb-2">BUSCAR &nbsp;<i class="fa fa-search"></i></button>
                    </div>
                </form>
                </div> <!-- .row -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body" id="contentTblVentas">
                                <table class='table table-sm table-striped table-bordered table-responsive' id="tblVentasArticulos" style="width: 100%;">
                                    <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                        <tr>
                                            <th>Folio R1</th>
                                            <th>Fecha</th>
                                            <th>Articulo</th>
                                            <th>Linea</th>
                                            <th>Talla</th>
                                            <th>Color</th>
                                            <th>Cantidad</th>
                                            <th>Precio</th>
                                            <th>Total</th>
                                            <th>Lista Precio</th>
                                            <th>Cliente</th>
                                            <th>Vendedor</th>
                                            <th>Almacen</th>
                                            <th>Comentarios</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 14px;"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> <!-- .row -->

            </div> <!-- card-body -->
        </div> <!-- .card -->
    </div> <!-- .content .mt-3 -->

<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="scripts/ventasArticulos.js"></script>
<?php
}
ob_end_flush();
?>