<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");

class Inventario
{
    public function __construct()
    {

    }

    public function insertar($sku_manufactura, $sku_custom, $inventario, $id_sucursal)
    {
        $sql = "INSERT INTO resurtido_inve VALUES (NULL, '$sku_manufactura', '$sku_custom', '$inventario', '$id_sucursal', now(), now())";

        return ejecutarConsulta($sql);
    }

    public function editar($id, $sku_manufactura, $sku_custom, $inventario, $id_sucursal)
    {
        $sql = "UPDATE resurtido_inve SET sku_manufactura = '$sku_manufactura', sku_custom = '$sku_custom', inventario = '$inventario', modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($id)
    {
        $sql = "SELECT * FROM resurtido_inve WHERE id = '$id'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar($id_sucursal)
    {
        $sql = "SELECT
            r.sku_manufactura,
            r.sku_custom,
            inv.estilo as linea,
            c.sugerido,
            r.inventario,
            (r.inventario - c.sugerido) AS faltante,
            ROUND( ( (r.inventario - c.sugerido)/if( c.sugerido > inv.pza_x_caja, inv.pza_x_caja, c.sugerido ) ) ) AS propuesto,
            if( c.sugerido > inv.pza_x_caja, inv.pza_x_caja, c.sugerido ) AS num_piezas,
            ROUND( ( (r.inventario - c.sugerido)/if( c.sugerido > inv.pza_x_caja, inv.pza_x_caja, c.sugerido ) ) ) * if( c.sugerido > inv.pza_x_caja, inv.pza_x_caja, c.sugerido ) * -1 AS piezas_surtir
        FROM
            resurtido_inve AS r
        JOIN
            catalogo AS c
        ON
            r.sku_custom = c.sku_custom
        AND
            r.id_sucursal = c.id_sucursal
        JOIN
            inventario_sae AS inv
        ON
            inv.cve_ing = c.sku_manufactura
        OR
            inv.cve_esp = c.sku_manufactura
        WHERE c.sugerido > 0 AND r.id_sucursal = '$id_sucursal'";

        return ejecutarConsulta($sql);
    }

    public function delete($id_sucursal)
    {
        $sql = "DELETE FROM resurtido_inve WHERE id_sucursal = '$id_sucursal'";
            
        ejecutarConsulta($sql);
    }
}

?>