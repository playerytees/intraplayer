<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

    require 'header.php';

    if ($_SESSION['embarques']==1) {
?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="text-playerytees">Partidas</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li><a href="#">Embarques</a></li>
                        <li class="active">Partidas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row" id="listadotemporary">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title float-left text-playerytees"> Partidas para ser Embarcadas </strong>
                        <a href="embarque.php" class="btn btn-danger btn-sm float-right">Regresar</a>
                    </div>
                    <div class="card-body">
                        <table id="tablatemporary" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th></th>
                                <th>Partida</th>
                                <th>Estilo</th>
                                <th>Color</th>
                                <th>Talla</th>
                                <th>Piezas</th>
                                <th>Cajas</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button id="btn_guardar" type="button" class="btn btn-playerytees btn-sm" onclick="guardarembarque(event)">
                            Guardar
                        </button>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
        <div class="row" id="listarpartidas">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title text-playerytees"> Partidas Pendientes </strong>
                    </div>
                    <div class="card-body">
                        <table id="tablapartidas" class="table table-bordered table-sm table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th></th>
                                <th>Partida</th>
                                <th>Estilo</th>
                                <th>Color</th>
                                <th>Talla</th>
                                <th>Piezas</th>
                                <th>Cajas</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <a id="" href="packing.php" class="btn btn-playerytees btn-sm" target="_blank">
                            Imprimir
                        </a>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content .mt-3 -->
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
?>
<script src="../public/assets/js/jquery.tabletojson.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="scripts/partidas.js"></script>
<?php
}
ob_end_flush();
?>