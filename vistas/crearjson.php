<?php
require "../config/Conexion.php";
//generamos la consulta
$sql = "SELECT * FROM etiqueta WHERE fecha >= DATE_SUB(CURDATE(), INTERVAL 15 DAY)";
mysqli_set_charset($conexion, "utf8"); //formato de datos utf8

if(!$result = mysqli_query($conexion, $sql)) die();

$etiquetas = array(); //creamos un array

while($row = mysqli_fetch_array($result))
{
    $barcode=$row['barcode'];
    $partida=$row['partida'];
    $estilo=$row['estilo'];
    $color=$row['color'];
    $talla=$row['talla'];
    $piezas=$row['pza_x_caja'];

    $etiquetas[] = array('barcode'=> $barcode, 'partida'=> $partida, 'estilo'=> $estilo, 'color'=> $color, 'talla'=> $talla, 'pza_x_caja'=> $piezas);
}

//desconectamos la base de datos
$close = mysqli_close($conexion)or die("Ha sucedido un error inexperado en la desconexion de la base de datos");

//Creamos el JSON
$json_string = json_encode($etiquetas);
// echo $json_string;

//Si queremos crear un archivo json, sería de esta forma:

$file = 'C:\intraplayer\data\json\etiquetas.json';
file_put_contents($file, $json_string);
?>