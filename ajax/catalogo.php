<?php
require_once '../modelos/Catalogo.php';

$catalogo = new Catalogo();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$sku_manufactura = isset($_POST['sku_manufactura'])?limpiarCadena($_POST['sku_manufactura']):"";
$sku_custom = isset($_POST['sku_custom'])?limpiarCadena($_POST['sku_custom']):"";
$sugerido = isset($_POST['sugerido'])?limpiarCadena($_POST['sugerido']):"";
$id_sucursal = isset($_POST['id_sucursal'])?limpiarCadena($_POST['id_sucursal']):"";

switch ($_GET['opcion']) {
    case 'guardar':
        $catalogo->deletecatalogo($id_sucursal);
        $datosTabla = json_decode($_POST['tabla_catalog'], true);

        foreach($datosTabla as $key => $value)
        {
            $response = $catalogo->insertar($value['clave_manufactura'], $value['clave_personalizada'], $value['sugerido'], $id_sucursal);
        }
        echo $response;
        break;
    case 'mostrar':
        $response = $catalogo->mostrar($id);
        echo json_encode($response);
        break;
    case 'listar':
        $response = $catalogo->listar($id_sucursal);
        $data = array();

        while ( $reg=$response->fetch_object() ) {
            $data[] = array(
                '0' => $reg->sku_manufactura,
                '1' => $reg->sku_custom,
                '2' => $reg->sugerido,
                '3' => $reg->descr,
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        echo "La opcion seleccionada no existe";
        break;
}
?>