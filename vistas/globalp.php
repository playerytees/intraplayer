<?php require 'header.php'; ?>
    <div class="breadcrumbs">
        <div class="col-sm-12">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>Inventario Global Playerytees</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="content mt-3">
        <div class="animated fadeIn" id="listadoSucursales">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Sucursales</strong>
                        </div> <!-- .card-header -->
                        <div class="card-body">
                            <table id="tabla_globalp" class="table table-bordered table-sm" style="width: 100%;">
                                <thead>
                                    <th>Clave Articulo</th>
                                    <th>Descripcion</th>
                                    <th>Linea</th>
                                    <th>Existencias</th>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-md-12 -->
            </div> <!-- .row -->
        </div> <!-- .animated .fadeIn -->
    </div> <!-- .content -->
<?php require 'footer.php'; ?>
<script src="scripts/globalp.js"></script>
<script>
    $(".loader").hide();
</script>