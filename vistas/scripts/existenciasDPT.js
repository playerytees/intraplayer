var tabla;
var tablaProxLlegadas;

function init() {
    $(".loader").hide();
    $("#contenido").hide();
    $("#carga-almacen").hide();
    $('.form-control-chosen').select2({
        language: "es",
        width: '100%'
    });
    listarAlmacen();
    setTimeout(function(){listarMarca();}, 1000);
    listarEstilo('CE01', '3');
    listarColor('CE01', '3', '');
    listar('CE01', '3', '', '');
    $("#title-descr").text("/ Existencias DPT");
    $("#btnbuscar").hide();
    $('#codigoModal').on('shown.bs.modal', function() {
        $('#buscarCodigo').trigger('focus');
    });
    $("#LlegadasModal").on('shown.bs.modal', function () {
        listarProximasLlegadas();
    });
}

function listar(almacen, marca, estilo, color) {
    $("#contenido").show();
    $.post("../ajax/existenciasDPT.php?opcion=listar", {idAlmacen: almacen, idMarca: marca, idEstilo: estilo, idColor: color},
        function (data, textStatus, jqXHR) {
            $("#contenido").hide();
            $("#stock_table").append(data);
            tabla = $("#stock_table").dataTable({
                "scrollY": "400px",
                "scrollX": true,
                "paging": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                // dom: 'Bfrtp',
                dom: '<"top"lp>rt<"bottom"B><"clear">',
                buttons: [
                    'copyHtml5',
                    {
                        extend: 'excelHtml5',
                        title: 'Existencias DPT'
                    },
                    {
                        extend: 'csvHtml5',
                        title: "Existencias DPT"
                    }
                    // 'pdfHtml5'
                ]
            }).DataTable();

            $('#stock_table td:last-child').addClass('text-playerytees');
            $("#btnbuscar").show();
        }
    );
}

function listarAlmacen()
{
    $.post("../ajax/existenciasDPT.php?opcion=listarAlmacen",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "<option></option>";
            for (let i = 0; i < data.length; i++) {  
                select += `<option ${(data[i][0]=="CE01"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_almacen").append(select);
            $("#select_almacen").trigger("chosen:updated");
        }
    );
}

function listarMarca() {
    $.post("../ajax/existenciasDPT.php?opcion=listarMarca",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                if ($("#select_almacen").val() == "CE01") {
                    select += `<option ${(data[i][0]=="3"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
                } else {
                    select += `<option ${(data[i][0]=="5"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
                }
            }
            $("#select_marca").append(select);
            $("#select_marca").trigger("chosen:updated");
        }
    );
}

function listarEstilo(almacen, marca) { // (almacen = 'CE01', marca = '5') {
    $.post("../ajax/existenciasDPT.php?opcion=listarEstilo", {idAlmacen: almacen, idMarca: marca},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            // var select = `<option value="${data[0][0]}" selected>${data[0][1]}</option>`;
            for (let i = 1; i < data.length; i++) {
                select += `<option value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_estilo").append(select);
            $("#select_estilo").trigger("chosen:updated");
        }
    );
}

function listarColor(almacen, marca, estilo) {
    $.post("../ajax/existenciasDPT.php?opcion=listarColor", {idAlmacen: almacen, idMarca: marca, idEstilo: estilo},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                select += `<option value="'${data[i][0]}'">${data[i][0]}</option>`;
            }
            $("#select_color").append(select);
            $("#select_color").trigger("chosen:updated");
        }
    );
}

$("#select_almacen").change(function (e) { 
    e.preventDefault();
    $("#select_marca").empty();
    listarMarca();

    $("#select_estilo").empty();
    // $("#select_estilo").append("<option value='-1'>TODOS</option>");
    listarEstilo($(this).val(), ($("#select_almacen").val()=='CE01')?'3':'5');

    $("#select_color").empty();
    //$("#select_color").append("<option value='-1'>TODOS</option>");
    setTimeout(function(){listarColor($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val());}, 2000);

    if ($("#stock_table tr").length > 1) {
        tabla.clear();
        if ($.fn.dataTable.isDataTable('#stock_table')) {
            tabla.destroy();
            $("#stock_table").html("");
        }
    }

    setTimeout(function(){listar($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val(), $("#select_color").val());},3000);
});

$("#select_marca").change(function (e) { 
    e.preventDefault();
    $("#select_estilo").empty();
    // $("#select_estilo").append("<option value='-1'>TODOS</option>");
    listarEstilo($("#select_almacen").val(), $(this).val());

    $("#select_color").empty();
    //$("#select_color").append("<option value='-1'>TODOS</option>");
    setTimeout(function(){listarColor($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val());}, 1000);

    tabla.clear();
    if ($.fn.dataTable.isDataTable('#stock_table')) {
        tabla.destroy();
        $("#stock_table").html("");
    }
    setTimeout(function(){listar($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val(), $("#select_color").val());},2000);
});

$("#select_estilo").change(function (e) { 
    e.preventDefault();
    $("#select_color").empty();
    //$("#select_color").append("<option value='-1'>TODOS</option>");
    setTimeout(function(){listarColor($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val());}, 1000);

    tabla.clear();
    if ( $.fn.dataTable.isDataTable( '#stock_table' ) ) {
        tabla.destroy();
        $("#stock_table").html("");
    }
    setTimeout(function(){listar($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val(), $("#select_color").val());},1500);
});

$("#select_color").change(function (e) { 
    e.preventDefault();
    tabla.clear();
    if ($.fn.dataTable.isDataTable('#stock_table')) {
        tabla.destroy();
        $("#stock_table").html("");
    }
    listar($("#select_almacen").val(), $("#select_marca").val(), $("#select_estilo").val(), $(this).val());
});

$("#buscarCodigo").keyup(function (e) { 
    e.preventDefault();
    if ($("#buscarCodigo").val()) {
        $.post("../ajax/existenciasDPT.php?opcion=busquedaCodigo", {idAlmacen: $("#select_almacen").val(), codigo: $(this).val()},
            function (data, textStatus, jqXHR) {
               var tablaBuscar = `<table class="table table-bordered table-sm">
                    <tr>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Existencia</th>
                    </tr>
                <tbody>`;
                data = JSON.parse(data);
                for (let i = 0; i < data.length; i++) {
                    tablaBuscar += `<tr><td>${data[i][0]}</td><td>${data[i][1]}</td><td>${data[i][2]}</td></tr>`;
                }
                tablaBuscar += `</tbody></table>`;
                $("#mostrarTabla").html(tablaBuscar);
            }
        );
    }
});

function pulsarenter(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13 && tecla != 32);
}

$("#btnBusquedaAlmacen").click(function (e) { 
    e.preventDefault();
    $("#carga-almacen").show();
    $("#tblAlmacen tbody").html('');
    if ($("#buscarAlmacen").val()) {
        $.post("../ajax/existenciasDPT.php?opcion=busquedaAlmacen", {codigo: $("#buscarAlmacen").val()},
            function (data, textStatus, jqXHR) {
                $("#carga-almacen").hide();
                data = JSON.parse(data);
                var tablaAlmacen = "";
                for (let i = 0; i < data.length; i++) {
                    tablaAlmacen += `<tr><td>${data[i]['Codigo']}</td><td>${data[i]['Descripcion']}</td><td>${data[i]['Almacen']}</td><td>${data[i]['Existencia']}</td></tr>`;
                }
                $("#tblAlmacen tbody").append(tablaAlmacen);
            }
        );
    } 
});

function listarProximasLlegadas() {
    var estilo_ = $('#select_estilo').select2('data');

    var estilos = new Array();
    for (let i = 0; i < estilo_.length; i++) {
        estilos.push("'"+estilo_[i].text+"'");
    }

    $("#tblProximasLlegadas").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "frtp",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/proximasLlegadas.php?opcion=estiloColor",
            type: "POST",
            data: { estilo : estilos, color : $("#select_color").val()},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 5
    }).DataTable();
}

$("#btnImprimirCodigo").click(function (e) { 
    e.preventDefault();
    var codigo = $("#codigo_tag").val();
    if ($("#codigo_tag").val() != "") {
        window.open("impresiondeetiqueta.php?codigo="+codigo+"","_reporte","{opciones}");    
    }
});

init();