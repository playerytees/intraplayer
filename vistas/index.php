<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php'; ?>

        <div class="breadcrumbs">
            <div class="col-sm-12">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1 class="text-playerytees">Index</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="col-12">
                
            </div>
        </div> <!-- .content -->

<?php require 'footer.php'; ?>
<script>
    $(".loader").hide();
</script>
<?php
}
ob_end_flush();
?>