function init() {
    $(window).resize();

    if ($(window).width() <= 767) {
        $("body").removeClass("open");
    } else {
        $("body").addClass("open");
    }
}

init();