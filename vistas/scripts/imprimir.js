function init()
{
    var browser = getBrowserInfo().split(' ');

    if (browser[0] == "Chrome") {
        $("table").css({"height": "100%", "width": "100%"});
    }
}

function imprimir()
{
    
}

$("#btn_print_area").click(function (e) {
    e.preventDefault();
    var cve_ref = $("#input_cve_ref").val().toUpperCase();
    $.post("../ajax/etiqueta.php?opcion=mostrarinventario", {cve_art:cve_ref},
        function (data, textStatus, jqXHR) {
            if ( data != "null" ) {
                data = JSON.parse(data);
                console.log(data);
                var orden = $("#input_orden").val().toUpperCase();
                $(".labelStyle").html(data.estilo);
                $(".labelSize").html(data.talla);
                $(".labelColor").html(data.color_ing);
                $(".labelQty").html(data.pza_x_caja);

                $(".labelOrden").html(orden);

                JsBarcode(".barcode", data.cve_ing, {width: 1.5, height: 45, fontSize: 18, font: "Arial", fontOptions: "bold"});

                $("#print_area").printArea();
            } else {
                let mensaje = $("#mensaje");
                    mensaje.slideDown(300);
                    mensaje.delay(3000);
                    mensaje.slideUp(300);
                /* toastr.warning('Verifique que los datos sean correctos', 'Advertencia!', {"timeOut": 2000, "progressBar": true}); */
                $(".labelStyle").html("");
                $(".labelSize").html("");
                $(".labelColor").html("");
                $(".labelQty").html("");
                $(".labelOrden").html("");
                $(".barcode").html("");
            }
        }
    );
});

$("#txtBuscraXProd").keyup(function(e){
    e.preventDefault();
    if ($("#txtBuscraXProd").val() != "") {
        $.ajax({
            data: {"cve_art": $("#txtBuscraXProd").val()},
            type: "POST",
            url: "../ajax/etiqueta.php?opcion=listarinventario",
            success: function(data){
                var tabla = `<table width='100%' border='1' cellpadding='10' cellspacing='0' bordercolor='#666666' id='Exportar_a_Excel' style='border-collapse:collapse;'>
                    <tr>
                        <th>Clave ESP</th>
                        <th>Clave ING</th>
                    </tr>
                <tbody>`;
                data = JSON.parse(data);
                for (let i = 0; i < data.length; i++) {  
                    tabla += `<tr><td>${data[i][0]}</td><td>${data[i][1]}</td></tr>`;
                }
                tabla += `</tbody></table>`;
                $("div.scroll").html(tabla);
            }
        });
    }
});

var getBrowserInfo = function() {
    var ua= navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1] === 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if( ( tem= ua.match( /version\/(\d+)/i ) ) != null ) M.splice(1, 1, tem[1]);
    return M.join(' ');
}

init();