<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['usuarios']==1) {
?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 style="float: left;" class="text-playerytees">Usuarios </h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li class="active">Usuarios</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row" id="listadoregistros">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title d-sm-inline text-playerytees">Lista de Usuarios</strong>
                        <button id="btnagregar" class="btn btn-playerytees btn-sm float-sm-right" type="button" onclick="mostrarform(true)">Nuevo Usuario</button>
                    </div> <!-- .card-header -->
                    <div class="card-body">
                        <table id="tbllistado" class="table table-bordered table-sm table-hover table-responsive-sm" style="width: 100%;">
                            <thead>
                                <th>No.</th>
                                <th>Nombre</th>
                                <th>Usuario</th>
                                <th>Correo</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-md-12 -->
        </div> <!-- .row -->
        <div class="row" id="formularioregistros">
            <div class="col-lg-12">
                <div class="card">
                    <form id="formulario" method="POST" class="form-horizontal">
                    <div class="card-header">
                        <b id="title_usr" class="text-playerytees">Nuevo Usuario</b>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="nombreUsuario" class="form-control-label text-playerytees">Nombre:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="hidden" name="id_usuario" id="id_usuario">
                                <input id="nombreUsuario" name="nombreUsuario" class="form-control" type="text" maxlength="45" required>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="emailRegistro" class=" form-control-label text-playerytees">Correo Electronico:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input id="emailRegistro" name="emailRegistro" class="form-control" type="email" maxlength="45">
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="AccessUser" class=" form-control-label text-playerytees">Usuario:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input id="AccessUser" name="AccessUser" class="form-control" type="text" maxlength="60" required>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group" id="divpassword">
                            <div class="col col-md-2">
                                <label for="AccessPass" class=" form-control-label text-playerytees">Contraseña:</label>
                            </div>
                            <div class="col-12 col-md-4">
                                <input id="AccessPass" name="AccessPass" class="form-control" type="password" maxlength="45" required pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Mayúsculas, Minúscula, Número, Minimo 8 Caracteres">
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-2">
                                <label for="AccessPass" class=" form-control-label text-playerytees">Permisos:</label>
                            </div>
                            <div class="col-md-10" >
                            <div class="row">
                                <div class="col"><h6>General</h6><ul style="list-style: none;" id="permisos_general" required>

                                </ul></div>
                                <div class="col"><h6>Sucursales</h6><ul style="list-style: none;" id="permisos_sucursales" required>

                                </ul></div>
                                <div class="col"><h6>Empresas</h6><ul style="list-style: none;" id="permisos_empresas" required>

                                </ul></div>
                                <div class="col"><h6>Reportes</h6><ul style="list-style: none;" id="permisos_reportes" required>

                                </ul></div>
                                <div class="col"><h6>Ajustes</h6><ul style="list-style: none;" id="permisos_ajustes" required>

                                </ul></div>
                            </div>
                            </div>
                        </div>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-playerytees btn-sm">Guardar</button>
                        <button type="reset" class="btn btn-danger btn-sm" onclick="cancelarform()">Cancelar</button>
                    </div> <!-- .card-footer -->
                    </form> <!-- .form -->
                </div> <!-- .card -->
            </div> <!-- .col-lg-12 -->
        </div> <!-- .row -->
    </div> <!-- .content -->
<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="../public/assets/js/toastr.min.js"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script type="text/javascript" src="scripts/usuario.js"></script>
<?php
}
ob_end_flush();
?>