<?php
require_once "../modelos/PedidosAbiertos.php";
session_start();
$pedidos = new PedidosAbiertos();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$empresa = isset($_POST["empresa"])?htmlspecialchars(trim($_POST["empresa"])):"";
$cliente = isset($_POST["cliente"])?htmlspecialchars(trim($_POST["cliente"])):"";
$articulo = isset($_POST["articulo"])?htmlspecialchars(trim(($_POST["articulo"]))):"";
$estacion = isset($_POST["estacion"])?$_POST["estacion"]:"";

switch ($_GET["opcion"]) {
    case 'listar':
        if (empty($estacion)) {
            $estacion = array("''");
        }
        $estaciones = implode(",", array_filter($estacion));
        $result = $pedidos->listar( $empresa, "'$cliente'", "'$articulo'", "$estaciones" );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => $reg->FolioR1,
                '1' => date_format(date_create(utf8_encode($reg->FechaCreacion)), 'd/m/y'),
                '2' => $reg->Articulo,
                '3' => $reg->Talla,
                '4' => $reg->Color,
                '5' => $reg->Linea,
                '6' => utf8_encode($reg->Marca),
                '7' => number_format($reg->CantidadPendiente, 0, '.', ','),
                '8' => $reg->CodigoCliente,
                '9' => "<div title='".utf8_encode($reg->NombreCliente)."' style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width:200px;'>".utf8_encode($reg->NombreCliente)."</div>",
                '10' => "<div title='".utf8_encode($reg->Vendedor)."' style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width:200px;'>".utf8_encode($reg->Vendedor)."</div>",
                '11' => "<div title='".utf8_encode($reg->Comentarios)."' style='text-overflow: ellipsis; white-space: nowrap; overflow: hidden; width:200px;'>".utf8_encode($reg->Comentarios)."</div>",
                '12' => utf8_encode($reg->OrdenCompra),
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);

        break;

    case 'listarEstacion':
        $result = $pedidos->listarEstacion( $empresa );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => utf8_encode($reg->Code),
                '1' => utf8_encode($reg->Name)
            );
        }

        echo json_encode($data);

        break;

    case 'listarMarca':
        $result = $pedidos->listarMarca( $empresa );
        $data = array();

        foreach ($result as $reg) {
            $data[] = array(
                '0' => utf8_encode($reg->FirmCode),
                '1' => utf8_encode($reg->FirmName)
            );
        }

        echo json_encode($data);

        break;

    default:
        echo "No se encontro la opcion";
        break;
}