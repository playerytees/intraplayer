<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['pedidosAbiertos']==1) {
?>
<style>
#tblPedidosAbiertos_filter{
    float: right;
}
</style>
    <div class="content mt-2">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Pedidos Abiertos</strong>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <buttton type="butoon" class="btn btn-outline-success btn-sm d-none" id="btnDPT">Punto Textil</buttton>
                        <buttton type="butoon" class="btn btn-outline-primary btn-sm active" id="btnGP">Global playerytees</buttton>
                    </div>
                </div>
                <div class="row">
                <form action="" class="w-100" method="POST">
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="input_cliente" class="control-label mb-1 text-playerytees">Codigo Cliente:</label>
                            <input type="hidden" name="input_empresa" id="input_empresa" value="GP_BD">
                            <input type="text" name="input_cliente" id="input_cliente" class="form-control" required />
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="input_articulo" class="control-label mb-1 text-playerytees">Articulo:</label>
                            <input type="text" name="input_articulo" id="input_articulo" class="form-control" required />
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-4 col-xl-3">
                        <div class="form-group">
                            <label for="select_estacion" class="control-label mb-1 text-playerytees">Estacion:</label>
                            <select name="select_estacion[]" id="select_estacion" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar estacion..." multiple="multiple">
                            </select>
                        </div> <!-- .form-group -->
                    </div> <!-- .col-3 -->
                    <div class="col-12 col-md-12 col-xl-1">
                        <button type="submit" id="btnBuscar" class="btn btn-playerytees my-3">BUSCAR &nbsp;<i class="fa fa-search"></i></button>
                    </div>
                </form>
                </div> <!-- .row -->

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div id="contentTblVentas">
                                <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tblPedidosAbiertos" style=width:100%;">
                                    <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                        <tr>
                                            <th>Folio R1</th>
                                            <th>Fecha</th>
                                            <th>Articulo</th>
                                            <th>Talla</th>
                                            <th>Color</th>
                                            <th>Linea</th>
                                            <th>Marca</th>
                                            <th>Pendiente</th>
                                            <th>Codigo</th>
                                            <th>Cliente</th>
                                            <th>Vendedor</th>
                                            <th>Comentarios</th>
                                            <th>Destino</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 14px;"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- .row -->

            </div> <!-- card-body -->
        </div> <!-- .card -->
    </div> <!-- .content .mt-3 -->

<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="scripts/pedidosAbiertos.js"></script>
<?php
}
ob_end_flush();
?>