<?php
session_start();
require_once "../modelos/Usuario.php";
 
$usuario=new Usuario();
 
$id_usuario=isset($_POST["id_usuario"])?limpiarCadena($_POST["id_usuario"]):"";
$nombreUsuario=isset($_POST['nombreUsuario'])?limpiarCadena($_POST['nombreUsuario']):"";
$AccessUser=isset($_POST['AccessUser'])?limpiarCadena($_POST['AccessUser']):"";
$AccessPass=isset($_POST['AccessPass'])?limpiarCadena($_POST['AccessPass']):"";
$emailRegistro=isset($_POST['emailRegistro'])?limpiarCadena($_POST['emailRegistro']):"";

switch ($_GET["opcion"]){
    case 'guardaryeditar':
        $caracteres = "abcdefghijklmnopqrstuvwxyz1234567890";
        $nueva_clave = "";
        for ($i = 5; $i < 35; $i++) {
            $nueva_clave .= $caracteres[rand(5,35)];
        }        

        $aleatorio = $nueva_clave;
        $valor = "07";
        $salt = "$2y$".$valor."$".$aleatorio."$";

        $passwordConSalt = empty($AccessPass)?"":crypt($AccessPass, $salt);
 
        if (empty($id_usuario)){
            $rspta=$usuario->insertar($nombreUsuario, $AccessUser, $passwordConSalt, $emailRegistro, $_POST['permiso']);
            echo $rspta;
        }
        else {
            $rspta=$usuario->editar($id_usuario, $nombreUsuario, $AccessUser, $passwordConSalt, $emailRegistro, $_POST['permiso']);
            echo $rspta;
        }
    break;
 
    case 'desactivar':
        $rspta=$usuario->desactivar($id_usuario);
        echo $rspta ? "Usuario Desactivado" : "Usuario no se puede desactivar";
    break;
 
    case 'activar':
        $rspta=$usuario->activar($id_usuario);
        echo $rspta ? "Usuario activado" : "Usuario no se puede activar";
    break;
 
    case 'mostrar':
        $rspta=$usuario->mostrar($id_usuario);

        echo json_encode($rspta);
    break;
 
    case 'listar':
        $rspta=$usuario->listar();
        //Vamos a declarar un array
        $data= Array();

        while ($reg=$rspta->fetch_object()){
            $data[]=array(
                "0"=>$reg->id_usuario,
                "1"=>$reg->nombreUsuario,
                "2"=>$reg->AccessUser,
                "3"=>$reg->emailRegistro,
                "4"=>$reg->Estado,
                "5"=>"<a href='#' onclick='mostrar(".$reg->id_usuario.")' > Editar </a>"
            );
        }
        $results = array(
            "sEcho"=>1,
            "iTotalRecords"=>count($data),
            "iTotalDisplayRecords"=>count($data),
            "aaData"=>$data);
        echo json_encode($results);
 
    break;
 
    case 'permisos':
        //Obtenemos todos los permisos de la tabla permisos
        require_once "../modelos/Permiso.php";
        $permiso = new Permiso();
        $rspta = $permiso->listar();
 
        //Obtener los permisos asignados al usuario
        $id=$_GET['id'];
        $marcados = $usuario->listarmarcados($id);
        //Declaramos el array para almacenar todos los permisos marcados
        $valores=array();
 
        //Almacenar los permisos asignados al usuario en el array
        while ($per = $marcados->fetch_object())
            {
                array_push($valores, $per->idpermiso);
            }
 
        //Mostramos la lista de permisos en la vista y si están o no marcados
        $permisos= Array();
        while ($reg = $rspta->fetch_object())
        {
            $sw=in_array($reg->id,$valores)?'checked':'';
            $permisos[]=array(
                "id"=>$reg->id,
                "nombre"=>$reg->nombre,
                "descripcion"=>$reg->descripcion,
                "checked"=>$sw,
            );
        }
        echo json_encode($permisos);
    break;
 
    case 'verificar':
        $logina=$_POST['logina'];
        $clavea=$_POST['clavea'];
 
        $rspta=$usuario->verificar($logina);

        $fetch=$rspta->fetch_object();
 
        if (isset($fetch))
        {
            //Comparamos si la contraseña es correcta
            if( ($logina == $fetch->AccessUser) && ( password_verify($clavea, $fetch->AccessPass) ) && ( $fetch->Estado == 'A') )
            {
                //Declaramos las variables de sesión
                $_SESSION['idusuario']=$fetch->id_usuario;
                $_SESSION['nombre']=$fetch->nombreUsuario;
                $_SESSION['user']=$fetch->AccessUser;
                $_SESSION['email']=$fetch->emailRegistro;

                //Obtenemos los permisos del usuario
                $marcados = $usuario->listarmarcados($fetch->id_usuario);
    
                //Declaramos el array para almacenar todos los permisos marcados
                $valores=array();
    
                //Almacenamos los permisos marcados en el array
                while ($per = $marcados->fetch_object())
                {
                    array_push($valores, $per->idpermiso);
                }

                //Determinamos los accesos del usuario
                in_array(1,$valores)?$_SESSION['embarques']=1:$_SESSION['embarques']=0;
                in_array(2,$valores)?$_SESSION['resurtido']=1:$_SESSION['resurtido']=0;
                in_array(3,$valores)?$_SESSION['usuarios']=1:$_SESSION['usuarios']=0;
                in_array(4,$valores)?$_SESSION['tag']=1:$_SESSION['tag']=0;
                in_array(5,$valores)?$_SESSION['existencias']=1:$_SESSION['existencias']=0;
                in_array(6,$valores)?$_SESSION['5febrero']=1:$_SESSION['5febrero']=0;
                in_array(7,$valores)?$_SESSION['cdmx']=1:$_SESSION['cdmx']=0;
                in_array(8,$valores)?$_SESSION['cuernavaca']=1:$_SESSION['cuernavaca']=0;
                in_array(9,$valores)?$_SESSION['hermosillo']=1:$_SESSION['hermosillo']=0;
                in_array(10,$valores)?$_SESSION['isabel']=1:$_SESSION['isabel']=0;
                in_array(11,$valores)?$_SESSION['lorey']=1:$_SESSION['lorey']=0;
                in_array(12,$valores)?$_SESSION['monterrey']=1:$_SESSION['monterrey']=0;
                in_array(13,$valores)?$_SESSION['oaxaca']=1:$_SESSION['oaxaca']=0;
                in_array(14,$valores)?$_SESSION['pachuca']=1:$_SESSION['pachuca']=0;
                in_array(15,$valores)?$_SESSION['noria']=1:$_SESSION['noria']=0;
                in_array(16,$valores)?$_SESSION['sanluis']=1:$_SESSION['sanluis']=0;
                in_array(17,$valores)?$_SESSION['DPT']=1:$_SESSION["DPT"]=0;
                in_array(18,$valores)?$_SESSION["GP"]=1:$_SESSION["GP"]=0;
                in_array(19,$valores)?$_SESSION["oaxaca2"]=1:$_SESSION["oaxaca2"]=0;
                in_array(20,$valores)?$_SESSION['CEDIS']=1:$_SESSION["CEDIS"]=0;
                in_array(21,$valores)?$_SESSION['VentasTotales']=1:$_SESSION['VentasTotales']=0;
                in_array(22,$valores)?$_SESSION['CedisDPT']=1:$_SESSION['CedisDPT']=0;
                in_array(23,$valores)?$_SESSION['Distribuidores']=1:$_SESSION['Distribuidores']=0;
                in_array(24,$valores)?$_SESSION['proximasllegadas']=1:$_SESSION['proximasllegadas']=0;
                in_array(25,$valores)?$_SESSION['imprimiretiqueta']=1:$_SESSION['imprimiretiqueta']=0;
                in_array(26,$valores)?$_SESSION['pagos']=1:$_SESSION['pagos']=0;
                in_array(27,$valores)?$_SESSION['ventasArticulos']=1:$_SESSION['ventasArticulos']=0;
                in_array(28,$valores)?$_SESSION['ventasMarca']=1:$_SESSION['ventasMarca']=0;
                in_array(29,$valores)?$_SESSION['pedidosAbiertos']=1:$_SESSION['pedidosAbiertos']=0;
                in_array(30,$valores)?$_SESSION['RegistrarPresupuesto']=1:$_SESSION['RegistrarPresupuesto']=0;
                in_array(31,$valores)?$_SESSION['sanluis2']=1:$_SESSION['sanluis2']=0;

                echo json_encode($fetch);
            }
            else
            {
                echo "null";
            }
        }
        else
        {
            echo "null";
        }
    break;
 
    case 'salir':
        //Limpiamos las variables de sesión
        session_unset();
        //Destruìmos la sesión
        session_destroy();
        //Redireccionamos al login
        header("Location: ../index.html");
 
    break;
}
?>