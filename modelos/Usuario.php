<?php 
//Incluímos inicialmente la conexión a la base de datos
require "../config/Conexion.php";
 
Class Usuario
{
    public function __construct()
    {
 
    }
 
    public function insertar($nombreUsuario, $AccessUser, $AccessPass, $emailRegistro, $permisos)
    {
        $sql="INSERT INTO usuarios (nombreUsuario, AccessUser, AccessPass, emailRegistro, tipo, Estado) VALUES ('$nombreUsuario', '$AccessUser', '$AccessPass', '$emailRegistro', 'NULL', 'A')";
        //ejecutarConsulta($sql);
        $idusuarionew=ejecutarConsulta_retornarID($sql);
 
        $num_elementos=0;
        $sw=true;
 
        while ($num_elementos < count($permisos))
        {
            $sql_detalle = "INSERT INTO usuario_permiso(idusuario, idpermiso) VALUES('$idusuarionew', '$permisos[$num_elementos]')";
            ejecutarConsulta($sql_detalle) or $sw = false;
            $num_elementos=$num_elementos + 1;
        }
 
        return $sw;
    }
 
    public function editar($id_usuario, $nombreUsuario, $AccessUser, $AccessPass, $emailRegistro, $permisos)
    {
        if ($AccessPass != "") {
            $sql="UPDATE usuarios SET nombreUsuario='$nombreUsuario', AccessUser='$AccessUser', AccessPass='$AccessPass', emailRegistro='$emailRegistro' WHERE id_usuario='$id_usuario'";
            ejecutarConsulta($sql);
        } else {
            $sql="UPDATE usuarios SET nombreUsuario='$nombreUsuario', AccessUser='$AccessUser', emailRegistro='$emailRegistro' WHERE id_usuario='$id_usuario'";
            ejecutarConsulta($sql);
        }

        //Eliminamos todos los permisos asignados para volverlos a registrar
        $sqldel="DELETE FROM usuario_permiso WHERE idusuario='$id_usuario'";
        ejecutarConsulta($sqldel);

        $num_elementos=0;
        $sw=true;

        while ($num_elementos < count($permisos))
        {
            $sql_detalle = "INSERT INTO usuario_permiso(idusuario, idpermiso) VALUES('$id_usuario', '$permisos[$num_elementos]')";
            ejecutarConsulta($sql_detalle) or $sw = false;
            $num_elementos=$num_elementos + 1;
        }

        return $sw;
    }

    public function desactivar($id_usuario)
    {
        $sql="UPDATE usuarios SET estado='I' WHERE id_usuario='$id_usuario'";
        return ejecutarConsulta($sql);
    }

    public function activar($id_usuario)
    {
        $sql="UPDATE usuarios SET condicion='A' WHERE id_usuario='$id_usuario'";
        return ejecutarConsulta($sql);
    }

    public function mostrar($id_usuario)
    {
        $sql="SELECT * FROM usuarios WHERE id_usuario='$id_usuario'";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql="SELECT * FROM usuarios";
        return ejecutarConsulta($sql);
    }

    public function listarmarcados($id_usuario)
    {
        $sql="SELECT * FROM usuario_permiso WHERE idusuario='$id_usuario'";
        return ejecutarConsulta($sql);
    }

    public function verificar($login)
    {
        $sql="SELECT id_usuario, nombreUsuario, AccessUser, AccessPass, emailRegistro, Estado FROM usuarios WHERE AccessUser='$login' AND Estado='A'"; 
        return ejecutarConsulta($sql);
    }
}

?>