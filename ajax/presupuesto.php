<?php
require_once "../modelos/Presupuesto.php";
session_start();
$presupuesto = new Presupuesto();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$sucursal = isset($_POST['sucursal'])?limpiarCadena($_POST['sucursal']):"";
$meta_anterior = isset($_POST['meta_anterior'])?limpiarCadena($_POST['meta_anterior']):"";
$meta_actual = isset($_POST['meta_actual'])?limpiarCadena($_POST['meta_actual']):"";
$mes = isset($_POST['mes'])?limpiarCadena($_POST['mes']):"";

switch ($_GET['opcion']) {
    case 'guardartable':
        $tblJsonDecode = json_decode($_POST['tblExcel'], true);
        $response = "";
        foreach ($tblJsonDecode as $key => $value) {
            $exists = $presupuesto->consultarSiExiste($value['Mes'], $value['Anio'],$value['Sucursal']);

            if ($exists==null) {
                $response = $presupuesto->guardarTable($value['Sucursal'],$value['Meta_anterior'], $value['Meta_actual'],$value['Mes'], $value['Anio']);
            } else {
                $response = "Existe";
            }
        }

        echo $response;
        break;
    case 'agregaryeditar':
        $mes_anio = explode('-', $mes);
        $anio = $mes_anio[0];
        $mes_only = $mes_anio[1];
        if (empty($id)) {
            $exists = $presupuesto->consultarSiExiste($mes_only,$anio,$sucursal);
            if ($exists==null) {
                $response=$presupuesto->agregar($sucursal,$meta_anterior,$meta_actual,$mes_only,$anio);

                echo $response;
            } else {
                echo "Existe";
            }
        } else {
            /* $exists = $presupuesto->consultarSiExiste($mes_only,$anio,$sucursal); */
            /* if ($exists==null) { */
                $response=$presupuesto->editar($id,$sucursal,$meta_anterior,$meta_actual,$mes_only,$anio);
                
                echo $response;
            /* } else {
                echo "Existe";
            } */
        }
        break;
    case 'mostrar':
        $response = $presupuesto->mostrar($id);
        echo json_encode($response);
        break;
    case 'listar':
        $response=$presupuesto->listar();
        $data=array();

        while ($reg=$response->fetch_object()) {
            $data[]=array(
                "0" => $reg->id,
                "1" => $reg->sucursal,
                "2" => $reg->meta_anterior,
                "3" => $reg->meta_actual,
                "4" => $reg->mes,
                "5" => $reg->anio,
                "6" => '<div class="text-center"><a href="#" onclick="mostrar('.$reg->id.')" ><i class="fa fa-edit"></i></a></div>',
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        # code...
        break;
}