<table style="width: 100%; height: 100%; margin-top: 20px;">
  <tr>
    <td>
        <table style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
            <tr>
                <td style="font-weight: bold;">Cliente:</td>
                <td class="lblcliente"></td>
            </tr>
            <tr>
                <td  style="font-weight: bold;">Etiqueta:</td>
                <td class="lbletiqueta"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Composición:</td>
                <td class="lblcomposicion" colspan="2"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Talla:</td>
                <td class="lbltalla"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Cantidad:</td>
                <td class="lblcant"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Folio:</td>
                <td class="lblfolio-ch"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">OC:</td>
                <td class="lbl-oc-ch"></td>
            </tr>
        </table>
    </td>
    <td>&nbsp;</td>
    <td>
        <table style="width: 100%; height: 100%; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; margin-left: 20px;">
            <tr>
                <td style="font-weight: bold;">Cliente:</td>
                <td class="lblcliente"></td>
            </tr>
            <tr>
                <td  style="font-weight: bold;">Etiqueta:</td>
                <td class="lbletiqueta"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Composición:</td>
                <td class="lblcomposicion" colspan="2"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Talla:</td>
                <td class="lbltalla"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Cantidad:</td>
                <td class="lblcant"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Folio:</td>
                <td class="lblfolio-ch"></td>
            </tr>
            <tr>
                <td style="font-weight: bold;">OC:</td>
                <td class="lbl-oc-ch"></td>
            </tr>
        </table>
    </td>
  </tr>
</table> 
<script src="../../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script>
function init()
{
    var cliente = getParameterByName('cliente');
    var etiqueta = getParameterByName('etiqueta');
    var composicion = getParameterByName('composicion');
    var talla = getParameterByName('talla');
    var cantidad = getParameterByName('cantidad');
    var folio = getParameterByName('folio');
    var oc = getParameterByName('oc');

    $(".lblcliente").text(cliente);
    $(".lbletiqueta").text(etiqueta);
    $(".lblcomposicion").text(composicion);
    $(".lbltalla").text(talla);
    $(".lblcant").text(cantidad);
    $(".lblfolio-ch").text(folio);
    $(".lbl-oc-ch").text(oc);

	if (window.print) {
        setTimeout(function(){window.print();window.close();},300); // 500ms = 0.5s - 1000 = 1s
    }
}

function getParameterByName(name)
{
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

init();
</script>