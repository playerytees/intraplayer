<?php
    require "../config/ConexionHana.php";

    class ExistenciasDPT extends ConexionHana
    {
        public function __construct()
        {

        }

        public function listar($almacen, $marca, $estilo, $color)
        {
            $sqlestilo = ($estilo == "'-1'") ? 'AND T2."ItmsGrpCod" NOT IN (\'273\',\'299\',\'301\', \'316\', \'457\', \'458\')':'AND T2."ItmsGrpCod" IN ('.$estilo.')';
            $sqlcolor = ($color == "'-1'") ? "":'AND T0."U_Color" IN ('.$color.')';

            $sql = 'SELECT
                T2."ItmsGrpNam" "estilo",
                T2."U_DescripcionC" "descripcion",
                T3."Name" "color",
                T0."U_Talla" "talla",
                CAST(IFNULL(T1."OnHand", 0) AS integer) "existencia"
            FROM
                "DPT_BD"."OITM" T0
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            INNER JOIN
                "DPT_BD"."OITB" T2
            ON
                (T2."ItmsGrpCod" = T0."ItmsGrpCod")
            INNER JOIN
                "DPT_BD"."@MG_COLOR" T3
            ON
                (T3."Code" = T0."U_Color")
            WHERE
                T1."WhsCode" = '.$almacen.'
            AND
                T0."FirmCode" = '.$marca.'
            '.$sqlestilo.'
            '.$sqlcolor.'
            AND
                T0."validFor" = \'Y\'
            AND
                T0."U_Color" IS NOT NULL
            ORDER BY
                T2."ItmsGrpNam" ASC';

            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function listarMarca()
        {
            $sql = 'SELECT "FirmCode", "FirmName" FROM "DPT_BD"."OMRC" WHERE "FirmCode" NOT IN (\'-1\', \'4\', \'6\', \'7\')';
            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function listarEstilo($almacen, $marca)
        {
            $sql = 'SELECT
                T2."ItmsGrpCod",
                T2."ItmsGrpNam"
            FROM
                "DPT_BD"."OITM" T0 
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            INNER JOIN
                "DPT_BD"."OITB" T2
            ON
                (T2."ItmsGrpCod" = T0."ItmsGrpCod")
            INNER JOIN
                "DPT_BD"."OWHS" T3
            ON
                (T1."WhsCode" = T3."WhsCode")
            INNER JOIN
                "DPT_BD"."OMRC" T4
            ON
                (T0."FirmCode" = T4."FirmCode")
            WHERE
                T1."WhsCode" = '.$almacen.'
            AND
                T4."FirmCode" = '.$marca.'
            GROUP BY T2."ItmsGrpCod", T2."ItmsGrpNam"';
            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function listarColor($almacen, $marca, $estilo)
        {
            $sqlestilo = ($estilo == "'-1'") ? "":'AND T2."ItmsGrpCod" IN ('.$estilo.')';

            $sql = 'SELECT
                T3."Code" "codigo",
                T3."Name" "color"
            FROM
                "DPT_BD"."OITM" T0
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            INNER JOIN
                "DPT_BD"."OITB" T2
            ON
                (T2."ItmsGrpCod" = T0."ItmsGrpCod")
            INNER JOIN
                "DPT_BD"."@MG_COLOR" T3
            ON
                (T3."Code" = T0."U_Color")
            WHERE
                T1."WhsCode" = '.$almacen.'
            AND
                T0."FirmCode" = '.$marca.'
            '.$sqlestilo.'
            AND T0."U_Color" IS NOT NULL
            AND T0."validFor" = \'Y\'
            GROUP BY T3."Code", T3."Name"
            ORDER BY T3."Name" ASC';

            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function listarAlmacen()
        {
            $sql = 'SELECT "WhsCode", "WhsName" FROM "DPT_BD"."OWHS" WHERE "WhsCode" NOT IN(\'DE09\', \'QU03\', \'R01\', \'TR09\', \'TR08\', \'TR10\')';
            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function listarSucursal($almacen)
        {
            try {
                $sql = 'SELECT "WhsCode", "WhsName" FROM "DPT_BD"."OWHS" WHERE "WhsCode" IN('.$almacen.')';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function estiloDescrColor($almacen, $marca, $estilo, $color)
        { // TODO VERIFICAR
            $sqlestilo = ($estilo == "'-1'") ? 'AND T2."ItmsGrpCod" NOT IN (\'273\',\'299\',\'301\', \'316\', \'457\', \'458\')':'AND T2."ItmsGrpCod" IN ('.$estilo.')';
            $sqlcolor = ($color == "'-1'") ? "":'AND T0."U_Color" IN ('.$color.')';

            $sql = 'SELECT
                T2."ItmsGrpNam" "estilo",
                T3."Name" "color",
                T2."U_DescripcionC" "descripcion"
            FROM
                "DPT_BD"."OITM" T0
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            INNER JOIN
                "DPT_BD"."OITB" T2
            ON
                (T2."ItmsGrpCod" = T0."ItmsGrpCod")
            INNER JOIN
                "DPT_BD"."@MG_COLOR" T3
            ON
                (T3."Code" = T0."U_Color")
            WHERE
                T1."WhsCode" = '.$almacen.'
            AND
                T0."FirmCode" = '.$marca.'
            '.$sqlestilo.'
            '.$sqlcolor.'
            AND
                T0."validFor" = \'Y\'
            AND
                T0."U_Color" IS NOT NULL
            GROUP BY T2."ItmsGrpNam", T3."Name", T2."U_DescripcionC"
            ORDER BY T2."ItmsGrpNam" ASC';

            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function listarPorAlmacen($marca, $estilo, $color)
        {
            $sqlestilo = ($estilo == "'-1'") ? 'AND T2."ItmsGrpCod" NOT IN (\'273\',\'299\',\'301\', \'316\', \'457\', \'458\')':'AND T2."ItmsGrpCod" IN ('.$estilo.')';
            $sqlcolor = ($color == "'-1'") ? "":'AND T0."U_Color" IN ('.$color.')';

            $sql = 'SELECT
                T3."WhsName" "almacen",
                T2."ItmsGrpNam" "estilo",
                T2."U_DescripcionC" "descripcion",
                T0."U_Color" "color",
                T0."U_Talla" "talla",
                CAST(IFNULL(T1."OnHand", 0) AS integer) "existencia"
            FROM
                "DPT_BD"."OITM" T0
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            INNER JOIN
                "DPT_BD"."OITB" T2
            ON
                (T2."ItmsGrpCod" = T0."ItmsGrpCod")
            INNER JOIN
                "DPT_BD"."OWHS" T3
            ON
                (T1."WhsCode" = T3."WhsCode")
            WHERE
                T0."FirmCode" = '.$marca.'
            '.$sqlestilo.'
            '.$sqlcolor.'
            AND
                T0."validFor" = \'Y\'
            AND
                T0."U_Color" IS NOT NULL
            ORDER BY
                T2."ItmsGrpNam" ASC';

            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function estiloDescrColorAlmacen($marca, $estilo, $color)
        {
            $sqlestilo = ($estilo == "'-1'") ? 'AND T2."ItmsGrpCod" NOT IN (\'273\',\'299\',\'301\', \'316\', \'457\', \'458\')':'AND T2."ItmsGrpCod" IN ('.$estilo.')';
            $sqlcolor = ($color == "'-1'") ? "":'AND T0."U_Color" IN ('.$color.')';

            $sql = 'SELECT
                T3."WhsName" "almacen",
                T2."ItmsGrpNam" "estilo",
                T0."U_Color" "color",
                T2."U_DescripcionC" "descripcion"
            FROM
                "DPT_BD"."OITM" T0
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            INNER JOIN
                "DPT_BD"."OITB" T2
            ON
                (T2."ItmsGrpCod" = T0."ItmsGrpCod")
            INNER JOIN
                "DPT_BD"."OWHS" T3
            ON
                (T1."WhsCode" = T3."WhsCode")
            WHERE
                T0."FirmCode" = '.$marca.'
            '.$sqlestilo.'
            '.$sqlcolor.'
            AND T0."validFor" = \'Y\'
            AND T0."U_Color" IS NOT NULL
            AND T1."WhsCode" NOT IN (\'CO10\', \'DE09\', \'EX07\', \'EX07\', \'MP09\', \'QU03\', \'R01\', \'TR08\', \'TR09\', \'TR10\')
            GROUP BY T2."ItmsGrpNam", T0."U_Color", T2."U_DescripcionC", T3."WhsName"
            ORDER BY T2."ItmsGrpNam" ASC';

            $stmt = $this->connect()->query($sql);
            return $stmt;
        }

        public function busquedaPorCodigo($almacen, $codigo)
        {
            try {
                $sql = 'SELECT
                T0."ItemCode" "Codigo",
                T0."ItemName" "Descripcion",
                CAST(IFNULL(T1."OnHand", 0) AS integer) "Existencia"
            FROM
                "DPT_BD"."OITM" T0
            INNER JOIN
                "DPT_BD"."OITW" T1
            ON
                (T1."ItemCode" = T0."ItemCode")
            WHERE
                T1."WhsCode" = '.$almacen.'
            AND
                T0."ItemCode" LIKE '.$codigo.'
            AND
                T0."validFor" = \'Y\'';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt;

            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function busquedaPorAlmacen($marca, $estilo, $color)
        {
            try {
                $sqlestilo = (empty($estilo)) ? "":'AND T0."ItmsGrpCod" IN ('.$estilo.')';
                $sqlcolor = (empty($color)) ? "":'AND T0."U_Color" IN ('.$color.')';
                $sql = 'SELECT
                    T0."ItemCode" "Codigo",
                    SUBSTRING(T0."ItemName", 1, 30) "Descripcion",
                    T2."WhsName" "Almacen",
                    CAST(IFNULL(T1."OnHand", 0) AS integer) "Existencia"
                FROM
                    "DPT_BD"."OITM" T0
                INNER JOIN "DPT_BD"."OITW" T1
                ON (T1."ItemCode" = T0."ItemCode")
                INNER JOIN "DPT_BD"."OWHS" T2
                ON (T1."WhsCode" = T2."WhsCode")
                WHERE
                    T0."FirmCode" IN ('.$marca.')
                '.$sqlestilo.'
                '.$sqlcolor.'
                AND T1."WhsCode" NOT IN (\'CO10\', \'DE09\', \'EX07\', \'EX07\', \'MP09\', \'QU03\', \'R01\', \'TR08\', \'TR09\', \'TR10\')
                AND T0."validFor" = \'Y\'
                ORDER BY T0."FirmCode";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function imprimirEtiqueta($marca, $estilo, $color)
        {
            try {
                $sql = 'SELECT "ItemCode" FROM "DPT_BD"."OITM" WHERE "FirmCode" = '.$marca.' AND "ItmsGrpCod" IN('.$estilo.') AND "U_Color" IN('.$color.') AND "validFor" = \'Y\' ORDER BY "ItmsGrpCod";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }