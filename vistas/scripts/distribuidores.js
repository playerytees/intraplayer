var tabla;

function init() {
    $(".loader").hide();
    listar( $("#fecha").val() );
}

function listar(fecha) {
    tabla = $("#tabladistribuidores").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var diasHabiles;
            var diasTranscurridos;
            var diasRestantes;

            var prom_diario;
            var tendencia;
            var difVsAA;
            var PorcentajeVsAA;
            var difVsCuota;
            var PorcentajeVsCuota;
            var prom_necesario;

            $( api.column( 0 ).footer() ).html("TOTAL:");
            $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 1 ).data().sum().toFixed(2) ) );
            $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 2 ).data().sum().toFixed(2) ) );

            $.post("../ajax/ventas.php?opcion=bussiness_days", {fecha: fecha}, function (data, textStatus, jqXHR) {
                data = JSON.parse(data);

                diasHabiles = data[0][0];
                diasTranscurridos = data[0][1];
                diasRestantes = data[0][2];

                prom_diario = tabla.column( 2 ).data().sum().toFixed(2) / diasTranscurridos;
                tendencia = prom_diario * diasHabiles;
                difVsAA = tendencia - tabla.column( 5 ).data().sum().toFixed(2);
                PorcentajeVsAA = difVsAA / tabla.column( 5 ).data().sum().toFixed(2);
                difVsCuota = tendencia - tabla.column( 8 ).data().sum().toFixed(2);
                PorcentajeVsCuota = difVsCuota / tabla.column( 8 ).data().sum().toFixed(2);
                prom_necesario = ( tabla.column( 8 ).data().sum().toFixed(2) - tabla.column( 2 ).data().sum().toFixed(2) ) / diasRestantes;

                $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_diario ) );
                $( api.column( 4 ).footer() ).html( new Intl.NumberFormat("en-US").format( tendencia ) );
                $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 5 ).data().sum().toFixed(2) ) );
                $( api.column( 6 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsAA ) );
                $( api.column( 7 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsAA*100 ) );
                $( api.column( 8 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 8 ).data().sum().toFixed(2) ) );
                $( api.column( 9 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsCuota ) );
                $( api.column(10 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsCuota*100 ) );
                $( api.column(11 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_necesario ) );
            });
        },
        "ajax": {
            url: "../ajax/distribuidores.php?opcion=listar",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "asc"]]
    }).DataTable();
}

$("#btn-ver").click(function (e) { 
    e.preventDefault();
    listar($("#fecha").val());
});

init();