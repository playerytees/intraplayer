var tabla_gp;
var tabla_dpt;
var tblAlmacen;
var tablaproximasllegadas;

function init() {
    $(".loader").hide();
    $("#contenido-gp").hide();
    $("#contenido-dpt").hide();
    $("#carga-almacen").hide();
    $("#btn_almacen_gp").hide();
    $("#btnBuscarFecha_dpt").hide();
    $("#btnBuscarFecha_gp").hide();
    $('.form-control-chosen').select2({
        language: "es",
        width: '100%',
        closeOnSelect: false
    });

    listarAlmacenDPT();
    listarAlmacenGP();

    listarMarcaDPT('CE01');
    listarMarcaGP();

    listarEstiloDPT('CE01', '3');
    listarEstiloGP('CE01', '5');

    listarColorDPT('CE01', '3', '');
    listarColorGP('CE01', '5', '');

    listarDPT('CE01', '3', '', '');
    listarGP('CE01', '5', '', '');

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
           .columns.adjust();
     });
     $("#LlegadasModal").on('shown.bs.modal', function () {
        $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
    });
}

function listarAlmacenDPT() {
    $.post("../ajax/existenciasDPT.php?opcion=listarAlmacen",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "<option></option>";
            for (let i = 0; i < data.length; i++) {  
                select += `<option ${(data[i][0]=="CE01"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_almacen_dpt").append(select);
            $("#select_almacen_dpt").trigger("chosen:updated");
        }
    );
}

function listarAlmacenGP() {
    $.post("../ajax/existenciasGP.php?opcion=listarAlmacen",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "<option></option>";
            for (let i = 0; i < data.length; i++) {  
                select += `<option ${(data[i][0]=="CE01"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_almacen_gp").append(select);
            $("#select_almacen_gp").trigger("chosen:updated");
        }
    );
}

function listarMarcaDPT(almacen) {
    $.post("../ajax/existenciasDPT.php?opcion=listarMarca",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                if (almacen == "CE01") {
                    select += `<option ${(data[i][0]=="3"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
                } else {
                    select += `<option ${(data[i][0]=="5"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
                }
            }
            $("#select_marca_dpt").append(select);
            $("#select_marca_dpt").trigger("chosen:updated");
        }
    );
}

function listarMarcaGP() {
    $.post("../ajax/existenciasGP.php?opcion=listarMarca",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                select += `<option ${(data[i][0]=="5"?"selected":"")} value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_marca_gp").append(select);
            $("#select_marca_gp").trigger("chosen:updated");
        }
    );
}

function listarEstiloDPT(almacen, marca) {
    $.post("../ajax/existenciasDPT.php?opcion=listarEstilo", {idAlmacen: almacen, idMarca: marca},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            // var select = `<option value="${data[0][0]}" selected>${data[0][1]}</option>`;
            for (let i = 0; i < data.length; i++) {
                select += `<option value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_estilo_dpt").append(select);
            $("#select_estilo_dpt").trigger("chosen:updated");
        }
    );
}

function listarEstiloGP(almacen, marca) {
    $.post("../ajax/existenciasGP.php?opcion=listarEstilo", {idAlmacen: almacen, idMarca: marca},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            // var select = `<option value="${data[0][0]}" selected>${data[0][1]}</option>`;
            for (let i = 0; i < data.length; i++) {
                select += `<option value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#select_estilo_gp").append(select);
            $("#select_estilo_gp").trigger("chosen:updated");
        }
    );
}

function listarColorDPT(almacen, marca, estilo) {
    $.post("../ajax/existenciasDPT.php?opcion=listarColor", {idAlmacen: almacen, idMarca: marca, idEstilo: estilo},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                select += `<option value="'${data[i][1]}'">${data[i][0]}</option>`;
            }
            $("#select_color_dpt").append(select);
            $("#select_color_dpt").trigger("chosen:updated");
        }
    );
}

function listarColorGP(almacen, marca, estilo) {
    $.post("../ajax/existenciasGP.php?opcion=listarColor", {idAlmacen: almacen, idMarca: marca, idEstilo: estilo},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                select += `<option value="'${data[i][1]}'">${data[i][0]}</option>`;
            }
            $("#select_color_gp").append(select);
            $("#select_color_gp").trigger("chosen:updated");
        }
    );
}

function listarDPT(almacen, marca, estilo, color) {
    $("#contenido-dpt").show();
    $("#btnlistar-dpt").prop("disabled",true);
    $.post("../ajax/existenciasDPT.php?opcion=listar", {idAlmacen: almacen, idMarca: marca, idEstilo: estilo, idColor: color},
        function (data, textStatus, jqXHR) {
            $("#contenido-dpt").hide();
            $("#btnlistar-dpt").prop("disabled",false);
            $("#stock_table_dpt").append(data);
            tabla_dpt = $("#stock_table_dpt").dataTable({
                "scrollY": "400px",
                "scrollX": true,
                "paging": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                // dom: 'Bfrtp',
                dom: '<"top"lp>rt<"bottom"B><"clear">',
                buttons: [
                    'copyHtml5',
                    {
                        extend: 'excelHtml5',
                        title: 'Existencias DPT'
                    },
                    {
                        extend: 'csvHtml5',
                        title: "Existencias DPT"
                    }
                    // 'pdfHtml5'
                ]
            }).DataTable();

            $('#stock_table_dpt td:last-child').addClass('text-playerytees');
        }
    );
}

function listarGP(almacen, marca, estilo, color) {
    $("#contenido-gp").show();
    $("#btnlistar-gp").prop("disabled",true);
    $.post("../ajax/existenciasGP.php?opcion=listar", {idAlmacen: almacen, idMarca: marca, idEstilo: estilo, idColor: color},
        function (data, textStatus, jqXHR) {
            $("#contenido-gp").hide();
            $("#btnlistar-gp").prop("disabled",false);
            $("#stock_table_gp").append(data);
            tabla_gp = $("#stock_table_gp").dataTable({
                "scrollY": "400px",
                "scrollX": true,
                "paging": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                // dom: 'Bfrtp',
                dom: '<"top"lp>rt<"bottom"B><"clear">',
                buttons: [
                    'copyHtml5',
                    {
                        extend: 'excelHtml5',
                        title: "Existencias GP"
                    },
                    {
                        extend: 'csvHtml5',
                        title: 'Existencias GP'
                    }
                    // 'pdfHtml5'
                ]
            }).DataTable();

            $('#stock_table_gp td:last-child').addClass('text-playerytees');
        }
    );
}

$("#select_almacen_dpt").change(function (e) { 
    e.preventDefault();
    listarMarcaDPT($(this).val());

    $("#select_estilo_dpt").empty();
    listarEstiloDPT($(this).val(), ($(this).val()=='CE01')?'3':'5');

    $("#select_color_dpt").empty();
    setTimeout(function(){listarColorDPT($("#select_almacen_dpt").val(), $("#select_marca_dpt").val(), $("#select_estilo_dpt").val());}, 1500);
});

$("#select_almacen_gp").change(function (e) { 
    e.preventDefault();
    listarMarcaGP();

    $("#select_estilo_gp").empty();
    listarEstiloGP($(this).val(), '5');

    $("#select_color_gp").empty();
    setTimeout(function(){listarColorGP($(this).val(), $("#select_marca_gp").val(), $("#select_estilo_gp").val());}, 1500);
});

$("#select_marca_dpt").change(function (e) { 
    e.preventDefault();
    $("#select_estilo_dpt").empty();
    listarEstiloDPT($("#select_almacen_dpt").val(), $(this).val());

    $("#select_color_dpt").empty();
    setTimeout(function(){listarColorDPT($("#select_almacen_dpt").val(), $("#select_marca_dpt").val(), $("#select_estilo_dpt").val());}, 1000);
});

$("#select_marca_gp").change(function (e) { 
    e.preventDefault();
    $("#select_estilo_gp").empty();
    listarEstiloGP($("#select_almacen_gp").val(), $(this).val());

    $("#select_color_gp").empty();
    setTimeout(function(){listarColorGP($("#select_almacen_gp").val(), $("#select_marca_gp").val(), $("#select_estilo_gp").val());}, 1000);
});

$("#select_estilo_dpt").change(function (e) { 
    e.preventDefault();

    $("#select_color_dpt").empty();
    listarColorDPT($("#select_almacen_dpt").val(), $("#select_marca_dpt").val(), $(this).val());
});

$("#select_estilo_gp").change(function (e) { 
    e.preventDefault();

    $("#select_color_gp").empty();
    listarColorGP($("#select_almacen_gp").val(), $("#select_marca_gp").val(), $(this).val());
});

$("#btnlistar-dpt").click(function (e) { 
    e.preventDefault();
    tabla_dpt.clear();
    if ($.fn.dataTable.isDataTable('#stock_table_dpt')) {
        tabla_dpt.destroy();
        $("#stock_table_dpt").html("");
    }
    listarDPT($("#select_almacen_dpt").val(), $("#select_marca_dpt").val(), $("#select_estilo_dpt").val(), $("#select_color_dpt").val());
});

$("#btnlistar-gp").click(function (e) { 
    e.preventDefault();
    tabla_gp.clear();
    if ($.fn.dataTable.isDataTable('#stock_table_gp')) {
        tabla_gp.destroy();
        $("#stock_table_gp").html("");
    }
    listarGP($("#select_almacen_gp").val(), $("#select_marca_gp").val(), $("#select_estilo_gp").val(), $("#select_color_gp").val());
});

$("#btn_codigo_dpt").click(function (e) { 
    e.preventDefault();
    $("#codigoModalLabel-codigo").html("Busqueda de código - Punto Textil");
    $("#buscarCodigo-dpt").show();
    $("#buscarCodigo-gp").hide();
    $("#mostrarTabla").html("");
});

$("#btn_codigo_gp").click(function (e) { 
    e.preventDefault();
    $("#codigoModalLabel-codigo").html("Busqueda de código - Global playerytees");
    $("#buscarCodigo-gp").show();
    $("#buscarCodigo-dpt").hide();
    $("#mostrarTabla").html("");
});

$("#buscarCodigo-dpt").keyup(function (e) { 
    e.preventDefault();
    if ($("#buscarCodigo-dpt").val()) {
        $.post("../ajax/existenciasDPT.php?opcion=busquedaCodigo", {idAlmacen: $("#select_almacen_dpt").val(), codigo: $(this).val()},
            function (data, textStatus, jqXHR) {
                var tablaBuscar = `<table class="table table-bordered table-sm">
                    <caption>${$("#select_almacen_dpt :selected").html()}</caption>
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Existencia</th>
                    </tr>
                    </thead>
                <tbody>`;
                data = JSON.parse(data);
                for (let i = 0; i < data.length; i++) {
                    tablaBuscar += `<tr><td>${data[i][0]}</td><td>${data[i][1]}</td><td>${data[i][2]}</td></tr>`;
                }
                tablaBuscar += `</tbody></table>`;
                $("#mostrarTabla").html(tablaBuscar);
            }
        );
    }
});

$("#buscarCodigo-gp").keyup(function (e) { 
    e.preventDefault();
    if ($("#buscarCodigo-gp").val()) {
        $.post("../ajax/existenciasGP.php?opcion=busquedaCodigo", {idAlmacen: $("#select_almacen_gp").val(), codigo: $(this).val()},
            function (data, textStatus, jqXHR) {
                var tablaBuscar = `<table class="table table-bordered table-sm">
                    <caption>${$("#select_almacen_gp :selected").html()}</caption>
                    <thead>
                    <tr>
                        <th>Código</th>
                        <th>Descripción</th>
                        <th>Existencia</th>
                    </tr>
                    </thead>
                <tbody>`;
                data = JSON.parse(data);
                for (let i = 0; i < data.length; i++) {
                    tablaBuscar += `<tr><td>${data[i][0]}</td><td>${data[i][1]}</td><td>${data[i][2]}</td></tr>`;
                }
                tablaBuscar += `</tbody></table>`;
                $("#mostrarTabla").html(tablaBuscar);
            }
        );
    }
});

$("#btn_almacen_dpt").click(function (e) { 
    e.preventDefault();
    if ($("#select_estilo_dpt").val()!=null && $("#select_color_dpt").val()!=null) {
        if ($("#tblAlmacen tr").length > 1) {
            tblAlmacen.clear();
            if ($.fn.dataTable.isDataTable('#tblAlmacen')) {
                tblAlmacen.destroy();
                $("#tblAlmacen").html("");
            }
        }
        busquedaEnAlmacen($("#select_marca_dpt").val(), $("#select_estilo_dpt").val(), $("#select_color_dpt").val());   
    }
});

$("#btn_almacen_gp").click(function (e) { 

});

function busquedaEnAlmacen(marca, estilo, color) {
    $("#loadingAlmacen").show();
    $.post("../ajax/existenciasDPT.php?opcion=listarPorAlmacen", {idMarca: marca, idEstilo: estilo, idColor: color},
        function (data, textStatus, jqXHR) {
            $("#loadingAlmacen").hide();
            $("#tblAlmacen").append(data);
            tblAlmacen = $("#tblAlmacen").dataTable({
                "scrollY": "400px",
                "scrollX": true,
                "paging": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                // dom: 'Bfrtp',
                dom: '<"top"lp>rt<"bottom"B><"clear">',
                buttons: [
                    'copyHtml5',
                    {
                        extend: 'excelHtml5',
                        title: 'Existencias DPT'
                    },
                    {
                        extend: 'csvHtml5',
                        title: "Existencias DPT"
                    }
                    // 'pdfHtml5'
                ]
            }).DataTable();

            $('#tblAlmacen td:last-child').addClass('text-playerytees');
        }
    );
}

$("#btnproximasllegadas-dpt").click(function (e) { 
    e.preventDefault();
    listarProximasLlegadas($('#select_estilo_dpt').select2('data'), $("#select_color_dpt").val());
    $("#btnBuscarFecha_dpt").show();
    $("#btnBuscarFecha_gp").hide();
});

$("#btnproximasllegadas-gp").click(function (e) { 
    e.preventDefault();
    listarProximasLlegadas($('#select_estilo_gp').select2('data'), $("#select_color_gp").val());
    $("#btnBuscarFecha_gp").show();
    $("#btnBuscarFecha_dpt").hide();
});

function listarProximasLlegadas(estilo, color, max="", min="") {
    var estilo_ = estilo;

    var estilos = new Array();
    for (let i = 0; i < estilo_.length; i++) {
        estilos.push("'"+estilo_[i].text+"'");
    }

    if ($("#tblProximasLlegadas tr").length > 1) {
        tablaproximasllegadas.clear();
        if ($.fn.dataTable.isDataTable('#tblProximasLlegadas')) {
            tablaproximasllegadas.destroy();
            $("#tblProximasLlegadas").html("");
        }
    }

    $("#loadingProximasLlegadas").show();
    $.post("../ajax/proximasLlegadas.php?opcion=estiloColor", { estilo : estilos, color : color, max : max, min : min},
        function (data, textStatus, jqXHR) {
            $("#loadingProximasLlegadas").hide();
            $("#tblProximasLlegadas").append(data);
            tablaproximasllegadas = $("#tblProximasLlegadas").dataTable({
                "scrollY": "250px",
                "scrollX": true,
                "paging": false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
                },
                // dom: 'Bfrtp',
                dom: '<"top"lp>rt<"bottom"B><"clear">',
                buttons: [
                    'copyHtml5',
                    {
                        extend: 'excelHtml5',
                        title: 'Proximas Llegadas'
                    },
                    {
                        extend: 'csvHtml5',
                        title: "Proximas Llegadas"
                    }
                    // 'pdfHtml5'
                ]
            }).DataTable();
        }
    );
}

$("#btnimprimiretiqueta").click(function (e) { 
    e.preventDefault();
    let marca = $("#select_marca_dpt").val();
    let estilo = $("#select_estilo_dpt").val();
    let color = $("#select_color_dpt").val();

    $.ajax({
        type: "POST",
        url: "../ajax/existenciasDPT.php?opcion=imprimirEtiqueta",
        data: {idMarca: marca, idEstilo: estilo, idColor: color},
        success: function (response) {
            response = JSON.parse(response);
            window.open("impresiondeetiqueta.php?codigo="+response+"","_reporte","{opciones}");
        }
    });
});

function pulsarenter(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    return (tecla != 13 && tecla != 32);
}

$("#btnBuscarFecha_dpt").click(function (e) { 
    e.preventDefault();
    listarProximasLlegadas($('#select_estilo_dpt').select2('data'), $("#select_color_dpt").val(), $("#max").val(), $("#min").val());
});

$("#btnBuscarFecha_gp").click(function (e) { 
    e.preventDefault();
    listarProximasLlegadas($('#select_estilo_gp').select2('data'), $("#select_color_gp").val(), $("#max").val(), $("#min").val());
});

init();