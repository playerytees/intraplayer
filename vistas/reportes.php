<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
    require 'header.php';
    require "../config/Conexion.php";
    if($_SESSION['embarques']==1) {
?>
<div class="breadcrumbs" id="breadcrumbs">
    <div class="col-sm-4">
        <div class="page-header float-left">
            <div class="page-title">
                <h1>Reportes</h1>
            </div>
        </div>
    </div>
    <div class="col-sm-8">
        <div class="page-header float-right">
            <div class="page-title">
                <ol class="breadcrumb text-right">
                    <li><a href="#">Embarques</a></li>
                    <li class="active">Reportes</li>
                </ol>
            </div>
        </div>
    </div>
</div>

<div class="content mt-3">
    <div class="card">
        <div class="card-header"><strong>Gererar Reportes</strong></div>
        <div class="card-body card-block">
            <form class="row form-group" method="post" action="" id="form_id">
                <div class="col-12 col-md-6 col-xl-5">
                    <div class="form-group">
                        <label for="fecha_inicio" class=" form-control-label">Fecha Inicio</label>
                        <input type="date" id="fecha_inicio" name="fecha_inicio" required class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-5">
                    <div class="form-group">
                        <label for="fecha_fin" class=" form-control-label">Fecha Fin</label>
                        <input type="date" id="fecha_fin" name="fecha_fin" required class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-xl-2">
                    <div class="form-group">
                        <label for="fecha_fin" class=" form-control-label invisible d-none d-xl-inline-block">Generar</label>
                        <button type="submit" class="btn btn-playerytees btn-sm form-control" name="btn_generar">Generar Reporte</button>
                    </div>
                </div>
            </form>
<?php
if (isset($_POST['btn_generar'])) {
    echo '<button type="button" id="btn_excel" class="btn btn-playerytees btn-sm" onclick="descargarExcel()">Excel</button>
    <button type="button" id="btn_print" class="btn btn-playerutees btn-sm" onclick="imprimir();">Imprimir</button>';
    $fecha_inicio = $_POST['fecha_inicio'];
    $fecha_fin = $_POST['fecha_fin'];
    echo "<center><h5>Reporte desde <b id='idfechainicio'>$fecha_inicio</b> hasta <b id='idfechafin'>$fecha_fin</b></h5></center><br>";
    $consulta = mysqli_query($conexion, "SELECT de.id, de.barcode, de.partida, de.estilo, de.color, trim(de.talla), GROUP_CONCAT( (de.piezas*de.cajas) )  AS piezas, de.cajas, e.fecha, e.id FROM detalle_embarque AS de, embarque AS e WHERE de.id_embarque = e.id AND e.fecha >= '$fecha_inicio' AND e.fecha <= '$fecha_fin' GROUP BY partida, estilo, color, talla, e.id ORDER BY de.id ASC");

    $sql = mysqli_query($conexion, "SELECT de.estilo, SUM(de.piezas*de.cajas) as suma, SUM(de.cajas) as cajas, de.color, de.partida, de.talla, e.fecha, e.id FROM detalle_embarque AS de, embarque AS e WHERE de.id_embarque = e.id AND e.fecha >= '$fecha_inicio' AND e.fecha <= '$fecha_fin' GROUP BY partida, estilo, color, e.id ORDER BY de.id ASC");

    $arrayTallas[0]="talla";
    $arraypartida[0]="partida";
    $arrayestilo[0]="estilo";
    $arraycolor[0]="color";
    $arraysuma[0]="suma";
    $arraycajas[0]="cajas";
    $arrayfecha[0]="fecha";
    $arrayfolio1[0]="folio";

while($data = mysqli_fetch_array($sql))
{
    array_push($arrayestilo, $data[0]);
    array_push($arraysuma, $data[1]);
    array_push($arraycajas, $data[2]);
    array_push($arraycolor, $data[3]);
    array_push($arraypartida, $data[4]);
    array_push($arrayfecha, date_format(date_create($data[6]),'d/m/Y'));
    array_push($arrayfolio1, $data[7]);
}

$numRows=0;
$dataBDProx[][]="";

while($dataProx = mysqli_fetch_array($consulta))
{
    $arraypiezas = explode(',', $dataProx[6]);
    $piezas = ( count($arraypiezas) > 1 )? array_sum($arraypiezas) : $arraypiezas[0];
    //$dataBDProx [$numRows][0]= $dataProx[0];//id
    //$dataBDProx [$numRows][1]= $dataProx[1];//barcode
    $dataBDProx [$numRows][0]= $dataProx[2];//partida
    $dataBDProx [$numRows][1]= $dataProx[3];//estilo
    $dataBDProx [$numRows][2]= $dataProx[4];//color
    $dataBDProx [$numRows][3]= $dataProx[5];//talla
    $dataBDProx [$numRows][4]= $piezas;//piezas
    $dataBDProx [$numRows][5]= $dataProx[7];//cajas
    $dataBDProx [$numRows][6]= $dataProx[9];//Folio
    if(in_array($dataProx[5], $arrayTallas)){

    }else {
        $arrayTallas[] = $dataProx[5];
    }
    $numRows++;
}

$matriz[0][0] = "MAQUILA";
$matriz[0][1] = "PARTIDA";
$matriz[0][2] = "ESTILO";
$matriz[0][3] = "COLOR";

$matriz[0][4] = "3";

$matriz[0][5] = "4";
$matriz[0][6] = "6";
$matriz[0][7] = "8";

$matriz[0][8] = "9";

$matriz[0][9] = "10";
$matriz[0][10] = "12";
$matriz[0][11] = "14";
$matriz[0][12] = "XS";
$matriz[0][13] = "S";
$matriz[0][14] = "M";
$matriz[0][15] = "L";
$matriz[0][16] = "XL";
$matriz[0][17] = "XXL";
$matriz[0][18] = "3XL";
$matriz[0][19] = "4XL";
$matriz[0][20] = "5XL";
$matriz[0][21] = "UNI";
$matriz[0][22] = "PIEZAS";
$matriz[0][23] = "CAJAS";
$matriz[0][24] = "FECHA";
$matriz[0][25] = "FOLIO";

$filMat = 1;

for($tit = 1; $tit < count($arraypartida); $tit++)
{
    $partida = explode("-", $arraypartida[$tit]);
    $matriz[$tit][0] = ($partida[0] == "FAB") ? "Fabrica" : "Maquila";//maquila
    $matriz[$tit][1] = (count($partida) > 2) ? $partida[1]."-".$partida[2] : $partida[1];//partida
    $matriz[$tit][2] = $arrayestilo[$tit];//estilo
    $matriz[$tit][3] = $arraycolor[$tit];//color
    $matriz[$tit][22] = $arraysuma[$tit];//total piezas
    $matriz[$tit][23] = $arraycajas[$tit];//total cajas
    $matriz[$tit][24] = $arrayfecha[$tit];//fecha
    $matriz[$tit][25] = $arrayfolio1[$tit];//folio
}

for($rel = 1; $rel < count($arraypartida); $rel++)
{
    for($cols = 4; $cols < 22; $cols++)
    {
        $matriz[$rel][$cols] = "";
    }
}

//RECORRER PARA PONER NUMERO PIEZAS
for($pbd = 0; $pbd < $numRows; $pbd++)//filas del query DB
{
    for($rel = 1; $rel < count($arraypartida); $rel++)//filas
    {
        if($dataBDProx[$pbd][0] == $arraypartida[$rel] AND $dataBDProx[$pbd][1] == $matriz[$rel][2] AND $dataBDProx[$pbd][2] == $matriz[$rel][3] AND $dataBDProx[$pbd][6] == $matriz[$rel][25])
        {
            for($cols = 4; $cols < 22; $cols++)//columnas
            {
                $tallas = ($dataBDProx[$pbd][3]=='2XL')?'XXL':$dataBDProx[$pbd][3];
                if($tallas == $matriz[0][$cols])
                {
                    $matriz[$rel][$cols] = $dataBDProx[$pbd][4];
                    //$matriz[$rel][2] += $dataBDProx[$pbd][3];//total suma piezas del row
                }
            }
        }
    }
}

echo "<div id='print_table'><table style='width: 100%; font-family: Arial; border-collapse: collapse;' border='1' id='OrdenEmbarque'>
        <thead style='text-align: center;'>
            <tr>";
        for($fm = 0; $fm < 1; $fm++)
        {
          for($rm=0; $rm < 26; $rm++)
          {
            echo "<th>".$matriz[$fm][$rm]."</th>";
          }
        }
        echo "</tr></thead><tbody style='text-align: right;'>";
        for($fm = 1; $fm < count($arraypartida); $fm++)//filas
        {
          echo "<tr>";
          for($rm=0; $rm < 26; $rm++)//columnas
          {
            echo "<td>".$matriz[$fm][$rm]."</td>";
          }
          echo "</tr>";
        }
        echo "<tr>
            <td colspan='4' align='right'><b>TOTAL:</b></td>";
                unset($matriz[0]);
                for ($i=4; $i < 24; $i++)
                {
                    echo "<td>".array_sum(array_column($matriz, $i))."</td>";
                }
        echo "</tr>";
        echo "</tbody></table></div>";

}
?>

        </div>
    </div>
</div>
<?php
}
else
{
    require 'noacceso.php';
}
require 'footer.php';
}
ob_end_flush();
?>
<script>

function init() { 
document.querySelector(".loader").style.display = "none";
var browser = getBrowserInfo().split(' ');
(browser[0] == "Chrome")?$("#btn_print").show():$("#btn_print").hide();
}

function descargarExcel()
{
    var fecha_inicio = document.getElementById('idfechainicio').innerHTML;
    var fecha_fin = document.getElementById('idfechafin').innerHTML;
    //Creamos un Elemento Temporal en forma de enlace
    var tmpElemento = document.createElement('a');
    // obtenemos la información desde el div que lo contiene en el html
    // Obtenemos la información de la tabla
    var data_type = 'data:application/vnd.ms-excel';
    var tabla_div = document.getElementById('OrdenEmbarque');
    var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
    tmpElemento.href = data_type + ', ' + tabla_html;
    document.body.appendChild(tmpElemento); // Firefox
    //Asignamos el nombre a nuestro EXCEL
    tmpElemento.download = 'Reporte_'+fecha_inicio+'_al_'+fecha_fin+'.xls';
    // Simulamos el click al elemento creado para descargarlo
    tmpElemento.click();
    console.log(fecha_inicio);
}

function imprimir() { 
    window.print();
}

var getBrowserInfo = function() {
    var ua= navigator.userAgent, tem,
    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1] === 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if( ( tem= ua.match( /version\/(\d+)/i ) ) != null ) M.splice(1, 1, tem[1]);
    return M.join(' ');
}

init();
</script>
<style>
@media print{
    @page {size: landscape}
    #left-panel,
    #header,
    .breadcrumbs,
    h1,
    .breadcrumb,
    .card-header,
    #form_id,
    #btn_excel,
    #btn_print{
        display: none;
    }
    body{
        background: #fff;
    }
    .card-body{
        padding: 0;
    }
    .card{
        margin: 0;
        border: 0;
    }
}

table tr td:first-child,
table tr td:nth-child(2),
table tr td:nth-child(3),
table tr td:nth-child(4) {text-align: left;}
</style>