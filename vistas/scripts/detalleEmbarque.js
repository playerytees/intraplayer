var tabla1, tabla2;

function init()
{
    listar();
    listarpartidas();
    $(".loader").hide();
}

function listar()
{
    tabla1 = $("#tabladepartidas").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Blfrtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/detalleEmbarque.php?opcion=listar",
            type: "POST",
            data: {id_embarque: $("#id_embarque").val()},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarpartidas()
{
    tabla2 = $("#tbPartidas").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Bfrtp",
        buttons: [
        ],
        "ajax": {
            url: "../ajax/detalleEmbarque.php?opcion=listarpartidas",
            type: "POST",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function asignarfolio(id,barcode,partida,estilo,color,talla,pza_x_caja,cajas, partidalimpia, codigo)
{
    $.confirm({
        title: 'Agregar',
        content: '' +
            '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<label>Cajas que desea agregar</label>' +
            '<input type="number" id="cajasagregar" class="form-control" value="' + cajas + '" min="1" max="' + cajas + '" />' +
            '</div>' +
            '</form>',
        buttons: {
            formSubmit: {
                text: 'Agregar',
                btnClass: 'btn-blue',
                action: function () {
                    var cajasagregar = this.$content.find('#cajasagregar').val();
                    if (!cajasagregar) {
                        $.alert('Se requiere un numero de cajas');
                        return false;
                    }
                    /*if (cajasagregar > cajas) {
                        $.alert('No se puede exceder el numero de cajas');
                        return false;
                    }*/
                    if (cajasagregar < 1) {
                        $.alert('El valor debe ser mayor o igual a 1');
                        return false;
                    }
                    $.ajax({
                        type: "POST",
                        url: "../ajax/detalleEmbarque.php?opcion=asignarfolio",
                        data: { id: id, barcode: barcode, partida: partida, estilo: estilo, color: color, talla: talla, pza_x_caja: pza_x_caja, cajas: cajasagregar, id_embarque: $("#id_embarque").val(), partidalimpia: partidalimpia, codigo: codigo },
                        success: function (response) {
                            console.log(response);
                            $.alert(response);
                            tabla1.ajax.reload();
                            tabla2.ajax.reload();
                        }
                    });
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // si el usuario envía el formulario presionando Enter en el campo.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // hacer referencia al botón y hacer clic en él
            });
        }
    });
}

function quitardefolio(id, barcode, partida, estilo, color, talla, piezas, cajas, partidalimpia, codigo)
{
    console.log(id, barcode);
    $.confirm({
        title: 'Advertencia!',
        content: '¿Está seguro de quitar la Partida? <br> La partida se guardara en Partidas pendientes',
        buttons: {
            confirm: {
                text: "Aceptar",
                btnClass: 'btn-blue',
                action: function () {
                    $.post("../ajax/detalleEmbarque.php?opcion=quitardefolio", { id: id, barcode: barcode, partida: partida, estilo: estilo, color: color, talla: talla, pza_x_caja: piezas, cajas: cajas, partidalimpia: partidalimpia, codigo: codigo }, function (response) {
                        console.log(response);
                        $.alert(response);
                        tabla1.ajax.reload();
                        tabla2.ajax.reload();
                    });
                }
            },
            cancel: {
                text: "Cancelar",
                btnClass: 'btn-red',
                action: function () {
                    $.alert('Se ha Cancelado la operación');
                }
            }
        }
    });
}

function guardar()
{
    $.confirm({
        title: 'Advertencia!',
        content: 'Una vez Guardado el embarque ya no se podra modificar',
        buttons: {
            confirm: {
                text: "Aceptar",
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POSt",
                        url: "../ajax/folioEmbarque.php?opcion=desactivar",
                        data: {id: $("#id_embarque").val()},
                        success: function (response) {
                            console.log(response);
                            $.alert(response);
                            $(location).attr("href", "folio_embarque.php");
                        }
                    });
                }
            },
            cancel: {
                text: "Cancelar",
                btnClass: 'btn-red',
                action: function () {
                    $.alert('Se ha Cancelado la operación');
                }
            }
        }
    });
}

init();