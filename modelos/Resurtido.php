<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");

class Resurtido
{
    public function __construct()
    {

    }

    public function idresurtidonew($id_sucursal)
    {
        $sqlresurtido="INSERT INTO resurtido_folio VALUES (NULL, '".date("Y-m-d")."', '$id_sucursal', now(), now())";
        
        return ejecutarConsulta_retornarID($sqlresurtido);
    }

    public function insertar($manufactura, $personalizado, $linea, $minimo, $existencias, $faltante, $surtir, $num_piezas, $piezas_surtir, $exist_cedis, $dif_cedis, $id_sucursal, $idresurtidonew)
    {
        $sql = "INSERT INTO resurtido VALUES (NULL, '$manufactura', '$personalizado', '$linea', '$minimo', '$existencias', '$faltante', '$surtir', '$num_piezas', '$piezas_surtir', '$exist_cedis', '$dif_cedis', '$id_sucursal', '$idresurtidonew', now(), now())";

        return ejecutarConsulta($sql);
    }

    public function resurtir($id, $resurtir)
    {
        $sql = "UPDATE resurtido SET resurtir = '$resurtir' WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($id)
    {
        $sql = "SELECT sucursales.descr FROM resurtido JOIN sucursales ON resurtido.id_sucursal = sucursales.id WHERE resurtido.id_resurtidofolio = '$id' LIMIT 1";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listarfolio($id_sucursal)
    {
        $sql = "SELECT rf.id, rf.created, s.descr FROM resurtido_folio rf JOIN sucursales s ON rf.id_sucursal = s.id WHERE rf.id_sucursal = '$id_sucursal'";

        return ejecutarConsulta($sql);
    }

    public function listar($id_resurtidofolio)
    {
        $sql = "SELECT * FROM resurtido WHERE piezas_surtir > 0 AND dif_cedis > 0 AND id_resurtidofolio = '$id_resurtidofolio'";

        return ejecutarConsulta($sql);
    }

    public function insertar_resurtido_inve($sku_manufactura, $sku_custom, $inventario, $id_sucursal)
    {
        $sql = "INSERT INTO resurtido_inve VALUES (NULL, '$sku_manufactura', '$sku_custom', '$inventario', '$id_sucursal', now(), now())";

        return ejecutarConsulta($sql);
    }

    public function deleteresurtidoinve($id_sucursal)
    {
        $delete = "DELETE FROM resurtido_inve WHERE id_sucursal = '$id_sucursal'";
            
        ejecutarConsulta($delete);
    }
}

?>