<?php
require_once "../modelos/Sucursales.php";

$sucursales = new Sucursales();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$descr = isset($_POST['descr'])?limpiarCadena($_POST['descr']):"";
$direccion = isset($_POST['direccion'])?limpiarCadena($_POST['direccion']):"";
$encargado = isset($_POST['encargado'])?limpiarCadena($_POST['encargado']):"";
$telefono = isset($_POST['telefono'])?limpiarCadena($_POST['telefono']):"";
$estado = isset($_POST['estado'])?limpiarCadena($_POST['estado']):"";
$descripcion = isset($_POST['descripcion'])?limpiarCadena($_POST['descripcion']):"";

switch ($_GET['opcion']) {
    case 'guardaryeditar':
        if (empty($id))
        {
            $response = $sucursales->insertar($descr, $direccion, $encargado, $telefono, $estado, $descripcion);
            echo $response;
		}
        else
        {
			$response = $sucursales->editar($id, $descr, $direccion, $encargado, $telefono, $estado, $descripcion);
			echo $response;
		}
        break;
    case 'dasactivar':
        $response = $sucursales->desactivar($id);
        echo $response ? "Sucursal/Distribuidor eliminado correctamente" : "Sucursal/Distribuidor no se pudo eliminar";
        break;
    case 'activar':
        $response = $sucursales->activar($id);
        echo $response ? "Sucursal/Distribuidor activado" : "Sucursal/Distribuidor no se pudo activar";
        break;
    case 'mostrar':
        $response = $sucursales->mostrar($id);
        echo json_encode($response);
        break;
    case 'listar':
        $response = $sucursales->listar();
        $data = array();
        
        while ( $reg=$response->fetch_object() ) {
            $data[] = array(
                '0' => $reg->id,
                '1' => $reg->descr,
                '2' => $reg->direccion,
                '3' => $reg->encargado,
                '4' => $reg->telefono,
                '5' => '<i class="fa fa-edit mx-1" title="Editar" style="cursor: pointer;" onclick="mostrar('.$reg->id.')"></i> <a href="catalogo.php?sucursal='.$reg->id.'" title="Catalogo"> <i class="fa fa-book mx-1"></i> </a> <a href="inventario.php?sucursal='.$reg->id.'" title="Existencias"> <i class="fa fa-truck mx-1"></i> </a> <a href="resurtido.php?sucursal='.$reg->id.'" title="Reportes"> <i class="fa fa-eye mx-1"></i> </a>'
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        echo "La opcion seleccionada no existe";
        break;
}
?>