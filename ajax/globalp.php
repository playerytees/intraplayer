<?php
require_once '../modelos/GlobalP.php';

$globalp = new GlobalP();

switch ($_GET['opcion']) {
    case 'listar':
        $response = $globalp->listar();
        $data = array();

        while( $reg=ibase_fetch_object($response) ) {
            $data[] = array(
                "0" => utf8_encode($reg->CVE_ART),
                "1" => utf8_encode($reg->DESCR),
                "2" => utf8_encode($reg->LIN_PROD),
                "3" => utf8_encode($reg->EXIST),
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );
        
        echo json_encode($results);
        break;
    default:
        echo "La opcion seleccionada no existe";
        break;
}
?>