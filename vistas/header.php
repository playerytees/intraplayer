<?php
if (strlen(session_id()) < 1)
    session_start();
?>
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Intraplayer - Inicio</title>
    <meta name="description" content="Intraplayer">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="apple-touch-icon" href="apple-icon.png"> -->
    <link rel="shortcut icon" href="../public/images/logo_playerytees_oficial.png"/>

    <?php $var = explode("/", $_SERVER['SCRIPT_NAME']); ?>
    <?php if($var[count($var) - 1] == 'etiqueta.php' || $var[count($var) - 1] == 'imprimir.php'): ?>
        <style>
            @import url("../public/assets/css/normalize.css");
            @import url("../public/assets/css/bootstrap.min.css");
            @import url("../public/assets/css/font-awesome.min.css");
            @import url("../public/assets/css/font-awesome.min.css");
            @import url("../public/assets/css/cs-skin-elastic.css");
            @import url("../public/assets/css/lib/datatable/dataTables.bootstrap.min.css");
            @import url("../public/assets/scss/style.css");
            @import url("../public/assets/css/lib/vector-map/jqvmap.min.css");
            @import url("../public/assets/css/jquery-confirm.min.css");
            @import url("../public/assets/css/estilos.css");
            /*@import url("https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800");*/
        </style>
    <?php else: ?>
        <link rel="stylesheet" href="../public/assets/css/normalize.css">
        <link rel="stylesheet" href="../public/assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="../public/assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="../public/assets/css/cs-skin-elastic.css">
        <link rel="stylesheet" href="../public/assets/css/lib/datatable/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="../public/assets/scss/style.css">
        <link href="../public/assets/css/lib/vector-map/jqvmap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../public/assets/css/jquery-confirm.min.css">
        <link rel="stylesheet" href="../public/assets/css/estilos.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
        <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'> -->
    <?php endif; ?>
        <link rel="stylesheet" href="https://unpkg.com/microtip/microtip.css">
    <!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.3/html5shiv.min.js"></script> -->
    <style>
        .loader {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
            background: url('ajax-loader.gif') 50% 50% no-repeat rgb(249,249,249);
            opacity: .8;
        }
        [aria-label][role~="tooltip"] {
            z-index: 1 !important;
        }
    </style>
</head>
<body class="open">

<div class="loader"></div>

        <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-md navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="../public/images/PLAYERYTEES.PNG" width="95" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="../public/images/PLAYERYTEES.PNG" alt="L"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <?php /*
                    <li class="active">
                        <a href="#"> <i class="menu-icon fa fa-dashboard"></i>Dashboard</a>
                    </li>
                    <h3 class="menu-title">PLAYERYTEES</h3><!-- /.menu-title -->
                    <li>
                        <a href="sucursales.php"><i class="menu-icon fa fa-map-marker"></i>Sucursales</a>
                    </li>
                    <li>
                        <a href="catalogo.php"><i class="menu-icon fa fa-book"></i>Catalogo</a>
                    </li>
                    */ ?>
                    <?php if($_SESSION['embarques']==1): ?>
                    <h3 class="menu-title">EMBARQUES</h3>
                    <li>
                        <a href="etiqueta.php" aria-label="Etiqueta" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-tag"></i>Etiqueta</a>
                    </li>
                    <li>
                        <a href="embarque.php" aria-label="Nuevo Embarque" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-plus-circle"></i>Nuevo Embarque</a>
                    </li>
                    <li>
                        <a href="folio_embarque.php" aria-label="Buscar Folio" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-search"></i>Buscar Folio</a>
                    </li>
                    <li>
                        <a href="reportes.php" aria-label="Generar Reporte" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-tasks"></i>Generar Reporte</a>
                    </li>
                    <?php /*
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Embarque"> <i class="menu-icon fa fa-truck"></i>Embarque</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-plus-circle"></i><a href="embarque.php">Nuevo</a></li>
                            <!-- <li><i class="menu-icon fa fa-barcode"></i><a href="sobrantes.php">Sobrantes</a></li> -->
                            <li><i class="menu-icon fa fa-search"></i><a href="folio_embarque.php">Buscar</a></li>
                        </ul>
                    </li>
                    */ ?>
                    <?php endif; ?>
                    <?php if($_SESSION['DPT']==1 OR $_SESSION['GP']==1): ?>
                    <h3 class="menu-title">INVENTARIOS</h3>
                    <li>
                        <a href="existencias.php" aria-label="Existencias" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-bars"></i>Existencias</a>
                    </li>
                    <?php endif; ?>
                    <?php if($_SESSION['DPT']==1): ?>
                    <!-- <li>
                        <a href="existenciasDPT.php" aria-label="Existencias DPT" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-bars"></i>Existencias DPT</a>
                    </li> -->
                    <?php endif; ?>
                    <?php if($_SESSION['GP']==1): ?>
                    <!-- <li>
                        <a href="existenciasGP.php" aria-label="Existencias GP" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-bars"></i>Existencias GP</a>
                    </li> -->
                    <?php endif; ?>
                    <?php if($_SESSION['existencias']==1 OR $_SESSION['Distribuidores']==1): ?>
                    <h3 class="menu-title">VENTAS</h3>
                    <li>
                        <a href="VentasTienda.php" aria-label="Ventas Tienda" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-bar-chart-o"></i>Ventas Tienda</a>
                    </li>
                    <?php endif; ?>
                    <?php if($_SESSION['ventasArticulos']==1 OR $_SESSION['ventasMarca']==1 OR $_SESSION['pedidosAbiertos']==1): ?>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Ventas"> <i class="menu-icon fa fa-bar-chart-o"></i>Reportes</a>
                        <ul class="sub-menu children dropdown-menu">
                            <?php if($_SESSION['ventasArticulos']==1): ?>
                            <li><i class="menu-icon fa fa-bar-chart-o"></i><a href="ventasArticulos.php">Ventas Articulos</a></li>
                            <?php endif; ?>
                            <?php if($_SESSION['ventasMarca']==1): ?>
                            <li><i class="menu-icon fa fa-bar-chart-o"></i><a href="ventasMarca.php">Ventas por Marca</a></li>
                            <?php endif; ?>
                            <?php if($_SESSION['pedidosAbiertos']==1): ?>
                            <li><i class="menu-icon fa fa-bar-chart-o"></i><a href="pedidosAbiertos.php">Pedidos Abiertos</a></li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <?php endif; ?>

                    <?php /* if($_SESSION['embarques']==1): ?>
                    <li>
                        <a href="reportes.php" aria-label="Reportes" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-tasks"></i>Reportes</a>
                    </li>
                    <?php endif; */ ?>
                    <?php if($_SESSION['proximasllegadas']==1 OR $_SESSION['resurtido']==1): ?>
                    <h3 class="menu-title">ALMACÈN</h3>
                    <?php if($_SESSION['resurtido']==1): ?>
                    <li>
                        <a href="sucursales.php" aria-label="Resurtido" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-truck"></i>Resurtido</a>
                    </li>
                    <?php endif; ?>
					<?php if($_SESSION['proximasllegadas']==1): ?>
                    <li>
                        <a href="proximasllegadas.php" aria-label="Proximas Llegadas" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-truck"></i>Proximas Llegadas</a>
                    </li>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php if($_SESSION['tag']==1): ?>
                    <h3 class="menu-title">TAG HERE</h3>
                    <li>
                        <a href="tag.php" aria-label="Tag Here" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-tag"></i>Tag Here</a>
                    </li>
                    <?php endif; ?>
                    <?php if($_SESSION['usuarios']==1 || $_SESSION['RegistrarPresupuesto']==1): ?>
                    <h3 class="menu-title">AJUSTES</h3>
                    <?php endif; ?>
                    <?php if($_SESSION['usuarios']==1): ?>
                    <li>
                        <a href="usuario.php" aria-label="Usuarios" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-users"></i>Usuarios</a>
                    </li>
                    <?php endif; ?>
                    <?php if($_SESSION['RegistrarPresupuesto']==1): ?>
                    <li>
                        <a href="presupuesto.php" aria-label="Presupuesto" data-microtip-position="right" role="tooltip"><i class="menu-icon fa fa-money"></i>Presupuesto</a>
                    </li>
                    <?php endif; ?>
                    <?php /*
                    <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-laptop"></i>Components</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-puzzle-piece"></i><a href="#">Buttons</a></li>
                            <li><i class="fa fa-id-badge"></i><a href="#">Badges</a></li>
                            <li><i class="fa fa-bars"></i><a href="#">Tabs</a></li>
                            <li><i class="fa fa-share-square-o"></i><a href="#">Social Buttons</a></li>
                            <li><i class="fa fa-id-card-o"></i><a href="#">Cards</a></li>
                            <li><i class="fa fa-exclamation-triangle"></i><a href="#">Alerts</a></li>
                            <li><i class="fa fa-spinner"></i><a href="#">Progress Bars</a></li>
                            <li><i class="fa fa-fire"></i><a href="#">Modals</a></li>
                            <li><i class="fa fa-book"></i><a href="#">Switches</a></li>
                            <li><i class="fa fa-th"></i><a href="#">Grids</a></li>
                            <li><i class="fa fa-file-word-o"></i><a href="#">Typography</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Tables</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="#">Basic Table</a></li>
                            <li><i class="fa fa-table"></i><a href="#">Data Table</a></li>
                        </ul>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-th"></i>Forms</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-th"></i><a href="#">Basic Form</a></li>
                            <li><i class="menu-icon fa fa-th"></i><a href="#">Advanced Form</a></li>
                        </ul>
                    </li>

                    <h3 class="menu-title">Icons</h3><!-- /.menu-title -->

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-tasks"></i>Icons</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-fort-awesome"></i><a href="#">Font Awesome</a></li>
                            <li><i class="menu-icon ti-themify-logo"></i><a href="#">Themefy Icons</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="#"> <i class="menu-icon fa fa-envelope"></i>Widgets </a>
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-bar-chart"></i>Charts</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-line-chart"></i><a href="#">Chart JS</a></li>
                            <li><i class="menu-icon fa fa-area-chart"></i><a href="#">Flot Chart</a></li>
                            <li><i class="menu-icon fa fa-pie-chart"></i><a href="#">Peity Chart</a></li>
                        </ul>
                    </li>

                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-area-chart"></i>Maps</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-map-o"></i><a href="#">Google Maps</a></li>
                            <li><i class="menu-icon fa fa-street-view"></i><a href="#">Vector Maps</a></li>
                        </ul>
                    </li>
                    <h3 class="menu-title">Extras</h3><!-- /.menu-title -->
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="#">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="#">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="#">Forget Pass</a></li>
                        </ul>
                    </li>
                    */ ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->

    <!-- Right Panel -->

    <div id="right-panel" class="right-panel"> <!-- Termina en el footer.php -->

        <!-- Header-->
        <header id="header" class="header" style="padding-bottom: 0;">

            <div class="header-menu">

                <div class="col-sm-7">
                    <a id="menuToggle" class="menutoggle pull-left"><i class="fa fa fa-tasks"></i></a>
                    <span><?= $_SESSION['nombre']; ?></span> <span id="title-descr"></span>
                    <?php /*
                    <div class="header-left">
                        <button class="search-trigger"><i class="fa fa-search"></i></button>
                        <div class="form-inline">
                            <form class="search-form">
                                <input class="form-control mr-sm-2" type="text" placeholder="Search ..." aria-label="Search">
                                <button class="search-close" type="submit"><i class="fa fa-close"></i></button>
                            </form>
                        </div>

                        <div class="dropdown for-notification">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="notification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            <span class="count bg-danger">5</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="notification">
                            <p class="red">You have 3 Notification</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <i class="fa fa-check"></i>
                                <p>Server #1 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <i class="fa fa-info"></i>
                                <p>Server #2 overloaded.</p>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <i class="fa fa-warning"></i>
                                <p>Server #3 overloaded.</p>
                            </a>
                          </div>
                        </div>

                        <div class="dropdown for-message">
                          <button class="btn btn-secondary dropdown-toggle" type="button"
                                id="message"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="ti-email"></i>
                            <span class="count bg-primary">9</span>
                          </button>
                          <div class="dropdown-menu" aria-labelledby="message">
                            <p class="red">You have 4 Mails</p>
                            <a class="dropdown-item media bg-flat-color-1" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/1.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jonathan Smith</span>
                                    <span class="time float-right">Just now</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-4" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/2.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Jack Sanders</span>
                                    <span class="time float-right">5 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-5" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/3.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Cheryl Wheeler</span>
                                    <span class="time float-right">10 minutes ago</span>
                                        <p>Hello, this is an example msg</p>
                                </span>
                            </a>
                            <a class="dropdown-item media bg-flat-color-3" href="#">
                                <span class="photo media-left"><img alt="avatar" src="images/avatar/4.jpg"></span>
                                <span class="message media-body">
                                    <span class="name float-left">Rachel Santos</span>
                                    <span class="time float-right">15 minutes ago</span>
                                        <p>Lorem ipsum dolor sit amet, consectetur</p>
                                </span>
                            </a>
                          </div>
                        </div>
                    </div>
                    */ ?>
                </div>

                <div class="col-sm-5">
                    <div class="user-area dropdown float-right">
                        <a href="../ajax/usuario.php?opcion=salir" style="font-size: 19px;"> <span>Cerrar Sesion</span> <i class="fa fa-sign-out"></i> </a>
                        <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <img class="user-avatar rounded-circle" src="../public/images/user.png" alt="User Avatar">
                        </a>

                        <div class="user-menu dropdown-menu">
                                <a class="nav-link" href="#"><i class="fa fa- user"></i>My Profile</a>

                                <a class="nav-link" href="#"><i class="fa fa- user"></i>Notifications <span class="count">13</span></a>

                                <a class="nav-link" href="#"><i class="fa fa -cog"></i>Settings</a>

                                <a class="nav-link" href="#"><i class="fa fa-power -off"></i>Logout</a>
                        </div> -->
                    </div>
<?php /*
                    <div class="language-select dropdown" id="language-select">
                        <a class="dropdown-toggle" href="#" data-toggle="dropdown"  id="language" aria-haspopup="true" aria-expanded="true">
                            <i class="flag-icon flag-icon-us"></i>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="language" >
                            <div class="dropdown-item">
                                <span class="flag-icon flag-icon-fr"></span>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-es"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-us"></i>
                            </div>
                            <div class="dropdown-item">
                                <i class="flag-icon flag-icon-it"></i>
                            </div>
                        </div>
                    </div>
*/ ?>
                </div>
            </div>

        </header><!-- /header -->
        <!-- Header-->