<?php
    require "../config/ConexionHana.php";

    class VentasGralSap extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listarSucursales($sucursal)
        {
            try {
                $sql = 'SELECT
                    CASE WHEN "Code" = \'CE1\' THEN \'CE1P\' ELSE "Code" END "Codigo",
                    "Name" "Nombre"
                FROM
                    "DPT_BD"."@SO1_01SUCURSAL"
                WHERE
                    "Code" IN('.$sucursal.')
                ORDER BY "Code";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarSucursalesGP()
        {
            try {
                $sql = 'SELECT
                    "Code" "Codigo",
                    "Name" "Nombre"
                FROM
                    "GP_BD"."@SO1_01SUCURSAL"
                WHERE "Code" = \'CE1\'
                ORDER BY "Code";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDiarias($fecha)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    CASE WHEN A."U_SO1_SUCURSAL" = \'CE1\' THEN \'CE1P\' ELSE A."U_SO1_SUCURSAL" END "Sucursal"
                FROM
                    "DPT_BD"."@SO1_01VENTA" A
                LEFT JOIN "DPT_BD"."OSLP" b ON (A."U_SO1_VENDEDOR" = b."SlpCode")
                LEFT JOIN "DPT_BD"."@SO1_01AUTEMPROL" T2 ON (TO_NVARCHAR(T2."Code") = TO_NVARCHAR(A."U_SO1_USUARIO"))
                LEFT JOIN "DPT_BD"."@SO1_01ESTTRABAJO" T3 ON (T3."Code" = A."U_SO1_ESTACION")
                LEFT JOIN "DPT_BD"."OCRD" C ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_STATUS" NOT IN (\'S\',\'N\')
                AND C."GroupCode" != \'102\'
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDiariasGP($fecha)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    A."U_SO1_SUCURSAL" "Sucursal"
                FROM
                    "GP_BD"."@SO1_01VENTA" A
                LEFT JOIN "GP_BD"."OSLP" b ON (A."U_SO1_VENDEDOR" = b."SlpCode")
                LEFT JOIN "GP_BD"."@SO1_01AUTEMPROL" T2 ON (TO_NVARCHAR(T2."Code") = TO_NVARCHAR(A."U_SO1_USUARIO"))
                LEFT JOIN "GP_BD"."@SO1_01ESTTRABAJO" T3 ON (T3."Code" = A."U_SO1_ESTACION")
                LEFT JOIN "GP_BD"."OCRD" C ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN (\'S\',\'N\')
                AND C."GroupCode" != \'102\'
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasDiariasDistribuidores($fecha, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    \'DIS102\' "Sucursal"
                FROM "'.$empresa.'"."@SO1_01VENTA" A
                LEFT JOIN "'.$empresa.'"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" = '.$fecha.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN(\'S\', \'N\')
                AND C."GroupCode" = \'102\'
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasMensuales($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    CASE WHEN A."U_SO1_SUCURSAL" = \'CE1\' THEN \'CE1P\' ELSE A."U_SO1_SUCURSAL" END "Sucursal"                    
                FROM
                    "DPT_BD"."@SO1_01VENTA" A
                LEFT JOIN "DPT_BD"."OSLP" b ON (A."U_SO1_VENDEDOR" = b."SlpCode")
                LEFT JOIN "DPT_BD"."@SO1_01AUTEMPROL" T2 ON (TO_NVARCHAR(T2."Code") = TO_NVARCHAR(A."U_SO1_USUARIO"))
                LEFT JOIN "DPT_BD"."@SO1_01ESTTRABAJO" T3 ON (T3."Code" = A."U_SO1_ESTACION")
                LEFT JOIN "DPT_BD"."OCRD" C ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_STATUS" NOT IN (\'S\',\'N\')
                AND C."GroupCode" != \'102\'
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasMensualesGP($start_date, $end_date)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    A."U_SO1_SUCURSAL" "Sucursal"                   
                FROM
                    "GP_BD"."@SO1_01VENTA" A
                LEFT JOIN "GP_BD"."OSLP" b ON (A."U_SO1_VENDEDOR" = b."SlpCode")
                LEFT JOIN "GP_BD"."@SO1_01AUTEMPROL" T2 ON (TO_NVARCHAR(T2."Code") = TO_NVARCHAR(A."U_SO1_USUARIO"))
                LEFT JOIN "GP_BD"."@SO1_01ESTTRABAJO" T3 ON (T3."Code" = A."U_SO1_ESTACION")
                LEFT JOIN "GP_BD"."OCRD" C ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN (\'S\',\'N\')
                AND C."GroupCode" != \'102\'
                GROUP BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function ventasMensualesDistribuidores($start_date, $end_date, $empresa)
        {
            try {
                $sql = 'SELECT
                    SUM(A."U_SO1_TOTALNETO") "Monto",
                    \'DIS102\' "Sucursal"
                FROM "'.$empresa.'"."@SO1_01VENTA" A
                LEFT JOIN "'.$empresa.'"."OCRD" C
                ON (A."U_SO1_CLIENTE" = C."CardCode")
                WHERE
                    A."U_SO1_FECHA" BETWEEN '.$start_date.' AND '.$end_date.'
                AND A."U_SO1_TIPO" IN (\'CA\', \'CR\')
                AND A."U_SO1_SUCURSAL" IN (\'CE1\')
                AND A."U_SO1_STATUS" NOT IN(\'S\', \'N\')
                AND C."GroupCode" = \'102\'
                GROUP BY A."U_SO1_SUCURSAL"
                ORDER BY A."U_SO1_SUCURSAL";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
