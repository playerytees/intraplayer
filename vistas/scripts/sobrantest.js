var arreglotag = [];
function init()
{
    $("#btn_imprimir").prop("disabled", true);
    barcode();
    $(".loader").hide();
}

function buscar()
{
    var textoBusqueda = $("input#busqueda").val();

    if (textoBusqueda != "") {
        $.post("../ajax/sobrante.php?opcion=buscar", {partida: textoBusqueda}, function(mensaje) {
            var tabla = `<table class="table table-sm table-hover">
            <thead>
                <tr>
                    <th>Partida</th>
                    <th>Estilo</th>
                    <th>Color</th>
                    <th>Talla</th>
                    <th>Sobrante</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>`;
            mensaje = JSON.parse(mensaje);
            for (let i = 0; i < mensaje.length; i++) {
                tabla += `<tr>
                    <td>${mensaje[i][0]}</td>
                    <td>${mensaje[i][1]}</td>
                    <td>${mensaje[i][2]}</td>
                    <td>${mensaje[i][3]}</td>
                    <td>${mensaje[i][4]}</td>
                    <td>${mensaje[i][5]}</td>
                </tr>`;
            }
            tabla += `</tbody></table>`;
            $("#resultadoBusqueda").html(tabla);
            $("#btn_agregar").removeClass("d-none");
            $("#btn_imprimir").removeClass("d-none");
        });
    } else {
        $("#resultadoBusqueda").html('');
	}
}

function agregarinfo()
{
    $(".trdata").remove();
    arreglotag.length = 0;
    $("input:checkbox:checked").each(function () { 
         $.post("../ajax/sobrante.php?opcion=agregarinfo", {id:$(this).val()},
             function (data, textStatus, jqXHR) {
                data = JSON.parse(data);
                var tabla = `<tr class="trdata">
                    <td>${data.partida}</td>
                    <td>${data.color}</td>
                    <td>${data.estilo}</td>
                    <td>${data.talla}</td>
                    <td><input style="width: 30px; border:none;" name="cantsob[]" id="cantsob" type="text" value=${data.sobrante}></td>
                </tr>`;

                $("#tabla_sobrante").append(tabla);

                $("#btn_imprimir").prop("disabled", false);

                arreglotag.push({idetiqueta: data.id, piezas: 0});
             }
         );
    });
}

function imprimir()
{
    $("#print_table").printArea();
    let i = 0;
    $("input[name='cantsob[]'").each(function() {
        arreglotag[i].piezas = $(this).val();
        i++;
    });
    $("#busqueda").prop("disabled", true);
    $("#btn_agregar").prop("disabled", true);
    $("#btn_buscar").prop("disabled", true);
    $("input:checkbox").prop("disabled", true);
    // $("#btn_imprimir").prop("disabled", true);
    $("input[name='cantsob[]'").prop("disabled", true);
    guardar();
}

function guardar()
{
    $.ajax({
        type: "POST",
        url: "../ajax/sobrante.php?opcion=guardar",
        data: {"barcode": $("#barcode_sobrante").attr('alt'), "arreglotag": JSON.stringify(arreglotag)},
        success: function (response) {
            console.log(response);
            // alert(response);
            // barcode();
        }
    });
}

function barcode()
{
    $.ajax({
        type: "POST",
        url: "../ajax/sobrante.php?opcion=ultimoid",
        success: function (response) {
            response = JSON.parse(response);
            let proximoid = parseInt(response.id) + 1;
            console.log(proximoid);
            $("#barcode_sobrante").attr("alt", "sob"+proximoid);
            JsBarcode("#barcode_sobrante", "sob"+proximoid, {width: 1.5, height: 23, displayValue: false, margin: 0});
        }
    });
}

function nuevo()
{
    $(".trdata").remove();
    $("input:checkbox").removeAttr("checked");
    arreglotag.length = 0;
    barcode();
    $("#btn_agregar").addClass("d-none");
    $("#btn_imprimir").addClass("d-none");
    $("#resultadoBusqueda").html("");
    $("#busqueda").val("");
    $("#busqueda").prop("disabled", false);
    $("#btn_buscar").prop("disabled", false);
}

init();