var tblVentasMarca;

function init() {
    $(".loader").hide();
    $("#contentTblVentas").hide();
    listarAlmacen("DPT_BD");
    listarMarca("DPT_BD");
    $('.form-control-chosen').select2({
        language: "es",
        width: '100%',
        closeOnSelect: false
    });
    $("#select_marca").prop("disabled", true);
    $("#select_sucursal").prop("disabled", true);
}

function listar(empresa, start_date, end_date, almacen, marca) {
    tblVentasMarca = $("#tblVentasMarca").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Bfrtip",
        buttons: [
            {
            extend: 'excelHtml5',
            title: 'Ventas-Bones-'+$("#input_start_date").val()+'-a-'+$("#input_end_date").val()
        },
        {
            extend: 'csvHtml5',
            title: 'Ventas-Bones-'+$("#input_start_date").val()+'-a-'+$("#input_end_date").val()
        }],
        "ajax": {
            url: "../ajax/ventasMarca.php?opcion=listar",
            type: "POST",
            data: {empresa: empresa, start_date: start_date, end_date: end_date, almacen: almacen, marca: marca},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarAlmacen(empresa) {
    $("#btnBuscar").attr("disabled", "disabled");
    $.post("../ajax/ventasMarca.php?opcion=listarAlmacen", {empresa: empresa},
        function (data, textStatus, jqXHR) {
            $("#btnBuscar").removeAttr("disabled");
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {  
                select += `<option value="'${data[i][0]}'">${data[i][1]}</option>`;
            }
            $("#select_sucursal").append(select);
            $("#select_sucursal").trigger("chosen:updated");
        }
    );
}

function listarMarca(empresa) {
    $("#btnBuscar").attr("disabled", "disabled");
    $.post("../ajax/ventasMarca.php?opcion=listarMarca", {empresa: empresa},
        function (data, textStatus, jqXHR) {
            $("#btnBuscar").removeAttr("disabled");
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {  
                select += `<option ${(data[i][1]=="BONES"?"selected":"")} value="'${data[i][0]}'">${data[i][1]}</option>`;
            }
            $("#select_marca").append(select);
            $("#select_marca").trigger("chosen:updated");
        }
    );
}

$("#btnBuscar").click(function (e) { 
    e.preventDefault();
    if($("#input_start_date").val() != "" && $("#input_end_date").val() != ""  && $("#select_marca :selected").length != 0 ) {
        listar(  $("#input_empresa").val(), $("#input_start_date").val(), $("#input_end_date").val(), $("#select_sucursal").val(), $("#select_marca").val() );
        $("#contentTblVentas").show();
    }
});

$("#btnDPT").click(function (e) { 
    e.preventDefault();
    $("#select_sucursal").empty();
    listarAlmacen("DPT_BD");
    $("#select_marca").empty();
    listarMarca("DPT_BD");
    $(this).addClass("active");
    $("#btnGP").removeClass("active");
    $("#input_empresa").val("DPT_BD");
});

$("#btnGP").click(function (e) { 
    e.preventDefault();
    $("#select_sucursal").empty();
    listarAlmacen("GP_BD");
    $("#select_marca").empty();
    listarMarca("GP_BD");
    $(this).addClass("active");
    $("#btnDPT").removeClass("active");
    $("#input_empresa").val("GP_BD");
});

init();