<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['GP']==1) {
?>
<style>
.dataTables_filter {
    float: left !important;
 }

 .dt-buttons {
     float: right !important;
 }
 #stock_table td:nth-child(2) {
    white-space: nowrap;
 }
 #stock_table td:nth-child(1), td:nth-child(2), td:nth-child(3) {
    text-align: left;
 }
 #tblAlmacen td:nth-child(1), td:nth-child(3) {
    white-space: nowrap;
 }
 @media (max-width: 425px) {
.dt-buttons { float: none !important;}	
}
</style>
    <!-- <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 class="text-playerytees">Existencias</h1>
                </div>
            </div>
        </div>
    </div> -->

    <div class="content mt-2">
        <div class="row">
            <div class="col-12 col-md-6 col-xl-3">
                <div class="card mb-2">
                    <div class="card-header p-1">
                        <strong class="card-title text-playerytees">Almacen:</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body text-center p-0">
                        <select name="select_almacen" id="select_almacen" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar almacen...">
                        </select>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->
            <div class="col-12 col-md-6 col-xl-3">
                <div class="card mb-2">
                    <div class="card-header p-1">
                        <strong class="card-title text-playerytees">Marca:</strong>
                    </div> <!-- .card-header-->
                    <div class="card-body text-center p-0">
                        <select name="select_marca" id="select_marca" class="form-control form-control-chosen d-none" data-placeholder="Seleccionar Marca..."></select>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->
            <div class="col-12 col-md-6 col-xl-3">
                <div class="card mb-2">
                    <div class="card-header p-1">
                        <strong class="card-title text-playerytees">Estilo:</strong>
                    </div> <!-- .card-header-->
                    <div class="card-body text-center p-0">
                        <select name="select_estilo" id="select_estilo" class="form-control form-control-chosen d-none" data-placeholder="TODOS">
                            <option value="-1">TODOS</option>
                        </select>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->
            <div class="col-12 col-md-6 col-xl-3">
                <div class="card mb-2">
                    <div class="card-header p-1">
                        <strong class="card-title text-playerytees">Color:</strong>
                    </div> <!-- .card-header-->
                    <div class="card-body text-center p-0">
                        <select name="select_color[]" id="select_color" class="form-control form-control-chosen d-none" data-placeholder="TODOS" multiple="multiple">
                            <!-- <option value="-1">TODOS</option> -->
                        </select>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->
        </div> <!-- .row -->

        <div class="row"></div> <!-- .row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <!-- <div class="card-header">
                        <strong class="card-title text-playerytees">Existencias</strong>
                    </div> --> <!-- .card-header -->
                    <div class="card-body">
                    <div id="contenido">
                            <div class="loading text-center">
                                <img src="ajax-loader.gif" alt="loading" /><br/>Un momento, por favor...
                            </div>
                        </div>
                        <table id="stock_table" class="table table-sm table-bordered table-striped" style="width: 100%; font-size: 14px;">
                            <!-- <thead>
                            </thead>
                            <tbody></tbody> -->
                        </table>
                        <button id="btnbuscar" type="button" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#codigoModal">Buscar por Código</button>
                        <?php if($_SESSION['VentasTotales']==1): ?>
                            <button id="btnbuscaralmacen" type="button" class="btn btn-playerytees btn-sm mt-2 ml-3" data-toggle="modal" data-target="#AlmacenModal">Buscar por Almacén</button>
                        <?php endif; ?>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->

        <div class="modal fade" id="codigoModal" tabindex="-1" role="dialog" aria-labelledby="codigoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="codigoModalLabel">Busqueda Código</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="" method="post" class="">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Buscar</div>
                                    <input type="text" id="buscarCodigo" name="buscarCodigo" class="form-control" onkeypress="return pulsarenter(event)">
                                    <div class="input-group-addon"><i class="fa fa-search"></i></div>
                                </div>
                            </div>
                        </form>
                        <div id="mostrarTabla" style="height: 300px; overflow: auto">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="AlmacenModal" tabindex="-1" role="dialog" aria-labelledby="codigoModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="codigoModalLabel">Busqueda por almacén</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                       <form action="" method="post" class="">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" id="buscarAlmacen" name="buscarAlmacen" placeholder="Codigo del articulo" class="form-control" required onkeypress="return pulsarenter(event)">
                                    <div class="input-group-btn">
                                        <button id="btnBusquedaAlmacen"  type="button" class="btn btn-playerytees"><i class="fa fa-search"></i> Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div id="" style="height: 350px; overflow: auto">
                            <table id="tblAlmacen" class="table table-bordered table-sm table-responsive-md">
                                <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Descripción</th>
                                    <th>Almacén</th>
                                    <th>Existencia</th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <center><img id="carga-almacen" src="ajax-loader.gif" alt="loading" /></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- .content .mt-3 -->

<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
<script type="text/javascript" src="scripts/existenciasGP.js"></script>
<?php
}
ob_end_flush();
?>