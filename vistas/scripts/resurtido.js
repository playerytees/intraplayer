var tabla;

function init()
{
    mostrarform(false);
    listarfolio();
    mostrar();
    $(".loader").hide();
}

function mostrarform(flag)
{
    if (flag) {
        $("#listadofolio").hide();
        $("#listado_final").show();
    } else {
        $("#listadofolio").show();
        $("#listado_final").hide();
    }
}

function cancelarform()
{
    mostrarform(false);
}

function listarfolio()
{
    tabla = $("#tablafolio").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Blfrtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/resurtido.php?opcion=listarfolio",
            type: "POST",
            data: { id_sucursal: $("#idsucursal").val() },
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listar(idfolio)
{
    mostrarform(true);
    tabla = $("#tabla_resurtido").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Blfrtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/resurtido.php?opcion=listar",
            type: "POST",
            data: { id_resurtidofolio: idfolio },
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function guardar()
{
    
}

function mostrar() {
    $.post("../ajax/sucursales.php?opcion=mostrar", { id: $("#idsucursal").val() },
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            $("#spansucursal").html(" / " + data.descr)
        }
    );
}

init();