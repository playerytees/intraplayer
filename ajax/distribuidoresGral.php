<?php
require_once "../modelos/Distribuidores.php";
session_start();
$distribuidores = new Distribuidores();

define('GP', 'GP_BD');
define('DPT', 'DPT_BD');

$fecha = isset($_POST["fecha"])?htmlspecialchars(trim($_POST["fecha"])):"";

switch ($_GET['opcion']) {
    case 'listar':
        require_once "../modelos/Ventas.php";
        $ventas = new Ventas();

        $data = array();
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0]."-".$fechaArray[1]."-"."01";

        $distribuidoresGP = $distribuidores->listar(GP);
        $distribuidoresDPT = $distribuidores->listar(DPT);

        $ventasDiarias = $distribuidores->ventas("'$fecha'", "'$fecha'", GP);
        $ventasMensuales = $distribuidores->ventas("'$inicioMes'", "'$fecha'", GP);

        $devolucionDiariaGP = $distribuidores->devoluciones("'$fecha'", "'$fecha'", GP);
        $devolucionMensualGP = $distribuidores->devoluciones("'$inicioMes'", "'$fecha'", GP);

        $pagosDiariosGP = $distribuidores->pagos("'$fecha'","'$fecha'",GP);
        $pagosMensualesGP = $distribuidores->pagos("'$inicioMes'", "'$fecha'", GP);

        foreach ($distribuidoresDPT as $distribuidorDPT) {
            if (!in_array($distribuidorDPT->Nombre, array_column($distribuidoresGP, 'Nombre'))) {
                array_push($distribuidoresGP, (object)array('Nombre' => $distribuidorDPT->Nombre));
            }
        }

        for ($i=0; $i < count($distribuidoresGP); $i++) { 
            if (in_array($distribuidoresGP[$i]->Nombre,  array_column($ventasMensuales, 'Nombre'))) {
                # code...
            } else {
                array_push($ventasMensuales, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
            
            if (in_array($distribuidoresGP[$i]->Nombre, array_column($ventasDiarias, 'Nombre'))) {
                # code...
            } else {
                array_push($ventasDiarias, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
            
            if (in_array($distribuidoresGP[$i]->Nombre, array_column($devolucionDiariaGP, 'Nombre'))) {
                # code...
            } else {
                array_push($devolucionDiariaGP, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }
            
            if (in_array($distribuidoresGP[$i]->Nombre, array_column($devolucionMensualGP, 'Nombre'))) {
                # code...
            } else {
                array_push($devolucionMensualGP, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Monto' => '0'));
            }

            if (in_array($distribuidoresGP[$i]->Nombre, array_column($pagosMensualesGP, "Nombre"))) {
                # code...
            } else {
                array_push($pagosMensualesGP, (object)array('Nombre' => $distribuidoresGP[$i]->Nombre, 'Pago' => '0'));
            }
        }

        $ventasDiariasDPT = $distribuidores->ventas("'$fecha'", "'$fecha'", DPT);
        $ventasMensualesDPT = $distribuidores->ventas("'$inicioMes'", "'$fecha'", DPT);
        if (!empty($ventasDiariasDPT)) { //Se verifica si existe alguna venta
            foreach ($ventasDiariasDPT as $ventaDiariaDPT) { // se recorre las ventas DPT
                //Se comparar si existe la sucursal si no se agrega al array
                if (in_array($ventaDiariaDPT->Nombre, array_column($ventasDiarias, 'Nombre'))) {
                    foreach ($ventasDiarias as $ventaDiaria) { // Recorrer las ventas GP
                        //comparar, si existe sumar su venta
                        if($ventaDiaria->Nombre == $ventaDiariaDPT->Nombre) { 
                            $ventaDiaria->Monto = floatval($ventaDiaria->Monto) + floatval($ventaDiariaDPT->Monto);
                        }
                    }
                } else {
                    array_push($ventasDiarias, $ventaDiariaDPT);
                }
            }
        }

        if (!empty($ventasMensualesDPT)) {
            foreach ($ventasMensualesDPT as $ventaMensualDPT) {
                if (in_array($ventaMensualDPT->Nombre, array_column($ventasMensuales, 'Nombre'))) {
                    foreach ($ventasMensuales as $ventaMensual) {
                        if($ventaMensual->Nombre == $ventaMensualDPT->Nombre) {
                            $ventaMensual->Monto = floatval($ventaMensual->Monto) + floatval($ventaMensualDPT->Monto);
                        }
                    }
                } else {
                    array_push($ventasMensuales, $ventaMensualDPT);
                }
            }
        }

        $pagosDiariosDPT =  $distribuidores->pagos("'$fecha'","'$fecha'",DPT);
        $pagosMensualesDPT = $distribuidores->pagos("'$inicioMes'", "'$fecha'", DPT);

        if (!empty($pagosMensualesDPT)) {
            foreach ($pagosMensualesDPT as $pagoMensualDPT) {
                if (in_array($pagoMensualDPT->Nombre, array_column($pagosMensualesGP, "Nombre"))) {
                    foreach ($pagosMensualesGP as $pagoMensualGP) {
                        if ($pagoMensualGP->Nombre == $pagoMensualDPT->Nombre) {
                            $pagoMensualGP->Pago = floatval($pagoMensualGP->Pago) + floatval($pagoMensualDPT->Pago);
                        }
                    }
                } else {
                    array_push($pagosMensualesGP, $pagoMensualDPT);
                }
                
            }
        }

        $metaActual = $ventas->mesActual($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);
        $metaAnterior = $ventas->mesAnterior($fechaArray[0], $fechaArray[1])->fetch_all(MYSQLI_ASSOC);

        for ($i=0; $i < count($ventasMensuales); $i++) { 
            // Indice Venta Diaria
            $indiceVD = array_search($ventasMensuales[$i]->Nombre, array_column($ventasDiarias, 'Nombre'));
            $indiceDVD = array_search($ventasMensuales[$i]->Nombre, array_column($devolucionDiariaGP, 'Nombre'));//Devolucion Venta Diaria
            $montoVD = ($indiceVD === false)?"000.00":floatval($ventasDiarias[$indiceVD]->Monto) - floatval($devolucionDiariaGP[$indiceDVD]->Monto);
            $indiceDVM = array_search($ventasMensuales[$i]->Nombre, array_column($devolucionMensualGP, 'Nombre'));//Devolucion Venta Mensual
            $montoVM = floatval($ventasMensuales[$i]->Monto) - floatval($devolucionMensualGP[$indiceDVM]->Monto);
            // Indice Meta Mes Año Actual MySQL
            $indiceMetaAct = array_search($ventasMensuales[$i]->Nombre, array_column($metaActual, 'sucursal'));
            $metaMesAct = ($indiceMetaAct === false)?"000.00":$metaActual[$indiceMetaAct]["meta"];
            // Indice Meta Mes Año Anterior MySQL
            $indiceMetaAnt = array_search($ventasMensuales[$i]->Nombre, array_column($metaAnterior, 'sucursal'));
            $metaMesAnt = ($indiceMetaAnt === false)?"000.00":$metaAnterior[$indiceMetaAnt]["meta"];

            $indicePM = array_search($ventasMensuales[$i]->Nombre, array_column($pagosMensualesGP, "Nombre"));

            $montoPM = ($indicePM===false) ? "0" : $pagosMensualesGP[$indicePM]->Pago;

            $porcentajeMetaAct = ($ventas->reglaD3($metaMesAct, $montoVM)>=100)?'<div class="progress"><div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAct, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAct, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAct, $montoVM).'%</div></div>':'<div class="progress"><div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAct, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAct, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAct, $montoVM).'%</div></div>';

            $porcentajeMetaAnt = ($ventas->reglaD3($metaMesAnt, $montoVM)>=100)?'<div class="progress"><div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAnt, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAnt, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAnt, $montoVM).'%</div></div>':'<div class="progress"><div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: '.$ventas->reglaD3($metaMesAnt, $montoVM).'%" aria-valuenow="'.$ventas->reglaD3($metaMesAnt, $montoVM).'" aria-valuemin="0" aria-valuemax="100">'.$ventas->reglaD3($metaMesAnt, $montoVM).'%</div></div>';

            $data[] = array(
                '0' => $ventasMensuales[$i]->Nombre,
                '1' => number_format($montoVD, 2, ".", ","),
                '2' => number_format($montoVM, 2, ".", ","),
                '3' => number_format($metaMesAct, 2, ".", ","),
                '4' => $porcentajeMetaAct,
                '5' => number_format($metaMesAnt, 2, ".", ","),
                '6' => $porcentajeMetaAnt
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    case 'listarPagos':
        $data = array();
        $fechaArray = explode("-", $fecha);
        $inicioMes = $fechaArray[0]."-".$fechaArray[1]."-"."01";

        $distribuidoresGP = $distribuidores->listar(GP);
        $distribuidoresDPT = $distribuidores->listar(DPT);

        $ventasMensualesGP = $distribuidores->ventas("'$inicioMes'", "'$fecha'", GP);

        $devolucionMensualGP = $distribuidores->devoluciones("'$inicioMes'", "'$fecha'", GP);

        $pagosMensualesGP = $distribuidores->pagos("'$inicioMes'", "'$fecha'", GP);

        $saldosVencidos = $distribuidores->saldoVencido("'$fecha'", GP);

        foreach ($distribuidoresGP as $distribuidorGP) {
            if (!in_array($distribuidorGP->Nombre, array_column($ventasMensualesGP, 'Nombre'))) {
                array_push($ventasMensualesGP, (object)array('Nombre' => $distribuidorGP->Nombre, 'Monto' => '0'));
            }
            if (!in_array($distribuidorGP->Nombre, array_column($devolucionMensualGP, 'Nombre'))) {
                array_push($devolucionMensualGP, (object)array('Nombre' => $distribuidorGP->Nombre, 'Monto' => '0'));
            }
            if (!in_array($distribuidorGP->Nombre, array_column($pagosMensualesGP, 'Nombre'))) {
                array_push($pagosMensualesGP, (object)array('Nombre' => $distribuidorGP->Nombre, 'Pago' => '0'));
            }
            if (!in_array($distribuidorGP->Nombre, array_column($saldosVencidos, 'Nombre'))) {
                array_push($saldosVencidos,  (object)array('Nombre' => $distribuidorGP->Nombre, 'saldoVencido' => '0'));
            }
        }

        $ventasMensualesDPT = $distribuidores->ventas("'$inicioMes'", "'$fecha'", DPT);
        if (!empty($ventasMensualesDPT)) {
            foreach ($ventasMensualesDPT as $ventaMensualDPT) {
                if (in_array($ventaMensualDPT->Nombre, array_column($ventasMensualesGP, 'Nombre'))) {
                    foreach ($ventasMensualesGP as $ventaMensual) {
                        if($ventaMensual->Nombre == $ventaMensualDPT->Nombre) {
                            $ventaMensual->Monto = floatval($ventaMensual->Monto) + floatval($ventaMensualDPT->Monto);
                        }
                    }
                } else {
                    array_push($ventasMensualesGP, $ventaMensualDPT);
                }
            }
        }

        $pagosMensualesDPT = $distribuidores->pagos("'$inicioMes'", "'$fecha'", DPT);
        if (!empty($pagosMensualesDPT)) {
            foreach ($pagosMensualesDPT as $pagoMensualDPT) {
                if (in_array($pagoMensualDPT->Nombre, array_column($pagosMensualesGP, "Nombre"))) {
                    foreach ($pagosMensualesGP as $pagoMensualGP) {
                        if ($pagoMensualGP->Nombre == $pagoMensualDPT->Nombre) {
                            $pagoMensualGP->Pago = floatval($pagoMensualGP->Pago) + floatval($pagoMensualDPT->Pago);
                        }
                    }
                } else {
                    array_push($pagosMensualesGP, $pagoMensualDPT);
                }
                
            }
        }

        foreach ($ventasMensualesGP as $ventaMensual) {
            $indiceDVM = array_search($ventaMensual->Nombre, array_column($devolucionMensualGP, 'Nombre'));//Devolucion Venta Mensual
            $montoVM = floatval($ventaMensual->Monto) - floatval($devolucionMensualGP[$indiceDVM]->Monto); // Monto Venta Mensual
            $indicePM = array_search($ventaMensual->Nombre, array_column($pagosMensualesGP, "Nombre"));
            $montoPM = ($indicePM===false) ? "0" : $pagosMensualesGP[$indicePM]->Pago;
            $indiceLC = array_search($ventaMensual->Nombre, array_column($distribuidoresGP, 'Nombre'));//Limite de Credito
            $LimiteCredito = ($indiceLC===false) ? "0" : $distribuidoresGP[$indiceLC]->LimiteCredito;
            $saldo = ($indiceLC===false) ? "0" : $distribuidoresGP[$indiceLC]->Saldo;
            $saldoDisponible = ($indiceLC===false) ? "0" : $distribuidoresGP[$indiceLC]->Disponible;
            $indiceSV = array_search($ventaMensual->Nombre, array_column($saldosVencidos, 'Nombre'));//Saldo Vencido
            $saldoVencido = ($indiceSV===false) ? "0" : $saldosVencidos[$indiceSV]->saldoVencido;

            $data[] = array(
                '0' => $ventaMensual->Nombre,
                '1' => number_format($montoVM, 2, ".", ","),
                '2' => number_format($LimiteCredito, 2, ".", ","),
                '3' => number_format($saldo, 2, ".", ","),
                '4' => number_format($saldoDisponible, 2, ".", ","),
                '5' => number_format($saldoVencido, 2, ".", ","),
                '6' => number_format($montoPM, 2, ".", ",")
            );
        }

        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        echo "No se encontro la opcion";
        break;
}