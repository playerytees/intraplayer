<?php
    require "../config/ConexionHana.php";

    class PedidosAbiertos extends ConexionHana
    {
        public function __construct()
        {
            # code...
        }

        public function listar($empresa, $cliente, $articulo, $estacion)
        {
            try {
                $sqlEstacion = ($estacion != "''")?' AND C."U_SO1_ESTACION" IN ('.$estacion.')' : '';
                $sqlCliente = ($cliente != "''")?' AND A."CardCode" = '.$cliente : '';
                $sqlArticulo = ($articulo != "''")?' AND B."ItemCode" = '.$articulo : '';
                if ($articulo != "''") {
                    $articuloArray = explode("-", $articulo);
                    $sqlArticulo = (!isset($articuloArray[1]) && !isset($articuloArray[2])) ? ' AND T4."ItmsGrpNam" = '.$articuloArray[0] : $sqlArticulo;
                }
                $sql = 'SELECT
                    IFNULL(A."U_SO1_01FOLIORETAIL1", \'\') "FolioR1",
                    A."DocDate" "FechaCreacion",
                    A."DocStatus" "Estado",
                    A."Comments" "Comentarios",
                    B."ItemCode" "Articulo",
                    IFNULL(T3."U_Talla", \'\') "Talla",
                    IFNULL(T3."U_Color", \'\') "Color",
                    T4."ItmsGrpNam" "Linea",
                    T5."FirmName" "Marca",
                    B."Quantity" - IFNULL(B."DelivrdQty",0) "CantidadPendiente",
                    A."CardCode" "CodigoCliente",
                    A."CardName" "NombreCliente",
                    T1."SlpName" "Vendedor",
                    A."U_ORDENCOMPRA" "OrdenCompra"
                FROM "'.$empresa.'"."ORDR" A
                JOIN "'.$empresa.'"."RDR1" B ON A."DocEntry" = B."DocEntry"
                JOIN "'.$empresa.'"."OSLP" T1 ON A."SlpCode" = T1."SlpCode"
                JOIN "'.$empresa.'"."OITM" T3 ON B."ItemCode" = T3."ItemCode"
                JOIN "'.$empresa.'"."OITB" T4 ON T4."ItmsGrpCod" = T3."ItmsGrpCod"
                JOIN "'.$empresa.'"."OMRC" T5 ON T5."FirmCode" = T3."FirmCode"
                JOIN "'.$empresa.'"."@SO1_01VENTA" C ON C."Name" = A."U_SO1_01FOLIORETAIL1"
                WHERE ( B."Quantity" - IFNULL(B."DelivrdQty",0) ) > 0
                AND A."DocStatus" = \'O\'
                '.$sqlEstacion.'
                '.$sqlCliente.'
                '.$sqlArticulo.';';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarEstacion($empresa)
        {
            try {
                $sql = 'SELECT "Code", "Name" FROM "'.$empresa.'"."@SO1_01ESTTRABAJO" ORDER BY "Name";';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }

        public function listarMarca($empresa)
        {
            try {
                $sql = 'SELECT "FirmCode", "FirmName" FROM "'.$empresa.'"."OMRC" WHERE "DataSource" = \'I\';';

                $stmt = $this->connect()->prepare($sql);

                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_OBJ);
            } catch (Exception $e) {
                die($e->getMessage());
            }
        }
    }
