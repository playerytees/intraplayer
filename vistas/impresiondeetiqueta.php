<?php $datos = explode(',', $_GET['codigo']); ?>
<?php $codigos = array_chunk($datos, 2); ?>
<?php for ($i=0; $i < count($datos); $i++) : ?>
<table style="width:100%; height: 100%; page-break-inside:auto; font-family: Arial, Helvetica, sans-serif;">
  <tr style="width:100%;page-break-inside:avoid; page-break-after:auto;">
    <td style="width: 100%; text-align: center; vertical-align: middle;">
        <svg style="<?= ($i==0)?'':'margin-top: 20px;' ?>" class="<?= "Cod-".$i; ?>"></svg>
    </td>
  </tr>
</table>
<?php endfor; ?>
<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script>
function init()
{
    <?php
        for ($i=0; $i < count($datos); $i++) {
            echo 'JsBarcode(".'."Cod-".$i.'", "'.$datos[$i].'", { width: 1, height: 40,  displayValue: true, fontSize: 18, font: "Arial", fontOptions: "bold" });';
        }
    ?>

    /* if (window.print) {
        setTimeout(function(){window.print();window.close();},300); // 500ms = 0.5s - 1000 = 1s
    } */
}

init();
</script>
