<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");

    if ($_SESSION['embarques']==1) {
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Packing</title>
	<link rel="shortcut icon" href="../public/images/logo_playerytees_oficial.png" />	
    <script src="../public/assets/js/lib/tableexport/xlsx.core.min.js"></script>
    <script src="../public/assets/js/lib/tableexport/Blob.min.js"></script>
    <script src="../public/assets/js/lib/tableexport/FileSaver.min.js"></script>
    <link rel="stylesheet" href="../public/assets/css/lib/tableexport/tableexport.min.css">
    <style>
        @media print {
            @page { margin: 10mm; }
            body{
                font-size: 10px;
                margin: 10px;
            }
            table{
                border-collapse: collapse;
            }
            img{
                width: 200px;
            }
            #btn_excel, .button-default{
                display: none;
            }
        }
    </style>
</head>
<body style="margin: 0 1.5em;">
    <table style="width: 100%; font-family: Arial; font-weight: bold; border-collapse: collapse;">
        <tr>
            <td rowspan="3"><img src="../public/images/punto_textil.jpg" alt="Punto Textil" width="250"></td>
            <td rowspan="3">ORIGEN: <br>
            AV. PEDREGAL 210 SECCION 3RA SAN LUIS TEOLOCHOLCO TLAXCALA <br>
            C.P. 90850  RFC  GPL 060609 2K7  TEL.2464616442
            </td>
            <td></td>
            <td>CHOFER:</td>
            <td style="font-weight: normal;">N/A</td>
        </tr>
        <tr>

            
            <td>CONFECCIONES PUNTO TEXTIL S.A. DE C.V.</td>
            <td>FOLIO:</td>
            <td style="font-weight: normal;">N/A</td>
        </tr>
        <tr>

            
            <td></td>
            <td>MAQUILA:</td>
            <td style="font-weight: normal;">N/A</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>FECHA:</td>
            <td style="font-weight: normal;"><?php echo date("Y-m-d") ?></td>
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;">LISTA DE EMBARQUE</td>
        </tr>
    </table>
    <br>
<?php
$tabla = ($_SESSION['user'] == 'embarques2') ? "embarque_pendiente2" : "embarque_pendiente";                       
$consulta = "SELECT id, barcode, partida, estilo, color, trim(talla), GROUP_CONCAT( (piezas*cajas) ) AS piezas, cajas FROM $tabla WHERE cajas > 0 AND sesion = '".$_SESSION['user']."' GROUP BY partida, estilo, color, talla ORDER BY id ASC";

$sql = mysqli_query($conexion, "SELECT estilo, SUM(piezas*cajas) as suma, SUM(cajas) as cajas, color, partida FROM $tabla WHERE cajas > 0 AND sesion = '".$_SESSION['user']."' GROUP BY partida, estilo, color ORDER BY id ASC;");

$resultado = mysqli_query($conexion, $consulta);
 
//$arregloTallas = array("3","4", "5", "6","7", "8","9", "10","11", "12","13", "14","15","16","17","18","19","21","23","25","27","28","30","32","34","36","38","40","42","44","46","48","50", "XS", "S", "M", "L", "XL", "XXL", "3XL", "4XL", "5XL", "6XL", "UNI");
$arregloTallas = array("XS", "S", "M", "L", "XL", "XXL", "3XL", "4XL", "5XL", "UNI");

$arrayTallas[0]="talla";
$arraypartida[0]="partida";
$arrayestilo[0]="estilo";
$arraycolor[0]="color";
$arraysuma[0]="suma";
$arraycajas[0]="cajas";

while($data = mysqli_fetch_array($sql))
{
    array_push($arrayestilo, $data[0]);
    array_push($arraysuma, $data[1]);
    array_push($arraycajas, $data[2]);
    array_push($arraycolor, $data[3]);
    array_push($arraypartida, $data[4]);
}

$numRows=0;
$dataBDProx[][]="";

while($dataProx = mysqli_fetch_array($resultado))
{
    $arraypiezas = explode(',', $dataProx[6]);
    $piezas = ( count($arraypiezas) > 1 )? array_sum($arraypiezas) : $arraypiezas[0];
    //$dataBDProx [$numRows][0]= $dataProx[0];//id
    //$dataBDProx [$numRows][1]= $dataProx[1];//barcode
    $dataBDProx [$numRows][0]= $dataProx[2];//partida
    $dataBDProx [$numRows][1]= $dataProx[3];//estilo
    $dataBDProx [$numRows][2]= $dataProx[4];//color
    $dataBDProx [$numRows][3]= $dataProx[5];//talla
    $dataBDProx [$numRows][4]= $piezas;//piezas
    $dataBDProx [$numRows][5]= $dataProx[7];//cajas
    if(in_array($dataProx[5], $arrayTallas)){
 
    }else {
        $arrayTallas[] = $dataProx[5];
    }
    $numRows++;
}

$matriz[0][0] = "MAQUILA";
$matriz[0][1] = "PARTIDA";
$matriz[0][2] = "ESTILO";
$matriz[0][3] = "COLOR";

$matriz[0][4] = "3";

$matriz[0][5] = "4";
$matriz[0][6] = "6";
$matriz[0][7] = "8";

$matriz[0][8] = "9";

$matriz[0][9] = "10";
$matriz[0][10] = "12";
$matriz[0][11] = "14";
$matriz[0][12] = "XS";
$matriz[0][13] = "S";
$matriz[0][14] = "M";
$matriz[0][15] = "L";
$matriz[0][16] = "XL";
$matriz[0][17] = "XXL";
$matriz[0][18] = "3XL";
$matriz[0][19] = "4XL";
$matriz[0][20] = "5XL";
$matriz[0][21] = "UNI";
$matriz[0][22] = "PIEZAS";
$matriz[0][23] = "CAJAS";

$filMat = 1;

for($tit = 1; $tit < count($arraypartida); $tit++)
{
    $partida = explode("-", $arraypartida[$tit]);
    $maquila = "";
    switch ($partida[0]) {
        case "FAB":
            $maquila = "Fabrica";
            break;
        case "C2":
            $maquila = "Confecciones 2";
            break;
        default:
           $maquila = "Maquila";
           break;
    }
    $matriz[$tit][0] = $maquila;//maquila
    $matriz[$tit][1] = (count($partida) > 2) ? $partida[1]."-".$partida[2] : $partida[1];//partida
    $matriz[$tit][2] = $arrayestilo[$tit];//estilo
    $matriz[$tit][3] = $arraycolor[$tit];//color
    $matriz[$tit][22] = $arraysuma[$tit];//total piezas
    $matriz[$tit][23] = $arraycajas[$tit];//total cajas
}

for($rel = 1; $rel < count($arraypartida); $rel++)
{
    for($cols = 4; $cols < 22; $cols++)
    {
        $matriz[$rel][$cols] = "";
    }
}

//RECORRER PARA PONER NUMERO PIEZAS
for($pbd = 0; $pbd < $numRows; $pbd++)//filas del query DB
{
    for($rel = 1; $rel < count($arraypartida); $rel++)//filas
    {
        if($dataBDProx[$pbd][0] == $arraypartida[$rel] AND $dataBDProx[$pbd][1] == $matriz[$rel][2] AND $dataBDProx[$pbd][2] == $matriz[$rel][3])
        {
            for($cols = 4; $cols < 22; $cols++)//columnas
            {
                $tallas = ($dataBDProx[$pbd][3]=='2XL')?'XXL':$dataBDProx[$pbd][3];
                if($tallas == $matriz[0][$cols])
                {
                    $matriz[$rel][$cols] = $dataBDProx[$pbd][4];
                    //$matriz[$rel][2] += $dataBDProx[$pbd][3];//total suma piezas del row
                }
            }
        }
    }
}

echo "<table style='width: 100%; font-family: Arial; border-collapse: collapse;' border='1' id='OrdenEmbarque'>
        <thead>
            <tr>";
        for($fm = 0; $fm < 1; $fm++)
        {
          for($rm=0; $rm < 24; $rm++)
          {
            echo "<th>".$matriz[$fm][$rm]."</th>";
          }
        }
        echo "</tr></thead><tbody>";
        for($fm = 1; $fm < count($arraypartida); $fm++)//filas
        {
          echo "<tr>";
          for($rm=0; $rm < 24; $rm++)//columnas
          {
            echo "<td>".$matriz[$fm][$rm]."</td>";
          }
          echo "</tr>";
        }
        echo "<tr>
            <td colspan='4' align='right'><b>TOTAL:</b></td>";
                unset($matriz[0]);
                for ($i=4; $i < 24; $i++)
                {
                    echo "<td>".array_sum(array_column($matriz, $i))."</td>";
                }
        echo "</tr>";
        echo "</tbody></table>";
?>
<br><br><br>
<table style='width: 100%; font-family: Arial; border-collapse: collapse; text-align: center;' id='tabla'>
    <tr>
        <td>RECIBE</td>
        <td>RECIBE</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>CHOFER - FABRICA</td>
        <td>ALMACEN - CEDIS</td>
    </tr>
</table>
<center><button type="button" id="btn_excel">Exportar a Excel</button></center>
</body>
</html>
<?php
}
else
{
    require 'noacceso.php';
}
?>
<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="../public/assets/js/lib/tableexport/tableexport.min.js"></script>
<script>
$("#btn_excel").click(function (e) {
    e.preventDefault();
    $("#OrdenEmbarque").tableExport();
    $(this).prop("disabled", true);
});
</script>
<?php
}
ob_end_flush();
?>