var tabla;
var myChart;
function init() {
    $("#formulario").on("submit", function (e) {
        guardar(e);
    });
    listar($("#fecha").val());
    listarSucursales();
    $(".loader").hide();
    mostrarform(false);
    grafica();
    addData();
}

function limpiar() {
    $("#txtmonto").val("");
}

function mostrarform(flag) {
    limpiar();
    if (flag) {
        $("#cardventas").hide();
        $("#formpresupuesto").show();
        $("#btnagregar").hide();
    } else {
        $("#cardventas").show();
        $("#formpresupuesto").hide();
        $("#btnagregar").show();
    }
}

function cancelarform() {
    limpiar();
    mostrarform(false);
}

function guardar(e) {
    e.preventDefault();
    var formData = new FormData($("#formulario")[0]);

    $.ajax({
        type: "POST",
        url: "../ajax/ventas.php?opcion=guardar",
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response) {
                $.alert({type: 'green', title: 'Correcto', content: "Presupuesto actualizado correctamente"});
            } else {
                $.alert({type: 'red', title: 'Error', content: 'No se ha podido actualizar'});
            }
            mostrarform(false);
            tabla.ajax.reload();
        }
    });
    limpiar();
}

function listar(fecha) {
    tabla = $("#tablaventas").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "rt",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var diasHabiles;
            var diasTranscurridos;
            var diasRestantes;

            var prom_diario;
            var tendencia;
            var difVsAA;
            var PorcentajeVsAA;
            var difVsCuota;
            var PorcentajeVsCuota;
            var prom_necesario;

            $( api.column( 0 ).footer() ).html("TOTAL:");
            $( api.column( 1 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 1 ).data().sum().toFixed(2) ) );
            $( api.column( 2 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 2 ).data().sum().toFixed(2) ) );

            $.post("../ajax/ventas.php?opcion=bussiness_days", {fecha: fecha},
                function (data, textStatus, jqXHR) {
                    data = JSON.parse(data);

                    diasHabiles = data[0][0];
                    diasTranscurridos = data[0][1];
                    diasRestantes = data[0][2];

                    prom_diario = tabla.column( 2 ).data().sum().toFixed(2) / diasTranscurridos;
                    tendencia = prom_diario * diasHabiles;
                    difVsAA = tendencia - tabla.column( 5 ).data().sum().toFixed(2);
                    PorcentajeVsAA = difVsAA / tabla.column( 5 ).data().sum().toFixed(2);
                    difVsCuota = tendencia - tabla.column( 8 ).data().sum().toFixed(2);
                    PorcentajeVsCuota = difVsCuota / tabla.column( 8 ).data().sum().toFixed(2);
                    prom_necesario = ( tabla.column( 8 ).data().sum().toFixed(2) - tabla.column( 2 ).data().sum().toFixed(2) ) / diasRestantes;

                    $( api.column( 3 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_diario ) );
                    $( api.column( 4 ).footer() ).html( new Intl.NumberFormat("en-US").format( tendencia ) );
                    $( api.column( 5 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 5 ).data().sum().toFixed(2) ) );
                    $( api.column( 6 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsAA ) );
                    $( api.column( 7 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsAA*100 ) );
                    $( api.column( 8 ).footer() ).html( new Intl.NumberFormat("en-US").format( tabla.column( 8 ).data().sum().toFixed(2) ) );
                    $( api.column( 9 ).footer() ).html( new Intl.NumberFormat("en-US").format( difVsCuota ) );
                    $( api.column(10 ).footer() ).html( new Intl.NumberFormat("en-US").format( PorcentajeVsCuota*100 ) );
                    $( api.column(11 ).footer() ).html( new Intl.NumberFormat("en-US").format( prom_necesario ) );
                }
            );
        },
        "initComplete": function(settings, json) {
            // sumaColumnas();
        },
        "ajax": {
            url: "../ajax/ventas.php?opcion=ventas",
            type: "POST",
            data: {fecha: fecha},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarSucursales() {
    $.post("../ajax/ventas.php?opcion=listarSucursales",
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {
                select += `<option value="${data[i][0]}">${data[i][1]}</option>`;
            }
            $("#selectsucursal").append(select);
        }
    );
}

$("#btn-ver").click(function (e) { 
    e.preventDefault();
    tabla.clear().draw();
    listar($("#fecha").val());
    addData();
    //tabla.ajax.reload();
});

function sumaColumnas() {
    $("#totalDailySales").text("$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 1 ).data().sum().toFixed(2) ) );
    $("#totalMonthlySales").text("$ "+ new Intl.NumberFormat("en-US").format( tabla.column( 2 ).data().sum().toFixed(2) ) );
    $("#AlcanceTotal").text( ((tabla.column( 2 ).data().sum() * 100)/tabla.column( 7 ).data().sum()).toFixed()+"%" );
}

function grafica() {
    var ctx = document.getElementById('grafica').getContext('2d');
    myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: "",
            datasets: [{
                label: 'Tendencia',
                data: "",
                backgroundColor: 'rgba(8, 151, 20, 0.8)',
                borderColor: 'rgba(8, 151, 20, 1)',
                borderWidth: 1
            },
            {
                label: 'Año Anterior',
                data: "",
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            },
            {
                label: 'Cuota',
                data: "",
                fill: false,
                backgroundColor: 'rgba(255, 0, 0, 0.5)',
                borderColor: 'rgba(255, 0, 0, 0.5)',
                type: 'line'
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                        /* min: 0,
                        max: 100,
                        callback: function(value) {
                            return value + "%"
                        } */
                    },
                    /* scaleLabel: {
                        display: true,
                        labelString: "Porcentaje"
                    } */
                }]
            }
        }
    });
}

function addData() {
    var etiquetas = [];
    var tendencia = [];
    var anioAnterior = [];
    var cuota = [];
    $(".loading").show();
    $("#btn-ver").prop("disabled", true);
   $.post("../ajax/ventas.php?opcion=grafica", {fecha: $("#fecha").val()},
        function (response, textStatus, jqXHR) {
            $(".loading").hide();
            $("#btn-ver").prop("disabled", false);
            response = JSON.parse(response);
            etiquetas.length=0;
            tendencia.length=0;
            anioAnterior.length=0;
            cuota.length = 0;
            for (let i = 0; i < response.length; i++) {
                etiquetas.push(response[i][0]);
                tendencia.push(response[i][1]);
                anioAnterior.push(response[i][2]);
                cuota.push(response[i][3]);
            }
            myChart.data.labels = etiquetas;
            myChart.data.datasets[0].data = tendencia;
            myChart.data.datasets[1].data = anioAnterior;
            myChart.data.datasets[2].data = cuota;
            myChart.update();
        }
    );
}

init();