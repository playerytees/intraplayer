<?php
ob_start();
session_start();

if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

    require 'header.php';

    if ($_SESSION['tag']==1) {
?>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 style="float: left;" class="text-playerytees">Etiquetas</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li class="active">Etiqueta</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Datos Etiqueta Chica</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcliente" class="form-control-label">Cliente</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcliente" id="txtcliente" class="form-control form-control-sm" placeholder="nombre del cliente">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtetiqueta" class="form-control-label">Etiqueta</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtetiqueta" id="txtetiqueta" class="form-control form-control-sm" placeholder="nombre de etiqueta">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcomposicion" class="form-control-label">Composicion</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcomposicion" id="txtcomposicion" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txttalla" class="form-control-label">Talla</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txttalla" id="txttalla" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcant" class="form-control-label">Cantidad</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcant" id="txtcant" class="form-control form-control-sm" onKeyPress="return soloNumeros(event)" maxlength="7">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtfolio-ch" class="form-control-label">Folio</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtfolio-ch" id="txtfolio-ch" class="form-control form-control-sm" maxlength="10">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txt-oc-ch" class="form-control-label">OC</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txt-oc-ch" id="txt-oc-ch" class="form-control form-control-sm" maxlength="25">
                            </div>
                        </div>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->

            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Etiqueta Chica</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <table style="margin: 0 auto;">
                            <tr>
                                <td style="font-weight: bold;">Cliente:</td>
                                <td id="lblcliente"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Etiqueta:</td>
                                <td id="lbletiqueta"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Composición:</td>
                                <td id="lblcomposicion" colspan="2"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Talla:</td>
                                <td id="lbltalla"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Cantidad:</td>
                                <td id="lblcant">0</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Folio:</td>
                                <td id="lblfolio-ch"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">OC:</td>
                                <td id="lbl-oc-ch"></td>
                            </tr>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" onclick="imprimirEtiquetaChica()">Imprimir</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->

            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="crad-title">Datos Etiqueta Grande</strong>
                    </div> <!-- .card-title -->
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcliente01" class="form-control-label">Cliente</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcliente01" id="txtcliente01" class="form-control form-control-sm" placeholder="nombre del cliente">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtetiqueta01" class="form-control-label">Etiqueta</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtetiqueta01" id="txtetiqueta01" class="form-control form-control-sm" placeholder="nombre de etiqueta">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcomposicion01" class="form-group-label">Composicion</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcomposicion01" id="txtcomposicion01" class="form-control form-control-sm">
                            </div>
                        </div>
						<div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtfolio" class="form-group-label">Folio</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtfolio" id="txtfolio" class="form-control form-control-sm" maxlength="10">
                            </div>
                        </div>
                        <hr>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txttalla01" class="form-control-label">Talla</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txttalla01" id="txttalla01" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtnumCajones" class="form-control-label">Cajones</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtnumCajones" id="txtnumCajones" class="form-control form-control-sm" placeholder="numero de cajones" onKeyPress="return soloNumeros(event)" maxlength="7">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtetiquetasxcajon" class="form-control-label">Etiquetas/Cajon</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtetiquetasxcajon" id="txtetiquetasxcajon" class="form-control form-control-sm" placeholder="etiquetas por cajon" onKeyPress="return soloNumeros(event)" maxlength="7">
                            </div>
                        </div>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" onclick="agregartallas()">Agregar</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- col-3 -->

            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Etiqueta Grande</strong>
                    </div> <!-- card-header -->
                    <div class="card-body card-block">
                        <table border="1" class="text-center table-responsive" style="margin: 0 auto;">
                            <tr>
                                <td colspan="2" class="font-weight-bold">CLIENTE:</td>
                                <td colspan="2" id="lblcliente01">Cliente</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="font-weight-bold">NOMBRE ETIQUETA</td>
                                <td colspan="2" id="lbletiqueta01">Nombre Etiqueta</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="font-weight-bold">COMPOSICIÓN:</td>
                                <td colspan="2" id="lblcomposicion01">Composicion</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">TALLA</td>
                                <td class="font-weight-bold">CAJONES</td>
                                <td class="font-weight-bold">ETIQUETAS <br> POR CAJÓN</td>
                                <td class="font-weight-bold">TOTAL</td>
                            </tr>
                            <!-- <tr>
                                <td id="lbltalla01">S</td>
                                <td id="lblnumCajones">0</td>
                                <td id="lbletiquetasxcajon">0</td>
                                <td id="lbletiquetasxcaja">0</td>
                            </tr> -->
                            <tr style="background-color: #000" class="font-weight-bold text-white" id="total">
                                <td>TOTAL <br> CAJONES</td>
                                <td id="lblsumaNumCajones">0</td>
                                <td>TOTAL <br> ETIQUETAS</td>
                                <td id="lblsumatotal">0</td>
                            </tr>
							
							<tr>
                                <td>Packing:</td>
                                <td><span id="lblfecha"></span>&nbsp;<span id="lblhora"></span></td>
                                <td>Folio:</td>
                                <td id="lblfolio"></td>
							</tr>
							
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" onclick="imprimirEtiquetaGrande()">imprimir</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->

        </div> <!-- .row -->

        <div class="row">
            <div class="col-12 col-md-6 col-xl-3 d-none">
                <div class="card">
                    <div class="card-header">
                        <strong class="crad-title">Datos Etiqueta 5X2.5cm</strong>
                    </div> <!-- .card-title -->
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtnombre03" class="form-control-label">Nombre</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtnombre03" id="txtnombre03" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtmaquina03" class="form-control-label">Máquina</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtmaquina03" id="txtmaquina03" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtfecha03" class="form-group-label">Fecha</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtfecha03" id="txtfecha03" class="form-control form-control-sm">
                            </div>
                        </div>
						<div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtfolio03" class="form-group-label">No. Folio</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtfolio03" id="txtfolio03" class="form-control form-control-sm" maxlength="10">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtrollo03" class="form-control-label">No. Rollo</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtrollo03" id="txtrollo03" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcantidad03" class="form-control-label">Cantidad</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcantidad03" id="txtcantidad03" class="form-control form-control-sm" onKeyPress="return soloNumeros(event)" maxlength="7">
                            </div>
                        </div>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- col-3 -->

            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="crad-title">Datos Etiqueta 2.5cm X 2.5cm</strong>
                    </div> <!-- .card-title -->
                    <div class="card-body card-block">
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcliente04" class="form-control-label">Cliente</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcliente04" id="txtcliente04" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtitem04" class="form-control-label">Item</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtitem04" id="txtitem04" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtoc04" class="form-group-label">OC</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtoc04" id="txtoc04" class="form-control form-control-sm">
                            </div>
                        </div>
						<div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtcantidad04" class="form-group-label">Cantidad</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtcantidad04" id="txtcantidad04" class="form-control form-control-sm" maxlength="10">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-4">
                                <label for="txtfecha04" class="form-control-label">Fecha</label>
                            </div>
                            <div class="col-12 col-md-8">
                                <input type="text" name="txtfecha04" id="txtfecha04" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- col-3 -->

            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Etiqueta 2.5cm X 2.5cm</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <table style="margin: 0 auto;">
                            <tr>
                                <td style="font-weight: bold;">Cliente:</td>
                                <td id="lblcliente04"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Item:</td>
                                <td id="lblitem04"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">OC:</td>
                                <td id="lbloc04"></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Cantidad:</td>
                                <td id="lblcantidad04">0</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;">Fecha:</td>
                                <td id="lblfecha04"></td>
                            </tr>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" onclick="imprimirEtiqueta25cmx25cm()">Imprimir</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->

            <div class="col-12 col-md-6 col-xl-3">
                <div class="card">
                    <div class="card-header">
                        <strong class="card-title">Etiqueta 5 X 2.5 cm</strong>
                    </div> <!-- card-header -->
                    <div class="card-body card-block">
                        <table border="1" class="text-center" style="margin: 0 auto;">
                            <tr>
                                <td><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></div></td>
                                <td>Tono</td>
                                <td class="font-weight-bold">Nombre:</td>
                                <td id="lblnombre03" style="min-width: 100px;"></td>
                            </tr>
                            <tr>
                                <td><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></div></td>
                                <td>Definición</td>
                                <td class="font-weight-bold">Máquina:</td>
                                <td id="lblmaquina03"></td>
                            </tr>
                            <tr>
                                <td><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></div></td>
                                <td>Anclaje</td>
                                <td class="font-weight-bold">Fecha:</td>
                                <td id="lblfecha03"></td>
                            </tr>
                            <tr>
                                <td><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></div></td>
                                <td>Reverso</td>
                                <td class="font-weight-bold">No. Folio:</td>
                                <td id="lblfolio03"></td>
                            </tr>
                            <tr>
                                <td><div style="width: 18px; height: 18px; border: 1px solid; margin: 5px;"></div></td>
                                <td>Desface</td>
                                <td class="font-weight-bold">No. Rollo:</td>
                                <td id="lblrollo03"></td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Talla:</td>
                                <td id="lbltalla03"></td>
                                <td class="font-weight-bold">Cantidad:</td>
                                <td id="lblcantidad03"></td>
                            </tr>
                            <tr>
                                <td colspan="4" class="font-weight-bold text-left">Composición:</td>
                            </tr>
                        </table>
                    </div> <!-- .card-body -->
                    <div class="card-footer">
                        <button class="btn btn-primary btn-sm" onclick="imprimirEtiqueta5x25cm()">imprimir</button>
                    </div> <!-- .card-footer -->
                </div> <!-- .card -->
            </div> <!-- .col-3 -->

        </div> <!-- .row -->
    </div> <!-- .content -->
<?php
}
else{
    require "noacceso.php";
}
require 'footer.php';
?>
<script type="text/javascript" src="scripts/tag.js"></script>
<?php
}
ob_end_flush();
?>