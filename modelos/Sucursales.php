<?php
require "../config/Conexion.php";
date_default_timezone_set("America/Mexico_City");

class Sucursales
{
    public function __construct()
    {

    }

    public function insertar($descr, $direccion, $encargado, $telefono, $estado, $descripcion)
    {
        $sql = "INSERT INTO sucursales VALUES (NULL, NULL, '$descr', '$direccion', '$encargado', '$telefono', NULL, '$estado', '$descripcion', now(), now())";

        return ejecutarConsulta($sql);
    }

    public function editar($id, $descr, $direccion, $encargado, $telefono, $estado, $descripcion)
    {
        $sql = "UPDATE sucursales SET descr = '$descr', direccion = '$direccion', encargado = '$encargado', telefono = '$telefono', estado = '$estado', descripcion = '$descripcion', modified = now() WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function desactivar($id)
    {
        $sql = "UPDATE sucursales SET estado = 'I' WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function activar($id)
    {
        $sql = "UPDATE sucursales SET estado = 'A' WHERE id = '$id'";

        return ejecutarConsulta($sql);
    }

    public function mostrar($id)
    {
        $sql = "SELECT * FROM sucursales WHERE id = '$id'";

        return ejecutarConsultaSimpleFila($sql);
    }

    public function listar()
    {
        $sql = "SELECT * FROM sucursales WHERE estado = 'A'";

        return ejecutarConsulta($sql);
    }

    public function catalogo_exist($id)
    {
        $sql = "SELECT * FROM sucursales s WHERE NOT EXISTS (SELECT NULL FROM catalogo c WHERE c.id_sucursal = s.id) AND s.id = '$id'";

        return ejecutarConsulta($sql);
    }
}

?>