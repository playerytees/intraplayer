<?php
require_once "../modelos/Inventario.php";

$inventario = new Inventario();

$id = isset($_POST['id'])?limpiarCadena($_POST['id']):"";
$sku_manufactura = isset($_POST['sku_manufactura'])?limpiarCadena($_POST['sku_manufactura']):"";
$sku_custom = isset($_POST['sku_custom'])?limpiarCadena($_POST['sku_custom']):"";
$existencias = isset($_POST['existencias'])?limpiarCadena($_POST['existencias']):"";
$id_sucursal = isset($_POST['id_sucursal'])?limpiarCadena($_POST['id_sucursal']):"";

switch ($_GET['opcion']) {
    case 'guardar':
        $inventario->delete($id_sucursal);
        $datosTabla = json_decode($_POST['tabla_inve'], true);

        foreach ($datosTabla as $key => $value)
        {
            $response = $inventario->insertar($value['clave_manufactura'], $value['clave_personalizada'], $value['existencias'], $id_sucursal);
        }

        echo $response;
        break;
    case 'listar':
        $response = $inventario->listar($id_sucursal);
        $data = array();
        $existencias = array();
        $connGlobal = ibase_connect('C:\xamppi\htdocs\DB\SAE70EMPRE01.FDB', 'SYSDBA', 'masterkey');
        $connPunto = ibase_connect('C:\xamppi\htdocs\DB\SAE70EMPRE02.FDB', 'sysdba', 'masterkey');

        $sqlGlobal = "SELECT CVE_ART, EXIST FROM MULT01 WHERE CVE_ALM = 1 AND EXIST > 0";
            
        $sqlPunto = "SELECT CVE_ART, EXIST FROM MULT02 WHERE CVE_ALM = 1 AND EXIST > 0";
            
        $resultGlobal = ibase_query($connGlobal, $sqlGlobal);

        while ($rowGlobal = ibase_fetch_object($resultGlobal)) {
            $existencias[] = array(
                'cve_art' => $rowGlobal->CVE_ART,
                'exist' => $rowGlobal->EXIST
            );
        }

        $resultPunto = ibase_query($connPunto, $sqlPunto);

        while ($rowPunto = ibase_fetch_object($resultPunto)) {
            $existencias[] = array(
                'cve_art' => $rowPunto->CVE_ART,
                'exist' => $rowPunto->EXIST
            );
        }
        
        while ($reg=$response->fetch_object()) {
            $pos = array_search($reg->sku_manufactura, array_column($existencias, 'cve_art'));
            //$difexist = $row->EXIST - $reg->piezas;
            $data[] = array(
                "0" => $reg->sku_manufactura,
                "1" => $reg->sku_custom,
                "2" => $reg->linea,
                "3" => $reg->sugerido,
                "4" => $reg->inventario,
                "5" => $reg->faltante,
                "6" => $reg->propuesto,
                "7" => $reg->num_piezas,
                "8" => $reg->piezas_surtir,
                "9" => $existencias[$pos]['exist'],
                "10" => $existencias[$pos]['exist'] - $reg->piezas_surtir
            );
        }
        $results = array(
            "sEcho" => 1,
            "iTotalRecords" => count($data),
            "iTotalDisplayRecords" => count($data),
            "aaData" => $data
        );

        echo json_encode($results);
        break;
    default:
        echo "La opcion seleccionada no existe";
        break;
}
?>