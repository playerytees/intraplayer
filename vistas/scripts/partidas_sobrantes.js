var tabla1, tabla2;

function init()
{
    listarsobrantes();
    listartemporary();
    $(".loader").hide();
}

function listarsobrantes()
{
    tabla1 = $("#tablasobrantes").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "frtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/sobrante.php?opcion=listarsobrantes",
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listartemporary()
{
    tabla2 = $("#tablatemporary").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "frtip",
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdf'
        ],
        "ajax": {
            url: "../ajax/sobrante.php?opcion=listartemporary",
            type: "get",
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 10,
        "order": [[0, "desc"]]
    }).DataTable();
}

function agregarsobrantes(id)
{
    $.confirm({
        title: '¡Agregar!',
        content: '¿Desea agregarlo para embarcar?',
        buttons: {
            confirm: {
                text: "Aceptar",
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "../ajax/sobrante.php?opcion=agregarsobrante",
                        data: {id: id},
                        success: function (response) {
                            $.alert(response);
                            tabla1.ajax.reload();
                            tabla2.ajax.reload();
                        }
                    });
                }
            },
            cancel: {
                text: "Cancelar",
                btnClass: 'btn-red',
                action: function () {
                    $.alert('Se ha Cancelado la operación');
                }
            }
        }
    });
}

function quitarsobrantes(id)
{
    $.confirm({
        title: '¡Quitar!',
        content: '¿Desea quitar del embarque?',
        buttons: {
            confirm: {
                text: "Aceptar",
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "../ajax/sobrante.php?opcion=quitarsobrante",
                        data: {id: id},
                        success: function (response) {
                            $.alert(response);
                            tabla1.ajax.reload();
                            tabla2.ajax.reload();
                        }
                    });
                }
            },
            cancel: {
                text: "Cancelar",
                btnClass: 'btn-red',
                action: function () {
                    $.alert('Se ha Cancelado la operación');
                }
            }
        }
    });
}

function guardarembarque(e)
{
    e.preventDefault();
    if (tabla2.data().length > 0) {
        $.alert("CORRECTO");
    } else {
        $.alert("No hay partidas que embarcar");
    }
}

init();