<table style="width: 600px; height: 355px; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
    <tr style="border-bottom: 1px solid;">
        <td colspan="3"><img id="code" style="margin-left: 15px;"></img></td>
        <td colspan="3" align="right"><img id="code" style="margin-right: 15px;"></img></td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold; width: 100px;">STYLE:</td>
        <td style="text-align: center; font-size: 25px;"><b><span class="labelStyle"></span></b></td>
        <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 355px; text-align: center;" rowspan="6">
            <div style="writing-mode: tb-rl; transform: rotate(180deg);">
                <span>Order</span> <span class="labelOrden"></span>
            </div>
        </td>
        <td style="text-align: center; font-weight: bold; width: 100px;">STYLE:</td>
        <td style="text-align: center; font-size: 25px;"><b><span class="labelStyle" id="labelStyle"></span></b></td>
        <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 355px; text-align: center;" rowspan="6">
            <div style="writing-mode: tb-rl; transform: rotate(180deg);">
                <span>Order </span><span class="labelOrden"></span>
            </div>
        </td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold;">SIZE:</td>
        <td style="text-align: center; font-size: 25px;"><b><span class="labelSize"></span></b></td>
        <td style="text-align: center; font-weight: bold;">SIZE:</td>   
        <td style="text-align:center; font-size: 25px;"><b><span class="labelSize"></span></b></td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold;">COLOR:</td>
        <td style="text-align: center;"><b><span class="labelColor" style="font-size: 25px;"></span></b></td>
        <td style="text-align: center; font-weight: bold;">COLOR:</td>
        <td style="text-align: center;"><b><span class="labelColor" style="font-size: 25px;"></span></b></td>
    </tr>
    <tr>
        <td style="text-align: center; border-left: 1px solid; font-weight: bold;">QUANTITY:</td>
        <td style="text-align: center; font-size: 30px"><b><span class="labelQty"></span></b></td>
        <td style="text-align: center; font-weight: bold;">QUANTITY:</td>
        <td style="text-align: center; font-size: 30px"><b><span class="labelQty"></span></b></td>
    </tr>
    <tr style="text-align: center; border-left: 1px solid;">
        <td colspan="2" valign="bottom" style="padding: 0; height: 50px;"><img class="barcode"></img></td>
        <td colspan="2" valign="bottom" style="padding: 0"><img class="barcode"></img></td>
    </tr>
    <tr style="font-size: 12px; background: black; color: white; height: 10px;">
        <td valign="bottom">Packing</td>
        <td valign="bottom" id="labelFecha" style="font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
        <td valign="bottom" style="text-align: right">Packing</td>
        <td valign="bottom" style="text-align: right; font-weight: bold;"><?php echo date("d/m/Y"); ?></td>
    </tr>
</table>

<script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
<script src="../public/assets/js/JsBarcode.code128.min.js"></script>
<script>
function init()
{
    var estilo = getParameterByName('estilo');
    var talla = getParameterByName('talla');
    var color = getParameterByName('color');
    var pza = getParameterByName('pza');
    var partida = getParameterByName('partida');
    var barcode = getParameterByName('barcode');
    var cve_art = getParameterByName('cve_art');

    $(".labelStyle").html(estilo);
    $(".labelSize").html(talla);
    $(".labelColor").html(color);
    $(".labelQty").html(pza);
    $(".labelOrden").html(partida);

    JsBarcode("#code", barcode, { width: 1, height: 15, displayValue: false, margin: 0 });
    JsBarcode(".barcode", cve_art, { width: 1, height: 30, fontSize: 18, font: "Arial", fontOptions: "bold" });

    window.print(this);
    window.close(this);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

init();
</script>