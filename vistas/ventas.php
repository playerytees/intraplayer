<?php
ob_start();
session_start();
date_default_timezone_set("America/Mexico_City");
if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['existencias']==1 OR $_SESSION['Distribuidores']==1) {
?>
<style>
/* #tablaventas td:nth-child(2), td:nth-child(3), td:nth-child(4), td:nth-child(5), td:nth-child(6), td:nth-child(7), td:nth-child(8), td:nth-child(10) {
    text-align: right;
} */
#tablaventas td:nth-child(1){
    white-space: nowrap;
    text-align: left;
}
/* #tablaventas td:nth-child(9), td:nth-child(11){
    text-align: center;
} */
</style>
    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <!-- <h1 style="float: left;" class="text-playerytees">Presupuesto </h1> -->
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li class="active">Ventas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>

    <div class="content mt-3">
        <div class="row">
            <div class="col-12">
                <div class="card" id="cardventas">
                    <div class="card-header pb-0">
                        <strong class="card-title text-playerytees float-left">Ventas</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <div class="form-inline">
                            <div class="form-group">
                                <label class="form-control-label mr-1">Seleccionar Fecha:</label>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input type="date" class="form-control form-control-sm" id="fecha" name="fecha" required value="<?= date("Y-m-d")?>">
                                    <button type="button" class="btn btn-playerytees" id="btn-ver">Ver</button>
                                </div>
                            </div>
                        </div>
                        <table class='table table-sm table-striped table-bordered table-responsive-xl' id="tablaventas" style="width: 100%;">
                            <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                <tr style="white-space: nowrap;">
                                    <th>SUCURSAL</th>
                                    <th>VENTA DIA</th>
                                    <th>VENTA ACUMULADA MES</th>
                                    <th>PROM DIARIO</th>
                                    <th>TENDENCIA</th>
                                    <th>AÑO ANTERIOR</th>
                                    <th>DIF VS A.A</th>
                                    <th>% VS A.A</th>
                                    <th>CUOTA</th>
                                    <th>DIF VS CUOTA</th>
                                    <th>% VS CUOTA</th>
                                    <th>PROM NECESARIO</th>
                                <!--<th>AÑO ANTERIOR</th> -->
                                </tr>
                            </thead>
                            <tbody style="font-size: 14px; text-align: right;"></tbody>
                            <?php if($_SESSION['VentasTotales']==1): ?>
                            <tfoot style="font-size: 14px; white-space: nowrap; text-align: right;">
                                <tr>
                                    <th class="text-left"></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <?php endif; ?>
                        </table>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
        <?php if($_SESSION['VentasTotales']==1): ?>
        <div class="row">
            <div class="col-xl-8">
                <div class="card">
                    <div class="card-body">
                        <div style="width: 100%; overflow: auto;">
                            <div id="chart-content" style="width: 800px; height: 400px; margin-left: auto; margin-right: auto;">
                                <canvas id="grafica"></canvas>
                            </div>
                        </div>
                    </div>
                </div> <!-- .card -->
            </div> <!-- .col-lg-6 -->
        </div> <!-- .row -->
        <?php endif; ?>
    </div> <!-- .content -->
<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script type="text/javascript" src="scripts/ventas.js"></script>
<?php
}
ob_end_flush();
?>