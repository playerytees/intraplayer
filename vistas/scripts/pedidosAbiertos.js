var tblPedidosAbiertos;

function init() {
    $(".loader").hide();
    $("#contentTblVentas").hide();
    listarEstacion("GP_BD");
    $('.form-control-chosen').select2({
        language: "es",
        width: '100%',
        closeOnSelect: false
    });
}

function listar(empresa, cliente, articulo, estacion) {
    tblPedidosAbiertos = $("#tblPedidosAbiertos").dataTable({
        "aProcessing": true,
        "aServerSide": true,
        dom: "Bfrtip",
        buttons: [
            {
            extend: 'excelHtml5',
            title: 'Pedidos-Abiertos'
        },
        {
            extend: 'csvHtml5',
            title: 'Pedidos-Abiertos'
        }],
        "ajax": {
            url: "../ajax/pedidosAbiertos.php?opcion=listar",
            type: "POST",
            data: {empresa: empresa, cliente: cliente, articulo: articulo, estacion: estacion},
            dataType: "json",
            error: function (e) {
                console.log(e.responseText);
            }
        },
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },
        "bDestroy": true,
        "iDisplayLength": 25,
        "order": [[0, "desc"]]
    }).DataTable();
}

function listarEstacion(empresa) {
    $.post("../ajax/pedidosAbiertos.php?opcion=listarEstacion", {empresa: empresa},
        function (data, textStatus, jqXHR) {
            data = JSON.parse(data);
            var select = "";
            for (let i = 0; i < data.length; i++) {  
                select += `<option value="'${data[i][0]}'">${data[i][1]}</option>`;
            }
            $("#select_estacion").append(select);
            $("#select_estacion").trigger("chosen:updated");
        }
    );
}

$("#btnBuscar").click(function (e) { 
    e.preventDefault();
    listar(  $("#input_empresa").val(), $("#input_cliente").val(), $("#input_articulo").val(),  $("#select_estacion").val() );
    $("#contentTblVentas").show();
});

$("#btnDPT").click(function (e) { 
    e.preventDefault();
    $(this).addClass("active");
    $("#btnGP").removeClass("active");
    $("#input_empresa").val("DPT_BD");
});

$("#btnGP").click(function (e) { 
    e.preventDefault();
    $(this).addClass("active");
    $("#btnDPT").removeClass("active");
    $("#input_empresa").val("GP_BD");
});

init();