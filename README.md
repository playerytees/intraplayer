# Intraplayer
Intraplayer es el nombre que se le da a la intranet de la empresa la cual abarca un _Sistema de Embarques_, un modulo de _Inventarios_, otro más de _Ventas_, por último un módulo de _Almacen_.

El proyecto actualmente sigue en funcionamiento con actualizaciones que se solicitan por parte del departamento de dirección general.

#### Tecnologias
- PHP 7.0
- MySQL 8.0
- Javascript
- Jquery 3.3.1
- Bootstrap 4.0
- ODBC

#### Librerias
- Font Awesome 4.7.0 
- DataTables 1.10.8
- jquery-confirm v3.3.0
- select2 4.0.7
- JsBarcode v3.11.5
- jquery.PrintArea 2.1
- toastr

## Sistema de Embarques

Se realizó un módulo de embarques en base a los requerimientos recabados cuya funcionalidad es la impresión de etiquetas; escaneo de cajas y contabilizar cada una de ellas mediante el código de barras impreso en la etiqueta y así optimizar el proceso de embarques.


##### Funcionalidad del sistema

- Impresión de etiquetas con la información introducida por el usuario.
- Guardar la información de las etiquetas impresas.
- Eliminar si la información de la etiqueta introducida sea errónea.
- Contabilizar y sumar las cajas escaneadas por el usuario.
- Listar la información de las cajas escaneadas y se podrá seleccionar que partidas se embarcaran.
- Lista de partidas que quedaran pendientes para otro embarque.
- Lista denominada lista de empaque la cual es un previo antes de ser enviado.


## Inventario

A través de una conexión **ODBC** hacia el *ERP* de *SAP B1* se obtiene el Inventario que se tiene en la empresa y de cada una de las sucursales, de esa manera podemos visualizar toda la información teniendo como filtros el nombre de la sucursal, el fabricante, el grupo de artículo, formando una matriz de tallas para mejor visualización del inventario.

Ejemplo:
| Estilo | Descripcion | Color | XS | S | M | L | XL | 2XL | 3XL | 4XL | 5XL | UNI |
|--------|-------------|-------|----|---|---|---|----|-----|-----|-----|-----|-----| 
|4000C   |PLAYERA CABALLERO CUELLO REDONDO | BLANCO | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 
|4000D   |PLAYERA DAMA CUELLO REDONDO      | BLANCO | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 | 0 |

##### Caracteristicas
- **Buscar por Código:** Una opción aún más detallada para buscar un artículo en específico introduciendo el código.
- **Buscar en Almacén:** Buscar el artículo por código y se visualizará las existencias del mismo en diferentes almacenes.
- **Próximas llegadas:** Mercancía que está próximo en llegar al almacén.


## Ventas

Con la misma conexion ODBC que se realizó anteriormente, se muestra las ventas realizadas por las sucursales la cual se conforma de la siguiente manera:
- Nombre de la sucursal.
- La venta del dia.
- La venta acumulada del mes actual.
- La meta a alcanzar.
- Porcentaje actual para lograr la meta.
- La venta del año pasado.
- Porcentaje de la venta actual con el año anterior.

También se encuentra una gráfica donde se muestra todas las sucursales sobre la ventas que han realizado y así comparar entre sucursales.


## Almacén

En este módulo se encuentra la opción de ir registrando los artículos que se están haciendo y que están próximos a llegar al almacén y esa información se muestra a los usuarios para saber si tendrán existencias en los próximos días sobre el artículo.
