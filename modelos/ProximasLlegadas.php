<?php
    require "../config/Conexion.php";

    class ProximasLlegadas
    {
        public function __construct()
        {

        }

        public function guardartable($partida,$codigo,$estilo,$color,$talla,$cantidad,$fechaentrega)
        {
            $sqlselect = "SELECT * FROM `bdpw_intraplayer`.`proximasllegadas` WHERE `partida` = '$partida' AND `codigo` = '$codigo';";

            $response = ejecutarConsultaSimpleFila($sqlselect);

            if ($response) {
                $sqlupdate = "UPDATE `bdpw_intraplayer`.`proximasllegadas`
                SET
                `cantidad` = '$cantidad',
                `fechaentrega` = '$fechaentrega' 
                WHERE `partida` = '$partida' AND `codigo` = '$codigo';";

                return ejecutarConsulta($sqlupdate);
            } else {
                $sql = "INSERT INTO `bdpw_intraplayer`.`proximasllegadas`
                (`partida`,
                `codigo`,
                `estilo`,
                `color`,
                `talla`,
                `cantidad`,
                `fechaentrega`)
                VALUES
                ('$partida',
                '$codigo',
                '$estilo',
                '$color',
                '$talla',
                '$cantidad',
                '$fechaentrega');
                ";

                return ejecutarConsulta($sql);
            }
        }

        public function agregar($partida,$codigo,$estilo,$color,$talla,$cantidad,$fechaentrega)
        {
            $sql = "INSERT INTO `bdpw_intraplayer`.`proximasllegadas`
            (`partida`,
            `codigo`,
            `estilo`,
            `color`,
            `talla`,
            `cantidad`,
            `fechaentrega`)
            VALUES
            ('$partida',
            '$codigo',
            '$estilo',
            '$color',
            '$talla',
            '$cantidad',
            '$fechaentrega');
            ";

            return ejecutarConsulta($sql);
        }

        public function editar($idproximasllegadas,$partida,$codigo,$estilo,$color,$talla,$cantidad,$fechaentrega)
        {
            $sql = "UPDATE `bdpw_intraplayer`.`proximasllegadas`
            SET
            `partida` = '$partida',
            `codigo` = '$codigo',
            `estilo` = '$estilo',
            `color` = '$color',
            `talla` = '$talla',
            `cantidad` = '$cantidad',
            `fechaentrega` = '$fechaentrega'
            WHERE `idproximasllegadas` = '$idproximasllegadas';
            ";

            return ejecutarConsulta($sql);
        }

        public function desactivar($idproximasllegadas)
        {
            $sql = "UPDATE `bdpw_intraplayer`.`proximasllegadas`
            SET
            `status` = '0'
            WHERE `idproximasllegadas` = '$idproximasllegadas';
            ";

            return ejecutarConsulta($sql);
        }

        public function activar($idproximasllegadas)
        {
            $sql = "UPDATE `bdpw_intraplayer`.`proximasllegadas`
            SET
            `status` = '1'
            WHERE `idproximasllegadas` = '$idproximasllegadas';
            ";

            return ejecutarConsulta($sql);
        }

        public function mostrar($idproximasllegadas)
        {
            $sql = "SELECT * FROM `bdpw_intraplayer`.`proximasllegadas` WHERE `idproximasllegadas` = '$idproximasllegadas'";

            return ejecutarConsultaSimpleFila($sql);
        }

        public function listar()
        {
            $sql = "SELECT * FROM `bdpw_intraplayer`.`proximasllegadas`";

            return ejecutarConsulta($sql);
        }

        public function filtrarEstiloColor($estilo, $color)
        {   
            $sql = "";
            if (empty($estilo) AND empty($color)) {
                $sql = "SELECT 
                a.fechaentrega,
                a.partida,
                a.estilo,
                a.color,
                a.cantidad,
                a.talla
            FROM
                `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
            WHERE
                -- a.`estilo` IN ('') AND a.`color` IN ('')
                a.`fechaentrega` >= DATE(NOW())
                AND b.partidalimpia IS NULL;";
            } else if (empty($estilo)) {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`color` IN ($color)
                    AND a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL;";
            } else if (empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo)
                    AND a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL;";
            } else {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo) AND a.`color` IN ($color)
                    AND a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL;";
            }

            return ejecutarConsulta($sql);
        }

        public function groupEstiloColor($estilo, $color)
        {   
            $sql = "";
            if (empty($estilo) AND empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    -- a.`estilo` IN ('') AND a.`color` IN ('')
                    a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            } else if (empty($estilo)) {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`color` IN ($color)
                    AND a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            } else if (empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo)
                    AND a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            } else {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo) AND a.`color` IN ($color)
                    AND a.`fechaentrega` >= DATE(NOW())
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            }

            return ejecutarConsulta($sql);
        }

        public function rangoFecha($estilo, $color, $min, $max)
        {   
            $sql = "";
            if (empty($estilo) AND empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    -- a.`estilo` IN ('') AND a.`color` IN ('')
                    a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL;";
            } else if (empty($estilo)) {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`color` IN ($color)
                    AND a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL;";
            } else if (empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo)
                    AND a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL;";
            } else {
                $sql = "SELECT 
                    a.fechaentrega,
                    a.partida,
                    a.estilo,
                    a.color,
                    a.cantidad,
                    a.talla
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                        LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo) AND a.`color` IN ($color)
                    AND a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL;";
            }

            return ejecutarConsulta($sql);
        }

        public function groupRangoFecha($estilo, $color, $min, $max)
        {   
            $sql = "";
            if (empty($estilo) AND empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    -- a.`estilo` IN ('') AND a.`color` IN ('')
                    a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            } else if (empty($estilo)) {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`color` IN ($color)
                    AND a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            } else if (empty($color)) {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo)
                    AND a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            } else {
                $sql = "SELECT 
                    a.fechaentrega, a.partida, a.estilo, a.color
                FROM
                    `bdpw_intraplayer`.`proximasllegadas` a
                    LEFT JOIN
                    `bdpw_intraplayer`.`detalle_embarque` b ON (a.partida = b.partidalimpia AND a.codigo = b.codigo)
                WHERE
                    a.`estilo` IN ($estilo) AND a.`color` IN ($color)
                    AND a.`fechaentrega` BETWEEN $min AND $max
                    AND b.partidalimpia IS NULL
                GROUP BY a.fechaentrega, a.partida, a.estilo, a.color;";
            }

            return ejecutarConsulta($sql);
        }

        public function getPartidaEmbarque()
        {
            $sql = "SELECT a.partida, b.cve_art codigo FROM detalle_embarque a LEFT JOIN etiqueta b ON (a.barcode = b.barcode AND a.partida = b.partida) WHERE b.cve_art IS NOT NULL;";

            return ejecutarConsulta($sql);
        }
    }
    
?>