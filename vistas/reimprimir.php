    <style>
        _:-ms-lang(x), _:-webkit-full-screen, .ms-height
        {
            height: 300px;
            margin-bottom: 30px;
        }
        @media screen\0 {
            .ms-height{
                height: 300px;
                margin-bottom: 30px;
            }
        }
        /* @media print {
            table {
                page-break-after: always;
            }
        } */
    </style>
    <?php require "../config/Conexion.php";
        $id = $_GET['id'];

        $sql = mysqli_query($conexion, "SELECT * FROM etiqueta WHERE id = '$id'");

        $row = mysqli_fetch_array($sql);

        $fecha = date("d/m/Y", strtotime( $row['fecha'] ) );
    ?>
    <div class="container mt-3">
        <div id="print_tag" class="ms-height">
        <?php /* for ($i=1; $i <= $row['cajas']; $i++): */ ?>
            <table style="width: 600px; height: 355px; font-family: Arial, Helvetica, sans-serif; border-collapse: collapse;">
                <tr style="border-bottom: 1px solid;">
                    <!-- <td colspan="2"><img id="code" style="margin-left: 15px;"></img></td>
                    <td colspan="2"><?php /* $i."/".$row["cajas"]; */ ?></td>
                    <td colspan="2" align="right"><img id="code" style="margin-right: 15px;"></img></td> -->
                    <td colspan="3"><img id="code" style="margin-left: 15px;"></img></td>
                    <td colspan="3" align="right"><img id="code" style="margin-right: 15px;"></img></td>
                </tr>
                <tr>
                    <td style="text-align: center; border-left: 1px solid; font-weight: bold; width: 100px;">STYLE:</td>
                    <td style="text-align: center; font-size: 25px;"><b><span class="labelStyle"> <?php echo $row['estilo']; ?> </span></b></td>
                    <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 355px; text-align: center;" rowspan="6">
                        <div style="writing-mode: tb-rl; transform: rotate(180deg);">
                            <span>Order</span> <span class="labelOrden"> <?php echo $row['partida']; ?> </span>
                        </div>
                    </td>
                    <td style="text-align: center; font-weight: bold; width: 100px;">STYLE:</td>
                    <td style="text-align: center; font-size: 25px;"><b><span class="labelStyle" id="labelStyle"> <?php echo $row['estilo']; ?> </span></b></td>
                    <td style="background: black; color: #fff; font-size: 12px; padding: 0; width: 15px; height: 355px; text-align: center;" rowspan="6">
                        <div style="writing-mode: tb-rl; transform: rotate(180deg);">
                            <span>Order </span><span class="labelOrden"> <?php echo $row['partida']; ?> </span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">SIZE:</td>
                    <td style="text-align: center; font-size: 25px;"><b><span class="labelSize"> <?php echo $row['talla']; ?> </span></b></td>
                    <td style="text-align: center; font-weight: bold;">SIZE:</td>   
                    <td style="text-align:center; font-size: 25px;"><b><span class="labelSize"> <?php echo $row['talla']; ?> </span></b></td>
                </tr>
                <tr>
                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">COLOR:</td>
                    <td style="text-align: center;"><b><span class="labelColor" style="font-size: 25px;"> <?php echo $row['color']; ?> </span></b></td>
                    <td style="text-align: center; font-weight: bold;">COLOR:</td>
                    <td style="text-align: center;"><b><span class="labelColor" style="font-size: 25px;"> <?php echo $row['color']; ?> </span></b></td>
                </tr>
                <tr>
                    <td style="text-align: center; border-left: 1px solid; font-weight: bold;">QUANTITY:</td>
                    <td style="text-align: center; font-size: 30px"><b><span class="labelQty"> <?php echo $row['pza_x_caja']; ?> </span></b></td>
                    <td style="text-align: center; font-weight: bold;">QUANTITY:</td>
                    <td style="text-align: center; font-size: 30px"><b><span class="labelQty"> <?php echo $row['pza_x_caja']; ?> </span></b></td>
                </tr>
                <tr style="text-align: center; border-left: 1px solid;">
                    <td colspan="2" valign="bottom" style="padding: 0; height: 50px;"><img class="barcode"></img></td>
                    <td colspan="2" valign="bottom" style="padding: 0"><img class="barcode"></img></td>
                </tr>
                <tr style="font-size: 12px; background: black; color: white; height: 10px;">
                    <td valign="bottom">Packing</td>
                    <td valign="bottom" id="labelFecha" style="font-weight: bold;"><?php echo $fecha; ?></td>
                    <td valign="bottom" style="text-align: right">Packing</td>
                    <td valign="bottom" style="text-align: right; font-weight: bold;"><?php echo $fecha; ?></td>
                </tr>
            </table>
        <?php //endfor; ?>
        </div>
        <!-- <button type="button" class="btn btn-primary mt-2" id="btn_print">Imprimir</button>
        <button type="button" class="btn btn-danger mt-2" onclick="javascript:window.close();">Cancelar</button> -->
    </div>
        <script src="../public/assets/js/JsBarcode.code128.min.js"></script>
        <script src="../public/assets/js/vendor/jquery-3.3.1.min.js"></script>
        <script src="../public/assets/js/jquery.PrintArea.js"></script>
        <script>
            $(document).ready(function () {
                JsBarcode(".barcode", "<?php echo $row['cve_art']; ?>", {width: 1, height: 30, fontSize: 18, font: "Arial", fontOptions: "bold"});

                JsBarcode("#code", "<?php echo $row['barcode']; ?>", {width: 1, height: 15, displayValue: false, margin: 0});

                $("#btn_print").click(function (e) { 
                    e.preventDefault();
                    $("#print_tag").printArea();
                });

                var getBrowserInfo = function() {
                    var ua= navigator.userAgent, tem,
                    M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                    if(/trident/i.test(M[1])){
                        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                        return 'IE '+(tem[1] || '');
                    }
                    if(M[1] === 'Chrome'){
                        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
                    }
                    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
                    if( ( tem= ua.match( /version\/(\d+)/i ) ) != null ) M.splice(1, 1, tem[1]);
                    return M.join(' ');
                }

                var browser = getBrowserInfo().split(' ');

                if (browser[0] == "Chrome") {
                    $("table").css({"height": "100%", "width": "100%"});
                }

                if (window.print) {
                    setTimeout(function(){window.print();window.close();},500); // 500ms = 0.5s - 1000 = 1s
                }
            });
        </script>