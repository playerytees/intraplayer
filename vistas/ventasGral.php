<?php
ob_start();
session_start();
date_default_timezone_set("America/Mexico_City");
if (!isset($_SESSION['nombre'])) {
    header("Location: login.php");
} else {

require 'header.php';

if ($_SESSION['existencias']==1) {
?>
<style>
#tablaventas td:nth-child(2), td:nth-child(3), td:nth-child(4), td:nth-child(6) {
    text-align: right;
}
#tablaventas td:nth-child(1){
    white-space: nowrap;
}
#tablaventas td:nth-child(5), td:nth-child(7){
    text-align: center;
}
</style>
    <!-- <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1 style="float: left;" class="text-playerytees">Presupuesto </h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="page-header float-right">
                <div class="page-title">
                    <ol class="breadcrumb text-right text-playerytees">
                        <li class="active">Presupuesto</li>
                    </ol>
                </div>
            </div>
        </div>
    </div> -->

    <div class="content mt-3">
        <div id="cardventas">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <strong class="card-title text-playerytees float-left">Ventas</strong>
                            <?php if($_SESSION['usuarios']==1 || $_SESSION['idusuario'] == '140'): ?>
                            <a id="btnagregar" href="#" class="float-right" onclick="mostrarform(true)"><u>Presupuesto</u></a>
                            <?php endif; ?>
                        </div> <!-- .card-header -->
                        <div class="card-body card-block">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label class="form-control-label mr-1">Seleccionar Fecha:</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <input type="date" class="form-control form-control-sm" id="fecha" name="fecha" required value="<?= date("Y-m-d")?>">
                                        <button type="button" class="btn btn-playerytees" id="btn-ver">Ver</button>
                                    </div>
                                </div>
                            </div>
                            <table class='table table-sm table-striped table-bordered table-responsive-sm' id="tablaventas" style="width: 100%;">
                                <thead style="background-color: rgba(6, 78, 125, 0.88); color: #fff; font-size: 13px; text-align: center;">
                                    <tr>
                                        <th>SUCURSAL</th>
                                        <th>VENTA DIA</th>
                                        <th>VENTA ACUMULADA MES</th>
                                        <th>META</th>
                                        <th>ALCANCE</th>
                                        <th>AÑO ANTERIOR</th>
                                        <th>ALCANCE</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size: 14px;"></tbody>
                            </table>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-12 -->
            </div> <!-- .row -->
            
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-four">
                                <div class="stat-icon dib">
                                    <i class="fa fa-dollar text-success"></i>
                                </div> <!-- .stat-icon -->
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-heading">Ventas Diarias DPT</div>
                                        <div class="stat-text" id="totalDailySales"></div>
                                    </div>
                                </div> <!-- .stat-content -->
                            </div> <!-- .stat-widget-four -->
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-four">
                                <div class="stat-icon dib">
                                    <i class="fa fa-dollar text-success"></i>
                                </div> <!-- .stat-icon -->
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-heading" style="font-size: 19px;">Ventas Mensuales DPT</div>
                                        <div class="stat-text" id="totalMonthlySales"></div>
                                    </div>
                                </div> <!-- .stat-content -->
                            </div> <!-- .stat-widget-four -->
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-lg-4 -->
                <div class="col-lg-4 col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="stat-widget-four">
                                <div class="stat-icon dib">
                                    <i class="fa fa-line-chart text-info"></i>
                                </div> <!-- .stat-icon -->
                                <div class="stat-content">
                                    <div class="text-left dib">
                                        <div class="stat-heading">Alcance Total DPT</div>
                                        <div class="stat-text" id="AlcanceTotal"></div>
                                    </div>
                                </div> <!-- .stat-content -->
                            </div> <!-- .stat-widget-four -->
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-lg-4 -->
            </div> <!-- .row -->

            <div class="row">
                <div class="col-xl-8">
                    <div class="card">
                        <div class="card-body">
                            <div style="width: 100%; overflow: auto;">
                                <div id="chart-content" style="width: 800px; height: 400px; margin-left: auto; margin-right: auto;">
                                    <canvas id="grafica"></canvas>
                                </div>
                            </div>
                        </div> <!-- .card-body -->
                    </div> <!-- .card -->
                </div> <!-- .col-lg-6 -->
            </div> <!-- .row -->
        </div> <!-- #cardventas -->

        <div class="row">
            <div class="col-12">
                <div class="card" id="formpresupuesto">
                    <div class="card-header">
                        <strong class="card-title text-playerytees">Actualizar Presupuesto</strong>
                    </div> <!-- .card-header -->
                    <div class="card-body card-block">
                        <form id="formulario" method="POST">
                            <div class="form-group">
                                <label for="selectsucursal" class="form-control-label">Sucursal:</label>
                                <select name="selectsucursal" id="selectsucursal" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label for="selectanio" class="form-control-label">Año:</label>
                                <select name="selectanio" id="selectanio" class="form-control">
                                    <option value="<?= date("Y"); ?>">Año Actual - <?= date("Y"); ?></option>
                                    <option value="<?= date( 'Y' ,  strtotime( '-1 year', strtotime( date("Y") ) ) ); ?>">Año Anterior - <?= date( 'Y' ,  strtotime( '-1 year', strtotime( date("Y") ) ) ); ?></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="selectmes" class="form-control-label">Mes:</label>
                                <select name="selectmes" id="selectmes" class="form-control">
                                    <option value="01">Enero</option>
                                    <option value="02">Febrero</option>
                                    <option value="03">Marzo</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Mayo</option>
                                    <option value="06">Junio</option>
                                    <option value="07">Julio</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="txtmonto" class="form-control-label">Monto:</label>
                                <input type="text" name="txtmonto" id="txtmonto" placeholder="Monto con IVA" class="form-control" maxlength="15" pattern="\d+(\.\d{2})?">
                            </div>
                            <div class="form-actions form-group">
                                <button type="submit" class="btn btn-playerytees">Actualizar</button>
                                <button type="reset" class="btn btn-danger" onclick="cancelarform()">Cancelar</button>
                            </div>
                        </form>
                    </div> <!-- .card-body -->
                </div> <!-- .card -->
            </div> <!-- .col-12 -->
        </div> <!-- .row -->
    </div> <!-- .content -->
<?php
}
else
{
    require "noacceso.php";
}
require 'footer.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js" integrity="sha256-xKeoJ50pzbUGkpQxDYHD7o7hxe0LaOGeguUidbq6vis=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" integrity="sha256-Uv9BNBucvCPipKQ2NS9wYpJmi8DTOEfTA/nH2aoJALw=" crossorigin="anonymous"></script>
<script src="../public/assets/js/jquery-confirm.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.19/api/sum().js"></script>
<script type="text/javascript" src="scripts/ventasGral.js"></script>
<?php
}
ob_end_flush();
?>